import pytest
import h5py
import io
import numpy
from tomo.tango.servers import nxtomo_generator


@pytest.fixture
def nxdata1():
    bio = io.BytesIO()

    with h5py.File(bio, "w") as h5:
        images = numpy.zeros(shape=(9, 4, 3))
        for i in range(len(images)):
            images[i] = i
        entry = h5.create_group("entry0000")
        entry["instrument/detector/count_time"] = [0.5] * 9
        entry["sample/rotation_angle"] = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        entry["instrument/detector/data"] = images
        entry["instrument/detector/image_key"] = [2, 2, 2, 1, 1, 1, 0, 0, 0]
        yield h5


def test_tomo_data(nxdata1):
    data = nxtomo_generator.GeneratorFromNxTomo()
    data.load_nxtomo_h5(nxdata1)
    assert data.detector_shape == (4, 3)
    assert data.get_dark(1)[0, 0] == 0
    assert data.get_flat(1)[0, 0] == 6
    assert data.get_projection(7, 1)[0, 0] == 14
    assert data.get_projection(8, 1)[0, 0] == 16
    assert data.get_projection(8, 0.5)[0, 0] == 8
