import pytest
from bliss.config import static


def test_creation(beacon, lima_simulator_tango_server):
    detectors = beacon.get("tomo_detectors")
    assert len(detectors.detectors) == 1
    assert detectors.detectors[0].name == "tomo_detector"


def test_impossible_config(beacon, lima_simulator_tango_server):
    with pytest.raises(RuntimeError) as exc_info:
        beacon.get("tomo_detectors_with_impossible_config")
    assert (
        "detector axis should be configured either on each tomo detector either on tomo detectors not both"
        in exc_info.value.initial_cause.args[0]
    )


@pytest.fixture
def setup_tomo_config_with_user_optics(beacon):
    config = beacon.get_config("tomo_config")
    config["detectors"] = static.ConfigReference(
        config, "$tomo_detectors_with_user_optics"
    )


def test_with_user_optics(
    beacon,
    lima_simulator_tango_server,
    setup_tomo_config_with_user_optics,
):
    """
    Load a tomo_config setup with user_optics.

    Make sure the described optics can be mounted to a detector.
    """
    tomo_config = beacon.get("tomo_config")
    detector = tomo_config.detectors.detectors[0]
    another_optic = tomo_config.detectors.user_optics[1]
    detector.optic = another_optic
    assert detector.optic.name == another_optic
