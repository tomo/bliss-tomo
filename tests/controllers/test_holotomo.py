import pytest
import gevent
from tomo.controllers.tomo_detector import TomoDetector
from tomo.controllers.holotomo import HolotomoState


@pytest.fixture
def holotomo(beacon):
    detector: TomoDetector = beacon.get("tomo_detector")
    detector.detector.image.roi = 0, 0, 100, 50
    detector.sample_pixel_size_mode = "auto"
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 1000
    detector.tomo_detectors.source_distance = 2000
    holotomo = beacon.get("holotomo")
    tomo_config.detectors.active_detector = detector
    # Test to make sure the setup is at a known state
    assert detector.optic is not None
    assert detector.optic.magnification == 10
    assert detector.sample_pixel_size == 0.05
    return holotomo


@pytest.fixture
def setup_holotomo_with_offset(beacon):
    config = beacon.get_config("holotomo")
    config["calcmode"] = "offset"


def test_creation(beacon, lima_simulator_tango_server, holotomo):
    assert holotomo.pixel_size is None
    assert holotomo.distances == []


def test_pixel_size(beacon, lima_simulator_tango_server, holotomo):
    """Request a pixel size divided by 2

    The first is supposed to be also divided by 2
    """
    # Fetch a smaller pixel size
    holotomo.pixel_size = 0.025
    assert holotomo.distances != []
    assert len(holotomo.distances) == 4
    assert holotomo.distances[0].position == -500
    assert holotomo.distances[0].z1 == 500
    assert holotomo.distances[0].pixel_size == 0.025
    assert holotomo.distances[3].position == pytest.approx(-214.6, abs=0.00001)
    assert holotomo.distances[3].z1 == pytest.approx(785.4, abs=0.00001)
    assert holotomo.distances[3].pixel_size == pytest.approx(0.03927, abs=0.00001)


def test_first_distance(beacon, lima_simulator_tango_server, holotomo):
    """Request a first distance between the sample and the detector

    The pixel size is supposed to be multiply by 1.5
    """
    holotomo.compute_from_first_distance(1500.0)
    assert len(holotomo.distances) == 4
    assert holotomo.distances[0].position == 500
    assert holotomo.distances[0].z1 == 1500
    assert holotomo.distances[0].pixel_size == 0.075
    assert holotomo.distances[3].position == pytest.approx(1356.2, abs=0.00001)
    assert holotomo.distances[3].z1 == pytest.approx(2356.2, abs=0.00001)
    assert holotomo.distances[3].pixel_size == pytest.approx(0.11781, abs=0.00001)


def test_offsets(
    beacon, lima_simulator_tango_server, setup_holotomo_with_offset, holotomo
):
    holotomo.pixel_size = 0.01
    assert holotomo.distances != []
    assert len(holotomo.distances) == 4
    assert holotomo.distances[0].z1 == pytest.approx(200, abs=0.00001)
    assert holotomo.distances[1].z1 == pytest.approx(201, abs=0.00001)
    assert holotomo.distances[2].z1 == pytest.approx(205, abs=0.00001)
    assert holotomo.distances[3].z1 == pytest.approx(215, abs=0.00001)


def test_invalided_on_detector_change(beacon, lima_simulator_tango_server, holotomo):
    """Compute the distances, and change the detector.

    The device is flagged as invalid.
    """
    holotomo.pixel_size = 1
    assert holotomo.state == HolotomoState.READY
    tomo_config = beacon.get("tomo_config")
    tomo_config.detectors.active_detector = None
    gevent.sleep(0.1)
    assert holotomo.state == HolotomoState.INVALID_DETECTOR_CHANGED


def test_invalided_on_detector_binning_change(
    beacon, lima_simulator_tango_server, holotomo
):
    """Compute the distances, and change the detector binning.

    The device is flagged as invalid.
    """
    holotomo.pixel_size = 1
    assert holotomo.state == HolotomoState.READY
    tomo_config = beacon.get("tomo_config")
    detector = tomo_config.detectors.active_detector
    detector.detector.image.binning = 2, 2
    gevent.sleep(0.1)
    assert holotomo.state == HolotomoState.INVALID_DETECTOR_BINNING_CHANGED


def test_invalided_on_detector_distance_change(
    beacon, lima_simulator_tango_server, holotomo
):
    """Compute the distances, and change the detector binning.

    The device is flagged as invalid.
    """
    holotomo.pixel_size = 1
    assert holotomo.state == HolotomoState.READY
    tomo_config = beacon.get("tomo_config")
    detector = tomo_config.detectors.active_detector
    detector.source_distance = detector.source_distance + 1
    gevent.sleep(0.1)
    assert holotomo.state == HolotomoState.INVALID_DETECTOR_DISTANCE_CHANGED
