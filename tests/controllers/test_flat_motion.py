from bliss.common import event


def test_parallel_move(beacon):
    tomoref = beacon.get("tomo_config_ref_parallel")
    refmot1 = beacon.get("refmot1")
    refmot2 = beacon.get("refmot2")
    assert tomoref.move_in_parallel is True
    tomoref.set_out_of_beam_position(refmot1, -1, refmot2, 1)
    tomoref.set_in_beam_position(refmot1, 0, refmot2, 0)

    success = True

    def check_state(refmot1_pos):
        nonlocal success
        if refmot1.is_moving and not refmot2.is_moving:
            success = False

    event.connect(refmot1, "position", check_state)
    tomoref.move_out()
    assert success is True


def test_serial_move(beacon):
    tomoref = beacon.get("tomo_config_ref_serial")
    refmot1 = beacon.get("refmot1")
    refmot2 = beacon.get("refmot2")
    assert tomoref.move_in_parallel is False
    tomoref.set_out_of_beam_position(refmot1, -1, refmot2, 1)
    tomoref.set_in_beam_position(refmot1, 0, refmot2, 0)
    success = True

    def check_state(refmot1_pos):
        nonlocal success
        if refmot1.is_moving and refmot2.is_moving:
            success = False

    event.connect(refmot1, "position", check_state)
    tomoref.move_out()
    assert success is True
