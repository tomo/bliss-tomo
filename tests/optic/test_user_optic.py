import gevent
from bliss.common import event
from ..helpers.event_utils import EventListener


def test_creation(beacon):
    beacon.get("user_optic")


def test_magnification_setter(beacon):
    optic = beacon.get("user_optic")
    optic.magnification = 2
    assert optic.magnification == 2
    optic.magnification = 10
    assert optic.magnification == 10


def test_magnification_event(beacon, mocker):
    optic = beacon.get("user_optic")
    listener = EventListener()
    optic.magnification = 2
    try:
        event.connect(optic, "magnification", listener)
        optic.magnification = 10
        # Wait for event received
        gevent.sleep(1)
    finally:
        event.disconnect(optic, "magnification", listener)

    # No idea why this stuff is called more than 2 times sometimes (sounds to be  BLISS internal)
    assert listener.event_count >= 1
    assert listener.last_value == 10
