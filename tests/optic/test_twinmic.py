import pytest
from tomo.optic.base_optic import OpticState


def test_select_zero_magnification(beacon):
    optic = beacon.get("twinmic_optic")
    with pytest.raises(ValueError):
        optic.objective = 1
