import pytest
import gevent
from bliss.common import event
from ..helpers.event_utils import EventListener
from tomo.optic.base_optic import OpticState


@pytest.fixture
def optic(beacon):
    optic = beacon.get("dzoom_optic")
    return optic


def test_creation(optic):
    assert optic.magnification == pytest.approx(0.20872, abs=0.0001)


def test_magnification_setter(optic):
    optic.magnification = 0.2
    assert optic.magnification == pytest.approx(0.18309, abs=0.0001)


def test_async_move(session, optic):
    optic.move_magnification(0.2, wait=False)
    assert optic.state == OpticState.MOVING
    optic.wait_move()
    assert optic.state == OpticState.READY
    assert optic.magnification == pytest.approx(0.18309, abs=0.0001)


def test_magnification_event(optic):
    listener = EventListener()
    optic.magnification = 0.21
    try:
        event.connect(optic, "magnification", listener)
        optic.magnification = 0.2
        # Wait for event received
        gevent.sleep(1)
    finally:
        event.disconnect(optic, "magnification", listener)

    # No idea why this stuff is called more than 2 times sometimes (sounds to be  BLISS internal)
    assert listener.event_count >= 1
    assert listener.last_value == pytest.approx(0.18309, abs=0.0001)


def test_magnification_setter__too_small(optic):
    with pytest.raises(ValueError) as exc_info:
        optic.magnification = 0.01
    assert "0.1" in exc_info.value.args[0]
    assert "1" in exc_info.value.args[0]
    assert "0.01" in exc_info.value.args[0]


def test_magnification_setter__too_big(optic):
    with pytest.raises(ValueError) as exc_info:
        optic.magnification = 2.1
    assert "0.1" in exc_info.value.args[0]
    assert "1" in exc_info.value.args[0]
    assert "2.1" in exc_info.value.args[0]
