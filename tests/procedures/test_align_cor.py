import pytest
from tomo.helpers import flatfield_correction


@pytest.fixture
def tomo_config(beacon):
    tomo_config = beacon.get("tomo_config")
    tomo_config.set_active()
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    active_detector = tomo_config.detectors.detectors[0]
    tomo_config.detectors.active_detector = active_detector
    tomo_config.reference.set_out_of_beam_displacement(tomo_config.y_axis, 1)
    return tomo_config


def test_align(beacon, session, lima_simulator, mocker, tomo_config, no_text_block):
    """Make sure the procedure is followed from the begining to the end"""
    align = beacon.get("test_procedure_align")
    assert align is not None

    compute_flat = mocker.spy(flatfield_correction, "compute_flat")
    compute_dark = mocker.spy(flatfield_correction, "compute_dark")
    compute_proj = mocker.spy(flatfield_correction, "compute_proj")

    mocker.patch.object(align, "request_and_wait_validation", return_value=None)

    align.run()

    assert compute_flat.call_count == 1
    assert compute_dark.call_count == 1
    assert compute_proj.call_count == 2


def test_align_cor_only(
    beacon, session, lima_simulator, mocker, tomo_config, no_text_block
):
    """Make sure the procedure is followed from the begining to the end"""
    align = beacon.get("test_procedure_align_cor_only")
    assert align is not None

    compute_flat = mocker.spy(flatfield_correction, "compute_flat")
    compute_dark = mocker.spy(flatfield_correction, "compute_dark")
    compute_proj = mocker.spy(flatfield_correction, "compute_proj")

    mocker.patch.object(align, "request_and_wait_validation", return_value=None)

    align.run()

    assert compute_flat.call_count == 1
    assert compute_dark.call_count == 1
    assert compute_proj.call_count == 2


def test_align_with_multi_proj(
    beacon, session, lima_simulator, mocker, tomo_config, no_text_block
):
    """Make sure the dark/flat/proj was computed from multiple images"""
    align = beacon.get("test_procedure_align_with_multi_proj")
    assert align is not None
    assert align._dark_n == 2
    assert align._flat_n == 3
    assert align._proj_n == 4

    compute_flat = mocker.spy(flatfield_correction, "compute_flat")
    compute_dark = mocker.spy(flatfield_correction, "compute_dark")
    compute_proj = mocker.spy(flatfield_correction, "compute_proj")

    mocker.patch.object(align, "request_and_wait_validation", return_value=None)

    align.run()

    assert compute_dark.call_count == 1
    assert compute_flat.call_count == 1
    assert compute_proj.call_count == 2
    assert len(compute_dark.call_args_list[0][0][0]) == 2
    assert len(compute_flat.call_args_list[0][0][0]) == 3
    assert len(compute_proj.call_args_list[0][0][0]) == 4
    assert len(compute_proj.call_args_list[1][0][0]) == 4
