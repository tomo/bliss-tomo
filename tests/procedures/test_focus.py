import numpy
import pytest
from tomo.helpers import flatfield_correction
from tomo.procedures import focus as focus_mdl
from tomo.scintillators import utils as scintillator_utils
from tomo.scintillators import optique_peter_ttss


@pytest.fixture
def tomo_config(beacon):
    tomo_config = beacon.get("tomo_config")
    tomo_config.set_active()
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    active_detector = tomo_config.detectors.detectors[0]
    tomo_config.detectors.active_detector = active_detector
    tomo_config.reference.set_out_of_beam_displacement(tomo_config.y_axis, 1)

    return tomo_config


def test_focus(
    beacon, session, pt_test_context, lima_simulator, mocker, tomo_config, no_text_block
):
    """
    Make sure the procedure is followed from the begining to the end.

    A known tilt and pixel size is mocked.

    Check that the scintillator geometry receive the right value.
    """
    focus = beacon.get("test_procedure_focus")
    assert focus is not None

    tomo_detector = detector = tomo_config.detectors.active_detector
    tomo_detector.sample_pixel_size_mode = "user"

    geometry = optique_peter_ttss.OptiquePeterTtss()
    spy_tilt_hardware_correction = mocker.spy(geometry, "tilt_hardware_correction")
    mocker.patch.object(
        tomo_detector.optic, "scintillator_geometry", return_value=geometry
    )

    optics_pixel_size = 0.65
    tomo_detector.user_sample_pixel_size = optics_pixel_size
    detector = tomo_detector.detector
    detector.image.flip = False, True

    mg = beacon.get("mg")
    mg.set_active()
    mg.add(detector.image)
    mg.enable(detector.image.fullname)

    camera_focus = mocker.Mock()
    nabu_tilt = numpy.array([-7.45529171e-07, 8.59808174e-08])
    nabu_result = 1.0, 2, nabu_tilt
    camera_focus.find_scintillator_tilt = mocker.Mock(return_value=nabu_result)

    mocker.patch.object(focus_mdl, "nabu", return_value=True)
    mocker.patch.object(focus_mdl, "CameraFocus", return_value=camera_focus)

    compute_flat = mocker.spy(flatfield_correction, "compute_flat")
    compute_dark = mocker.spy(flatfield_correction, "compute_dark")
    print_tilt_results = mocker.spy(scintillator_utils, "print_tilt_results")

    mocker.patch.object(focus, "wait_validation_for_correction", return_value=True)

    focus.run()

    assert compute_flat.call_count == 1
    assert compute_dark.call_count == 1
    assert print_tilt_results.call_count == 1
    assert spy_tilt_hardware_correction.call_count == 1
    correction = spy_tilt_hardware_correction.call_args_list[0].args
    assert correction == pytest.approx((0.001147, -0.000132), abs=0.000001)
