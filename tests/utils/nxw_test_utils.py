# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import gevent.event

from blisswriter.utils import scan_utils
from blisswriter.io import nexus
from blisswriter.utils import process_utils
from blisswriter.utils import logging_utils
from blisswriter.writer import scan_url_info


def _scan_uris(scans):
    """
    :param list(bliss.scanning.scan.Scan) scans:
    :returns list:
    """
    uris = []

    for scan in scans:
        uris += scan_url_info.scan_urls(scan.scan_info)

    assert uris
    return uris


def _on_timeout(writer, uris):
    # The writer might still have the file open (which would be a bug)
    # writer.kill()
    # writer.join()
    # process_utils.log_file_processes(logging_utils.print_err, r".+\.h5$")
    # process_utils.log_file_processes(logging_utils.print_err, r".+\.edf$")
    for uri in uris:
        pattern = ".+{}$".format(nexus.splitUri(uri)[0])
        process_utils.log_file_processes(logging_utils.print_err, pattern)
    assert not uris, uris


def wait_scan_data_finished(scans, writer=None, timeout=10):
    """
    :param list(bliss.scanning.scan.Scan) scans:
    :param PopenGreenlet writer: writer process
    :param num timeout:
    """
    uris = _scan_uris(scans)
    try:
        with gevent.Timeout(timeout):
            while uris:
                uris = [uri for uri in uris if not nexus.nxComplete(uri)]
                gevent.sleep(0.1)
    except gevent.Timeout:
        _on_timeout(writer, uris)
