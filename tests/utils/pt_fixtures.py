from __future__ import annotations
import asyncio
import io
import os
import pytest
from contextlib import contextmanager
from collections.abc import Callable
import gevent
from prompt_toolkit.application import create_app_session
from prompt_toolkit.input.defaults import create_pipe_input
from prompt_toolkit.output.plain_text import PlainTextOutput


@pytest.fixture
def clear_pt_context():
    """Clear the context used by prompt-toolkit in order to isolate tests"""
    from prompt_toolkit.application import current

    app_session = current._current_app_session.get()
    is_default = app_session._input is None and app_session._output is None
    yield
    if is_default:
        app_session._input = None
        app_session._output = None


@contextmanager
def asyncio_loop():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        yield loop
    finally:
        loop.close()


@pytest.fixture
def pt_test_context():
    class SimulatedOutput(PlainTextOutput):
        def __init__(self):
            self._cursor_up: bool = False
            self._output_memory = io.StringIO()
            PlainTextOutput.__init__(self, self._output_memory)

        def displayed_text(self) -> str:
            return self._output_memory.getvalue()

        def write(self, data: str):
            self._cursor_up = False
            super().write(data)

        def write_raw(self, data: str):
            self._cursor_up = False
            super().write_raw(data)

        def cursor_up(self, amount: int) -> None:
            # Just a guess for now
            self._cursor_up = True
            super().cursor_up(amount)

        def erase_down(self):
            if self._cursor_up:
                # Just a guess for now
                self._clear()
            super().erase_down()

        def _clear(self):
            """Clear the actual content of the buffer"""
            self._output_memory.seek(0, os.SEEK_SET)
            self._output_memory.truncate(0)

        def _flush_app(self):
            """Called internally by BLISS when an application is done"""
            print(self.displayed_text())
            self._clear()

    actions = []

    class Context:
        def __init__(self, pipe_input):
            self.input = pipe_input
            self.output = SimulatedOutput()
            self._app_session = None

        @property
        def app_session(self):
            return self._app_session

        def send_input(self, chars: str):
            self.input.send_text(chars)

        def send_input_later(
            self,
            timeout: float,
            chars: str,
            wait_callable: Callable[[], []] | None = None,
        ):
            def do_later():
                gevent.sleep(timeout)
                if wait_callable is not None:
                    wait_callable()
                self.input.send_text(chars)

            g = gevent.spawn(do_later)
            actions.append(g)

    with asyncio_loop():
        with create_pipe_input() as pipe_input:
            context = Context(pipe_input)
            with create_app_session(
                input=context.input, output=context.output
            ) as app_session:
                context._app_session = app_session
                yield context

        for g in actions:
            gevent.kill(g)
