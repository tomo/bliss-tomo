import gevent


def wait_end_of_saving(lima, timeout=5):
    try:
        proxy = lima._proxy
        with gevent.Timeout(timeout):
            while True:
                if proxy.last_image_saved == proxy.last_image_acquired:
                    break
                gevent.sleep(0.1)
    except gevent.Timeout:
        raise TimeoutError("Error while waiting for end of saving")
