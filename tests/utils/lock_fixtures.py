import pytest
import logging
from bliss.config.conductor.connection import Connection

_logger = logging.getLogger(__name__)


def is_test_passed(request):
    node = request.node
    if hasattr(node, "rep_call") and node.rep_call.passed:
        return True
    return False


@pytest.fixture(autouse=True)
def clean_locks(request):
    """
    Make sure the beacon locks are clean in the end of the tests

    - If a test succeed, this raises an error if there is still lock.
    - If a test fail, the locks are anyway cleaned up to isolate
      next tests. And a warning is dislayed.
    """
    yield
    try:
        connection = Connection()
    except RuntimeError:
        # Beacon is maybe not used
        return
    connection.connect()
    all_locks = connection.who_locked()
    if len(all_locks) == 0:
        return

    if is_test_passed(request):
        raise RuntimeError(f"Some lock are still acquired: {all_locks}")

    for lock, owner in all_locks.items():
        _logger.warning("Lock on '%s' is still owned by '%s'", lock, owner)
        connection.lock(lock, priority=999999)
        connection.unlock(lock)
