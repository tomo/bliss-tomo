import pytest
from bliss.config.conductor.connection import Connection
from tomo.helpers.locking_helper import lock, unlock, force_unlock, lslock, AlreadyLockedDevices


def test_force_unlock(beacon):
    """Make sure we can release the lock manually in case of problem"""
    srot = beacon.get("srot")
    lock(srot, timeout=1)

    conn2 = Connection()

    with pytest.raises(AlreadyLockedDevices):
        lock(srot, timeout=3, connection=conn2)

    force_unlock(srot)

    # Now we can lock it
    lock(srot, timeout=1, connection=conn2)
    unlock(srot)


def test_lslock(beacon, capsys):
    srot = beacon.get("srot")
    sx = beacon.get("sx")

    lock(srot, timeout=1)

    conn2 = Connection()
    lock(sx, timeout=1, connection=conn2)

    lslock()
    captured = capsys.readouterr()
    assert "sx " in captured.out
    assert "srot " in captured.out
