from tomo.helpers import string_utils


def test_octet():
    assert string_utils.memory_size(1024) == (1024, "B")


def test_megaoctet():
    assert string_utils.memory_size(1024 * 1024 * 42) == (42, "MB")


def test_gigaoctet():
    assert string_utils.memory_size(1024 * 1024 * 1024 * 85) == (85, "GB")
