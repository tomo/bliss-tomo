import contextlib
from bliss.common import event


class EventListener:
    """Listener for `bliss.common.event` to simplify unit testing"""

    def __init__(self):
        self.events = []

    def __call__(self, value, signal, sender):
        self.events.append((value, signal, sender))

    @contextlib.contextmanager
    def listening(self, obj: object, event_name: str):
        """Context manager to listen to an event, and eventually wait for it

        obj: event emitter
        event_name: signal name
        """
        try:
            event.connect(obj, event_name, self)
            yield
        finally:
            event.disconnect(obj, event_name, self)

    @property
    def last_value(self):
        return self.events[-1][0]

    @property
    def values(self):
        return [e[0] for e in self.events]

    @property
    def last_signal(self):
        return self.events[-1][1]

    @property
    def last_sender(self):
        return self.events[-1][2]

    @property
    def event_count(self):
        return len(self.events)
