from __future__ import annotations
import pytest
from tomo.helpers.proxy_param import ProxyParam
from fscan.fscantools import FScanParamBase


class FooPars(FScanParamBase):
    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            {"foo": 0},
            value_list={},
            no_settings=[],
        )


class BarPars(FScanParamBase):
    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            {"bar": 1},
            value_list={},
            no_settings=[],
        )


class BazPars(FScanParamBase):
    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            {"baz": 2},
            value_list={},
            no_settings=[],
        )


def test_proxy_synchronization(beacon):
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    pars = ProxyParam(foopars, barpars)

    # default
    assert pars.foo == 0
    assert pars.bar == 1

    # set proxy
    pars.foo = 2
    pars.bar = 3
    assert foopars.foo == 2
    assert barpars.bar == 3

    # get proxy
    foopars.foo = 5
    barpars.bar = 6
    assert pars.foo == 5
    assert pars.bar == 6


def test_proxy_dict(beacon):
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    pars = ProxyParam(foopars, barpars)

    assert pars.to_dict() == {"foo": 0, "bar": 1}
    pars.from_dict({"foo": 2, "bar": 3})
    assert foopars.foo == 2
    assert barpars.bar == 3


def test_proxy_reset(beacon):
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    pars = ProxyParam(foopars, barpars)

    foopars.foo = 5
    barpars.bar = 6
    pars.reset()
    assert foopars.foo == 0
    assert barpars.bar == 1


def test_unknown_param(beacon):
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    pars = ProxyParam(foopars, barpars)

    try:
        foopars.toto = 5
    except Exception as e:
        expected_exception_class = type(e)
    else:
        assert False, "Must raise an exception"

    with pytest.raises(expected_exception_class):
        pars.toto = 5


def test_composite_proxy_param(beacon):
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    bazpars = BazPars("bazpars")
    pars = ProxyParam(foopars, ProxyParam(barpars), bazpars)

    assert pars.foo == 0
    assert pars.bar == 1
    assert pars.baz == 2
    pars.foo = 3
    pars.bar = 4
    pars.baz = 5
    assert pars.foo == 3
    assert pars.bar == 4
    assert pars.baz == 5


def test_composite_proxy_dict(beacon):
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    bazpars = BazPars("bazpars")
    pars = ProxyParam(foopars, ProxyParam(barpars), bazpars)

    assert pars.to_dict() == {"foo": 0, "bar": 1, "baz": 2}
    pars.from_dict({"foo": 3, "bar": 4, "baz": 5})
    assert pars.to_dict() == {"foo": 3, "bar": 4, "baz": 5}


def test_unknown_dict_key(beacon):
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    pars = ProxyParam(foopars, barpars)

    try:
        foopars.from_dict({"toto": 5})
    except Exception as e:
        expected_exception_class = type(e)
    else:
        assert False, "Must raise an exception"

    with pytest.raises(expected_exception_class):
        pars.from_dict({"toto": 5})


def test_info(beacon):
    """For coverage"""
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    pars = ProxyParam(foopars, barpars)
    info = pars.__info__()
    assert foopars._name in info
    assert barpars._name in info


def test_hasattr(beacon):
    """For coverage"""
    foopars = FooPars("foopars")
    barpars = BarPars("barpars")
    pars = ProxyParam(foopars, barpars)
    assert hasattr(pars, "foo")
    assert hasattr(pars, "bar")
    assert not hasattr(pars, "2000")
