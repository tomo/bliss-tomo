from tomo.helpers.active_object import ActiveObject


class FakeObject:
    def __init__(self, name):
        self.name = name


def test_no_redis_key(session, current_session):
    active_fake_object = ActiveObject(
        object_class=FakeObject,
        object_key="activeobject_no_key",
        object_label="fake object",
    )
    assert active_fake_object.deref_active_object() is None


def test_set_get(session, current_session):
    open_shutter = current_session.config.get("open_shutter")
    active_fake_object = ActiveObject(
        object_class=object,
        object_key="activeobject_set_get",
        object_label="fake object",
    )
    active_fake_object.set_active_object(open_shutter)
    active_fake_object = ActiveObject(
        object_class=object,
        object_key="activeobject_set_get",
        object_label="fake object",
    )
    assert active_fake_object.deref_active_object() is open_shutter


def test_set_get_invalid_class(session, current_session):
    open_shutter = current_session.config.get("open_shutter")
    active_fake_object = ActiveObject(
        object_class=object,
        object_key="activeobject_set_get_invalid_class",
        object_label="fake object",
    )
    active_fake_object.set_active_object(open_shutter)
    active_fake_object = ActiveObject(
        object_class=FakeObject,
        object_key="activeobject_set_get_invalid_class",
        object_label="fake object",
    )
    assert active_fake_object.deref_active_object() is None
