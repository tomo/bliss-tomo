import pytest
import gevent
from tomo import presets
from tomo.scan.presets.inhibit_auto_projection import InhibitAutoProjectionPreset
from tomo.scan.presets.disable_active_detector_processing import (
    DisableActiveDetectorProcessing,
)
from tomo.scan.presets.move_axis import MoveAxisPreset
from bliss.common.shutter import BaseShutterState
from ..helpers.event_utils import EventListener


def test_dark_shutter_preset_on_closed_shutter(beacon, mocker):
    """Test that the shutter is opened at the end"""
    shutter = beacon.get("closed_shutter")
    assert shutter.state == BaseShutterState.CLOSED
    scan = mocker.Mock()
    scan.name = "mockedscan"
    scan.acq_chain.nodes_list = []
    preset = presets.DarkShutterPreset(shutter)

    preset.prepare(scan)
    assert shutter.state == BaseShutterState.CLOSED
    preset.start(scan)
    assert shutter.state == BaseShutterState.CLOSED
    preset.stop(scan)
    assert shutter.state == BaseShutterState.OPEN


def test_dark_shutter_preset_on_open_shutter(beacon, mocker):
    """Test that the shutter will stay closed only between prepare/stop"""
    shutter = beacon.get("open_shutter")
    assert shutter.state == BaseShutterState.OPEN
    scan = mocker.Mock()
    scan.acq_chain.nodes_list = []
    scan.name = "mockedscan"
    preset = presets.DarkShutterPreset(shutter)

    preset.prepare(scan)
    assert shutter.state == BaseShutterState.CLOSED
    preset.start(scan)
    assert shutter.state == BaseShutterState.CLOSED
    preset.stop(scan)
    assert shutter.state == BaseShutterState.OPEN


def test_dark_shutter_preset_without_open_shutter_option(beacon, mocker):
    """Test that the shutter will stay closed only between prepare/stop"""
    shutter = beacon.get("open_shutter")
    assert shutter.state == BaseShutterState.OPEN
    scan = mocker.Mock()
    scan.acq_chain.nodes_list = []
    scan.name = "mockedscan"
    preset = presets.DarkShutterPreset(shutter, open_shutter=False)

    preset.prepare(scan)
    assert shutter.state == BaseShutterState.CLOSED
    preset.start(scan)
    assert shutter.state == BaseShutterState.CLOSED
    preset.stop(scan)
    assert shutter.state == BaseShutterState.CLOSED


def test_fast_shutter_preset_on_closed_shutter(beacon, mocker):
    """Test that the shutter will be open during the acquisition"""
    shutter = beacon.get("closed_shutter")
    assert shutter.state == BaseShutterState.CLOSED
    scan = mocker.Mock()
    scan.acq_chain.nodes_list = []
    scan.name = "mockedscan"
    preset = presets.FastShutterPreset(shutter)

    preset.prepare(scan)
    assert shutter.state == BaseShutterState.CLOSED
    preset.start(scan)
    assert shutter.state == BaseShutterState.OPEN
    preset.stop(scan)
    assert shutter.state == BaseShutterState.CLOSED


def test_fast_shutter_preset_on_open_shutter(beacon, mocker):
    """Test that the shutter will be open during the acquisition"""
    shutter = beacon.get("open_shutter")
    assert shutter.state == BaseShutterState.OPEN
    scan = mocker.Mock()
    scan.acq_chain.nodes_list = []
    scan.name = "mockedscan"
    preset = presets.FastShutterPreset(shutter)

    preset.prepare(scan)
    assert shutter.state == BaseShutterState.OPEN
    preset.start(scan)
    assert shutter.state == BaseShutterState.OPEN
    preset.stop(scan)
    assert shutter.state == BaseShutterState.CLOSED  # TOMO: Is it normal?


def test_move_axis_preset__1_axis(beacon, mocker):
    """Test that the shutter will be open during the acquisition"""
    move_mock = mocker.patch("tomo.scan.presets.move_axis.move")
    sx = beacon.get("sx")
    preset = MoveAxisPreset(sx, 10.1)

    assert move_mock.call_count == 0
    preset.prepare(None)
    assert move_mock.call_count == 1
    call = move_mock.call_args_list[0]
    assert call[0] == (sx, 10.1)


def test_move_axis_preset__2_axis(beacon, mocker):
    """Test that the shutter will be open during the acquisition"""
    move_mock = mocker.patch("tomo.scan.presets.move_axis.move")
    sx = beacon.get("sx")
    sy = beacon.get("sy")
    preset = MoveAxisPreset(sx, 10.1, sy, 20.1)

    assert move_mock.call_count == 0
    preset.prepare(None)
    assert move_mock.call_count == 1
    call = move_mock.call_args_list[0]
    assert call[0] == (sx, 10.1, sy, 20.1)


def test_move_axis_preset__wrong_param(beacon, mocker):
    """Test that the shutter will be open during the acquisition"""
    sx = beacon.get("sx")
    sy = beacon.get("sy")
    with pytest.raises(TypeError):
        MoveAxisPreset(sx, sy)
    with pytest.raises(TypeError):
        MoveAxisPreset(sx, "10")
    with pytest.raises(ValueError):
        MoveAxisPreset(sx)


def test_inhibit_auto_projection(beacon):
    tomoconfig = beacon.get("tomo_config")
    preset = InhibitAutoProjectionPreset(tomoconfig)

    listener = EventListener()
    with listener.listening(tomoconfig.auto_projection, "inhibited"):
        preset.prepare(None)
        gevent.sleep(1)
        assert listener.values == [True]
        preset.start(None)
        gevent.sleep(1)
        assert listener.values == [True]
        preset.stop(None)
        gevent.sleep(1)
        assert listener.values == [True, False]


def test_disable_active_detector_processing(beacon, lima_simulator_tango_server):
    tomoconfig = beacon.get("tomo_config")
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]

    preset = DisableActiveDetectorProcessing(tomoconfig)

    lima = tomoconfig.detectors.active_detector.detector

    lima.processing.use_background = True
    lima.processing.use_flatfield = True
    preset.prepare(None)
    assert lima.processing.use_background is False
    assert lima.processing.use_flatfield is False
    preset.start(None)
    preset.stop(None)
    assert lima.processing.use_background is True
    assert lima.processing.use_flatfield is True
