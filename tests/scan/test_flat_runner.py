from tomo.scan.presets.reference_motor import ReferenceMotorPreset


def test_flat_motion(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
    no_text_block,
):
    """Setup a flat scan.

    Check that the motor was moved.
    """
    yr = beacon.get("yr")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    flat_runner = tomo_config.get_runner("flat")
    spy_move_in = mocker.spy(flat_runner._flat_motion, "move_in")
    spy_move_out = mocker.spy(flat_runner._flat_motion, "move_out")

    flat_preset = ReferenceMotorPreset(tomo_config.flat_motion)
    flat_runner.add_scan_preset(flat_preset)
    flat_scan = flat_runner(0.1, 2)

    yr_pos = flat_scan.scan_info["positioners"]["positioners_start"]["yr"]
    if False:
        # That's was we would like to have at some point, but it's not the case right now
        assert yr_pos == yr.position + 1
    else:
        assert yr_pos == yr.position

    spy_move_in.assert_called_once()
    spy_move_out.assert_called_once()
