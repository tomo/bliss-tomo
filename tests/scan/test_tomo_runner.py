import pytest
from bliss.common.scans.ct import sct
from bliss.common.scans import DEFAULT_CHAIN
from tomo.scan.tomo_runner import TomoRunner


@pytest.fixture
def clear_default_chain():
    yield
    DEFAULT_CHAIN.clear()


def test_default_chain__empty(clear_default_chain, session, beacon, lima_simulator):
    """Check that nothing is patched"""
    runner = TomoRunner()
    runner.set_default_chains(
        single_chain=None,
        acc_chain=None,
        fallback_single_chain=None,
        fallback_acc_chain=None,
    )
    with runner._setup_detectors_in_default_chain([lima_simulator]):
        s = sct(0.1, lima_simulator, run=False)
    acqobj = list(s.acq_chain.get_node_from_devices(lima_simulator))[0]
    assert acqobj.acq_params["acq_mode"] == "SINGLE"


def test_default_chain__default(clear_default_chain, session, beacon, lima_simulator):
    """Check that the default is applyed instead of the fallback"""
    patch1st = beacon.get("test_tomo_runner__default_chain_with_single")
    patch2nd = beacon.get("test_tomo_runner__default_chain_with_acc")

    runner = TomoRunner()
    runner.set_default_chains(
        single_chain=patch1st,
        acc_chain=None,
        fallback_single_chain=patch2nd,
        fallback_acc_chain=None,
    )
    with runner._setup_detectors_in_default_chain([lima_simulator]):
        s = sct(0.1, lima_simulator, run=False)
    acqobj = list(s.acq_chain.get_node_from_devices(lima_simulator))[0]
    assert acqobj.acq_params["acq_mode"] == "SINGLE"


def test_default_chain__fallback(clear_default_chain, session, beacon, lima_simulator):
    """CHeck that the fallback chain is applied if there is nothing in the main one"""
    patch1st = beacon.get("test_tomo_runner__default_chain_empty")
    patch2nd = beacon.get("test_tomo_runner__default_chain_with_acc")

    runner = TomoRunner()
    runner.set_default_chains(
        single_chain=patch1st,
        acc_chain=None,
        fallback_single_chain=patch2nd,
        fallback_acc_chain=None,
    )
    with runner._setup_detectors_in_default_chain([lima_simulator]):
        s = sct(0.1, lima_simulator, run=False)
    acqobj = list(s.acq_chain.get_node_from_devices(lima_simulator))[0]
    assert acqobj.acq_params["acq_mode"] == "ACCUMULATION"


def test_default_chain__default_restored(
    clear_default_chain, session, beacon, lima_simulator
):
    """Check that the DEFAULT_CHAIN is restored at termination"""
    patch1st = beacon.get("test_tomo_runner__default_chain_with_acc")
    patch = beacon.get("test_tomo_runner__default_chain_with_single")

    runner = TomoRunner()
    runner.set_default_chains(
        single_chain=patch1st,
        acc_chain=None,
        fallback_single_chain=None,
        fallback_acc_chain=None,
    )
    DEFAULT_CHAIN.set_settings(patch["chain_config"])
    with runner._setup_detectors_in_default_chain([lima_simulator]):
        s = sct(0.1, lima_simulator, run=False)
    acqobj = list(s.acq_chain.get_node_from_devices(lima_simulator))[0]
    assert acqobj.acq_params["acq_mode"] == "ACCUMULATION"
    assert (
        DEFAULT_CHAIN._settings[lima_simulator]["acquisition_settings"]["acq_mode"]
        == "SINGLE"
    )
