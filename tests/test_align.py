def test_align(beacon, session, lima_simulator, mocker, setup_common_tomo_config):
    align = beacon.get("test_align")
    assert align is not None

    tomo_config = align.tomo_config
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    active_detector = tomo_config.detectors.detectors[0]
    tomo_config.active_detector = active_detector
    align.sequence._detector = active_detector.detector

    mocker.patch.object(align, "align_calc", return_value=(1.0, 2.0))

    # Add a counter to allow to `ct` from align to work
    sim_ct_1 = beacon.get("sim_ct_1")
    mg = beacon.get("mg")
    mg.set_active()
    mg.add(sim_ct_1)

    align.align(questions_move=False)


def test_alignxc(beacon, session, lima_simulator, mocker):
    """
    Test coverage.
    """
    align = beacon.get("test_align")
    assert align is not None

    print_tilt_results = mocker.spy(align, "_print_alignxc_correction")

    tomo_config = align.tomo_config
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    active_detector = tomo_config.detectors.detectors[0]
    tomo_config.active_detector = active_detector
    align.sequence._detector = active_detector.detector
    active_detector.detector.image.roi = 0, 0, 16, 16

    mocker.patch.object(align, "alignxc_calc", return_value=(0.5, 0.5))

    # Add a counter to allow to `ct` from align to work
    sim_ct_1 = beacon.get("sim_ct_1")
    mg = beacon.get("mg")
    mg.set_active()
    mg.add(sim_ct_1)

    align.alignxc(start=100, stop=110, steps=10)

    print_tilt_results.assert_called_once()


def test_focus(beacon, session, lima_simulator, mocker, setup_common_tomo_config):
    align = beacon.get("test_align")
    assert align is not None

    tomo_config = align.tomo_config
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    active_detector = tomo_config.detectors.detectors[0]
    tomo_config.active_detector = active_detector
    align.sequence._detector = active_detector.detector

    mocker.patch.object(align, "focus_calc", return_value=(1.0, 2, 1.0))

    # Add a counter to allow to `ct` from align to work
    sim_ct_1 = beacon.get("sim_ct_1")
    mg = beacon.get("mg")
    mg.set_active()
    mg.add(sim_ct_1)

    # test with dark flat images
    align.focus()
    # test without dark flat images
    align.focus(dark_flat_images=False)
