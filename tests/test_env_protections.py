"""Test few protections between devices"""

import pytest
from bliss.common.cleanup import capture_exceptions
from tomo.sequencebasic import SequenceBasic
from tomo.sequencebasic import cleanup_sequence
from tomo.helpers.locking_helper import AlreadyLockedDevices, force_unlock


class _EmptySequence(SequenceBasic):
    def __init__(self, name, config):
        SequenceBasic.__init__(self, name, config)
        self._metadata

    def add_metadata(self, scan_info):
        pass

    def get_parameters(self):
        return {"scan_info": {}, "scan_time": 1}

    @cleanup_sequence
    def run(self, user_info=None):
        with capture_exceptions() as capture:
            with self._run_context(capture):
                self._rotation_axis.move(0)
                self._rotation_axis.move(45)


@pytest.fixture
def mocked_tomo_sequence(beacon, lima_simulator):
    tomoconfig = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()
    sequence = _EmptySequence("foo", {"tomo_config": tomoconfig})
    yield sequence


def test_sequence_when_pusher_in(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    setup_common_tomo_config,
    mocked_tomo_sequence,
):
    """If the pusher is in, we can't start the sequence"""
    pusher = beacon.get("pusher")
    axis = beacon.get("pusher_axis")
    axis.controller._set_home_pos(axis, 90.0)
    srot = beacon.get("srot")

    pusher.move_in()

    # The device is own by the pusher
    with pytest.raises(AlreadyLockedDevices) as exc:
        mocked_tomo_sequence.run()
    assert "pusher" in str(exc.value)

    force_unlock(srot)


def test_motion_when_pusher_in(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    setup_common_tomo_config,
    mocked_tomo_sequence,
):
    """If the pusher is in, we can't start the sequence"""
    pusher = beacon.get("pusher")
    axis = beacon.get("pusher_axis")
    axis.controller._set_home_pos(axis, 90.0)

    srot = beacon.get("srot")

    pusher.move_in()

    # The device is own by the pusher
    with pytest.raises(AlreadyLockedDevices) as exc:
        srot.move(10)
    assert "pusher" in str(exc.value)


def test_sequence_with_motion(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    setup_common_tomo_config,
    mocked_tomo_sequence,
):
    """The sequence usually contains srot motion"""
    srot = beacon.get("srot")
    mocked_tomo_sequence.run()
    # Check that the seqnence was completed
    assert srot.position == 45.0


def test_pusher_in_while_srot_moving(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    setup_common_tomo_config,
    mocked_tomo_sequence,
):
    """Start a motion on srot.

    Make the pusher can't be moved in.
    """
    srot = beacon.get("srot")
    pusher = beacon.get("pusher")
    axis = beacon.get("pusher_axis")
    axis.controller._set_home_pos(axis, 90.0)

    srot.velocity = 0.01
    srot.move(45, wait=False)

    with pytest.raises(AlreadyLockedDevices) as exc:
        pusher.move_in()
    assert "srot_motion" in str(exc.value)

    srot.stop()

    # Now it's fine
    pusher.move_in()
