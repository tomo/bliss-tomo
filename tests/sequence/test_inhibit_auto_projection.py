import pytest
import gevent
from tomo.sequence.presets.inhibit_auto_projection import InhibitAutoProjection
from tomo.sequencebasic import SequenceBasic
from ..helpers.event_utils import EventListener


class _EmptySequence(SequenceBasic):
    def __init__(self, name, config):
        SequenceBasic.__init__(self, name, config)
        self._metadata

    def add_metadata(self, scan_info):
        pass

    def show_info(self):
        # Override, else `_inpars.scan_time` is reachable, but do not exists
        pass


@pytest.fixture
def mocked_tomo_sequence(beacon, lima_simulator):
    tomoconfig = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()
    sequence = _EmptySequence("tomosequence", {"tomo_config": tomoconfig})
    yield sequence


def test_auto_projection(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    setup_common_tomo_config,
    mocked_tomo_sequence,
):
    tomoconfig = beacon.get("tomo_config")
    preset = InhibitAutoProjection(tomoconfig)
    sequence = mocked_tomo_sequence
    sequence.add_sequence_preset(preset)
    sequence._setup_sequence(run=False)
    sequence._inpars.scan_time = 1.0
    listener = EventListener()

    with listener.listening(tomoconfig.auto_projection, "inhibited"):
        sequence.run()
        # The listener can be disconnected bwfore the reception of the event
        gevent.sleep(5)

    assert listener.values == [True, False]
