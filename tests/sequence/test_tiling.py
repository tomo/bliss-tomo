import pytest
import gevent
import numpy
import time

from tomo.sequence.tiling import tiling
from tomo.sequence.tiling import tiling_estimation_time
from tomo.helpers import tiling_reader
from ..utils import nxw_test_utils


def wait_readable(reader: tiling_reader.TilingScanReader):
    for _ in range(10):
        if reader.is_finished:
            break
        time.sleep(2.0)
    else:
        raise RuntimeError("Tilingscan is still not readable")


def test_tiling_estimation_time(
    beacon,
    esrf_writer_session,
    nexus_writer_service,
    lima_simulator,
    mocker,
    setup_common_tomo_config,
):
    tomoconfig = beacon.get("tomo_config")
    tomo_detector = tomoconfig.detectors.active_detector
    detector = tomo_detector.detector

    tomo_detector.user_sample_pixel_size = 10
    tomo_detector.sample_pixel_size_mode = "user"

    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    time = tiling_estimation_time(
        -12,
        12,
        -10.1,
        10.1,
        -10,
        10,
        detector,
        expo_time=0.1,
        n_dark=1,
        n_flat=1,
        tomoconfig=tomoconfig,
    )
    assert time is not None
    assert 4 < time < 5


def test_tiling_sequence(
    beacon, esrf_writer_session, nexus_writer_service, lima_simulator, mocker
):
    tomoconfig = beacon.get("tomo_config")
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    gevent.sleep(1)
    tomo_detector = tomoconfig.detectors.active_detector
    detector = tomo_detector.detector

    tomo_detector.user_sample_pixel_size = 10
    tomo_detector.sample_pixel_size_mode = "user"

    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    scan = tiling(
        -12, 12, -10.1, 10.1, -10, 10, detector, expo_time=0.1, tomoconfig=tomoconfig
    )

    nxw_test_utils.wait_scan_data_finished(scans=[scan], writer=nexus_writer_service)
    filename = scan.scan_info["filename"]

    connector = tiling_reader.TilingScanReader(filename, "1.1")
    wait_readable(connector)
    assert numpy.array_equal(connector.front.detector_size, (1024, 1024))
    assert connector.front.pixel_size == 0.01
    assert connector.front.horizontal_range == (-10.1, 10.1)
    assert connector.front.vertical_range == (-10, 10)
    assert connector.front.nb_images == 4
    assert connector.front.position(3) == (4.98, -4.88)
    assert connector.front.image(0).ndim == 2
    assert connector.front.corrected_image(1).image.ndim == 2
    image, horizontal, vertical = connector.front.corrected_image(3)
    assert image.ndim == 2
    assert horizontal == 4.98
    assert vertical == -4.88

    assert numpy.array_equal(connector.side.detector_size, (1024, 1024))
    assert connector.side.pixel_size == 0.01
    assert connector.side.horizontal_range == (-12, 12)
    assert connector.side.vertical_range == (-10, 10)
    assert connector.side.nb_images == 6
    assert connector.side.position(5) == (6.88, 4.88)
    assert connector.side.image(0).ndim == 2
    assert connector.side.corrected_image(0).image.ndim == 2
    image, horizontal, vertical = connector.side.corrected_image(5)
    assert image.ndim == 2
    assert horizontal == 6.88
    assert vertical == 4.88

    assert connector.dark(numpy.float32).ndim == 2
    assert connector.flat("front", numpy.float32).ndim == 2
    assert connector.flat("side", numpy.float32).ndim == 2
    assert connector.corrected_flat("front", numpy.float32).ndim == 2
    assert connector.corrected_flat("side", numpy.float32).ndim == 2


def test_tiling_under_rot_sequence(
    beacon, esrf_writer_session, nexus_writer_service, lima_simulator, mocker
):
    tomoconfig = beacon.get("tomo_config")
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    gevent.sleep(1)
    tomo_detector = tomoconfig.detectors.active_detector
    detector = tomo_detector.detector

    tomo_detector.user_sample_pixel_size = 10
    tomo_detector.sample_pixel_size_mode = "user"

    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    scan = tiling(
        -12,
        12,
        -10.1,
        10.1,
        -10,
        10,
        detector,
        expo_time=0.1,
        tomoconfig=tomoconfig,
        lateral_motor_mode="under_rot",
    )

    nxw_test_utils.wait_scan_data_finished(scans=[scan], writer=nexus_writer_service)
    filename = scan.scan_info["filename"]

    connector = tiling_reader.TilingScanReader(filename, "1.1")
    wait_readable(connector)
    assert numpy.array_equal(connector.front.detector_size, (1024, 1024))
    assert connector.front.pixel_size == 0.01
    assert connector.front.horizontal_range == (-10.1, 10.1)
    assert connector.front.vertical_range == (-10, 10)
    assert connector.front.nb_images == 4
    assert connector.front.position(3) == (4.98, -4.88)
    assert connector.front.image(0).ndim == 2
    assert connector.front.corrected_image(1).image.ndim == 2
    image, horizontal, vertical = connector.front.corrected_image(3)
    assert image.ndim == 2
    assert horizontal == 4.98
    assert vertical == -4.88

    assert numpy.array_equal(connector.side.detector_size, (1024, 1024))
    assert connector.side.pixel_size == 0.01
    assert connector.side.horizontal_range == (-12, 12)
    assert connector.side.vertical_range == (-10, 10)
    assert connector.side.nb_images == 6
    assert connector.side.position(5) == (6.88, 4.88)
    assert connector.side.image(0).ndim == 2
    assert connector.side.corrected_image(0).image.ndim == 2
    image, horizontal, vertical = connector.side.corrected_image(5)
    assert image.ndim == 2
    assert horizontal == 6.88
    assert vertical == 4.88

    assert connector.dark(numpy.float32).ndim == 2
    assert connector.flat("front", numpy.float32).ndim == 2
    assert connector.flat("side", numpy.float32).ndim == 2
    assert connector.corrected_flat("front", numpy.float32).ndim == 2
    assert connector.corrected_flat("side", numpy.float32).ndim == 2


def test_tiling_sequence_line(
    beacon, esrf_writer_session, nexus_writer_service, lima_simulator, mocker
):
    tomoconfig = beacon.get("tomo_config")
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    gevent.sleep(1)
    tomo_detector = tomoconfig.detectors.active_detector
    detector = tomo_detector.detector

    tomo_detector.user_sample_pixel_size = 10
    tomo_detector.sample_pixel_size_mode = "user"

    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    scan = tiling(-1, 1, -1, 1, -10, 10, detector, expo_time=0.1, tomoconfig=tomoconfig)

    nxw_test_utils.wait_scan_data_finished(scans=[scan], writer=nexus_writer_service)
    filename = scan.scan_info["filename"]

    connector = tiling_reader.TilingScanReader(filename, "1.1")
    wait_readable(connector)
    assert numpy.array_equal(connector.front.detector_size, (1024, 1024))
    assert connector.front.pixel_size == 0.01
    assert connector.front.horizontal_range == (-1, 1)
    assert connector.front.vertical_range == (-10, 10)
    assert connector.front.nb_images == 2
    assert connector.front.position(1) == (0, 4.88)
    assert connector.front.image(0).ndim == 2
    image, horizontal, vertical = connector.front.corrected_image(1)
    assert image.ndim == 2
    assert horizontal == 0
    assert vertical == 4.88

    assert numpy.array_equal(connector.side.detector_size, (1024, 1024))
    assert connector.side.pixel_size == 0.01
    assert connector.side.horizontal_range == (-1, 1)
    assert connector.side.vertical_range == (-10, 10)
    assert connector.side.nb_images == 2
    assert connector.side.position(1) == (0, 4.88)
    assert connector.side.image(0).ndim == 2
    assert connector.side.corrected_image(0).image.ndim == 2
    image, horizontal, vertical = connector.side.corrected_image(1)
    assert image.ndim == 2
    assert horizontal == 0
    assert vertical == 4.88

    assert connector.dark(numpy.float32).ndim == 2
    assert connector.flat("front", numpy.float32).ndim == 2
    assert connector.flat("side", numpy.float32).ndim == 2
    assert connector.corrected_flat("front", numpy.float32).ndim == 2
    assert connector.corrected_flat("side", numpy.float32).ndim == 2


def test_tiling_sequence_point(
    beacon, esrf_writer_session, nexus_writer_service, lima_simulator, mocker
):
    tomoconfig = beacon.get("tomo_config")
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    gevent.sleep(1)
    tomo_detector = tomoconfig.detectors.active_detector
    detector = tomo_detector.detector

    tomo_detector.user_sample_pixel_size = 10
    tomo_detector.sample_pixel_size_mode = "user"

    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    scan = tiling(-1, 1, -1, 1, -1, 1, detector, expo_time=0.1, tomoconfig=tomoconfig)

    nxw_test_utils.wait_scan_data_finished(scans=[scan], writer=nexus_writer_service)
    filename = scan.scan_info["filename"]

    connector = tiling_reader.TilingScanReader(filename, "1.1")
    wait_readable(connector)
    assert numpy.array_equal(connector.front.detector_size, (1024, 1024))
    assert connector.front.pixel_size == 0.01
    assert connector.front.horizontal_range == (-1, 1)
    assert connector.front.vertical_range == (-1, 1)
    assert connector.front.nb_images == 1
    assert connector.front.position(0) == (0, 0)
    assert connector.front.image(0).ndim == 2
    image, horizontal, vertical = connector.front.corrected_image(0)
    assert image.ndim == 2
    assert horizontal == 0
    assert vertical == 0

    assert numpy.array_equal(connector.side.detector_size, (1024, 1024))
    assert connector.side.pixel_size == 0.01
    assert connector.side.horizontal_range == (-1, 1)
    assert connector.side.vertical_range == (-1, 1)
    assert connector.side.nb_images == 1
    assert connector.side.position(1) == (0, 0)
    assert connector.side.image(0).ndim == 2
    image, horizontal, vertical = connector.side.corrected_image(0)
    assert image.ndim == 2
    assert horizontal == 0
    assert vertical == 0

    assert connector.dark(numpy.float32).ndim == 2
    assert connector.flat("front", numpy.float32).ndim == 2
    assert connector.flat("side", numpy.float32).ndim == 2
    assert connector.corrected_flat("front", numpy.float32).ndim == 2
    assert connector.corrected_flat("side", numpy.float32).ndim == 2


def test_tiling_runner_vertical_scan(
    beacon, esrf_writer_session, nexus_writer_service, lima_simulator, mocker
):
    sct_mock = mocker.patch("tomo.scan.tiling_runner.sct")
    ascan_mock = mocker.patch("tomo.scan.tiling_runner.ascan")
    amesh_mock = mocker.patch("tomo.scan.tiling_runner.amesh")

    tomoconfig = beacon.get("tomo_config")
    mocked_detector = mocker.Mock()
    mocked_detector.sample_pixel_size = 10
    mocked_detector.detector.image.width = 1000
    mocked_detector.detector.image.height = 500

    tiling_runner = tomoconfig.get_runner("tiling")
    scan = tiling_runner(
        ystart=-1,
        ystop=1,
        zstart=-20,
        zstop=20,
        tomo_detector=mocked_detector,
        expo_time=0.1,
        run=False,
    )
    assert scan is not None

    assert sct_mock.call_count == 0, sct_mock.call_args
    assert amesh_mock.call_count == 0, amesh_mock.call_args
    assert ascan_mock.call_count == 1, ascan_mock.call_args

    call = ascan_mock.call_args_list[0]
    axis, start, stop, intervals = call[0][0:4]
    assert axis.name == "zr"
    assert start == pytest.approx(-17.5, abs=0.01)
    assert stop == pytest.approx(17.5, abs=0.01)
    assert intervals == 7


def test_tiling_runner_topdown(
    beacon, esrf_writer_session, nexus_writer_service, lima_simulator, mocker
):
    sct_mock = mocker.patch("tomo.scan.tiling_runner.sct")
    ascan_mock = mocker.patch("tomo.scan.tiling_runner.ascan")
    amesh_mock = mocker.patch("tomo.scan.tiling_runner.amesh")

    tomoconfig = beacon.get("tomo_config")
    mocked_detector = mocker.Mock()
    mocked_detector.sample_pixel_size = 10
    mocked_detector.detector.image.width = 1000
    mocked_detector.detector.image.height = 500

    tiling_runner = tomoconfig.get_runner("tiling")
    scan = tiling_runner(
        ystart=-20,
        ystop=20,
        zstart=20,
        zstop=-20,
        tomo_detector=mocked_detector,
        expo_time=0.1,
        run=False,
    )
    assert scan is not None

    assert sct_mock.call_count == 0, sct_mock.call_args
    assert amesh_mock.call_count == 1, amesh_mock.call_args
    assert ascan_mock.call_count == 0, ascan_mock.call_args

    call = amesh_mock.call_args_list[0]
    axis, start, stop, intervals = call[0][0:4]
    assert axis.name == "zr"
    assert start == pytest.approx(17.5, abs=0.01)
    assert stop == pytest.approx(-17.5, abs=0.01)
    assert intervals == 7
    axis, start, stop, intervals = call[0][4:8]
    assert axis.name == "sampy"
    assert start == pytest.approx(-15, abs=0.01)
    assert stop == pytest.approx(15, abs=0.01)
    assert intervals == 3


def test_tiling_runner_horizontal_scan(
    beacon, esrf_writer_session, nexus_writer_service, lima_simulator, mocker
):
    sct_mock = mocker.patch("tomo.scan.tiling_runner.sct")
    ascan_mock = mocker.patch("tomo.scan.tiling_runner.ascan")
    amesh_mock = mocker.patch("tomo.scan.tiling_runner.amesh")

    tomoconfig = beacon.get("tomo_config")
    mocked_detector = mocker.Mock()
    mocked_detector.sample_pixel_size = 10
    mocked_detector.detector.image.width = 1000
    mocked_detector.detector.image.height = 500

    tiling_runner = tomoconfig.get_runner("tiling")
    scan = tiling_runner(
        ystart=-20,
        ystop=20,
        zstart=-1,
        zstop=1,
        tomo_detector=mocked_detector,
        expo_time=0.1,
        run=False,
    )
    assert scan is not None

    assert sct_mock.call_count == 0, sct_mock.call_args
    assert amesh_mock.call_count == 0, amesh_mock.call_args
    assert ascan_mock.call_count == 1, ascan_mock.call_args

    call = ascan_mock.call_args_list[0]
    axis, start, stop, intervals = call[0][0:4]
    assert axis.name == "sampy"
    assert start == pytest.approx(-15.0, abs=0.01)
    assert stop == pytest.approx(15, abs=0.01)
    assert intervals == 3


def test_tiling_runner_single_ct(
    beacon, esrf_writer_session, nexus_writer_service, lima_simulator, mocker
):
    sct_mock = mocker.patch("tomo.scan.tiling_runner.sct")
    ascan_mock = mocker.patch("tomo.scan.tiling_runner.ascan")
    amesh_mock = mocker.patch("tomo.scan.tiling_runner.amesh")

    tomoconfig = beacon.get("tomo_config")
    mocked_detector = mocker.Mock()
    mocked_detector.sample_pixel_size = 10
    mocked_detector.detector.image.width = 1000
    mocked_detector.detector.image.height = 500

    tiling_runner = tomoconfig.get_runner("tiling")
    scan = tiling_runner(
        ystart=-1,
        ystop=1,
        zstart=-1,
        zstop=1,
        tomo_detector=mocked_detector,
        expo_time=0.1,
        run=False,
    )
    assert scan is not None

    assert sct_mock.call_count == 1, sct_mock.call_args
    assert amesh_mock.call_count == 0, amesh_mock.call_args
    assert ascan_mock.call_count == 0, ascan_mock.call_args

    call = sct_mock.call_args_list[0]
    assert call[0][0] == 0.1
