def test_pars(beacon):
    """Test that the synchronization of pars are right"""
    tomo_config = beacon.get("tomo_config")
    helical = beacon.get("helical")

    # tomo_config pars are shared
    tomo_config.pars.exposure_time = 10.0
    assert helical.pars.exposure_time == 10.0
    helical.pars.exposure_time = 11.0
    assert tomo_config.pars.exposure_time == 11.0

    # specialized pars are reachable
    assert helical.pars.slave_start_pos == -450.0
    helical.pars.slave_start_pos = 1.0
