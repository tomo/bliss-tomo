def test_pars(beacon):
    """Test that the synchronization of pars are right"""
    tomo_config = beacon.get("tomo_config")
    topotomo = beacon.get("topotomo")

    # tomo_config pars are shared
    tomo_config.pars.exposure_time = 10.0
    assert topotomo.pars.exposure_time == 10.0
    topotomo.pars.exposure_time = 11.0
    assert tomo_config.pars.exposure_time == 11.0

    # specialized pars are reachable
    assert topotomo.pars.nested_start_pos == 0.0
    topotomo.pars.nested_start_pos = 1.0
