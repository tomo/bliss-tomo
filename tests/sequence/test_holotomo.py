import pytest
from tomo.controllers.tomo_detector import TomoDetector


@pytest.fixture
def setup_holotomo(beacon):
    detector: TomoDetector = beacon.get("tomo_detector")
    detector.detector.image.roi = 0, 0, 100, 50
    detector.sample_pixel_size_mode = "auto"
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 1000
    detector.tomo_detectors.source_distance = 2000
    holotomo = beacon.get("holotomo")
    tomo_config.detectors.active_detector = detector
    # Test to make sure the setup is at a known state
    assert detector.optic is not None
    assert detector.optic.magnification == 10
    assert detector.sample_pixel_size == 0.05
    return holotomo


def test_holotomo(
    session,
    esrf_writer_session,
    nexus_writer_service,
    lima_simulator,
    beacon,
    setup_holotomo,
    mocker,
    setup_common_tomo_config,
):
    """Setup a fasttomo scan in step mode

    Check that the basic scan record the expected data
    """
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.detectors.active_detector = tomo_detector
    tomo_detector.detector.acquisition.mode = "SINGLE"
    tomo_detector.detector.image.roi = 0, 0, 4, 4

    holotomo = beacon.get("holotomo")
    holotomo.nb_distances = 2
    holotomo.pixel_size = 0.05
    assert len(holotomo.distances) == 2

    fast_scan = beacon.get("fasttomo")
    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 1  # speed up a bit the test
    fast_scan.pars.energy = 1
    fast_scan.pars.dark_at_start = False
    fast_scan.pars.flat_at_start = False
    fast_scan.pars.images_on_return = False

    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    holo_scan = beacon.get("holotomo_seq")
    holo_scan.pars.use_beam_tracker = False
    nbpoints = 5
    holo_scan.basic_scan(
        start_pos=0,
        end_pos=45,
        tomo_n=nbpoints - 1,
        expo_time=0.1,
    )

    technique = holo_scan._sequence.scan_info["technique"]
    assert technique["subscans"]["scan0"]["type"] == "tomo:step"
    assert len(holo_scan._scans) == 1

    assert "ascan" in holo_scan._scans[0].name

    ascan = holo_scan._scans[0]
    assert len(ascan.get_data()["axis:srot"]) == nbpoints
    assert ascan.get_data()["axis:srot"][0] == 0
    assert ascan.get_data()["axis:srot"][-1] == 45
    view = ascan.get_data()["lima_simulator:image"]
    assert len(view) == 5


def test_holotomo_estimation_time(
    session,
    esrf_writer_session,
    nexus_writer_service,
    lima_simulator,
    beacon,
    setup_holotomo,
    mocker,
    setup_common_tomo_config,
):
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.detectors.active_detector = tomo_detector
    tomo_detector.detector.acquisition.mode = "SINGLE"
    tomo_detector.detector.image.roi = 0, 0, 4, 4

    holotomo = beacon.get("holotomo")
    holotomo.nb_distances = 4
    holotomo.pixel_size = 0.05

    fast_scan = beacon.get("fasttomo")
    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 25
    fast_scan.pars.energy = 25
    fast_scan.pars.dark_at_start = True
    fast_scan.pars.flat_at_start = True
    fast_scan.pars.images_on_return = True

    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    holotomo_seq = beacon.get("holotomo_seq")
    assert holotomo_seq.estimation_time() == pytest.approx(2181, abs=30)
