def test_pars(beacon):
    """Test that the synchronization of pars are right"""
    tomo_config = beacon.get("tomo_config")
    pcotomo = beacon.get("pcotomo")

    # tomo_config pars are shared
    tomo_config.pars.exposure_time = 10.0
    assert pcotomo.pars.exposure_time == 10.0
    pcotomo.pars.exposure_time = 11.0
    assert tomo_config.pars.exposure_time == 11.0

    # specialized pars are reachable
    assert pcotomo.pars.shutter_time == 0
    pcotomo.pars.shutter_time = 1.0
    assert pcotomo.pars.download_after_ntomo == 1
    pcotomo.pars.download_after_ntomo = 3
