import pytest

try:
    from bliss.rest_service.rest_service import RestService
except ImportError:
    pytest.skip("No rest service in this BLISS version", allow_module_level=True)


@pytest.fixture
def rest_session(beacon, scan_tmpdir):
    session = beacon.get("rest_session")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    yield session
    session.close()


@pytest.fixture
def rest_service(rest_session):
    """Rest service on top of the rest_session"""
    service = RestService(rest_session)
    service.start()
    yield service
    service.stop()
