OBJ_REF = {
    "name": "dummy_air_bearing",
    "type": "airbearing",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {"state": "UNKNOWN"},
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("dummy_air_bearing")
    mockupshutter = rest_service.object_store.get_object("dummy_air_bearing")
    assert mockupshutter.state.model_dump() == OBJ_REF
