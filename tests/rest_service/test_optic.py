OBJ_REF = {
    "name": "tomo_detector__optic",
    "type": "optic",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "READY",
        "magnification": 10.0,
        "available_magnifications": None,
        "target_magnification": None,
        "magnification_range": None,
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("tomo_detector__optic")
    mockupshutter = rest_service.object_store.get_object("tomo_detector__optic")
    assert mockupshutter.state.model_dump() == OBJ_REF
