OBJ_REF = {
    "name": "tomo_imaging",
    "type": "tomoimaging",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "UNKNOWN",
        "update_on_move": False,
        "exposure_time": 0.5,
        "settle_time": 0.0,
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("tomo_imaging")
    mockupshutter = rest_service.object_store.get_object("tomo_imaging")
    assert mockupshutter.state.model_dump() == OBJ_REF
