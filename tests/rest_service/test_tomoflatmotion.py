OBJ_REF = {
    "name": "tomo_config_ref_parallel",
    "type": "tomoflatmotion",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "READY",
        "settle_time": 0.1,
        "power_onoff": False,
        "move_in_parallel": True,
        "available_axes": ["hardware:refmot1", "hardware:refmot2"],
        "motion": [],
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("tomo_config_ref_parallel")
    mockupshutter = rest_service.object_store.get_object("tomo_config_ref_parallel")
    assert mockupshutter.state.model_dump() == OBJ_REF
