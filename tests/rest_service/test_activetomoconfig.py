OBJ_REF = {
    "name": "ACTIVE_TOMOCONFIG",
    "type": "objectref",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {"ref": "hardware:tomo_config"},
    "user_tags": [],
    "locked": None,
}


def test_state(rest_session, rest_service):
    tomo_config = rest_session.config.get("tomo_config")
    tomo_config.set_active()
    rest_service.object_store.register_object("ACTIVE_TOMOCONFIG")
    mockupshutter = rest_service.object_store.get_object("ACTIVE_TOMOCONFIG")
    assert mockupshutter.state.model_dump() == OBJ_REF
