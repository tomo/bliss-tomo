OBJ_REF = {
    "name": "tomo_config__sample_stage",
    "type": "tomosamplestage",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "sx": "hardware:xr",
        "sy": "hardware:yr",
        "sz": "hardware:zr",
        "somega": "hardware:srot",
        "sampx": "hardware:sampx",
        "sampy": "hardware:sampy",
        "sampu": "hardware:sampu",
        "sampv": "hardware:sampv",
        "pusher": "hardware:",
        "air_bearing_x": "hardware:",
        "x_axis_focal_pos": None,
        "detector_center": [None, None],
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_session, rest_service):
    rest_session.config.get("tomo_config")
    rest_service.object_store.register_object("tomo_config__sample_stage")
    mockupshutter = rest_service.object_store.get_object("tomo_config__sample_stage")
    assert mockupshutter.state.model_dump() == OBJ_REF
