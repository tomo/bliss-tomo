OBJ_REF = {
    "name": "tomo_config",
    "type": "tomoconfig",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "imaging": "hardware:",
        "sample_stage": "hardware:tomo_config__sample_stage",
        "sxbeam": "hardware:",
        "detectors": "hardware:tomo_detectors",
        "reference_position": "hardware:tomo_config__parking_position",
        "energy": 999.0,
        "flat_motion": "hardware:tomo_config_ref",
        "holotomo": "hardware:",
        "latency_time": 0.0,
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("tomo_config")
    mockupshutter = rest_service.object_store.get_object("tomo_config")
    assert mockupshutter.state.model_dump() == OBJ_REF
