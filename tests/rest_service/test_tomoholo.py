OBJ_REF = {
    "name": "holotomo",
    "type": "tomoholo",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "READY",
        "pixel_size": None,
        "nb_distances": 4,
        "settle_time": 0.0,
        "distances": [],
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("holotomo")
    mockupshutter = rest_service.object_store.get_object("holotomo")
    assert mockupshutter.state.model_dump() == OBJ_REF
