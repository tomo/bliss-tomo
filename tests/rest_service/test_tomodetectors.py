OBJ_REF = {
    "name": "tomo_detectors",
    "type": "tomodetectors",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "READY",
        "detectors": ["hardware:tomo_detector"],
        "active_detector": "hardware:",
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("tomo_detectors")
    mockupshutter = rest_service.object_store.get_object("tomo_detectors")
    assert mockupshutter.state.model_dump() == OBJ_REF
