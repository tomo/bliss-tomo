OBJ_REF = {
    "name": "tomo_detector",
    "type": "tomodetector",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "READY",
        "camera_pixel_size": [1.0, 1.0],
        "actual_size": [1024, 1024],
        "sample_pixel_size_mode": "AUTO",
        "user_sample_pixel_size": None,
        "sample_pixel_size": 0.1,
        "field_of_view": 0.1024,
        "detector": "hardware:lima_simulator",
        "optic": "hardware:tomo_detector__optic",
        "source_distance": 40000.0,
        "acq_mode": "MANUAL",
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_session, rest_service, lima_simulator_tango_server):
    detector = rest_session.config.get("tomo_detector")
    tomo_config = rest_session.config.get("tomo_config")
    tomo_config.sample_stage.source_distance = 40000
    detector.tomo_detectors.source_distance = 40000

    rest_service.object_store.register_object("tomo_detector")
    mockupshutter = rest_service.object_store.get_object("tomo_detector")
    assert mockupshutter.state.model_dump() == OBJ_REF
