OBJ_REF = {
    "name": "tomo_config__parking_position",
    "type": "tomoreferenceposition",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {},
    "user_tags": [],
    "locked": None,
}


def test_state(rest_session, rest_service):
    rest_session.config.get("tomo_config")
    rest_service.object_store.register_object("tomo_config__parking_position")
    mockupshutter = rest_service.object_store.get_object(
        "tomo_config__parking_position"
    )
    assert mockupshutter.state.model_dump() == OBJ_REF
