OBJ_REF = {
    "name": "test_pusher",
    "type": "pusher",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {"state": "UNKNOWN"},
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("test_pusher")
    mockupshutter = rest_service.object_store.get_object("test_pusher")
    assert mockupshutter.state.model_dump() == OBJ_REF
