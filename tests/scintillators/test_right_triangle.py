import pytest
from tomo.scintillators import utils
from tomo.scintillators import right_triangle


def test_scintillator_correction():
    """Test a known geometry which was setup at BM05"""
    description = {
        "top_right": {
            "screw_distance_mm": 28.65,
            "screw_pitch_mm": 0.25,
        },
        "bottom_left": {
            "screw_distance_mm": 9,
            "screw_pitch_mm": 0.25,
        },
    }

    geometry = right_triangle.RightTriangleScintillator(description)

    tilt_corr_v_rad = 1.2e-03
    tilt_corr_h_rad = -2e-03

    results = geometry.tilt_hardware_correction(tilt_corr_v_rad, tilt_corr_h_rad)
    assert len(results) == 1
    corr1 = results[0].corrections
    assert corr1[0].role == "bottom_left"
    assert corr1[0].distance == pytest.approx(-0.018, abs=0.0001)
    assert corr1[1].role == "top_right"
    assert corr1[1].distance == pytest.approx(0.0344, abs=0.0001)

    screw_bl = utils.human_readable_turns(corr1[0].turns)
    assert screw_bl == utils.HumanReadableTurns(
        full_turns=0,
        half_turns=0,
        quarter_turns=0,
        eighth_turns=0,
        sixteenth_turns=1,
        orientation="anticlockwise",
    )

    screw_tr = utils.human_readable_turns(corr1[1].turns)
    assert screw_tr == utils.HumanReadableTurns(
        full_turns=0,
        half_turns=0,
        quarter_turns=0,
        eighth_turns=1,
        sixteenth_turns=0,
        orientation="clockwise",
    )

    utils.print_tilt_results(geometry, results)
