# EBS-Tomo

Fast tomography framework written in Python with Bliss

Check the [latest documentation from the master](https://tomo.gitlab-pages.esrf.fr/bliss-tomo/master/index.html)

[API](https://tomo.gitlab-pages.esrf.fr/bliss-tomo/master/api/index.html)

# Test

```
pytest tests
```

Some generated tomo scans can be exported for following way for reference.

```
pytest tests --export-saved-scan="/my/directory/for/scans"
```
