"""
Generate a fake program from a Musst mprg file.

With some luck, the result can be used with a FakeMusst.

It is better to work with a raw mprg file without replacement template.
Else this replacement can still be setup manually in this script.
See the `main` function.
"""

import os.path
import re
import sys
from tomo.controllers.fake_musst import FakeMusst


class MprgLexer:
    def __init__(self, filename, template_replacement):
        with open(filename, "rt") as f:
            data = f.read()
        for k, v in template_replacement.items():
            data = data.replace(k, v)
        self.__hash = FakeMusst.program_hash(data)
        self.__commands = data.split("\n")
        self.__unread = None
        self.__vars = set()
        self.__vars.add("TIMER")
        self.__foutput = None

    def set_output(self, foutput):
        self.__foutput = foutput

    def writeln(self, text=None):
        f = self.__foutput
        if f:
            if text:
                f.write(text)
            f.write("\n")

    def read_command(self):
        if self.__unread:
            c = self.__unread
            self.__unread = None
            return c
        while len(self.__commands) > 0:
            c = self.__commands.pop(0)
            c = c.strip()
            if c == "":
                continue
            if c.startswith("//"):
                continue
            return re.split("[ \t]+", c)
        return None

    def unread(self, c):
        """Unread a single command"""
        assert self.__unread is None
        self.__unread = c

    def read_init(self):
        self.writeln("import numpy")
        self.writeln("from tomo.controllers.fake_musst import FakeMusstProgram")
        self.writeln()

        self.writeln("class FscanProgram(FakeMusstProgram):")
        self.writeln("")
        self.writeln(f"    _HASH_SOURCES = ['{self.__hash}']")
        self.writeln("")
        self.writeln("    def _init(self):")
        while True:
            c = self.read_command()
            if c is None:
                break
            self.__vars.add(c[1])
            if c[0] == "SIGNED":
                assert len(c) == 2
                self.writeln(f"        self.register_variable('{c[1]}', numpy.int32)")
            elif c[0] == "UNSIGNED":
                assert len(c) == 2
                self.writeln(f"        self.register_variable('{c[1]}', numpy.uint32)")
            elif c[0] == "ALIAS":
                assert len(c) == 4
                assert c[2] == "="
                self.writeln(f"        self.{c[1]} = self.{c[3]}  # alias")
            else:
                self.unread(c)
                break

    def read_prog_and_sub(self):
        c = self.read_command()
        if c is None:
            return c
        if c[0] not in ["PROG", "SUB"]:
            self.unread(c)
            return None

        self.writeln()
        self.writeln(f"    def {c[0]}_{c[1]}(self):")

        until = "ENDSUB" if c[0] == "SUB" else "ENDPROG"
        self.read_block(until, nbindent=2)
        return True

    def gen_expression(self, tokens):
        if isinstance(tokens, str):
            # Would be good to have a real tokenizer
            tokens = tokens.replace("(", " ( ")
            tokens = tokens.replace(")", " ) ")
            tokens = tokens.replace("=", " = ")
            tokens = tokens.replace("&", " & ")
            tokens = tokens.replace(" =  = ", " == ")
            tokens = tokens.replace(" ! = ", " != ")
            tokens = tokens.replace("-", " - ")
            tokens = tokens.replace("+", " + ")
            tokens = tokens.replace("-  =", "-=")
            tokens = tokens.replace("+  =", "+=")
            tokens = tokens.replace("&  =", "&=")
            tokens = re.split(" +", tokens.strip())
        result = []
        for t in tokens:
            if t.startswith("$"):
                c = t[1:]
                channel = self.channel_ref(c)
                result.append(f"self.read_channel({channel})")
            elif t in self.__vars:
                result.append(f"self.{t}")
            else:
                result.append(t)
        return " ".join(result)

    def read_block(self, until, nbindent):
        indent = "    " * nbindent
        while True:
            c = self.read_command()

            if c[0] == until:
                return
            elif c[0] == "IF":
                assert c[-1] == "THEN"
                tokens = " ".join(c[1:-1])
                expr = self.gen_expression(tokens)
                self.writeln(indent + f"if {expr}:")
                self.read_block("ENDIF", nbindent=nbindent + 1)
            elif c[0] == "FOR":
                assert c[2] == "FROM"
                assert c[4] == "TO"
                expr_from = self.gen_expression(c[3])
                expr_to = self.gen_expression(c[5])
                self.writeln(
                    indent + f"for self.{c[1]} in range({expr_from}, {expr_to}):"
                )
                self.read_block("ENDFOR", nbindent=nbindent + 1)
            elif c[0] == "WHILE":
                assert c[-1] == "DO"
                tokens = " ".join(c[1:-1])[1:-1].split(" ")
                expr = self.gen_expression(tokens)
                self.writeln(indent + f"while {expr}:")
                self.read_block("ENDWHILE", nbindent=nbindent + 1)
            elif c[0] in self.__vars:
                c = self.gen_expression(" ".join(c))
                c = c.split(" ")
                assert c[1][-1] == "="
                expr = " ".join(c)
                self.writeln(indent + expr)
            elif c[0] == "AT":
                assert c[2] == "DO"
                if c[1] == "DEFEVENT":
                    self.writeln(
                        indent + f"with self.wait_for_defevent():  # AT {c[1]} = ..."
                    )
                else:
                    channel = self.channel_ref(c[1])
                    self.writeln(
                        indent
                        + f"with self.wait_for_target({channel}):  # AT {c[1]} = ..."
                    )
                for cnext in c[3:]:
                    if cnext == "NOTHING":
                        self.writeln(indent + "    " + "pass")

                    elif cnext == "STORE":
                        self.writeln(indent + "    " + f"self.STORE()")
                    elif cnext == "ATRIG":
                        self.writeln(indent + "    " + f"self.ATRIG()")
                    elif cnext == "BTRIG":
                        self.writeln(indent + "    " + f"self.BTRIG()")
                    else:
                        assert False, f"{cnext} unsupported"
            elif c[0].startswith("@"):
                c = self.gen_expression(" ".join([c[0][1:]] + c[1:]))
                c = c.split(" ")
                assert c[1] == "="
                channel = self.channel_ref(c[0].replace("self.", ""))
                expr = " ".join(c[2:])
                self.writeln(
                    indent + f"self.setup_target({channel}, {expr})  # {c[0]} = ..."
                )
            elif c == ["CTSTART", "TIMER"]:
                self.writeln(indent + f"self.{c[0]}_{c[1]}()")
            elif c == ["CTSTOP", "TIMER"]:
                self.writeln(indent + f"self.{c[0]}_{c[1]}()")
            elif c[0] == "GOSUB":
                assert len(c) == 2
                self.writeln(indent + f"self.SUB_{c[1]}()")
            elif c[0] == "ELSE":
                assert len(c) == 1
                self.writeln(("    " * (nbindent - 1)) + "else:")
            elif c[0] == "BTRIG":
                if len(c) == 1:
                    self.writeln(indent + "self.BTRIG()")
                elif len(c) == 2:
                    self.writeln(indent + f"self.BTRIG({c[1]})")
                else:
                    assert False, "Unsupported nb params"
            elif c == ["EMEM", "0", "AT", "0"]:
                self.writeln(indent + f"self.EMEM_0_AT_0()")
            elif c[0] == "STORELIST":
                clist = ", ".join([self.channel_ref(n) for n in c[1:]])
                self.writeln(indent + f"self.STORELIST({clist})")
            elif c[0] == "EVSOURCE":
                assert len(c) == 3
                channel = self.channel_ref(c[1])
                self.writeln(indent + f"self.EVSOURCE({channel}, self.{c[2]})")
            elif c[0] == "EXIT":
                expr = self.gen_expression(c[1:])
                self.writeln(indent + f"return self.EXIT({expr})")
            elif c[0] == "DEFEVENT":
                assert len(c) == 2
                channel = self.channel_ref(c[1])
                self.writeln(indent + f"self.DEFEVENT({channel})")
            else:
                self.writeln(indent + " ".join(c))

    def channel_ref(self, channel: str):
        if channel == "TIMER":
            return "self._CHTIMER"
        return f"self.{channel}"

    def read_while(self):
        while True:
            c = self.read_command()
            if c[0] in ["ENDWHILE"]:
                return
            self.writeln("        " + " ".join(c))

    def parse(self):
        self.read_init()
        while True:
            p = self.read_prog_and_sub()
            if p is None:
                break


def convert(input_filename, output_filename, template):
    mprg = MprgLexer(input_filename, template)
    with open(output_filename, "wt") as foutput:
        mprg.set_output(foutput)
        mprg.parse()


if __name__ == "__main__":
    root = os.path.abspath(os.path.dirname(__file__))

    if False:
        # Example of replacement template
        template_replacement = {
            "$DATA_ALIAS$": "ALIAS MOT1 = CH1\nALIAS DATA1 = CH2\nALIAS DATA2 = CH3\nALIAS DATA3 = CH4\n",
            "$DATA_STORE$": "MOT1 DATA1 DATA2 DATA3 ",
        }
    else:
        template_replacement = {}
    convert(sys.argv[1], sys.argv[2], template_replacement)
