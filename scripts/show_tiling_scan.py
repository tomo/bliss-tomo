import sys
from typing import Tuple

import numpy
from silx.gui import qt
from silx.gui.plot import Plot2D
from silx.gui.colors import Colormap
from tomo.helpers.tiling_reader import TilingScanReader
from silx.gui.plot.utils.axis import SyncAxes


def main():
    connector = TilingScanReader(sys.argv[1], sys.argv[2])

    app = qt.QApplication([])

    window = qt.QWidget()
    layout = qt.QHBoxLayout(window)

    colormap = Colormap("cividis")

    plotSide = Plot2D(window)
    plotSide.setDefaultColormap(colormap)
    plotSide.setKeepDataAspectRatio(True)
    plotSide.getYAxis().setInverted(True)
    layout.addWidget(plotSide)

    plotFront = Plot2D(window)
    plotFront.setDefaultColormap(colormap)
    plotFront.setKeepDataAspectRatio(True)
    plotFront.getYAxis().setInverted(True)
    layout.addWidget(plotFront)

    vconstraint = SyncAxes([plotFront.getYAxis(), plotSide.getYAxis()])

    def plotTiledPlane(plot: Plot2D, tiled_plane) -> Tuple[float, float]:
        vmin, vmax = numpy.inf, -numpy.inf
        pixel_size = tiled_plane.pixel_size
        for index in range(tiled_plane.nb_images):
            image, x, y = tiled_plane.corrected_image(index, dtype=float)

            # We want to display the sample stage from the detector point of view
            # Pixel index 0 (which is at the left side of the detector) is at our right
            # FIXME: this have to be checked with a real detector
            image = numpy.flip(image, 1)

            posx = x - image.shape[1] * pixel_size * 0.5
            posy = y - image.shape[0] * pixel_size * 0.5
            plot.addImage(
                data=image,
                origin=(posx, posy),
                scale=(pixel_size, pixel_size),
                legend=f"img[{x}, {y}]",
            )
            vmin, vmax = min(vmin, image.min()), max(vmax, image.max())
        return vmin, vmax

    front_vmin, front_vmax = plotTiledPlane(plotFront, connector.front)
    side_vmin, side_vmax = plotTiledPlane(plotSide, connector.side)

    vmin = min(front_vmin, side_vmin)
    vmax = max(front_vmax, side_vmax)
    colormap.setVRange(vmin, vmax)

    window.show()
    app.exec_()


if __name__ == "__main__":
    main()
