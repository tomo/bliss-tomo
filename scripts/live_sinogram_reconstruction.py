"""
OpenCL is expected in the env


.. code-block::

   conda install pyopencl ocl-icd-system

.. code-block::

   python scripts/live_sinogram_reconstruction.py /tmp/scans/inhouse/id002111/id00/id002111_id00.h5 sample_0001_9.1
"""

import sys
import threading
import time
import numpy

from silx.io import h5py_utils
from silx.gui import qt
from silx.gui.utils import concurrent
from silx.gui.plot import Plot2D
from silx.image import backprojection

from tomo.helpers.sinogram_reader import SinogramReader


class UpdateThread(threading.Thread):
    """Thread updating the image of a :class:`~sil.gui.plot.Plot2D`

    :param plot2d: The Plot2D to update."""

    def __init__(self, plot2d):
        self.plot2d = plot2d
        self.running = False
        self.future_result = None
        super(UpdateThread, self).__init__()

    def start(self):
        """Start the update thread"""
        self.running = True
        super(UpdateThread, self).start()

    def compute(self):
        with h5py_utils.File(sys.argv[1]) as h5:
            reader = SinogramReader(h5, sys.argv[2])
            translation_size = reader.translation_shape

            angles = numpy.deg2rad(
                reader.rotation[0::translation_size], dtype=numpy.float32
            )
            sino = reader.sinogram.reshape(-1, translation_size)
            sino = sino.astype(numpy.float32)

            fbp = backprojection.Backprojection(
                sino_shape=sino.shape, angles=angles, devicetype="all"
            )

            array = fbp.filtered_backprojection(sino)
            return array

    def run(self, pos={"x0": 0, "y0": 0}):
        """Method implementing thread loop that updates the plot

        It produces an image every 1s or so, and
        either updates the plot or skip the image
        """
        while self.running:
            time.sleep(1)

            image = self.compute()
            if self.future_result is None or self.future_result.done():
                # plot the data asynchronously, and
                # keep a reference to the `future` object
                self.future_result = concurrent.submitToQtMainThread(
                    self.plot2d.addImage, image, resetzoom=False
                )

    def stop(self):
        """Stop the update thread"""
        self.running = False
        self.join(2)


def main():
    global app
    app = qt.QApplication([])

    # Create a Plot2D, set its limits and display it
    plot2d = Plot2D()
    plot2d.getIntensityHistogramAction().setVisible(True)
    plot2d.setKeepDataAspectRatio(True)
    plot2d.getDefaultColormap().setName("cividis")
    plot2d.show()

    # Create the thread that calls submitToQtMainThread
    updateThread = UpdateThread(plot2d)
    updateThread.start()  # Start updating the plot

    app.exec()

    updateThread.stop()  # Stop updating the plot


if __name__ == "__main__":
    main()
