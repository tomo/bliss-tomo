from __future__ import annotations

import argparse
from silx.gui import qt
from silx.gui.plot import ImageView
from tomo.tango.servers import simulated_sample_stage
from tomo.tango.servers import visible_sample_stage


class QFloatSlider(qt.QSlider):
    def setRange(self, vmin, vmax):
        return qt.QSlider.setRange(self, int(vmin * 10), int(vmax * 10))

    def value(self):
        return super(QFloatSlider, self).value() / 10


class SampleStageWindow(qt.QMainWindow):
    def __init__(self):
        qt.QMainWindow.__init__(self)
        main = qt.QWidget(self)
        layout = qt.QHBoxLayout(main)
        w1 = self.createControl()
        w2 = self.createCamera()
        layout.addWidget(w1)
        layout.addWidget(w2)
        layout.setStretchFactor(w1, 0)
        layout.setStretchFactor(w2, 1)
        self.setCentralWidget(main)

    def setSampleStage(self, sampleStage):
        self._sampleStage = sampleStage
        self.__updateCamera()

    def __motorChanged(self):
        widget = self.sender()
        name = widget.objectName()
        value = widget.value()
        setattr(self._sampleStage, name, value)
        self.__updateCamera()

    def __checkedChanged(self):
        widget = self.sender()
        name = widget.objectName()
        value = widget.isChecked()
        setattr(self._sampleStage, name, value)
        self.__updateCamera()

    def __selectChanged(self, button):
        name = button.property("ss/name")
        value = button.property("ss/value")
        setattr(self._sampleStage, name, value)
        self.__updateCamera()

    def createControl(self):
        panel = qt.QWidget(self)
        layout = qt.QGridLayout(panel)
        row = 0

        def addMotor(name, range=(-200, 200)):
            nonlocal row
            label = qt.QLabel(panel)
            label.setAlignment(qt.Qt.AlignRight)
            label.setText(name)
            slider = QFloatSlider(panel)
            slider.setOrientation(qt.Qt.Horizontal)
            slider.setRange(range[0], range[1])
            slider.setObjectName(name)
            slider.valueChanged.connect(self.__motorChanged)
            layout.addWidget(label, row, 0)
            layout.addWidget(slider, row, 1)
            value = qt.QLabel(panel)
            value.setMinimumWidth(50)

            def motorChanged():
                widget = self.sender()
                v = widget.value()
                value.setText(f"{v}")

            slider.valueChanged.connect(motorChanged)
            layout.addWidget(value, row, 2)
            row += 1

        def addRotation(name):
            nonlocal row
            label = qt.QLabel(panel)
            label.setAlignment(qt.Qt.AlignRight)
            label.setText(name)
            dial = qt.QDial(panel)
            dial.setRange(0, 360)
            dial.setObjectName(name)
            dial.setNotchesVisible(True)
            dial.setWrapping(True)
            value = qt.QLabel(panel)
            value.setMinimumWidth(50)

            def motorChanged():
                widget = self.sender()
                v = widget.value()
                value.setText(f"{v}")

            dial.valueChanged.connect(motorChanged)
            dial.valueChanged.connect(self.__motorChanged)
            layout.addWidget(label, row, 0)
            layout.addWidget(dial, row, 1)
            layout.addWidget(value, row, 2)
            row += 1

        def addPush(name):
            nonlocal row
            label = qt.QLabel(panel)
            label.setAlignment(qt.Qt.AlignRight)
            # label.setText(name)
            button = qt.QPushButton(panel)
            button.setCheckable(True)
            button.setChecked(True)
            button.setObjectName(name)
            button.clicked.connect(self.__checkedChanged)
            button.setText(name)
            button.setDown(True)
            layout.addWidget(label, row, 0)
            layout.addWidget(button, row, 1)
            row += 1

        def addSelect(name, default, **kwargs):
            nonlocal row
            label = qt.QLabel(panel)
            label.setAlignment(qt.Qt.AlignRight)
            label.setText(name)
            widget = qt.QWidget(panel)
            subPanel = qt.QHBoxLayout(widget)
            wvalue = qt.QLabel(panel)
            wvalue.setMinimumWidth(50)
            layout.addWidget(label, row, 0)
            layout.addWidget(widget, row, 1)
            layout.addWidget(wvalue, row, 2)
            group = qt.QButtonGroup(widget)
            group.buttonClicked.connect(self.__selectChanged)

            def pushed():
                widget = self.sender()
                v = widget.property("ss/value")
                wvalue.setText(f"{v}")

            for kname, kvalue in kwargs.items():
                button = qt.QPushButton(panel)
                button.setCheckable(True)
                if kvalue == default:
                    button.setChecked(True)
                button.setText(kname)
                button.setProperty("ss/value", kvalue)
                button.setProperty("ss/name", name)
                button.clicked.connect(pushed)
                group.addButton(button)
                subPanel.addWidget(button)
            row += 1

        addMotor("sx", range=(0, 1000))
        addMotor("sy", range=(-1000, 1000))
        addMotor("sz", range=(-1000, 1000))
        addRotation("srot")
        addSelect("srot_dir", 1, cw=1, ccw=-1)
        layout.addItem(qt.QSpacerItem(0, 10), row, 0)
        row += 1
        addMotor("sampu", range=(-1000, 1000))
        addMotor("sampv", range=(-1000, 1000))
        layout.addItem(qt.QSpacerItem(0, 10), row, 0)
        row += 1
        addMotor("detector_y")
        addMotor("detector_magnification", range=(0, 10))
        addMotor("detector_distance", range=(-1, 1000.0))
        layout.addItem(qt.QSpacerItem(0, 10), row, 0)
        row += 1
        addPush("is_shutter_open")
        layout.addItem(qt.QSpacerItem(0, 10), row, 0)
        addMotor("sx0", range=(-100, 100))
        addMotor("sy0", range=(-1, 1))
        addMotor("sz0", range=(-1, 1))
        layout.addItem(qt.QSpacerItem(0, 10), row, 0)
        addMotor("sy_angle", range=(-1.0, 1.0))
        addMotor("sz_angle", range=(-1.0, 1.0))
        layout.addItem(qt.QSpacerItem(0, 10, vPolicy=qt.QSizePolicy.Expanding), row, 0)
        return panel

    def refresh(self):
        self.__updateCamera()

    def __updateCamera(self):
        ss = self._sampleStage
        image = ss.compute_image()
        self.__camera.setImage(image)
        if self._sampleStage.mode == "xray":
            xrot = image.shape[1] / 2 + ss.sy - ss.detector_y
            xx = (xrot, xrot)
            yy = (0, image.shape[0])
            self.__camera.addCurve(x=xx, y=yy, legend="rot", color="red")

    def createCamera(self):
        camera = ImageView(self)
        camera.setKeepDataAspectRatio(True)
        camera.setYAxisInverted(True)
        camera.setColormap("viridis")
        self.__camera = camera
        return camera


def main():
    app = qt.QApplication([])

    parser = argparse.ArgumentParser()
    parser.add_argument("model", help="Define the model to use")
    parser.add_argument("--scene", default=None, help="Define the scene to use")
    parser.add_argument("--dark", default=None, help="Define the dark to use")
    parser.add_argument("--beam", default=None, help="Define the beam shape to use")
    parser.add_argument(
        "--mode", default="xray", help="Rendering mode. One of xray/visible"
    )
    args = parser.parse_args()

    window = SampleStageWindow()

    mode = args.mode
    sample_stage: (
        visible_sample_stage.VisibleSampleStage
        | simulated_sample_stage.SimulatedSampleStage
    )
    if mode == "visible":
        sample_stage = visible_sample_stage.VisibleSampleStage()
        sample_stage.qtParent = window
    elif mode == "xray":
        sample_stage = simulated_sample_stage.SimulatedSampleStage()
        sample_stage.detector_size = 2048, 2048
    else:
        assert False, f"Mode '{mode}' unsupported"

    extra_files = {}
    if args.scene is not None:
        extra_files["scene_filename"] = args.scene
    if args.dark is not None:
        extra_files["dark_filename"] = args.dark
    if args.beam is not None:
        extra_files["beam_filename"] = args.beam
    sample_stage.load_filenames(args.model, **extra_files)

    window.setSampleStage(sample_stage)
    window.setVisible(True)

    qt.QTimer.singleShot(500, window.refresh)

    app.exec_()


if __name__ == "__main__":
    main()
