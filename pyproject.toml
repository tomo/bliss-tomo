[build-system]
requires = ["setuptools", "wheel"]
build-backend = "setuptools.build_meta"

[project]
name = "bliss-tomo"
version = "2.7.0"
description = "Fast Tomography"
authors = [
    { name="Clemence Muzelle", email="muzelle@esrf.fr" },
    { name="Valentin Valls", email="valentin.valls@esrf.fr" },
    { name="BLISS team", email="bliss@esrf.fr" }
]
license = {text = "MIT"}
requires-python = ">=3.8"
readme = "README.md"

dependencies = [
    # "bliss",
    "pint",
    "importlib-metadata >=4",
    # for daiquiri integration
    "mmh3",
]

[project.optional-dependencies]
align = [
    "nabu",
    "scikit-image",
]
dev = [
    "black",
    "pytest",
]

[tool.setuptools.packages]
find = {}

[project.urls]
"Homepage" = "https://gitlab.esrf.fr/tomo/bliss-tomo"
"Bug Tracker" = "https://gitlab.esrf.fr/tomo/bliss-tomo/issues"

[project.scripts]
NxTomoLimaCCDs = "tomo.tango.servers.nxtomo_lima_ds:main"
VoxelTomoLimaCCDs = "tomo.tango.servers.voxel_tomo_lima_ds:main"
VisibleTomoLimaCCDs = "tomo.tango.servers.visible_tomo_lima_ds:main"

[project.entry-points."bliss_rest"]
tomo = "tomo.rest_service.tomo_rest_plugin:TomoRestPlugin"

[tool.flake8]
# Check that this is aligned with your other tools like Black
max-line-length = 120
exclude = [
    # No need to traverse our git directory
    ".git",
    # There's no value in checking cache directories
    "__pycache__"
]
# Use extend-ignore to add to already ignored checks which are anti-patterns like W503.
extend-ignore = [
    # PEP 8 recommends to treat : in slices as a binary operator with the lowest priority, and to leave an equal
    # amount of space on either side, except if a parameter is omitted (e.g. ham[1 + 1 :]).
    # This behaviour may raise E203 whitespace before ':' warnings in style guide enforcement tools like Flake8.
    # Since E203 is not PEP 8 compliant, we tell Flake8 to ignore this warning.
    # https://black.readthedocs.io/en/stable/the_black_code_style/current_style.html#slices
    "E203"
]
