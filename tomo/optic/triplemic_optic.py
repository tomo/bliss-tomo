# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent
from bliss.shell.cli.user_dialog import UserChoice, Container
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.shell.standard import umv
from bliss.common import event
from bliss.common.axis import AxisState
from bliss.common.logtools import log_critical

from tomo.optic.base_optic import BaseOptic, OpticState


class TripleMicOptic(BaseOptic):
    """
    Class to handle triple optics of type TripleMic.
    The class has three parts:
    The standart methods implemented for every optic,
    methods to handle the three objectives and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:

    name : str
        The Bliss object name
    config : dictionary
        The Bliss configuration dictionary
    magnifications : list of float
        A list with the possible magnification lenses for this objective type
    magnification : float
        magnification value corresponding to current objective used
    selection_motor : Bliss Axis object
        The motor used to switch the objectives
    selection_positions : list of float
        The three predifined position for the three objectives to be in place
    selection_precisionss : list of float
        The precisions to be used to verify whether an objective is in place.
        One value for every selection position
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective

    focus_motors : list of Bliss Axis objects
        The focus motors for the three ojectives
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor

    **Example yml file**::

        - name: triplemic_optic
          plugin: bliss
          class: TripleMicOptic
          package: tomo.optic.triplemic_optic

          # The magnifications given are used in the order [obj1, obj2, obj3]
          # Possible lens magnification values are [2, 5, 7.5, 10, 20]
          selection_motor: $triplemic_sel
          image_flipping_hor: True
          image_flipping_vert: False

          rotc_motor: $triplemic_rot
          focus_type: "translation"     # translation or rotation
          focus_scan_range: 0.2
          focus_scan_steps: 20
          focus_lim_pos: 0.5
          focus_lim_neg: -0.5

          objectives:
          - position: 0
            precision: 0.1
            magnification: 5
            focus_motor: $triplemic_focus1
          - position: 15
            precision: 0.5
            magnification: 7.5
            focus_motor: $triplemic_focus2
          - position: 30
            precision: 0.5
            magnification: 10
            focus_motor: $triplemic_focus3"""

    def __init__(self, name, config):
        self.__name = name
        self.__config = config

        self._triplemic_magnifications = [
            i["magnification"] for i in config["objectives"]
        ]
        self._selection_motor = config["selection_motor"]
        self._selection_positions = [i["position"] for i in config["objectives"]]
        self._selection_precisions = [i["precision"] for i in config["objectives"]]
        self._focus_motors = [i["focus_motor"] for i in config["objectives"]]
        self._scintillators = [i.get("scintillator", "") for i in config["objectives"]]

        param_name = self.__name + ":parameters"
        param_defaults = {}

        # Initialise the TomoOptic class
        super().__init__(name, config, param_name, param_defaults)

        event.connect(self._selection_motor, "state", self.__motor_state_updated)
        # __motor_state_updated is not triggered at connection
        self.__motor_state_updated(self._selection_motor.state)
        self._update_available_magnifications(self._triplemic_magnifications)

    def list_motors(self):
        return [self.focus_motor, self.rotc_motor, self._selection_motor]

    @property
    def description(self):
        """
        The name string the current optics
        """
        name = "TripleMic_" + str(self.magnification)
        return name

    #
    # standart otics methods every otics has to implement
    #

    def __motor_state_updated(self, state: AxisState, *args, **kwargs):
        if state.MOVING:
            new_state = OpticState.MOVING
            new_magnification = None
        elif state.READY:
            try:
                objective = self.objective
            except ValueError:
                new_state = OpticState.INVALID
                new_magnification = None
            else:
                new_state = OpticState.READY
                new_magnification = self._triplemic_magnifications[objective - 1]
            self._update_target_magnification(None)
        else:
            new_state = OpticState.UNKNOWN
            new_magnification = None
        self._update_magnification(new_magnification)
        self._update_state(new_state)

    def calculate_magnification(self):
        """
        Returns magnification value according to current objective used
        """
        try:
            objective = self.objective
        except ValueError:
            return None

        return self._triplemic_magnifications[objective - 1]

    def _tune_magnification(self, user_magnification):
        """
        Sets the magnification of the current objective used
        """
        objective = self.objective
        self._triplemic_magnifications[objective - 1] = user_magnification
        self._update_magnification(user_magnification)
        self._update_available_magnifications(self._triplemic_magnifications)

    #
    # Specific objective handling
    #

    def move_magnification(self, magnification, wait=True):
        """Move the device to automatically mount a specific available magnification

        Attributes:
            magnification: The requested magnification
            wait: If `True` the function returns when the new
                  state is reached. Else the request is send to controller
                  and returns before the end.

        Raises:
            ValueError: If the magnification is not reachable
        """
        objective = self._get_objective_from_magnification(magnification)
        self._update_target_magnification(magnification)
        self._move_objective(objective, wait=wait)

    def optic_setup(self, submenu=False):
        """
        Set-up the objective to be used by chosing the objective's magnification
        """

        value_list = []
        # for i in self._triplemic_magnifications:

        # get the actual magnification value as default
        default1 = 0
        for i in range(0, len(self._triplemic_magnifications)):
            value_list.append((i, f"{i+1}: X" + str(self._triplemic_magnifications[i])))
            if str(self.magnification) == value_list[i][1][1:]:
                default1 = i

        dlg1 = UserChoice(values=value_list, defval=default1)
        ct1 = Container([dlg1], title="Objective")
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[ct1]], title="TripleMic Setup", cancel_text=cancel_text
        ).show()

        # returns False on cancel
        if ret != False:
            # get the objective chosen
            sel_objective = ret[dlg1] + 1
            # move to objective
            if self.objective != sel_objective:
                self.objective = sel_objective

            new_magnification = self.calculate_magnification()
            self._update_magnification(new_magnification)

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        try:
            ojective = self.objective
            magnification = self.magnification
            print(
                "Objective %d selected with a magnification of X%s"
                % (ojective, str(magnification))
            )

        except ValueError as err:
            print("Optics indicates a problem:\n", err)

    @property
    def objective(self):
        """
        Reads and sets the current objective (1, 2 or 3)
        """
        position = self._selection_motor.position
        return self._get_objective_from_position(position)

    @objective.setter
    def objective(self, value):
        """
        Moves to the objective 1, 2 or 3
        """
        self._move_objective(value, wait=True)

    def _move_objective(self, value, wait):
        if value < 1 or value > len(self._selection_positions):
            raise ValueError("Only the objectives 1, 2 and 3 can be chosen!")

        current = self.objective
        if current == value:
            return

        target_position = self._selection_positions[value - 1]
        if wait:
            self._execute_pre_move_hooks()
            umv(self._selection_motor, target_position)
            new_magnification = self._triplemic_magnifications[value - 1]
            self._update_magnification(new_magnification)
            self._execute_post_move_hooks()
        else:

            def run():
                try:
                    self._execute_pre_move_hooks()
                    self._selection_motor.move(target_position, wait=False)
                    self._execute_post_move_hooks()
                except Exception:
                    log_critical(self, "Error while moving objective", exc_info=True)
                    raise

            self._task = gevent.spawn(run)

    def _get_objective_from_position(self, position):
        for obj_id, (obj_position, obj_precision) in enumerate(
            zip(self._selection_positions, self._selection_precisions)
        ):
            if obj_position - obj_precision <= position <= obj_position + obj_precision:
                return obj_id + 1
        raise ValueError("No objective found for position %s", position)

    def _get_objective_from_magnification(self, magnification):
        epsilon = 0.00001
        for obj_id, obj_magnification in enumerate(self._triplemic_magnifications):
            if (
                obj_magnification - epsilon
                <= magnification
                <= obj_magnification + epsilon
            ):
                return obj_id + 1
        raise ValueError("No objective found for magnification %s", magnification)

    # @property
    # def scintillator(self):
    # """
    # Returns the Bliss Axis object of the focus motor to be used for the current objective
    # """
    # return self._scintillators[(self.objective - 1)]

    #
    # Focus related methods
    #
    @property
    def focus_motor(self):
        """
        Returns the Bliss Axis object of the focus motor to be used for the current objective
        """

        return self._focus_motors[(self.objective - 1)]

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """
        scan_params = super().focus_scan_parameters()
        # the focus scan range is dependent on the magnification
        if self.magnification == 2:
            scan_params["focus_scan_range"] = 0.2
        else:
            scan_params["focus_scan_range"] = 0.025

        return super().focus_scan_parameters()
