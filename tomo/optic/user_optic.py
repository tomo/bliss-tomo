# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss import setup_globals
from bliss import global_map
from bliss.shell.cli.user_dialog import UserInput
from bliss.shell.cli.user_dialog import UserFloatInput
from bliss.shell.cli.user_dialog import UserCheckBox
from bliss.shell.cli.user_dialog import UserIntInput
from bliss.shell.cli.pt_widgets import BlissDialog

from tomo.optic.base_optic import BaseOptic, OpticState


class UserOptic(BaseOptic):
    """
    Class to handle standard optics with one objective.
    The class has two parts:
    The standart methods implemented for every optic and
    methods to extract the used focus motor and the necessary focus scan parameters.


    **Parameters**:

    name : string
        Name of the optics as a free string
    magnification : float
        Magnification of the optics
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective

    focus_motor : string
        Name of the fucus motor to use. Musst be defined in the current session
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.

    **Example yml file**::

        name:  useroptic
        plugin: bliss
        class: UserOptics
        package: tomo.optic_user"""

    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        """

        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values

        param_name = f"optic:{name}"
        param_defaults = {}
        param_defaults["name"] = "Manual"
        param_defaults["magnification"] = 1
        param_defaults["image_flipping_hor"] = False
        param_defaults["image_flipping_vert"] = False
        param_defaults["rotc_motor"] = None
        param_defaults["focus_motor"] = None
        param_defaults["focus_type"] = "rotation"
        param_defaults["focus_scan_range"] = 20
        param_defaults["focus_scan_steps"] = 20
        # param_defaults["focus_lim_pos"] = 1000
        # param_defaults["focus_lim_neg"] = -1000

        # Initialise the TomoOptic class
        super().__init__(name, config, param_name, param_defaults)

        self._update_magnification(self.parameters.magnification)
        self._update_state(OpticState.READY)

    @property
    def description(self):
        """
        The name string the current optics
        """
        return self.parameters.name + "_X" + str(self.parameters.magnification)

    #
    # standart otics methods every otics has to implement
    #
    def list_motors(self):
        return []

    def _tune_magnification(self, user_magnification: float):
        """
        Sets the magnification of the current objective used
        """
        self._update_magnification(user_magnification)
        self.parameters.magnification = user_magnification

    @property
    def image_flipping(self):
        """
        Returns the implied horizontal and vertical image flipping as a list
        """
        return [self.parameters.image_flipping_hor, self.parameters.image_flipping_vert]

    def optic_setup(self, submenu=False):
        """
        Set-up the optic with values enterd by the user
        """
        focus_setup = False
        motors = list(global_map.get_axes_iter())
        motor_names = []
        for m in motors:
            motor_names.append(m.name)
        # print(motor_names)

        dlg_name = UserInput(label="Optic name?", defval=self.parameters.name)
        dlg_mag = UserFloatInput(
            label="Magnification?", defval=self.parameters.magnification
        )
        # dlg_flip_hor = UserCheckBox(
        #    label="Horizontal image flipping?",
        #    defval=self.parameters.image_flipping_hor,
        # )
        # dlg_flip_vert = UserCheckBox(
        #    label="Vertical image flipping?", defval=self.parameters.image_flipping_vert
        # )
        dlg_rotc = UserInput(
            label="Rotc motor name?",
            defval=self.parameters.rotc_motor,
            completer=motor_names,
        )
        dlg_foc = UserCheckBox(label="Focusing setup?", defval=False)
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [
                [dlg_name],
                [dlg_mag],
                # [dlg_flip_hor],
                # [dlg_flip_vert],
                [dlg_rotc],
                [dlg_foc],
            ],
            title="User Optic Setup",
            cancel_text=cancel_text,
        ).show()

        # returns False on cancel
        if ret:
            # get main parameters
            self.parameters.name = ret[dlg_name]
            self.parameters.magnification = float(ret[dlg_mag])
            self._update_magnification(self.parameters.magnification)
            # self.parameters.image_flipping_hor = ret[dlg_flip_hor]
            # self.parameters.image_flipping_vert = ret[dlg_flip_vert]
            self.parameters.rotc_motor = ret[dlg_rotc]
            focus_setup = ret[dlg_foc]

        if focus_setup:
            # get focus parameters when requested
            focus_types = ["translation", "rotation"]

            dlg_name = UserInput(
                label="Focus motor name?",
                defval=self.parameters.focus_motor,
                completer=motor_names,
            )
            dlg_type = UserInput(
                label="Focus type?",
                defval=self.parameters.focus_type,
                completer=focus_types,
            )
            dlg_range = UserFloatInput(
                label="Focus scan range?", defval=self.parameters.focus_scan_range
            )
            dlg_steps = UserIntInput(
                label="Focus scan steps?", defval=self.parameters.focus_scan_steps
            )
            cancel_text = "Back" if submenu else "Cancel"
            ret = BlissDialog(
                [[dlg_name], [dlg_type], [dlg_range], [dlg_steps]],
                title="User Optic Setup",
                cancel_text=cancel_text,
            ).show()

            # returns False on cancel
            if ret != False:
                self.parameters.focus_motor = ret[dlg_name]
                self.parameters.focus_type = ret[dlg_type]
                self.parameters.focus_scan_range = float(ret[dlg_range])
                self.parameters.focus_scan_steps = int(ret[dlg_steps])

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        print(
            "User defined objective [%s] : magnification = X%s"
            % (self.parameters.name, str(self.parameters.magnification))
        )

    #
    # rotc related methods
    #
    @property
    def rotc_motor(self):
        """
        Returns the Bliss Axis object of the rotc motor to be used for the current objective
        """
        rotc_axis = None
        if self.parameters.rotc_motor != None:
            rotc_axis = getattr(setup_globals, self.parameters.rotc_motor, None)
            if rotc_axis == None:
                error_msg = (
                    "The rotc motor "
                    + self.parameters.rotc_motor
                    + " is not defined in the current Bliss session"
                )
                raise ValueError(error_msg)

        return rotc_axis

    #
    # Focus related methods
    #
    @property
    def focus_motor(self):
        """
        Returns the Bliss Axis object of the focus motor to be used for the current objective
        """
        focus_axis = None
        if self.parameters.focus_motor != None:
            focus_axis = getattr(setup_globals, self.parameters.focus_motor, None)
            if focus_axis == None:
                error_msg = (
                    "The focus motor "
                    + self.parameters.focus_motor
                    + " is not defined in the current Bliss session"
                )
                raise ValueError(error_msg)

        return focus_axis

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """
        scan_params = {}

        scan_params["focus_type"] = self.parameters.focus_type
        scan_params["focus_scan_range"] = self.parameters.focus_scan_range
        scan_params["focus_scan_steps"] = self.parameters.focus_scan_steps
        # scan_params["focus_lim_pos"] = self.parameters.focus_lim_pos
        # scan_params["focus_lim_neg"] = self.parameters.focus_lim_neg

        return scan_params
