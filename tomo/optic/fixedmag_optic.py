# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.shell.cli.user_dialog import UserMsg, UserInput
from bliss.shell.cli.user_dialog import Container
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss import setup_globals

from tomo.optic.base_optic import BaseOptic, OpticState


class FixedmagOptic(BaseOptic):
    """
    Class to handle standard optics with one objective.
    The class has two parts:
    The standard methods implemented for every optic and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:

    name : str
        The Bliss object name
    config : dictionary
        The Bliss configuration dictionary
    magnification : float
        Optic magnification
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective

    rotc_motor : Bliss Axis object of camera rotation motor
        The camera rotation motor for the optics
    focus_motor : Bliss Axis object of focus motor
        The focus motor for the optics
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_range : float
        The range of the focus scan
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.

    **Example yml file**::

        - name:  fixedoptic
          plugin: bliss
          class: FixedOptic
          package: tomo.fixedmag_optic

          magnification: 6.5

          image_flipping_hor:  False
          image_flipping_vert: False

          rotc_motor:  $hrrotc
          focus_motor: $hrfocus
          focus_type: "rotation"     # translation or rotation
          focus_scan_range: 20
          focus_scan_steps: 10
    """

    def __init__(self, name, config):
        param_name = f"optic:{name}"
        param_defaults = {}
        self.objective = None
        super().__init__(name, config, param_name, param_defaults)
        magnification = config["magnification"]
        self._update_magnification(magnification)
        self._update_state(OpticState.READY)

    @property
    def description(self):
        """
        The name string the current optics
        """
        name = "Fixed_X" + str(self.magnification)
        return name

    #
    # fixed otics methods every otics has to implement
    #
    def list_motors(self):
        if self.z_motor is not None:
            return [self.focus_motor, self.rotc_motor, self.z_motor]
        else:
            return [self.focus_motor, self.rotc_motor]

    def _tune_magnification(self, value):
        """
        Sets the magnification of the current objective used
        """
        self._update_magnification(value)

    #
    # Specific objective handling
    #

    def optic_setup(self, submenu=False):
        """
        Set-up the optic by choosing from the list of available fixed optics
        """

        msg = f"Fixed optic with a magnification of X{self.magnification}"
        dlg1 = UserMsg(label=msg)

        rotc_dlg = UserInput(label="Rotc motor: ", defval=self.rotc_motor.name)
        focus_dlg = UserInput(label="Focus motor: ", defval=self.focus_motor.name)
        ct1 = Container([dlg1], title="Optic")
        ct2 = Container([rotc_dlg, focus_dlg], title="Optic motors")
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[ct1], [ct2]], title="Fixed Optic Setup", cancel_text=cancel_text
        ).show()

        if ret:
            try:
                self.set_rotc_motor(setup_globals.config.get(ret[rotc_dlg]))
            except Exception:
                raise ValueError(f"{ret[rotc_dlg]} motor does not exist")
            try:
                self.set_focus_motor(setup_globals.config.get(ret[focus_dlg]))
            except Exception:
                raise ValueError(f"{ret[focus_dlg]} motor does not exist")

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        print("Fixed objective : magnification = X%.4f" % (self.magnification))
