# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
import enum
import typing
from tomo.scintillators.utils import create_scintillator_geometry
from tomo.scintillators.base_scintillator import BaseScintillator
from bliss import global_map
from bliss.common.logtools import log_info, log_error
from bliss.shell.cli.user_dialog import UserMsg, UserInput
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.beacon_object import BeaconObject, EnumProperty
from bliss.common.protocols import HasMetadataForScan
from bliss.common.hook import MotionHook

from tomo.parameters import TomoParameters


class OpticState(enum.Enum):
    """State of a device optic"""

    READY = "READY"
    """The device provides a valid magnification"""

    INVALID = "INVALID"
    """The device state is wrong and can't provide a valid magnification"""

    MOVING = "MOVING"
    """The device is moving and can't provide a valid magnification for now"""

    UNKNOWN = "UNKNOWN"
    """The state of the device is unknown"""


class BaseOptic(BeaconObject, TomoParameters, HasMetadataForScan):
    """
    Base class for all detector optic objects.
    The class implements all the standard methods to be implemented for every optic.

    **Attributes**:

    scintillator : string
        Name of the scintillator associated with the optic
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
        The right is at left.
    image_flipping_vert : boolean
        Implied vertical image flipping by the objective
        The top is at bottom.

    rotc_motor : Bliss Axis object
        The camera rotation motor for the ojective
    focus_motor : Bliss Axis object
        The focus motor for the ojective
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_range : float
        The scan range for the focus scan
    focus_scan_steps : int
        The number of scan steps for the focus scan.
    focus_steps_per_unit : int
        Steps per unit of the focus motor. For focus motors using the same motor controller channel as other optics
    """

    def __init__(self, name, config, param_name=None, param_defaults=None):
        BeaconObject.__init__(self, config)

        # init logging
        self.log_name = name + ".optic"
        global_map.register(self, tag=self.log_name)
        log_info(self, "__init__() entering")

        param_defaults["scintillator"] = "None"

        # Initialise the TomoParameters class
        TomoParameters.__init__(self, param_name, param_defaults)

        self.motion_hooks: list[MotionHook] = []

        try:
            self.__scintillators = config["scintillators"]
        except Exception:
            self.__scintillators = None

        try:
            self.__image_flipping_hor = config["image_flipping_hor"]
        except Exception:
            self.__image_flipping_hor = False
        try:
            self.__image_flipping_vert = config["image_flipping_vert"]
        except Exception:
            self.__image_flipping_vert = False

        try:
            self.__rotc_mot = config["rotc_motor"]
        except Exception:
            self.__rotc_mot = None

        try:
            self.__focus_mot = config["focus_motor"]
        except Exception:
            self.__focus_mot = None

        try:
            self.__z_mot = config["z_motor"]
        except Exception:
            self.__z_mot = None

        try:
            self.__focus_type = config["focus_type"]
        except Exception:
            self.__focus_type = "unknown"
        try:
            self.__focus_scan_range = config["focus_scan_range"]
        except Exception:
            self.__focus_scan_range = 0
        try:
            self.__focus_scan_steps = config["focus_scan_steps"]
        except Exception:
            self.__focus_scan_steps = 0
        # try:
        #    self.__focus_lim_pos = config["focus_lim_pos"]
        # except:
        #    self.__focus_lim_pos = 0
        # try:
        #    self.__focus_lim_neg = config["focus_lim_neg"]
        # except:
        #    self.__focus_lim_neg = 0
        try:
            self.__scintillator = config["scintillator"]
        except Exception:
            self.__scintillator = None
        try:
            self.__focus_steps_per_unit = config["focus_steps_per_unit"]
        except Exception:
            self.__focus_steps_per_unit = 0

        log_info(self, "__init__() leaving")

    name = BeaconObject.config_getter("name")

    _state = EnumProperty(
        "state",
        default=OpticState.UNKNOWN,
        unknown_value=OpticState.UNKNOWN,
        enum_type=OpticState,
    )

    _magnification = BeaconObject.property_setting("magnification")

    _available_magnifications = BeaconObject.property_setting(
        "available_magnifications"
    )

    __magnification_range = BeaconObject.property_setting("magnification_range")

    _target_magnification = BeaconObject.property_setting("target_magnification")

    @property
    def state(self):
        """Returns the actual state of the device"""
        return self._state

    @state.setter
    def state(self, state):
        """Set the actual state of the device"""
        raise RuntimeError("State is read only")

    def _update_state(self, state):
        """Called by the optic implementation to update the actual state"""
        if self._state == state:
            return
        self._state = state

    @property
    def magnification(self):
        """Returns the actual magnification of the device"""
        return self._magnification

    @magnification.setter
    def magnification(self, magnification):
        """Called by the user to tune the actual magnification"""
        self._tune_magnification(magnification)

    def _tune_magnification(self, user_magnification: float):
        """Tune the actual magnification.

        Have to me implemented by the the optic device to react when user
        set a value to the magnification property.
        """
        raise NotImplementedError(
            "User tuning of magnification for %s optic type is not implemented (name %s)",
            type(self),
            self.name,
        )

    def _update_magnification(self, magnification):
        """Called by the optic implementation to update the actual magnification"""
        self._magnification = magnification

    def move_magnification(self, magnification: float, wait: bool = True):
        """Move the device to automatically mount a specific available magnification.

        Have to be implemented if the optic device supports many magnifications.

        Arguments:
            magnification: The requested magnification
            wait: If `True` the function returns when the new
                  state is reached. Else the request is send to controller
                  and returns before the end.

        Raises:
            ValueError: If the magnification is not reachable
        """
        raise NotImplementedError(
            "Move command for %s optic type is not implemented (name %s)",
            type(self),
            self.name,
        )

    def wait_move(self):
        """Block until the actual move request is not finished"""
        raise NotImplementedError(
            "Wait move command for %s optic type is not implemented (name %s)",
            type(self),
            self.name,
        )

    @property
    def target_magnification(self) -> typing.Optional[float]:
        """Returns the target of the actual move request, else None"""
        return self._target_magnification

    def _update_target_magnification(self, target_magnification):
        """Called by the optic implementation to tell where the magnification
        is about to move

        It have to be called when the user requested is known. Before the move
        with the target value, and after the move with None.
        """
        self._target_magnification = target_magnification

    def _execute_pre_move_hooks(self):
        for h in self.motion_hooks:
            h.pre_move([self])

    def _execute_post_move_hooks(self):
        for h in self.motion_hooks:
            h.post_move([self])

    @property
    def available_magnifications(self):
        """Returns the actual list of available magnifications, if some"""
        return self._available_magnifications

    @available_magnifications.setter
    def available_magnifications(self, magnifications):
        raise RuntimeError("Available magnifications list is read only")

    def _update_available_magnifications(self, magnifications):
        """Called by the optic implementation to update the available list of available magnifications"""
        self._available_magnifications = magnifications

    @property
    def magnification_range(self):
        """Returns the valid magnification_range, if one"""
        return self.__magnification_range

    def _update_magnification_range(self, magnification_range):
        """Called by the optic implementation to update the magnification range"""
        self.__magnification_range = magnification_range

    def scintillator_geometry(self) -> BaseScintillator | None:
        """Returns the actual scintillator geometry.

        By default, the configuration from the key `scintillator_geometry`
        is returned, else None.

        This can be overloaded by specific optics.
        """
        config = self.config.get("scintillator_geometry", None)
        if config is None:
            return None

        try:
            return create_scintillator_geometry(config)
        except Exception:
            log_error("Error while reading the scintillator geometry", exc_info=True)
            return None

    def scintillator_setup(self, submenu=False):
        """
        User dialog which allows to specify the associated scintillator.

        A scintillators list in the yml configuration file can offer a list of possible scintillators.

        It is always possible to specify another scintillator.
        """

        dlg1 = UserMsg(label="Scintillators: {}".format(self.scintillators))
        if self.scintillator == "None" and len(self.scintillators) == 1:
            self.scintillator = self.scintillators[0]
        dlg2 = UserInput(
            label="Used scintillator",
            defval=self.scintillator,
            completer=self.scintillators,
        )
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[dlg1], [dlg2]], title="Scintillator Setup", cancel_text=cancel_text
        ).show()
        if ret:
            # get the scintillator chosen
            self.scintillator = ret[dlg2]

    @property
    def description(self):
        """
        The description string of the current optic.

        Must be implemented for every optic.
        """
        pass

    @property
    def type(self):
        """
        The class name of the optic.
        """
        return self.__class__.__name__

    @property
    def scintillators(self):
        """
        Returns the list of configured scintillators associated with the optic
        """
        return self.__scintillators

    @property
    def scintillator(self):
        """
        Returns the name of the scintillator associated with the optic
        """
        self.__scintillator = self.parameters.scintillator
        return self.__scintillator

    @scintillator.setter
    def scintillator(self, value):
        """
        Sets the name of the scintillator associated with the optic
        """
        if value == "":
            value = "None"

        self.__scintillator = value
        self.parameters.scintillator = value

    @property
    def focus_steps_per_unit(self):
        """
        Returns the steps per unit value of the focus motor
        """
        return self.__focus_steps_per_unit

    @focus_steps_per_unit.setter
    def focus_steps_per_unit(self, value):
        """
        Sets the steps per unit value of the focus motor
        """
        self.__focus_steps_per_unit = value

    @property
    def image_flipping(self):
        """
        Returns the implied horizontal and vertical image flipping as a list
        """
        return [self.__image_flipping_hor, self.__image_flipping_vert]

    def optic_setup(self, submenu=False):
        """
        User dialog which allows to configure the optic.
        Implemented in the specific optic class
        """
        pass

    def setup(self, submenu=False):
        """
        Set-up the optic.

        Must be implemented for every optic
        """
        self.optic_setup()
        self.scintillator_setup(submenu=submenu)

    def status(self):
        """
        Prints the current objective in use and its magnification.

        If an optics cannot be determined, the reason gets printed.

        Must be implemented for every optic.
        """
        pass

    def set_rotc_motor(self, motor):
        """
        Sets the current rotc motor
        """
        self.__rotc_mot = motor

    @property
    def rotc_motor(self):
        """
        Returns the Bliss Axis object of the rotation motor to be used for the current objective
        """
        return self.__rotc_mot

    def set_focus_motor(self, motor):
        """
        Sets the current focus motor
        """
        self.__focus_mot = motor

    @property
    def focus_motor(self):
        """
        Returns the Bliss Axis object of the focus motor to be used for the current optic
        """
        return self.__focus_mot

    def set_z_motor(self, motor):
        """
        Sets the current z motor
        """
        self.__z_mot = motor

    @property
    def z_motor(self):
        """
        Returns the Bliss Axis object of the z motor to be used for the current optic
        """
        return self.__z_mot

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """
        # if self.__focus_mot == None:
        #    raise ValueError("No focus motor defined for the optic!")

        scan_params = {}
        scan_params["focus_type"] = self.__focus_type
        scan_params["focus_scan_range"] = self.__focus_scan_range
        scan_params["focus_scan_steps"] = self.__focus_scan_steps
        # scan_params["focus_lim_pos"] = self.__focus_lim_pos
        # scan_params["focus_lim_neg"] = self.__focus_lim_neg

        return scan_params

    def focus_config_apply(self):
        """
        Update focus motor steps per unit value on the motor controller
        """
        self.__focus_mot.steps_per_unit = self.__focus_steps_per_unit

    def settings_to_config(self, rotc_motor=True, focus_motor=True):
        if rotc_motor:
            self._config["rotc_motor"] = self.rotc_motor
        if focus_motor:
            self._config["focus_motor"] = self.focus_motor
        self._config.save()

    def list_motors(self):
        raise NotImplementedError

    def __info__(self):
        info_str = f"{self.name} optic info:\n"
        info_str += f"  description   = {self.description}\n"
        if getattr(self, "objective", None) is not None:
            info_str += f"  objective     = {self.objective} \n"
        info_str += f"  magnification = {self.magnification} \n"
        info_str += f"  flipping      = {self.image_flipping} \n"
        info_str += f"  scintillator  = {self.scintillator} \n"
        if self.rotc_motor is None:
            info_str += "  rotc motor    = None \n"
        else:
            info_str += f"  rotc motor    = {self.rotc_motor.name} \n"
        if self.focus_motor is None:
            info_str += "  focus motor   = None \n"
        else:
            info_str += f"  focus motor   = {self.focus_motor.name} \n"
        if self.focus_motor is not None:
            if self.focus_steps_per_unit == 0:
                info_str += (
                    f"  focus steps per unit = {self.focus_motor.steps_per_unit}\n"
                )
            else:
                info_str += f"  focus motor  steps per unit = {self.focus_motor.steps_per_unit}\n"
                info_str += (
                    f"  focus config steps per unit = {self.__focus_steps_per_unit} \n"
                )
        if self.z_motor is None:
            info_str += "  z motor   = None \n"
        else:
            info_str += f"  z motor   = {self.z_motor.name} \n"

        info_str += f"  focus scan    = {self.focus_scan_parameters()} \n"

        return info_str

    def scan_metadata(self):
        from tomo.globals import USE_METADATA_FOR_SCAN

        if not USE_METADATA_FOR_SCAN:
            return None

        meta = {
            "magnification": self.magnification,
        }
        return meta
