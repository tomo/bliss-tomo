# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


from bliss.shell.cli.user_dialog import Container
from bliss.shell.cli.pt_widgets import BlissDialog
from tomo.optic.base_optic import BaseOptic, OpticState
from bliss.shell.cli.user_dialog import UserChoice


class RevolvedHasselbladOptic(BaseOptic):
    """
    Class to handle two lenses Hasselblad optics.
    The class has two parts:
    The standart methods implemented for every optic and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:

    name : str
        The Bliss object name
    config : dictionary
        The Bliss configuration dictionary
    magnifications : list of float
        A list with the possible magnification lenses that can be used as top or bottom lenses
    magnification : float
        magnification value corresponding to current objective used
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective

    focus_motors : list of Bliss Axis objects
        The focus motors for the optics
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_range : float
        The range of the focus scan
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor

    **Parameters**:

    top_magnification : float
        The magnification of the top lens. To be configured in the setup.
    bottom_magnification : float
        The magnification of the bottom lens. To be configured in the setup.

    **Example yml file**::

        - name:  mrhasselblad
          plugin: bliss
          class: HasselbladOptic
          package: tomo.beamline.ID19

          magnifications: [24, 100, 210, 300]

          image_flipping_hor:  False
          image_flipping_vert: False

          rotc_motor:  $rotc1p29A
          focus_motor: $focus0p45A
          focus_type: "translation"     # translation or rotation
          focus_scan_range: 20
          focus_scan_steps: 20
          focus_lim_pos:  0.5
          focus_lim_neg: -0.5"""

    def __init__(self, name, config):
        self.__name = name
        self.__config = config

        self._hasselbad_magnifications = config["magnifications"]

        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values

        param_name = self.__name + ":parameters"
        param_defaults = {}
        param_defaults["top_magnification"] = self._hasselbad_magnifications[0]
        param_defaults["bottom_magnification"] = self._hasselbad_magnifications[0]

        # Initialise the BaseOptic class
        super().__init__(name, config, param_name, param_defaults)

        self._hasselbad_magnification = self.calculate_magnification()

        self._update_available_magnifications(self._hasselbad_magnifications)
        self._update_magnification(self._hasselbad_magnification)
        self._update_state(OpticState.READY)

    @property
    def description(self):
        """
        The name string the current optics
        """
        name = (
            "Revolved_Hasselblad_"
            + str(self.parameters.top_magnification)
            + "_"
            + str(self.parameters.bottom_magnification)
        )
        return name

    def list_motors(self):
        return []

    def calculate_magnification(self):
        """
        Returns magnification value according to top and bottom lenses used
        """
        return (
            1.0
            * self.parameters.top_magnification
            / self.parameters.bottom_magnification
        )

    def _tune_magnification(self, user_magnification: float):
        """
        Sets the magnification of the current objective used
        """
        self._update_magnification(user_magnification)

    @property
    def magnification(self):
        """Returns the actual magnification of the device"""
        return self._magnification

    @magnification.setter
    def magnification(self, magnification):
        """
        Sets the magnification of the current objective used
        """
        if isinstance(magnification, float):
            print(
                f"Optic {self.name} was tuned to magnification = {magnification}. You still have to mount lens manually to that optic"
            )
            self._tune_magnification(magnification)
            return

        if (
            magnification[0] not in self._available_magnifications
            or magnification[1] not in self._available_magnifications
        ):
            if magnification[0] not in self._available_magnifications:
                raise ValueError(
                    f"Objective {magnification[0]} is not allowed. Please choose a value among {self._available_magnifications}"
                )
            elif magnification[1] not in self._available_magnifications:
                raise ValueError(
                    f"Objective {magnification[1]} is not allowed. Please choose a value among {self._available_magnifications}"
                )
        self.parameters.top_magnification = magnification[0]
        self.parameters.bottom_magnification = magnification[1]
        magnification = self.calculate_magnification()
        self._tune_magnification(magnification)

    #
    # Specific objective handling
    #

    def optic_setup(self, submenu=False):
        """
        Set-up the magnification for the two objectives mounted.
        They can be chosen from the list of possible magnifications.
        """

        value_list = []
        for i in self._hasselbad_magnifications:
            value_list.append((i, "X" + str(i)))

        # get the actual magnification values as default
        default1 = 0
        default2 = 0

        for i in range(0, len(value_list)):
            if self.parameters.top_magnification == value_list[i][0]:
                default1 = i
            if self.parameters.bottom_magnification == value_list[i][0]:
                default2 = i

        dlg1 = UserChoice(values=value_list, defval=default1)
        dlg2 = UserChoice(values=value_list, defval=default2)

        ct1 = Container([dlg1], title="Top Lens")
        ct2 = Container([dlg2], title="Bottom Lens")

        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[ct1, ct2]], title="Hasselblad Setup", cancel_text=cancel_text
        ).show()

        # returns False on cancel
        if ret != False:
            # magnification top lens
            self.parameters.top_magnification = float(ret[dlg1])
            # magnification bottom lens
            self.parameters.bottom_magnification = float(ret[dlg2])

            new_magnification = self.calculate_magnification()
            self._update_magnification(new_magnification)

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        try:
            top = self.parameters.top_magnification
            bottom = self.parameters.bottom_magnification

            print(
                "Hasselblad Objective with X%s top lens and X%s bottom lens : magnification = X%s"
                % (str(top), str(bottom), str(self._hasselbad_magnification))
            )

        except ValueError as err:
            print("Optics indicates a problem:\n", err)
