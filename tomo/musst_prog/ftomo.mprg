//****************************************************************
// MUSST programs for tomography experiment
// - FRELONCAL : frelon readout calibration
// - TOMO : main tomo trigger loop
// - TOMO_CLEAN : cleanup after an abort
//
// Documentation written at beginning of program.
//****************************************************************

// TOMO PROG INPUT VARIABLES
UNSIGNED MOTCHAN
SIGNED MOTZERO
UNSIGNED PTCHAN
SIGNED PTDELTA
UNSIGNED GATEMODE
SIGNED GATEWIDTH
UNSIGNED NPULSES
UNSIGNED STOREMASK
UNSIGNED MOTERR

// TOMO PROG AUXILIARY VARIABLES
SIGNED NEXT_TG
SIGNED LAST_TG
UNSIGNED USECHAN
SIGNED SCANDIR

UNSIGNED CUMERR
UNSIGNED NBCORR
SIGNED MOTCORR

// TOMO PROG RETURN VARIABLES
UNSIGNED NPOINTS
UNSIGNED TTOTAL

// variable to check tomo prog is loaded
UNSIGNED ISTOMO

// PROGRAM ALIASES
ALIAS MOT1 = CH1
ALIAS MOT2 = CH2
ALIAS MOT3 = CH3
ALIAS MOT4 = CH4
ALIAS MOT5 = CH5
ALIAS MOT6 = CH6


//****************************************************************
// TOMO : main tomography sequencer program
//****************************************************************
// Description:
//   The program will start sending one trigger on a predefined
//   position (only motor position allowed) then generates defined
//   number of pulses either on input position steps or in time.
//   Triggers are all sent on  and  (signal width in
//   parameters)
//****************************************************************
// Input parameters:
//
//  - MOTCHAN  : motor channel number (1..6)
//  - MOTZERO  : start trigger motor position
//  - PTCHAN   : point trigger channel (0: timer, or 1..6: motor position)
//  - PTDELTA  : motor steps or time between triggers
//  - GATEMODE : 0= timer (width defined by GATEWIDTH)
//               1= high on first trig, low on last trig
//               2= motor (steps defined by GATEWIDTH)
//  - GATEWIDTH: time width of  trigger
//  - NPULSES  : number of triggers to generate
//
// Output parameters:
//  - NPOINTS  : number of triggers sent
//  - TTOTAL   : total cycle time
//
// Buffered Data:
//  - time + motor steps (defined by MOTCHAN)
//  - data are stored on :
//       start position trigger
//       each point trigger
//       last position (even if no trigger)
//
//****************************************************************

//****************************************************************
// STORE_INIT : init store list depending on STOREMASK
//****************************************************************
SUB STORE_INIT
   .DACT &= ~0x000007FF
   .DACT |= (STOREMASK << 1) & 0x000007FF
   
   EMEM 0 AT 0
ENDSUB

SUB EVENT_INIT
   IF (USECHAN == 1) THEN
      DEFEVENT MOT1
      IF (SCANDIR > 0) THEN
         EVSOURCE MOT1 UP
      ELSE
         EVSOURCE MOT1 DOWN
      ENDIF
   ELSEIF (USECHAN == 2) THEN
      DEFEVENT MOT2
      IF (SCANDIR > 0) THEN
         EVSOURCE MOT2 UP
      ELSE
         EVSOURCE MOT2 DOWN
      ENDIF
   ELSEIF (USECHAN == 3) THEN
      DEFEVENT MOT3
      IF (SCANDIR > 0) THEN
         EVSOURCE MOT3 UP
      ELSE
         EVSOURCE MOT3 DOWN
      ENDIF
   ELSEIF (USECHAN == 4) THEN
      DEFEVENT MOT4
      IF (SCANDIR > 0) THEN
         EVSOURCE MOT4 UP
      ELSE
         EVSOURCE MOT4 DOWN
      ENDIF
   ELSEIF (USECHAN == 5) THEN
      DEFEVENT MOT5
      IF (SCANDIR > 0) THEN
         EVSOURCE MOT5 UP
      ELSE
         EVSOURCE MOT5 DOWN
      ENDIF
   ELSEIF (USECHAN == 6) THEN
      DEFEVENT MOT6
      IF (SCANDIR > 0) THEN
         EVSOURCE MOT6 UP
      ELSE
         EVSOURCE MOT6 DOWN
      ENDIF
   ELSE
      DEFEVENT TIMER
   ENDIF
ENDSUB
  
SUB SET_TARGET
   IF (USECHAN == 1) THEN
      @MOT1 = NEXT_TG
   ELSEIF (USECHAN == 2) THEN
      @MOT2 = NEXT_TG
   ELSEIF (USECHAN == 3) THEN
      @MOT3 = NEXT_TG
   ELSEIF (USECHAN == 4) THEN
      @MOT4 = NEXT_TG
   ELSEIF (USECHAN == 5) THEN
      @MOT5 = NEXT_TG
   ELSEIF (USECHAN == 6) THEN
      @MOT6 = NEXT_TG
   ELSEIF (USECHAN == 0) THEN
      @TIMER = NEXT_TG
   ENDIF
ENDSUB 

SUB INC_TARGET
   NEXT_TG = LAST_TG + PTDELTA

   IF (USECHAN > 0) THEN
      CUMERR += MOTERR
      IF ((CUMERR & 0x80000000) != 0) THEN
         CUMERR &= 0x7FFFFFFF
         NEXT_TG += MOTCORR
         NBCORR += 1
      ENDIF
   ENDIF

   LAST_TG = NEXT_TG
ENDSUB

//****************************************************************
// TOMO : main tomo loop. Return the number of trigger sent.
//****************************************************************
PROG TOMO

   // --- Reset Trigger Out B signal
   BTRIG 0

   // --- Initialise variables
   TTOTAL = 0
   NPOINTS = 0

   // --- Set event data pointer to the first position of buffer
   EMEM 0 AT 0

   // --- Reset timer
   CTSTOP TIMER
   CTRESET TIMER
   TIMER = 0
   CTSTART ONEVENT TIMER

   // --- init store values
   GOSUB STORE_INIT

   // --- init motor variable
   USECHAN = MOTCHAN
   LAST_TG = MOTZERO
   NEXT_TG = MOTZERO

   IF (PTDELTA < 0) THEN
      SCANDIR= -1
      MOTCORR= -1
   ELSE
      SCANDIR= 1
      MOTCORR= 1
   ENDIF

   CUMERR= 0
   NBCORR= 0

   // --- first position trigger
   GOSUB EVENT_INIT
   GOSUB SET_TARGET
   AT DEFEVENT DO STORE ATRIG BTRIG

   IF (GATEMODE == 0) THEN
      @TIMER = $TIMER + GATEWIDTH
      AT TIMER DO BTRIG
   ELSEIF (GATEMODE == 2) THEN
      NEXT_TG = NEXT_TG + GATEWIDTH
      GOSUB SET_TARGET
      AT DEFEVENT DO BTRIG
   ENDIF

   NPOINTS = 1

   // --- init next point trigger
   USECHAN = PTCHAN
   IF (USECHAN == 0) THEN
      LAST_TG = 0
      NEXT_TG = 0
   ENDIF
   GOSUB EVENT_INIT

   // --- main loop
   WHILE (NPOINTS < NPULSES) DO
      GOSUB INC_TARGET
      GOSUB SET_TARGET
      IF (GATEMODE == 0) THEN
         AT DEFEVENT DO STORE ATRIG BTRIG
         @TIMER = $TIMER + GATEWIDTH
         AT TIMER DO BTRIG
         GOSUB EVENT_INIT
      ELSEIF (GATEMODE == 1) THEN
         AT DEFEVENT DO STORE ATRIG
      ELSEIF (GATEMODE == 2) THEN
         AT DEFEVENT DO STORE ATRIG BTRIG
         NEXT_TG = NEXT_TG + GATEWIDTH
         GOSUB SET_TARGET
         AT DEFEVENT DO BTRIG
      ENDIF

      NPOINTS += 1
   ENDWHILE

   // --- last store event
   GOSUB INC_TARGET
   GOSUB SET_TARGET

   IF (GATEMODE == 1) THEN
      AT DEFEVENT DO STORE BTRIG
   ELSE
      AT DEFEVENT DO STORE
   ENDIF

   NPOINTS += 1
   TTOTAL= TIMER
   
   // --- change motchan target value to handle reverse movement of motor
   USECHAN = MOTCHAN
   
   IF (SCANDIR > 0) THEN
      SCANDIR = -1
   ELSE
      SCANDIR = 1
   ENDIF
   
   GOSUB EVENT_INIT
   NEXT_TG = LAST_TG - 1
   GOSUB SET_TARGET

   EXIT NPOINTS
ENDPROG


//****************************************************************
// TOMO_CLEAN : cleanup to be used after ABORT command
//****************************************************************
PROG TOMO_CLEAN
   CTSTOP TIMER
   TIMER = 0

   BTRIG 0
ENDPROG
