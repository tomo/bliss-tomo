from bliss import setup_globals
from bliss import current_session
import contextlib
from tomo.sequencebasic import SequenceBasic, SequenceBasicPars, ScanType
from tomo.scan.presets.common_header import CommonHeaderPreset
from tomo.scan.presets.musst_timer_config import MusstTimerConfigPreset
from tomo.scan.presets.move_axis import MoveAxisPreset
from tomo.helpers.locking_helper import lock_context
from tomo.helpers.locked_axis_motion_hook import LockedAxisMotionHook
from tomo.scan.presets.locked_lima_detectors import LockedLimaDetectorsPreset
from tomo.sequencebasic import cleanup_sequence
from tomo.helpers.proxy_param import ProxyParam
from tomo.sequence.presets.inhibit_auto_projection import InhibitAutoProjection
from bliss.common.cleanup import axis as cleanup_axis
from bliss.common.cleanup import cleanup, capture_exceptions
from bliss.common.logtools import log_error
from bliss.common.standard import mv
from bliss.shell.standard import umv
from .utils import setup
from bliss.shell.dialog.helpers import dialog
from bliss.scanning.scan import ScanAbort
from bliss.scanning.group import ScanSequenceError

from tomo.scan.presets.beam_check import BeamLost


class ZHeliTomoPars(SequenceBasicPars):
    DEFAULT = dict(
        **SequenceBasicPars.DEFAULT,
        **{
            "step_size": 0.1,
            "step_time": 0,
            "slave_start_pos": -450.0,
            "slave_step_size": 0.8,
            "acq_time": 0.8,
            "nb_turns": 3,
            "acc_margin": 0.0,
            "slave_acc_margin": 0.0,
            "sampling_time": 0.5,
            "back_and_forth": False,
            "cover": True,
        },
    )
    OBJKEYS = [*SequenceBasicPars.OBJKEYS, "motor", "slave_motor"]
    NOSETTINGS = [*SequenceBasicPars.NOSETTINGS, "save_flag"]

    def _validate_tomo_n(self, value):
        if not int(value) > 0:
            raise ValueError("tomo_n should be > 0")
        return int(value)

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False


class ZHelicalTomo(SequenceBasic):
    def __init__(self, name, config):
        self.name = name + "_sequence"
        super().__init__(name, config)
        self._slave_motor = self._z_axis
        tomo_config = config["tomo_config"]
        inhibitautoproj = InhibitAutoProjection(tomo_config)
        self.add_sequence_preset(inhibitautoproj)
        self._restart = False

    def _create_parameters(self):
        """
        Create the parameters for the ZHelicalTomo sequence
        """
        tomo_config = self.tomo_config
        sequence_pars = ZHeliTomoPars(self.name)
        return ProxyParam(tomo_config.pars, sequence_pars)

    def add_metadata(self, scan_info):
        super().add_metadata(scan_info)
        scan_info["technique"]["scan"]["sequence"] = "tomo:helical"
        scan_info["title"] = "tomo:helical"

    def check_coverage(self):
        """
        Check if the z_step is adequate to the field of view
        """
        fov = (
            self._detectors.get_image_pixel_size(self._detector)
            * self._detector.image.height
            / 1000
        )
        if self._inpars.field_of_view == "Half":
            z_step = self.pars.slave_step_size
            if z_step > fov:
                raise AttributeError(f"Z_step should be inferior to {fov}")
        else:
            z_step = self.pars.slave_step_size
            if z_step > (fov * 2):
                raise AttributeError(f"Z_step should be inferior to {fov * 2}")

    def init_sequence(self):
        super().init_sequence()

        if self.pars.back_and_forth:
            tomo_range = self.pars.range
            tomo_start_pos = self.pars.start_pos
            slave_step_size = self.pars.slave_step_size
            self._inpars.return_to_start_pos = self.pars.return_to_start_pos
            self.pars.return_to_start_pos = False
            tomo_stop_pos = self.pars.start_pos + self.pars.range * self.pars.nb_turns
            if (tomo_range > 0 and self._rotation_axis.position >= tomo_stop_pos) or (
                tomo_range < 0 and self._rotation_axis.position <= tomo_stop_pos
            ):
                if self.pars.cover:
                    self.pars.start_pos = tomo_start_pos + tomo_range * (
                        self.pars.nb_turns + 2
                    )
                else:
                    self.pars.start_pos = (
                        tomo_start_pos + tomo_range * self.pars.nb_turns
                    )
                self.pars.range = -tomo_range
                self.pars.slave_step_size = -slave_step_size

    def validate(self):
        if self._restart is False:
            self._z_posi = self._z_axis.position
            self._rot_posi = self._rotation_axis.position
            self._tomo_start_pos = self.pars.start_pos
            self._tomo_range = self.pars.range

        self._inpars.end_pos = (
            self.pars.start_pos + self.pars.range * self.pars.nb_turns
        )

        pixel_size = self._detectors.get_image_pixel_size(self._detector)
        yrot_px = self._y_axis.position / pixel_size

        if abs(yrot_px) > 20:
            self._inpars.field_of_view = "Half"
        else:
            self._inpars.field_of_view = "Full"

        self.check_coverage()

        darks_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "dark"
        ]
        flats_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "flat"
        ]
        returns_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name == "return_ref"
        ]
        projs_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name in ScanType.values
        ]

        nb_proj_scan = len(projs_in_sequence)
        nb_dark_scan = len(darks_in_sequence)
        nb_flat_scan = len(flats_in_sequence)
        nb_return_scan = len(returns_in_sequence)

        proj_time = self._inpars.proj_time
        dark_time = nb_dark_scan * self.pars.dark_n * self.pars.exposure_time
        flat_runner = self.get_runner("flat")
        one_flat_time = flat_runner._estimate_scan_duration(self._inpars)
        flat_time = nb_flat_scan * one_flat_time

        return_runner = self.get_runner("return_ref")
        return_time = nb_return_scan * return_runner._estimate_scan_duration(
            self._inpars
        )

        scan_time = dark_time + flat_time + proj_time + return_time
        self._inpars.scan_time = scan_time

    def add_proj(
        self,
        start_pos1,
        end_pos1,
        start_pos2,
        end_pos2,
        tomo_n,
        expo_time,
        proj_scan=None,
    ):
        detector = self._detector
        header = dict()
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)

        title = f"projections 0 - {int(tomo_n * self.pars.nb_turns)}"
        self._inpars.projection = end_pos1

        if proj_scan is not None:
            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan_z(
                start_pos1,
                end_pos1,
                start_pos2,
                end_pos2,
                tomo_n,
                expo_time,
                run=False,
            )

            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)

        proj_scan.scan_info.update({"title": title})

    def add_projections_group(
        self,
        start_pos1=None,
        end_pos1=None,
        start_pos2=None,
        end_pos2=None,
        tomo_n=None,
        expo_time=None,
        flat_on=None,
        proj_scan=None,
    ):
        """
        Divide tomo acquisition into several projection groups according
        to flat_on parameter
        One projection group is one projection scan followed by flat
        images
        flat_on indicates the number of projections after which flat
        images must be taken
        If number of projection groups deduced from tomo_n and flat_on
        is not an integer, an additional group with a reduced number of
        projections is created
        """
        if flat_on is None:
            flat_on = self.pars.flat_on

        if tomo_n is None:
            tomo_n = self.pars.tomo_n

        if tomo_n < flat_on:
            flat_on = tomo_n

        nb_groups = int(tomo_n / flat_on)
        groups1 = [start_pos1]
        groups2 = [start_pos2]
        step1 = (end_pos1 - start_pos1) / tomo_n
        step2 = (end_pos2 - start_pos2) / tomo_n

        for i in range(nb_groups):
            groups1.append(groups1[-1] + step1 * flat_on)
            groups2.append(groups2[-1] + step2 * flat_on)

        for i in range(0, len(groups1) - 1):
            self.add_proj_group(
                start_pos1=groups1[i],
                end_pos1=groups1[i + 1],
                start_pos2=groups2[i],
                end_pos2=groups2[i + 1],
                tomo_n=flat_on,
                expo_time=expo_time,
                proj_scan=proj_scan,
            )

        if tomo_n % flat_on != 0:
            self.add_proj_group(
                start_pos1=groups1[-1],
                end_pos1=end_pos1,
                start_pos2=groups2[-1],
                end_pos2=end_pos2,
                tomo_n=tomo_n % flat_on,
                expo_time=expo_time,
                proj_scan=proj_scan,
            )

    def add_proj_group(
        self,
        start_pos1=None,
        end_pos1=None,
        start_pos2=None,
        end_pos2=None,
        tomo_n=None,
        expo_time=None,
        proj_scan=None,
    ):
        """
        Set the runner that sequence must use to acquire projection
        images of one projection group
        If proj_scan is not specified, default projection runner
        corresponding to scan type parameter will be used
        Add projection scan to the sequence
        Add image common header preset to projection scan
        Add flat scan to the sequence
        """
        "Add one group of projections"
        if proj_scan is not None:
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan_z(
                self._rotation_axis,
                start_pos1,
                end_pos1,
                self._slave_motor,
                start_pos2,
                end_pos2,
                tomo_n,
                expo_time,
                run=False,
            )
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        detector = self._detector
        header = {}
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)
        proj_scan.add_preset(header_preset)
        self._inpars.nb_proj_groups += 1
        self._inpars.start_proj_groups.append([start_pos1, start_pos2])
        self._inpars.end_proj_groups.append([end_pos1, end_pos2])
        self._inpars.points_proj_groups.append(tomo_n)
        end_proj = sum(self._inpars.points_proj_groups)
        title = f"projections 0 - {int(tomo_n * self.pars.nb_turns)}"
        proj_scan.scan_info.update({"title": title})
        self._inpars.projection = end_proj
        self.add_flat()

    def projection_scan(
        self, motor, start_pos, end_pos, tomo_n, expo_time, scan_info=None, run=True
    ):
        """
        Get runner and build scan corresponding to tomo scan type parameter
        Return projection scan object
        """

        if self.pars.scan_type not in ScanType.values:
            raise ValueError(
                f"{self.pars.scan_type} is" " not a recognized type of scan"
            )

        runner = self.get_runner(self.pars.scan_type)

        if self.pars.scan_type == ScanType.CONTINUOUS:
            latency_time = self.pars.latency_time

            tomo_scan = runner(
                motor,
                start_pos,
                end_pos,
                tomo_n,
                expo_time,
                latency_time=latency_time,
                scan_info=scan_info,
                run=False,
            )
        else:
            raise ValueError(f"{self.pars.scan_type} is not supported")

        dev = runner._fscan.master.sync_list.find_device_for_motors(motor)
        if dev.is_musst():
            musst_timer_cfg_preset = MusstTimerConfigPreset(dev.board)
            tomo_scan.add_preset(musst_timer_cfg_preset)

        self._inpars.proj_time += runner._estimate_scan_duration(self._inpars)

        if run:
            tomo_scan.run()

        return tomo_scan

    def projection_scan_z(
        self,
        start_pos1,
        end_pos1,
        start_pos2,
        end_pos2,
        tomo_n,
        expo_time,
        scan_info=None,
        run=True,
    ):
        if self.pars.scan_type == ScanType.CONTINUOUS:
            runner = self.get_runner("HELICAL")

            latency_time = self.pars.latency_time
            tomo_scan = runner(
                self._rotation_axis,
                start_pos1,
                end_pos1,
                self._slave_motor,
                start_pos2,
                end_pos2,
                tomo_n * self.pars.nb_turns,
                expo_time,
                latency_time=latency_time,
                scan_info=scan_info,
                run=False,
            )

            musst = runner._f2scan.master.sync_dev.board
            musst_timer_cfg_preset = MusstTimerConfigPreset(musst)
            tomo_scan.add_preset(musst_timer_cfg_preset)

            self._inpars.proj_time += runner._estimate_scan_duration(self._inpars)

        else:
            raise ValueError(f"{self.pars.scan_type} is not supported")

        if run:
            tomo_scan.run()

        return tomo_scan

    def continuous_scan(
        self, motor, start_pos, end_pos, tomo_n, expo_time, scan_info=None, run=True
    ):
        """
        Get runner and build scan corresponding to tomo scan type parameter
        Return projection scan object
        """

        runner = self.get_runner("CONTINUOUS")

        if self.pars.scan_type == ScanType.CONTINUOUS:
            latency_time = self.pars.latency_time
            tomo_scan = runner(
                motor,
                start_pos,
                end_pos,
                tomo_n,
                expo_time,
                latency_time=latency_time,
                scan_info=scan_info,
                run=False,
            )
        else:
            raise ValueError(f"{self.pars.scan_type} is not supported")

        self._inpars.proj_time += runner._estimate_scan_duration(self._inpars)

        if run:
            tomo_scan.run()

        return tomo_scan

    def full_turn_scan(
        self,
        z_step=None,
        nb_turns=None,
        tomo_start_pos=None,
        collection_name="",
        dataset_name="",
        z_start_pos=None,
        cover=True,
        direction=1,
        scan_info=None,
        run=True,
    ):

        if dataset_name != "":
            setup_globals.newdataset(dataset_name)
        else:
            dataset_name = setup_globals.SCAN_SAVING.dataset_name
        if collection_name != "":
            setup_globals.newcollection(collection_name)
        else:
            collection_name = setup_globals.SCAN_SAVING.collection_name

        if tomo_start_pos is not None:
            self.pars.start_pos = tomo_start_pos
        if nb_turns is not None:
            self.pars.nb_turns = nb_turns
        if z_start_pos is not None:
            self.pars.slave_start_pos = z_start_pos
        if z_step is not None:
            self.pars.slave_step_size = z_step * direction

        self.pars.cover = cover
        self.pars.range = 360 * direction

        self._setup_sequence("tomo:zhelical", run=run, scan_info=scan_info)

    def basic_scan(
        self,
        z_step=None,
        nb_turns=None,
        tomo_start_pos=None,
        tomo_n=None,
        expo_time=None,
        collection_name="",
        dataset_name="",
        z_start_pos=None,
        cover=True,
        direction=1,
        scan_info=None,
        run=True,
    ):
        if dataset_name != "":
            setup_globals.newdataset(dataset_name)
        else:
            dataset_name = setup_globals.SCAN_SAVING.dataset_name
        if collection_name != "":
            setup_globals.newcollection(collection_name)
        else:
            collection_name = setup_globals.SCAN_SAVING.collection_name

        if tomo_start_pos is not None:
            self.pars.start_pos = tomo_start_pos
        if nb_turns is not None:
            self.pars.nb_turns = nb_turns
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time
        if z_start_pos is not None:
            self.pars.slave_start_pos = z_start_pos
        if z_step is not None:
            self.pars.slave_step_size = z_step * direction

        self.pars.cover = cover
        self.pars.range = 360 * direction

        self._setup_sequence("tomo:zhelical", run=run, scan_info=scan_info)

    def _add_sz_stop_pos_preset(self, stop_pos):
        scan = [
            self._scans[i]
            for i in range(len(self._scans_names))
            if self._scans_names[-1] in ScanType.values
        ][-1]
        sz_preset = MoveAxisPreset(self._slave_motor, stop_pos)
        scan.add_preset(sz_preset)

    def build_sequence(self):
        # self._detector.saving.mode = 2
        # self._detector.saving.frames_per_file = self.pars.tomo_n
        i = 0
        slave_end_pos = (
            self.pars.slave_start_pos + self.pars.nb_turns * self.pars.slave_step_size
        )
        start_pos0 = self.pars.start_pos
        end_pos0 = self.pars.start_pos + self.pars.range
        if self.pars.cover:
            start_pos1 = end_pos0
        else:
            start_pos1 = self.pars.start_pos
        end_pos1 = start_pos1 + self.pars.nb_turns * self.pars.range
        start_pos2 = end_pos1
        end_pos2 = start_pos2 + self.pars.range
        if self._restart:
            current_scan = current_session.scans[-1]
            prev_scan_type = current_scan.name
            rot_pos = self._rotation_axis.position
            start_pos = int(rot_pos / self.pars.range) * self.pars.range
            if prev_scan_type == "f2scan":
                i = 4
                curr_pos = self._z_axis.position
                turns_done = (
                    curr_pos - self.pars.slave_start_pos
                ) // self.pars.slave_step_size
                slave_start_pos = self.pars.slave_start_pos + (
                    turns_done * self.pars.slave_step_size
                )
                self.pars.slave_start_pos = slave_start_pos
                turns_left = int(self.pars.nb_turns - turns_done)
                self.pars.nb_turns = turns_left
                slave_step_size = self.pars.slave_step_size
                slave_end_pos = slave_start_pos + turns_left * slave_step_size
                if self.pars.cover:
                    turns_done += 1
                start_pos1 = self.pars.start_pos + (turns_done * 360)
                end_pos1 = start_pos1 + turns_left * 360
                start_pos2 = end_pos1
                end_pos2 = start_pos2 + self.pars.range
                self.pars.start_pos = start_pos1
            if prev_scan_type == "fscan":
                if start_pos == self.pars.start_pos:
                    i = 3
                else:
                    i = 5
                    self.pars.start_pos = start_pos
            if "flat" in prev_scan_type or "dark" in prev_scan_type:
                if start_pos == self.pars.start_pos:
                    if "dark" in prev_scan_type:
                        i = 1
                    else:
                        i = 2
                else:
                    if "dark" in prev_scan_type:
                        i = 6
                    else:
                        i = 7
            if "return_ref" in prev_scan_type:
                i = 8

        if self.pars.dark_at_start and i <= 1:
            self.add_dark()
        if self.pars.flat_at_start and i <= 2:
            self.add_flat()

        if self.pars.projection_groups:
            if self.pars.cover and i <= 3:
                super().add_projections_group(
                    start_pos0,
                    end_pos0,
                    self.pars.tomo_n,
                    self.pars.exposure_time,
                    flat_on=self.pars.flat_on,
                )
            if i <= 4:
                self.add_projections_group(
                    start_pos1,
                    end_pos1,
                    self.pars.slave_start_pos,
                    slave_end_pos,
                    self.pars.tomo_n,
                    self.pars.exposure_time,
                    flat_on=self.pars.flat_on,
                )
            if self.pars.cover and i <= 5:
                super().add_projections_group(
                    start_pos2,
                    end_pos2,
                    self.pars.tomo_n,
                    self.pars.exposure_time,
                    flat_on=self.pars.flat_on,
                )
        else:
            if self.pars.cover and i <= 3:
                super().add_proj(
                    start_pos0,
                    end_pos0,
                    self.pars.tomo_n,
                    self.pars.exposure_time,
                )
            if i <= 4:
                self.add_proj(
                    start_pos1,
                    end_pos1,
                    self.pars.slave_start_pos,
                    slave_end_pos,
                    self.pars.tomo_n,
                    self.pars.exposure_time,
                )
            if self.pars.cover and i <= 5:
                super().add_proj(
                    start_pos2,
                    end_pos2,
                    self.pars.tomo_n,
                    self.pars.exposure_time,
                )
        if self.pars.cover and i <= 5:
            self._add_sz_stop_pos_preset(slave_end_pos)
        if self.pars.dark_at_end and i <= 6:
            self.add_dark()
        if self.pars.flat_at_end and i <= 7:
            self.add_flat()
        if self.pars.images_on_return and i <= 8:
            self.add_return()

    def add_return(self, return_scan=None):
        """
        Set the runner that sequence must use to acquire static images
        on return
        If return_scan is not specified, default return runner will be
        used
        """
        if return_scan is not None:
            self.add_helper_scan("return_ref", return_scan)
        else:
            return_runner = self._tomo_runners["return_ref"]
            self._inpars.start_pos = 360 * (self.pars.nb_turns + 1)
            rot_pos_list = return_runner.get_motor_positions(self._inpars)
            self._inpars.start_pos = self.pars.start_pos
            return_scan = return_runner(
                rot_pos_list,
                self.pars.exposure_time,
                save=self.pars.save_flag,
                run=False,
            )
            self.add_helper_scan("return_ref", return_scan)

    @contextlib.contextmanager
    def _run_context(self, capture, lock: bool = True):
        """Common context executed at the :meth:`run`."""
        tomo_config = self._tomo_config
        lima_detector = tomo_config.detectors.active_detector.detector
        rotation_axis = tomo_config.rotation_axis
        with contextlib.ExitStack() as stack:
            if lock:
                stack.enter_context(lock_context(rotation_axis, owner=self.name))
                for mh in rotation_axis.motion_hooks:
                    # The rotation was locked by the sequence, we have to
                    # disable it for LockedAxisMotionHook because it is not reentrant
                    if isinstance(mh, LockedAxisMotionHook):
                        stack.enter_context(mh.disabled_context())

                stack.enter_context(lock_context(lima_detector, owner=self.name))

            stack.enter_context(tomo_config.auto_projection.inhibit())
            stack.enter_context(capture())
            yield

    @cleanup_sequence
    def run(self, user_info=None,_lock: bool = True):
        """
        Prepare and execute the sequence
        Start acquisition presets
        Add and run all the scans contained in the sequence
        Stop acquisition presets
        Move back rotation axis to initial position if
        return_to_start_pos parameter is set to True
        """
        if user_info is not None:
            if self._prepare_done:
                raise RuntimeError(
                    "Scan prepare was already done "
                    "Use user_info at creation or at run, not at both."
                )
        if not self._prepare_done:
            self.prepare(user_info)

        restore_list = (cleanup_axis.POS, cleanup_axis.VEL)
        detector = self._detector
        self._srot_veli = self._rotation_axis.velocity
        self._sz_veli = self._z_axis.velocity
        capture2 = None
        with capture_exceptions(raise_index=None) as capture:
            with self._run_context(capture, lock=_lock):
                with cleanup(
                    self._y_axis,
                    restore_list=restore_list,
                    verbose=True,
                ):
                    self._start_presets()
                    mv(self._rotation_axis, self.pars.start_pos)
                    with self._sequence.sequence_context() as scan_seq:
                        self._current_start = (
                            self._machinfo.counters.current.value
                            if self._machinfo is not None
                            else None
                        )
                        with capture_exceptions(raise_index=None) as capture2:
                            with capture2():
                                for scan in self._scans:
                                    scan_seq.add(scan)
                                for scan in self._scans:
                                    with contextlib.ExitStack() as stack:
                                        # Assume that the detector was already locked by the sequence
                                        # or the parent of the sequence
                                        # So what we have to disable the dedicated preset
                                        # It would be better not to generate them at the first place,
                                        # but it looks too tricky with the actual architercture
                                        for preset in scan._preset_list:
                                            if isinstance(preset, LockedLimaDetectorsPreset):
                                                stack.enter_context(preset.disabled_context())
                                        scan.run()
                        self._current_stop = (
                            self._machinfo.counters.current.value
                            if self._machinfo is not None
                            else None
                        )
                        self._send_icat_metadata()
                        self._stop_presets()

        if len(capture.failed) > 0:
            log_error(
                self,
                f"A problem occured during {self.name.split(':')[-1]}"
                " sequence, sequence aborted",
            )
            log_error(self, capture.exception_infos)
            log_error(self, "\n")
            self._prepare_done = False
            self._stop_presets()
            if capture2:
                _, first_error, _ = capture2.failed[0]  # sys.exec_info()
            else:
                _, first_error, _ = capture.failed[0]  # sys.exec_info()
            if isinstance(first_error, BeamLost):
                self._restart = True
                self._beam_lost(user_info)
            elif isinstance(
                first_error,
                (KeyboardInterrupt, ScanSequenceError, ScanAbort, ValueError),
            ):
                self._cleanup()
                raise first_error
            else:
                print("SCAN FAILED WE REDO IT")
                if hasattr(setup_globals, "restart_server"):
                    setup_globals.restart_server(detector)
                self._restart = True
                scan_saving = current_session.scan_saving
                scan_saving.newcollection(scan_saving.collection_name + "_cs_")
                self.run(user_info)

        self._cleanup()


    def _cleanup(self):
        if self._restart:
            self._restart = False
            return
        self._prepare_done = False
        self._rotation_axis.velocity = self._srot_veli
        self._z_axis.velocity = self._sz_veli
        if not self.pars.back_and_forth:
            mv(self._rotation_axis,int(self._rotation_axis.position/360)*360)
            self._rotation_axis.position = (
                self._rotation_axis.position % self.pars.range
            )
            self._rotation_axis.dial = (
                self._rotation_axis.position * self._rotation_axis.sign
            )
        self.pars.range = self._tomo_range
        self.pars.start_pos = self._tomo_start_pos
        if self.pars.return_to_start_pos:
            rot_axis_position = (self._rotation_axis, self._rot_posi)
            z_axis_position = (self._z_axis, self._z_posi)
            umv(*rot_axis_position, *z_axis_position)
        if self.pars.back_and_forth:
            self.pars.return_to_start_pos = self._inpars.return_to_start_pos


# Setup the standard BLISS `menu`
@dialog(ZHelicalTomo.__name__, "setup")
def _setup(sequence):
    setup(sequence)
