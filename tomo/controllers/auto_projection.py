import weakref
import contextlib
import logging
import gevent
import time
from bliss.common import event
from bliss.common.logtools import log_error, log_info
from bliss.config.beacon_object import BeaconObject
from tomo.helpers import shell_utils
from tomo.helpers.locking_helper import is_locked, lock_context, AlreadyLockedDevices


_logger = logging.getLogger(__name__)


def _patch_call_callback_on_update(pars, callback):
    """Ugly hack to be able to know that something have changed is the settings

    This could be cleaned up with an event is the BLISS settings
    """
    pars__set_value = getattr(pars, "_FScanParamBase__set_value")

    def set_value(name, value):
        callback(name, value)
        pars__set_value(name, value)

    object.__setattr__(pars, "_FScanParamBase__set_value", set_value)


class AutoProjection(BeaconObject):
    """
    Trigger a tomo projection when the sample stage have moved.

    For that, the related tomo config have to be active, this AutoProjection
    service have to be enabled, and not inhibited.
    """

    def __init__(self, tomoconfig):
        assert tomoconfig is not None
        name = f"{tomoconfig.name}.auto_projection"
        super().__init__(config={}, name=name)
        self.__tomoconfig = weakref.ref(tomoconfig)
        event.connect(tomoconfig, "is_moving", self._is_moving_updated)
        self.__inhibited_count = 0
        self.__last_end_of_move_time = 0.0

        # As this instance is generated and not read from yml, we have to register it's name
        # static.get_config()._name2instance[name] = self

        # sync the exposure_time with pars
        self.__exposure_time = tomoconfig.pars.exposure_time
        _patch_call_callback_on_update(tomoconfig.pars, self.__pars_was_updated)
        event.connect(self, "exposure_time", self.__exposure_time_updated)

    def __pars_was_updated(self, name, value):
        if name == "exposure_time":
            self.__exposure_time = value

    def __exposure_time_updated(self, value):
        """Called when the exposure time was updated (also from the remote session)"""
        tomoconfig = self._tomoconfig
        if tomoconfig.pars.exposure_time != value:
            tomoconfig.pars.exposure_time = value

    @property
    def _tomoconfig(self):
        # dereference the weakref
        return self.__tomoconfig()

    __enabled = BeaconObject.property_setting(
        name="enabled",
        default=False,
        doc="True this object will auto update the projection when the sample stage have moved",
    )

    __exposure_time = BeaconObject.property_setting(
        name="exposure_time",
        default=1.0,
        doc="Exposure time to use with the projection",
    )

    __settle_time = BeaconObject.property_setting(
        name="settle_time",
        default=0,
        doc="Amount of second to wait between the end of the scan and the auto projection",
    )

    __inhibited = BeaconObject.property_setting(
        name="inhibited",
        default=False,
        doc="True if the auto update is inhibited by a section of the code",
    )

    @property
    def enabled(self):
        return self.__enabled

    @enabled.setter
    def enabled(self, enable: bool):
        self.__enabled = enable

    @property
    def exposure_time(self):
        return self.__exposure_time

    @exposure_time.setter
    def exposure_time(self, exposure_time: float):
        self._tomoconfig.pars.exposure_time = exposure_time
        # The patch will already syn the expo time
        # self.__exposure_time = exposure_time

    @property
    def inhibited(self):
        return self.__inhibited

    @contextlib.contextmanager
    def inhibit(self):
        """Reentrant context to inhibit the auto projection"""
        _logger.info("Start autoupdate inhibition")
        self.__inhibited_count += 1
        if self.__inhibited_count == 1:
            self.__inhibited = True
        try:
            yield
        finally:
            self.__inhibited_count -= 1
            if self.__inhibited_count == 0:
                self.__inhibited = False
            _logger.info("Stop autoupdate inhibition")

    @property
    def settle_time(self):
        return self.__settle_time

    @settle_time.setter
    def settle_time(self, settle_time: float):
        self.__settle_time = settle_time

    def _is_moving_updated(self, is_moving):
        if not self._tomoconfig.active:
            return
        if not is_moving:
            _logger.info("Sample stage motion terminated")
            self._end_of_move()

    def _end_of_move(self):
        # Handle the update of the projection if needed
        in_daiquiri = not shell_utils.is_in_shell()
        if not in_daiquiri:
            # For now inhibit the auto proj from the BLISS shell
            return

        self.__last_end_of_move_time = time.time()
        if self._tomoconfig.active and self.enabled and not self.inhibited:
            self._prepare_auto_projection()

    def _prepare_auto_projection(self):
        """Prepare auto tomo projection.

        Make sure the session is a BLISS shell, and execute it inside the
        terminal.
        """
        self._auto_projection()

    def wait_for_stabilization(self):
        """Sleep the remaining time between the last end of move end the
        `settle_time`."""
        settle_time = self.settle_time
        if settle_time is not None and settle_time > 0:
            time_after_move = time.time() - self.__last_end_of_move_time
            remaining_time = settle_time - time_after_move
            if remaining_time > 0:
                gevent.sleep(remaining_time)

    def _auto_projection(self):
        """Execute the tomo projection procedure"""
        tomo_config = self._tomoconfig
        active_detector = tomo_config.detectors.active_detector
        if active_detector is None:
            log_error(
                tomo_config,
                "Error while projection requested: No active detector defined",
            )
            return

        if is_locked(active_detector.detector):
            return

        try:
            with lock_context(active_detector.detector, owner="autoproj"):
                # Not so critical
                _logger.info("Take auto projection")
                log_info(
                    tomo_config, "Auto ct on detector %s", active_detector.detector.name
                )
                runner = tomo_config.get_runner("count")
                with runner.disabled_locking_detector_context():
                    self.wait_for_stabilization()
                    return runner(
                        self.exposure_time, active_detector.detector, save=False
                    )
        except AlreadyLockedDevices:
            _logger.debug("Detector already in use. Autoproj was skipped")
        except Exception:
            _logger.debug("Failed while taking count", exc_info=True)
            raise

    def __info__(self):
        def yesno(value):
            return "Yes" if value else "No "

        info_str = f"{self._tomoconfig.name} auto projection info:\n"
        info_str += "-----------------------------------------\n"
        info_str += f"enabled              {yesno(self.enabled)}\n"
        info_str += f"inhibited (for now)  {yesno(self.inhibited)} ({self.__inhibited_count} reg contexts)\n"
        info_str += "-------------------  --------------------\n"
        info_str += f"exposure time        {self.exposure_time:0.3f} s\n"
        info_str += f"settle time          {self.settle_time:0.3f} s\n"
        return info_str
