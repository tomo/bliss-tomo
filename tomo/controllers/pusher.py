from __future__ import annotations
import os
import typing
import enum
from bliss.common.logtools import log_warning, log_error
from bliss.config.beacon_object import BeaconObject, EnumProperty
from tomo.helpers.beacon_object_helper import check_unexpected_keys
from bliss.config.conductor import connection
from tomo.helpers.locking_helper import lock, unlock
from bliss.common.hook import MotionHook


class PusherState(enum.Enum):
    UNKNOWN = "Unknown location"
    RETRACTED = "Retracted from the sample stage"
    IN_TOUCH = "Touching the sample"
    MOVING_IN = "About to move in touch"
    MOVING_OUT = "About to be retracted"


class Pusher(BeaconObject):
    """
    Controller which is there to provide a .

    .. code-block:: yaml

        - name: pusher
          plugin: bliss
          class: Pusher
          package: tomo.controllers.pusher

          # Axis used to push
          axis: $bliss_spush

          # Distance to move to restract the pusher
          retracted_offset: -90

          # Distance to cover the difference between the switch and the metal
          # 0 is like disabled
          switch_offset: 0.0

          # Device to lock when it's in touch
          lock_in_touch: $test_pusher_srot

    NB: previous spec macros was using "?ISG ?SW" (backdoor command to
        read hardware signal (JMC)). Usage of <axis>.state.HOME should be
        equivalent.
    """

    def __init__(self, name: str, config: dict[str, typing.Any]):
        BeaconObject.__init__(self, config=config, name=name)

        check_unexpected_keys(
            self, config, ["axis", "retracted_offset", "switch_offset", "lock_in_touch"]
        )

        self.motion_hooks: list[MotionHook] = []

        self._axis = config["axis"]
        self._retracted_offset = config["retracted_offset"]
        if self._retracted_offset >= 0.0:
            raise ValueError(
                f"Retracted offset is supposed to be negative. Found {self._retracted_offset}"
            )
        self._switch_offset = config.get("switch_offset", 0)
        if self._switch_offset < 0.0:
            raise ValueError(
                f"Switch offset is supposed to be positive. Found {self._switch_offset}"
            )
        self._lock_in_touch = config.get("lock_in_touch")
        self._connection: connection.Connection | None = None

    state = EnumProperty(
        name="state",
        enum_type=PusherState,
        default=PusherState.UNKNOWN,
        unknown_value=PusherState.UNKNOWN,
        doc="State of the device",
    )

    def move_in(self):
        """Move the pusher in touch of the sample stage"""
        axis = self._axis
        if axis.state.HOME:
            log_warning(self, "Pusher seems to be already IN, aborting")
            return

        # no rotation while pusher is in contact => lock rotation.
        if self._lock_in_touch:
            if self._connection is None:
                self._connection = connection.Connection()
                self._connection.set_client_name(f"{self.name},pid:{os.getpid()}")
            lock(self._lock_in_touch, connection=self._connection, timeout=3)

        self.state = PusherState.MOVING_IN

        # Go to the home position
        axis.home()

        # to cover the difference between switch and metal
        if self._switch_offset:
            axis.move(self._switch_offset, relative=True)

        # update pusher state
        self.state = PusherState.IN_TOUCH

    def move_out(self):
        """Move the pusher out of the sample stage"""
        axis = self._axis

        # update pusher state
        self.state = PusherState.MOVING_OUT

        # check home signal
        home_sig_before = axis.state.HOME

        # retract pusher
        # excentric pusher
        axis.move(self._retracted_offset)

        home_sig_after = axis.state.HOME

        # warn if home signal has not changed -> switch sticked ?
        if home_sig_before == home_sig_after:
            log_warning(
                self,
                "-------------PUSHER--WARNING--------------------------------------------------",
            )
            log_warning(
                self,
                "-- It seems that pusher home switch home status has not changed after pushout.",
            )
            log_warning(self, "-- The home switch may be blocked or pusher in limits.")
            log_warning(
                self,
                "------------------------------------------------------------------------------",
            )

        # no rotation while pusher is in contact
        if self._lock_in_touch:
            if self._connection:
                unlock(self._lock_in_touch, connection=self._connection)
                self._connection.close()
                self._connection = None

        # update pusher state
        self.state = PusherState.RETRACTED

    def sync_hard(self):
        """Synchronize the device with the real location of the controller"""
        axis = self._axis
        if axis.state.HOME:
            self.state = PusherState.IN_TOUCH
        elif abs(axis.position - self._retracted_offset) < 0.0001:
            self.state = PusherState.RETRACTED
        else:
            self.state = PusherState.UNKNOWN

    def push(self, distance: float):
        """Push the sample stage of about this distance"""
        if self.state != PusherState.IN_TOUCH:
            log_error(self, "Not in touch. Use `move_in` first")
            return
        axis = self._axis
        self._execute_pre_push_hooks()
        axis.move(distance, relative=True)
        self._execute_post_push_hooks()

    def _execute_pre_push_hooks(self):
        """
        Allows to do an action before pushing
        """
        for h in self.motion_hooks:
            h.pre_move([self])

    def _execute_post_push_hooks(self):
        """
        Allows to do an action after pushing
        """
        for h in self.motion_hooks:
            h.post_move([self])

    def __info__(self):
        state = self.state
        axis = self._axis
        result = f"{self.name}\n"
        result += f"   state:            {state.name}\n"
        result += f"   retracted_offset: {self._retracted_offset:0.3f}\n"
        result += f"   switch_offset:    {self._switch_offset:0.3f}\n"
        result += f"   axis:             {axis.name}\n"
        result += f"     └ position:     {axis.position:0.4f}\n"
        return result
