# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
from typing import TYPE_CHECKING
import typing
import enum
import weakref
import pint

from bliss.config import static
from bliss.config.beacon_object import BeaconObject, EnumProperty
from bliss.common import event
from bliss.common.scans import DEFAULT_CHAIN
from bliss.common.logtools import log_debug
from bliss.controllers.lima.lima_base import Lima
from bliss.common.protocols import HasMetadataForScan
from bliss.common.tango import DevFailed
from bliss import global_map
from ..optic.base_optic import BaseOptic
from ..helpers.nexuswriter_utils import hdf5_device_names
from bliss.shell.cli.user_dialog import (
    UserMsg,
    UserIntInput,
    UserFloatInput,
    UserInput,
    UserCheckBox,
    UserChoice,
)
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.shell.dialog.controller.lima_dialogs import (
    lima_saving_parameters_dialog,
    lima_image_dialog,
)
from tomo.helpers.user_optional_float_input import UserOptionalFloatInput
from tomo.helpers.user_sample_pixel_size import UserSamplePixelSize
from tomo.helpers.auto_sample_pixel_size import AutoSamplePixelSize
from tomo.helpers.beacon_object_helper import check_unexpected_keys
from bliss.common.utils import RED, GREEN, YELLOW
from tomo.helpers import info_utils


if TYPE_CHECKING:
    from .tomo_detectors import TomoDetectors


class TomoDetectorState(enum.Enum):
    UNKNOWN = "UNKNOWN"
    READY = "READY"
    OFFLINE = "OFFLINE"
    INVALID = "INVALID"
    MOUNTING = "MOUNTING"
    UNMOUNTING = "UNMOUNTING"


class TomoDetectorPixelSizeMode(enum.Enum):
    AUTO = "AUTO"
    USER = "USER"


def float_or_none(value) -> float | None:
    """Make sure it's python float, not a numpy data"""
    if value is None:
        return None
    return float(value)


class TomoDetector(BeaconObject, HasMetadataForScan):
    """
    Define a tomo detector.

    It is a virtual device compound for now by a lima `detector` and an optional
    `optic`.

    It provides a `sample_pixel_size` (in um) and a `field_of_view` (in mm).
    """

    PixelSizeMode = TomoDetectorPixelSizeMode

    State = TomoDetectorState

    def __init__(self, name, config):
        BeaconObject.__init__(self, config=config, name=name, share_hardware=False)
        global_map.register(self, tag=name)

        check_unexpected_keys(self, config, ["detector", "optic", "detector_axis"])

        detector = config["detector"]

        assert isinstance(detector, Lima)

        self.detector: Lima = detector

        self.__tomo_detectors = None
        self.__local_optic: BaseOptic | None = None
        """Have to be stored to properly handle the optic changes"""

        self._is_unmounting = False
        self._is_mounting = False
        self._is_online = False
        self._source_detector_distance: float | None = None
        self._detector_unit = None

        self._user_sample_pixel_size_geometry = UserSamplePixelSize()
        self._auto_sample_pixel_size_geometry = AutoSamplePixelSize()
        self._auto_sample_pixel_size_geometry.use_magnification = False

        event.connect(
            detector.image._image_params, "binning", self._on_detector_binning_changed
        )

        self.detector_axis = config.get("detector_axis")

        if self._sample_pixel_size_mode is None:
            self._sample_pixel_size_mode = TomoDetectorPixelSizeMode.AUTO

        # Configure the optic

        event.connect(self, "optic", self.__on_optic_changed)
        optic = self._optic
        if optic is not None:
            self.__on_optic_changed(optic)

        if optic is None:
            # Update it from the configuration if not yet defined
            fixed_optic = config.get("optic")
            if fixed_optic is not None:
                self.optic = fixed_optic

        # For compatibility between 2.5 and 2.6
        user_optic_name = self._user_optic_name
        if user_optic_name is not None:
            # At the first restart this will move the content of the setting
            # from `user_optic_name` to `optic`.
            # FIXME: For bliss-tomo >= 2.7 this can be dropped
            self.optic = user_optic_name
            self._user_optic_name = None

    # FIXME: For bliss-tomo >= 2.7 this can be dropped
    _user_optic_name = BeaconObject.property_setting(
        name="user_optic_name", doc="For compatibility"
    )

    _ = """Optic mounted on the detector.

    The optic can be modified dynamically by the user.

    The user defined optic is kept until reset_user_optic() is executed to
    reset to the configurd optic in the yml file.
    """
    _optic = BeaconObject.config_obj_property_setting(name="optic", doc=_)

    _ = """Acquisition mode for lima.

- MANUAL: The accumulation is read from the Lima detector
- SINGLE: The single mode will be enforced to the lima detector
- ACCUMULATION: The accumulation mode will be enforced to the Lima detector
"""
    acq_mode = BeaconObject.property_setting(name="acq_mode", default="MANUAL", doc=_)

    @property
    def optic(self) -> BaseOptic | None:
        return self._optic

    @optic.setter
    def optic(self, value: str | BaseOptic | None):
        """Set an optic to this detector.

        To simplify user use case, the input can be a BLISS object name.
        In this case the value is first dereferences, and can raise a bliss
        object exception.

        Argument:
            value: An optic or a reference to an optic. The optic
                can be None.

        Raises:
            ObjectNotFound: if name is not found in configuration
            ObjectCreationFailed: if the requested name have failed during it's creation
        """
        if isinstance(value, str):
            config = static.get_config()
            obj = config.get(value)
        else:
            obj = value
        if obj is not None and not isinstance(obj, BaseOptic):
            raise TypeError(f"Expected BaseOptic, found {type(obj)}")

        # Setting the optic will trigger the `__on_optic_changed` event
        self._optic = obj

        # That's done on a single session, that's maybe not a good idea
        # and it's maybe not needed.
        self.sync_hard()

    def reset_optic(self):
        """Reset the optic using the one from the configuration"""
        obj = self.config["optic"]
        if obj is not None and not isinstance(obj, BaseOptic):
            raise TypeError(f"Expected BaseOptic, found {type(obj)}")
        self._optic = obj

    @property
    def _test_optic_name(self):
        """Helper only used for unittests"""
        optic = self.optic
        if optic is None:
            return ""
        return optic.name

    def __on_optic_changed(self, value: str | BaseOptic, *args, **kwargs):
        """Called when the link to the optic have changed."""
        if isinstance(value, str):
            if value == "":
                obj = None
            else:
                config = static.get_config()
                obj = config.get(value)
        else:
            obj = value

        if not isinstance(obj, BaseOptic):
            raise TypeError(f"Expected BaseOptic, found {type(obj)}")

        if self.__local_optic is obj:
            return
        if self.__local_optic is not None:
            optic = self.__local_optic
            self._local_optic = None
            self._disconnect_optic(optic)
        self._connect_optic(obj)
        self.__local_optic = obj

    def _connect_optic(self, optic: BaseOptic | None):
        """Called when this optic is connect to this TomoDetector"""
        auto_px_geometry = self._auto_sample_pixel_size_geometry
        if optic is not None:
            event.connect(optic, "magnification", self._on_optic_magnification_changed)
            auto_px_geometry.magnification = optic.magnification
            # Have to be done at the end to avoid inconsistent update
            auto_px_geometry.use_magnification = True
        else:
            # Have to be done first to avoid inconsistent update
            auto_px_geometry.use_magnification = False
            auto_px_geometry.magnification = None

    def _disconnect_optic(self, optic: BaseOptic):
        """Called when this optic is connect to this TomoDetector"""
        event.disconnect(optic, "magnification", self._on_optic_magnification_changed)

    @property
    def tomo_detectors(self) -> TomoDetectors:
        """Returns the TomoDetectors object which own this TomoDetector"""
        if self.__tomo_detectors is None:
            raise RuntimeError(
                f"{self.name} is not connected to a TomoDetectors. Unexpected architecture."
            )
            return None
        return self.__tomo_detectors()

    def _set_tomo_detectors(self, tomo_detectors: TomoDetectors):
        assert self.__tomo_detectors is None
        self.__tomo_detectors = weakref.ref(tomo_detectors)
        self._post_init()

    def _post_init(self):
        """Called once at start up, when the component is aware of
        it's environment.

        The tomo_detectors and tomo_config are linked and usable.
        """
        tomo_detectors = self.tomo_detectors
        tomo_config = tomo_detectors.tomo_config
        assert tomo_config is not None
        assert tomo_detectors is not None

        detector_axis = self.detector_axis
        if detector_axis:
            unit = detector_axis.unit
            if unit is None or not pint.Unit(unit).is_compatible_with("mm"):
                raise ValueError(
                    f"Axis '{detector_axis.name}' dont use a proper unit. Found '{unit}'"
                )
            self._detector_unit = unit
            event.connect(detector_axis, "position", self._on_detector_position_changed)
            self._on_detector_position_changed(detector_axis.position)
        else:
            event.connect(
                tomo_detectors, "source_distance", self._on_source_holder_changed
            )
            self._on_source_holder_changed(tomo_detectors.source_distance)

        event.connect(
            tomo_config.sample_stage,
            "source_distance",
            self._on_source_sample_distance_changed,
        )
        self._on_source_sample_distance_changed(
            tomo_config.sample_stage.source_distance
        )

        event.connect(
            self,
            "source_distance",
            self._on_source_distance_changed,
        )

        event.connect(
            self,
            "camera_pixel_size",
            self._on_camera_pixel_size_changed,
        )

        lima = self.detector

        user_px_geometry = self._user_sample_pixel_size_geometry
        user_px_geometry.camera_binning = lima.image.binning
        user_px_geometry.unbinned_sample_pixel_size = (
            self.user_unbinned_sample_pixel_size
        )
        event.connect(
            user_px_geometry,
            "sample_pixel_size",
            self._on_user_pixel_size_changed,
        )

        self._sync_auto_sample_pixel_size()
        auto_px_geometry = self._auto_sample_pixel_size_geometry
        event.connect(
            auto_px_geometry,
            "sample_pixel_size",
            self._on_auto_pixel_size_changed,
        )

        self.sync_hard()

    def _sync_auto_sample_pixel_size(self):
        """Update the auto pixel size with the actual state"""
        auto_px_geometry = self._auto_sample_pixel_size_geometry
        tomo_detectors = self.tomo_detectors
        tomo_config = tomo_detectors.tomo_config
        lima = self.detector
        auto_px_geometry.camera_binning = lima.image.binning
        auto_px_geometry.camera_pixel_size = self.camera_pixel_size
        auto_px_geometry.sample_distance = tomo_config.sample_stage.source_distance
        auto_px_geometry.detector_distance = self.source_distance
        optic = self.optic
        if optic is not None:
            auto_px_geometry.magnification = optic.magnification

    def _on_camera_pixel_size_changed(self, *args, **kwargs):
        camera_pixel_size = self.camera_pixel_size
        auto_geometry = self._auto_sample_pixel_size_geometry
        auto_geometry.camera_pixel_size = camera_pixel_size

    def _on_detector_binning_changed(self, *args, **kwargs):
        binning = self.detector.image.binning
        user_geometry = self._user_sample_pixel_size_geometry
        user_geometry.camera_binning = binning
        auto_geometry = self._auto_sample_pixel_size_geometry
        auto_geometry.camera_binning = binning

    _ = """State of the detector"""
    state = EnumProperty(
        name="state",
        enum_type=TomoDetectorState,
        default=TomoDetectorState.UNKNOWN,
        unknown_value=TomoDetectorState.UNKNOWN,
    )

    _ = """Actual size of the camera in pixels
    """
    _actual_size = BeaconObject.property_setting(name="actual_size", doc=_)

    @property
    def actual_size(self):
        return self._actual_size

    _ = """Pixel size of the camera in micrometer

    This is updated asynchronously from events, so should not be used in scripts
    """
    _camera_pixel_size = BeaconObject.property_setting(name="camera_pixel_size", doc=_)

    @property
    def camera_pixel_size(self):
        # Make sure it's a list, it could be stored as numpy array in redis
        ps = self._camera_pixel_size
        if ps is None:
            return None
        return list(ps)

    _ = """Pixel size of the sample in micrometer

    This is updated asynchronously from events, so should not be used in scripts
    """
    _sample_pixel_size = BeaconObject.property_setting(name="sample_pixel_size", doc=_)

    @property
    def sample_pixel_size(self) -> float | None:
        return self._sample_pixel_size

    _ = """Mode of computation for the pixel size

    This can be on of `TomoDetectorPixelSizeMode`
    """
    _sample_pixel_size_mode = BeaconObject.property_setting(
        name="sample_pixel_size_mode", doc=_
    )

    @property
    def sample_pixel_size_mode(self):
        return self._sample_pixel_size_mode

    @sample_pixel_size_mode.setter
    def sample_pixel_size_mode(self, value):
        if isinstance(value, str):
            value = value.upper()
        value = TomoDetectorPixelSizeMode(value)
        if self._sample_pixel_size_mode != value:
            self._sample_pixel_size_mode = value
            self._update_sample_pixel_size()
            self._update_state()

    _ = """Fixed user sample pixel size.
    """
    _user_sample_pixel_size = BeaconObject.property_setting(
        name="user_sample_pixel_size", doc=_
    )

    _ = """Fixed user unbinned sample pixel size.
    """
    _user_unbinned_sample_pixel_size = BeaconObject.property_setting(
        name="user_unbinned_sample_pixel_size", doc=_
    )

    @property
    def user_sample_pixel_size(self) -> float | None:
        return self._user_sample_pixel_size

    @user_sample_pixel_size.setter
    def user_sample_pixel_size(self, value: float | None):
        geometry = self._user_sample_pixel_size_geometry
        geometry.sample_pixel_size = value

    @property
    def user_unbinned_sample_pixel_size(self):
        return self._user_unbinned_sample_pixel_size

    @user_unbinned_sample_pixel_size.setter
    def user_unbinned_sample_pixel_size(self, value):
        geometry = self._user_sample_pixel_size_geometry
        geometry.unbinned_sample_pixel_size = value

    @property
    def optics_pixel_size(self) -> float | None:
        """
        Return the magnified detector pixel size.
        """
        tomo_config = self.tomo_detectors.tomo_config
        source_sample_distance = tomo_config.sample_stage.source_distance
        detector_source_distance = self.source_distance
        if source_sample_distance is None or detector_source_distance is None:
            return None
        sample_pixel_size = self.sample_pixel_size
        if sample_pixel_size is None:
            return None
        dist_coef = detector_source_distance / source_sample_distance
        optics_pixel_size = sample_pixel_size * dist_coef
        return optics_pixel_size

    def _on_user_pixel_size_changed(self, *args, **kwargs):
        """Called when the user pixel size from the bahaviour have changed"""
        geometry = self._user_sample_pixel_size_geometry
        self._user_sample_pixel_size = geometry.sample_pixel_size
        self._user_unbinned_sample_pixel_size = geometry.unbinned_sample_pixel_size
        if self._sample_pixel_size_mode == TomoDetectorPixelSizeMode.USER:
            self._update_sample_pixel_size()

    def _on_auto_pixel_size_changed(self, *args, **kwargs):
        """Called when the user pixel size from the bahaviour have changed"""
        geometry = self._auto_sample_pixel_size_geometry
        self._auto_sample_pixel_size = geometry.sample_pixel_size
        if self._sample_pixel_size_mode == TomoDetectorPixelSizeMode.AUTO:
            self._update_sample_pixel_size()

    _ = """Sample pixel size computed automatically.
    """
    _auto_sample_pixel_size = BeaconObject.property_setting(
        name="auto_sample_pixel_size", doc=_
    )

    @property
    def auto_unbinned_sample_pixel_size(self) -> float | None:
        binning = self._binning(None)
        if binning is None or self.auto_sample_pixel_size is None:
            return None
        return self.auto_sample_pixel_size / binning

    @property
    def auto_sample_pixel_size(self) -> float | None:
        return self._auto_sample_pixel_size

    _detector_axis_focal_pos = BeaconObject.property_setting(
        name="detector_axis_focal_pos",
        default=None,
        doc="Position of the focal point into the detector axis",
    )

    @property
    def source_distance(self) -> float | None:
        """
        Return source detector distance in mm
        """
        if self.detector_axis is None:
            offset = self._offset_detector_distance
            distance = self.tomo_detectors.source_distance
            if distance is None:
                return None
            return distance + offset

        focal_pos = self._detector_axis_focal_pos
        if focal_pos is None:
            return None
        pos = self.detector_axis.position if self.detector_axis is not None else 0
        return pos - focal_pos

    @source_distance.setter
    def source_distance(self, distance: float | None):
        """
        Set source detector distance in mm
        """
        if distance is not None and distance < 0:
            raise RuntimeError(f"Distance {distance} is not expected")

        if self.detector_axis is None:
            if distance is not None:
                offset = self._offset_detector_distance
                distance -= offset
            self.tomo_detectors.source_distance = distance
            # Note: No need to do more, the events will trig the update
            return

        if distance is None:
            self._detector_axis_focal_pos = None
        else:
            pos = self.detector_axis.position if self.detector_axis is not None else 0
            self._detector_axis_focal_pos = pos - distance
        self._send_source_distance(distance)

    def _on_detector_position_changed(self, position, signal=None, sender=None):
        """Callback called when the detector_axis position change"""
        focal_pos = self._detector_axis_focal_pos
        if focal_pos is not None:
            distance = position - focal_pos
        else:
            distance = None
        self._send_source_distance(distance)

    def _on_source_holder_changed(self, distance, signal=None, sender=None):
        if distance is not None:
            offset = self._offset_detector_distance
            distance += offset
        self._send_source_distance(distance)

    def _send_source_distance(self, distance):
        """Called to emit a new distance position"""
        event.send(self, "source_distance", distance)

    _ = """Distance in mm between the sample and the detector
    """

    @property
    def sample_detector_distance(self):
        """Distance in mm between the sample and the detector"""
        tomo_config = self.tomo_detectors.tomo_config
        source_sample_distance = tomo_config.sample_stage.source_distance
        if source_sample_distance is None:
            return None
        source_detector_distance = self.source_distance
        if source_detector_distance is None:
            return None
        return source_detector_distance - source_sample_distance

    @sample_detector_distance.setter
    def sample_detector_distance(self, value: float):
        """Distance in mm between the sample and the detector"""
        tomo_config = self.tomo_detectors.tomo_config
        source_sample_distance = tomo_config.sample_stage.source_distance
        if source_sample_distance is None:
            raise RuntimeError(
                "source_sample_distance have to be defined first to use this API"
            )
        self.source_distance = value + source_sample_distance

    _ = """Distance in mm between the front of detector stage and detector
    """
    _offset_detector_distance = BeaconObject.property_setting(
        name="offset_detector_distance", default=0.0, doc=_
    )

    @property
    def offset_detector_distance(self):
        return self._offset_detector_distance

    @offset_detector_distance.setter
    def offset_detector_distance(self, offset):
        self._offset_detector_distance = offset
        source_holder_distance = self.tomo_detectors.source_distance
        self._send_source_distance(source_holder_distance + offset)

    def _on_source_holder_distance_changed(self, distance):
        if distance is not None:
            offset = self._offset_detector_distance
            distance += offset
        self._send_source_distance(distance)

    def _on_source_distance_changed(self, value):
        self._auto_sample_pixel_size_geometry.detector_distance = value

    def _on_source_sample_distance_changed(self, value):
        self._auto_sample_pixel_size_geometry.sample_distance = value

    _ = """Field of view in millimeter

    This is updated asynchronously from events, so should not be used in scripts
    """
    _field_of_view = BeaconObject.property_setting(name="field_of_view", doc=_)

    @property
    def field_of_view(self):
        return self._field_of_view

    def sync(self):
        """Deprecated: use `sync_hard` instead"""
        self.sync_hard()

    def check_online(self):
        """True if the hardware is online"""
        detector = self.detector
        try:
            proxy = detector.proxy
            proxy.ping()
            proxy.state()
        except Exception:
            return False
        return True

    def check_ready_for_acquisition(self):
        """True if the hardware is ready for a new acquisition"""
        detector = self.detector
        try:
            proxy = detector.proxy
            acq_status = proxy.acq_status
        except Exception:
            return False
        return acq_status == "Ready"

    def sync_hard(self):
        """Read the hardware to synchronize the state of the device"""
        if not self.check_online():
            self._is_online = False
            self._actual_size = None
            self._camera_pixel_size = None
            self._sample_pixel_size = None
            self._field_of_view = None
            self.state = TomoDetectorState.OFFLINE
        else:
            self._is_online = True
            self._actual_size = self.get_actual_camera_size()
            self._camera_pixel_size = self.get_camera_pixel_size()
            self._sync_auto_sample_pixel_size()
            self._update_sample_pixel_size()

    @property
    def is_online(self):
        return self._is_online

    def get_actual_camera_size(self) -> typing.Tuple[int, int]:
        """Returns the actual camera image size"""
        detector = self.detector
        if detector is None:
            raise RuntimeError("The detector is not online")

        # FIXME: This will not work if executed from Daiquiri, because image.roi
        #        is stored in the local BLISS session.
        roi = detector.image.roi
        width = roi[2]
        height = roi[3]
        return int(width), int(height)

    def get_camera_pixel_size(self) -> typing.Tuple[float, float]:
        """Returns the camera pixel size (unbinned) in micrometer"""
        proxy = self._detector_proxy()
        if proxy is None:
            raise RuntimeError("The detector is not online")
        ps = proxy.camera_pixelsize

        # Lima returns pixel size in meter
        # Some cameras was returning returning it in micron (PCO, Andor, Andor3)
        camera_type = proxy.camera_type.lower()
        if camera_type in ["pco", "andor", "andor3"]:
            # Handle patched and non patched Lima cameras
            if ps[0] > 0.1:
                # Sounds like it's already in micron
                pass
            else:
                ps = ps[0] * 1e6, ps[1] * 1e6
        else:
            ps = ps[0] * 1e6, ps[1] * 1e6

        return ps

    @property
    def camera_pixelsize_um(self) -> typing.Tuple[float, float]:
        """Returns the camera pixel size in micrometer

        Deprecated as it accessing to a proxy, prefer using `get_camera_pixel_size`
        """
        return self.get_camera_pixel_size()

    def _on_optic_magnification_changed(self, magnification, signal=None, sender=None):
        """Triggered when the value of the magnification have changed"""
        self._auto_sample_pixel_size_geometry.magnification = magnification

    def _binning(self, proxy) -> typing.Optional[int]:
        try:
            # NOTE: Assume the binning is the same in both direction
            return self.detector.image.binning[0]
        except (AttributeError, DevFailed):
            if proxy is None:
                proxy = self._detector_proxy()
            log_debug(
                self,
                "Fail to reach camera binning from Lima Tango device %s",
                proxy.name,
            )
            return None

    def _detector_proxy(self) -> typing.Optional[typing.Any]:
        detector = self.detector
        if detector is None:
            return None
        return detector._proxy

    def _compute_user_sample_pixel_size(self) -> typing.Optional[float]:
        """Compute the actual sample pixel size from devices.

        An optional magnification can be set to skip access to the optic device
        """
        proxy = self._detector_proxy()
        if proxy is None:
            return None

        pixel_size = self.user_unbinned_sample_pixel_size
        if pixel_size is None:
            return None
        binning = self._binning(proxy)
        if binning is None:
            return None
        pixel_size = pixel_size * binning
        return pixel_size

    def _compute_field_of_view(self, sample_pixel_size) -> typing.Optional[float]:
        if sample_pixel_size is None:
            return None
        image_width = self.detector.image.roi[2]
        return (sample_pixel_size * image_width) / 1000

    def _update_sample_pixel_size(self):
        if self.sample_pixel_size_mode == TomoDetectorPixelSizeMode.AUTO:
            px = self.auto_sample_pixel_size
        else:
            px = self.user_sample_pixel_size

        fov = self._compute_field_of_view(px)
        if self.sample_pixel_size != px:
            self._sample_pixel_size = float_or_none(px)
        if self.field_of_view != fov:
            self._field_of_view = fov
        self._update_state()

    def _set_unmounting(self, is_unmounting: bool):
        if self._is_unmounting == is_unmounting:
            return
        self._is_unmounting = is_unmounting
        self._update_state()

    def _set_mounting(self, is_mounting: bool):
        if self._is_mounting == is_mounting:
            return
        self._is_mounting = is_mounting
        self._update_state()

    def _update_state(self):
        if not self._is_online:
            state = TomoDetectorState.OFFLINE
        if self.sample_pixel_size is None:
            state = TomoDetectorState.INVALID
        elif self._is_mounting:
            state = TomoDetectorState.MOUNTING
        elif self._is_unmounting:
            state = TomoDetectorState.UNMOUNTING
        else:
            state = TomoDetectorState.READY
        self._set_state(state)

    def _set_state(self, state):
        if self.state == state:
            return
        self.state = state

    def __info__(self):
        tomo_config = self.tomo_detectors.tomo_config

        self.sync_hard()

        import tabulate

        def format_state(state: TomoDetectorState):
            if state == TomoDetectorState.READY:
                return GREEN(state.name)
            if state == TomoDetectorState.OFFLINE:
                return RED(state.name)
            return YELLOW(state.name)

        table = []
        table.append(["Role", ""])
        table.append(["state", format_state(self.state)])
        table.append(["detector", info_utils.format_device(self.detector)])
        table.append(["optic", info_utils.format_device(self.optic)])

        table.append([])
        table.append(["distances", ""])
        table.append(
            [
                "   source <-> sample",
                info_utils.format_quantity(
                    tomo_config.sample_stage.source_distance, "mm"
                ),
            ]
        )
        if self.detector_axis is None:
            table.append(
                [
                    "   source <-> holder",
                    info_utils.format_quantity(
                        self.tomo_detectors.source_distance, "mm"
                    ),
                ]
            )
            table.append(
                [
                    "              + offset",
                    info_utils.format_quantity(self.offset_detector_distance, "mm"),
                ]
            )
        else:
            table.append(
                [
                    "   focal position",
                    info_utils.format_quantity(self._detector_axis_focal_pos, "mm"),
                ]
            )
            table.append(
                ["   detector axis", info_utils.format_device(self.detector_axis)]
            )
        table.append(
            [
                "   source <-> detector",
                info_utils.format_quantity(self.source_distance, "mm"),
            ]
        )

        table.append([])
        table.append(["sample pixel size", ""])
        table.append(
            [
                "   active",
                info_utils.format_quantity(self.sample_pixel_size, "um"),
                f"[{self.sample_pixel_size_mode.value.lower()}]",
            ]
        )
        table.append(
            ["   auto", info_utils.format_quantity(self.auto_sample_pixel_size, "um")]
        )
        table.append(
            ["   user", info_utils.format_quantity(self.user_sample_pixel_size, "um")]
        )

        table.append([])
        table.append(
            ["field of view", info_utils.format_quantity(self.field_of_view, "mm")]
        )

        info_str = f"{self.name} tomo detector:\n"
        info_str += tabulate.tabulate(table, headers="firstrow", tablefmt="simple")
        return info_str

    def get_data_axes(self) -> tuple[str, str]:
        """Return the physical axes related to the acquisition data axes.

        The order is the numpy/hdf5 one: 1st slow, 2nd fast.

        The supported axes are:

        - `y`: related to the y-translation of the smaple stage
        - `-y`: related to the reversed y-translation of the smaple stage
        - `z`: related to the z-translation of the smaple stage
        - `-z`: related to the reversed z-translation of the smaple stage
        """
        lima = self.detector
        optic = self.optic
        if optic is not None:
            optic_h_flip, optic_v_flip = optic.image_flipping
        else:
            optic_h_flip = False
            optic_v_flip = False
        # FIXME: Implement the physical detector rotation
        # FIXME: Implement the lima rotation
        det_v_flip, det_h_flip = lima.image.flip
        return (
            "-z" if not (det_v_flip ^ optic_v_flip) else "z",
            "y" if not (det_h_flip ^ optic_h_flip) else "-y",
        )

    def scan_metadata(self):
        from tomo.globals import USE_METADATA_FOR_SCAN

        if not USE_METADATA_FOR_SCAN:
            return None

        # No metadata if the detector is not the active one
        tomo_detectors = self.tomo_detectors
        if tomo_detectors is None:
            return None
        if tomo_detectors.active_detector is not self:
            return None
        tomo_config = tomo_detectors.tomo_config
        if tomo_config is None:
            return None
        if not tomo_config.active:
            return None

        model_mapping = {
            "detector": self.detector,
            "optic": self.optic,
        }
        meta = {
            k: hdf5_device_names(obj)
            for k, obj in model_mapping.items()
            if obj is not None
        }
        meta["sample_pixel_size"] = self.sample_pixel_size
        meta["data_axes"] = self.get_data_axes()
        meta = {k: v for k, v in meta.items() if v is not None}
        return meta

    def _has_mountable_optics(self) -> bool:
        user_optics = self.tomo_detectors.user_optics
        return user_optics is not None

    def setup(self, submenu=False):
        """
        Set-up this controller
        """
        from tomo.helpers.select_dialog2 import select_dialog2

        detector = self.detector
        tomo_config = self.tomo_detectors.tomo_config

        choice = None
        while True:
            value_list = []

            if self.optic:
                value_list.extend(
                    [
                        ("optic", f"Setup optic {self.optic.name}"),
                    ]
                )
            if self._has_mountable_optics():
                value_list.extend(
                    [
                        ("mounted_optic", "Select Mounted Optic"),
                    ]
                )

            value_list.extend(
                [
                    (""),
                    ("scintillator", "Scintillator Setup"),
                    ("pixel", "User Pixel Size Setup"),
                    ("mode", "Acquisition mode"),
                    (""),
                    ("saving", "Image saving"),
                    ("config", "Image configuration"),
                    ("profiles", "Roi profiles"),
                ]
            )
            if self.shutter_is_configurable():
                value_list.extend([("shutter", "Shutter Mode")])
            if self.pixel_rate_is_configurable():
                value_list.extend([("pixelrate", "Pixel Rate")])

            cancel_text = "Back" if submenu else "Cancel"
            choice = select_dialog2(
                title=f"Detector '{self.name}' Setup",
                values=value_list,
                selection=choice,
                cancel_text=cancel_text,
            )
            if choice is None:
                return

            if choice == "optic":
                self.optic_setup(submenu=True)
            elif choice == "scintillator":
                self.scintillator_setup(submenu=True)
            elif choice == "pixel":
                self.user_pixel_size_setup(submenu=True)
            elif choice == "mode":
                self.mode_setup(submenu=True)
            elif choice == "saving":
                lima_saving_parameters_dialog(detector)
            elif choice == "config":
                self.sync_hard()
                if self.is_online:
                    lima_image_dialog(detector)
                    runner = tomo_config.get_runner("count")
                    runner(tomo_config.pars.exposure_time, detector)
                else:
                    dlg1 = UserMsg(label="Detector is off")
                    cancel_text = "Back" if submenu else "Cancel"
                    BlissDialog([[dlg1]], title="Error", cancel_text=cancel_text).show()
            elif choice == "profiles":
                self.sync_hard()
                if self.is_online:
                    self.roi_profiles_setup()
                else:
                    dlg1 = UserMsg(label="Detector is off")
                    cancel_text = "Back" if submenu else "Cancel"
                    BlissDialog([[dlg1]], title="Error", cancel_text=cancel_text).show()
            elif choice == "shutter":
                self.shutter_setup(submenu=True)
            elif choice == "pixelrate":
                self.pixel_rate_setup(submenu=True)
            elif choice == "mounted_optic":
                self.mounted_optic_setup(submenu=True)
            else:
                print(f"Unsupported choice {choice}")

    def apply_acq_mode(self):
        """Apply the acq_mode setting to the Lima detector"""
        if self.acq_mode == "MANUAL":
            return
        self.detector.acquisition.mode = self.acq_mode

    def mode_setup(self, submenu=False):
        """
        User dialog allowing to select acquisition mode (SINGLE or ACCUMULATION)
        and set maximum exposure time per subframe in accumulation.
        If default chain for accumulation/single mode is provided in tomo config,
        set it accordingly.
        """
        detector = self.detector
        acc_mode = self.acq_mode
        modes = ["MANUAL", "SINGLE", "ACCUMULATION"]
        dlg_acc_mode = UserChoice(
            values=[(m, m) for m in modes], defval=modes.index(acc_mode)
        )

        dlg_acc_max_expo_time = UserFloatInput(
            label="Maximum time per frame [s]?",
            defval=detector.accumulation.max_expo_time,
        )

        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[dlg_acc_mode], [dlg_acc_max_expo_time]],
            title=f"{detector.name}: Acquisition Mode",
            cancel_text=cancel_text,
        ).show()
        if ret is False:
            return

        self.acq_mode = ret[dlg_acc_mode]
        detector.accumulation.max_expo_time = ret[dlg_acc_max_expo_time]
        tomo_detectors = self.tomo_detectors
        tomo_config = tomo_detectors.tomo_config
        if (
            self.acq_mode == "ACCUMULATION"
            and tomo_config.default_acc_chain is not None
        ):
            DEFAULT_CHAIN.set_settings(tomo_config.default_acc_chain["chain_config"])
        if self.acq_mode == "SINGLE" and tomo_config.default_single_chain is not None:
            DEFAULT_CHAIN.set_settings(tomo_config.default_single_chain["chain_config"])

    def roi_profiles_setup(self, submenu=False):
        """
        User dialog used to define or remove roi profiles on active detector
        """
        list_dlg = []
        detector = self.detector

        if len(detector.roi_profiles.counters) > 0:
            list_dlg.append([UserMsg(label="Rois currently defined:")])
            for roi in detector.roi_profiles.get_rois():
                roi_name = detector.roi_profiles.counters[
                    detector.roi_profiles.get_rois().index(roi)
                ].name
                list_dlg.append(
                    [
                        UserMsg(
                            label=f"Roi name: {roi_name}      Line selected in the image: {roi.y}"
                        )
                    ]
                )
        else:
            list_dlg.append([UserMsg(label="No rois currently defined")])
        list_dlg.append([UserCheckBox(label="Add a roi")])
        list_dlg.append([UserCheckBox(label="Remove a roi")])
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            list_dlg, title="Roi Profiles Setup", cancel_text=cancel_text
        ).show()

        if ret:
            if ret[list_dlg[-2][0]]:
                dlg_image_roi = UserMsg(
                    label=f"Image roi currently defined: {detector.image.roi}"
                )
                dlg_name = UserInput(label="Enter roi name")
                dlg_y = UserIntInput(
                    label="Enter the line you want to acquire in the image"
                )
                ret = BlissDialog(
                    [[dlg_image_roi], [dlg_name], [dlg_y]],
                    title="Add Roi Profiles Setup",
                ).show()
                if ret:
                    detector.roi_profiles[ret[dlg_name]] = [
                        0,
                        ret[dlg_y],
                        detector.image.width,
                        1,
                    ]
            elif ret[list_dlg[-1][0]]:
                list_dlg = []
                if len(detector.roi_profiles.counters) > 0:
                    list_dlg.append([UserMsg(label="Rois currently defined:")])
                    for roi in detector.roi_profiles.get_rois():
                        roi_name = detector.roi_profiles.counters[
                            detector.roi_profiles.get_rois().index(roi)
                        ].name
                        list_dlg.append(
                            [
                                UserMsg(
                                    label=f"Roi name: {roi_name}      Line selected in the image: {roi.y}"
                                )
                            ]
                        )
                    list_dlg.append(
                        [
                            UserInput(
                                label="Enter the name of the roi you want to remove"
                            )
                        ]
                    )
                    list_dlg.append([UserCheckBox(label="Remove all rois")])
                    ret = BlissDialog(
                        list_dlg, title="Remove Roi Profiles Setup"
                    ).show()
                    if ret:
                        if ret[list_dlg[-1][0]]:
                            detector.roi_profiles.clear()
                        else:
                            detector.roi_profiles.remove(ret[list_dlg[-2][0]])

    def pixel_rate_is_configurable(self):
        """
        Check if detector is edge and have several pixel rates available to display pixel rate setup
        """
        detector = self.detector
        if detector.camera_type == "Pco" and len(detector.camera.pixel_rate_valid_values) > 1:
            return True
        return False

    def pixel_rate_setup(self, submenu=False):
        """
        User dialog used to change pixel rate on active detector.
        Specific to pco edge
        """
        detector = self.detector
        current_pixel_rate = detector.camera.pixel_rate
        pixel_rates = []
        for pixel_rate in detector.camera.pixel_rate_valid_values:
            pixel_rates.append((pixel_rate, str(pixel_rate)))
        dlg_pixel_rate = UserChoice(values=pixel_rates, defval=pixel_rates.index((current_pixel_rate,str(current_pixel_rate))))
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[dlg_pixel_rate]],
            title=f"{detector.name}: Pixel Rate",
            cancel_text=cancel_text,
        ).show()
        if ret is False:
            return

        detector.camera.pixel_rate = ret[dlg_pixel_rate]

    def shutter_is_configurable(self):
        """
        Check if detector is edge 5.5 to display shutter setup
        """
        detector = self.detector
        if detector.camera_type == "Pco" and "5.5" in detector.camera.cam_name:
            return True
        return False

    def shutter_setup(self, submenu=False):
        """
        User dialog used to change shutter type (global/rolling) on active detector.
        Specific to pco edge 5.5
        """
        detector = self.detector
        current_mode = detector.camera.rolling_shutter
        modes = [(1, "ROLLING SHUTTER")]
        if detector.camera.rolling_shutter_info.find("global") != -1:
            modes.append((2, "GLOBAL SHUTTER"))
        if detector.camera.rolling_shutter_info.find("globalReset") != -1:
            modes.append((4, "GLOBAL RESET"))
        values = [mode[0] for mode in modes]
        dlg_shutter_mode = UserChoice(values=modes, defval=values.index(current_mode))
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[dlg_shutter_mode]],
            title=f"{detector.name}: Shutter Mode",
            cancel_text=cancel_text,
        ).show()
        if ret is False:
            return

        detector.camera.rolling_shutter = ret[dlg_shutter_mode]
        detector.accumulation.max_expo_time = 0
        self.acq_mode = "SINGLE"
        tomo_detectors = self.tomo_detectors
        tomo_config = tomo_detectors.tomo_config
        if tomo_config.default_single_chain is not None:
            DEFAULT_CHAIN.set_settings(tomo_config.default_single_chain["chain_config"])
        dlg1 = UserMsg(
            label="Please wait 30s, then restart lima server and wait until server is running"
        )
        cancel_text = "Back" if submenu else "Cancel"
        BlissDialog([[dlg1]], cancel_text=cancel_text).show()
        return

    def optic_setup(self, submenu=False):
        """
        Calls the setup (Bliss dialogue) of the optic object

        :param detector: Detector name or detector object
        :type detector: string or object
        """
        optic = self.optic
        if optic is None:
            dlg1 = UserMsg(label="No optic defined")
            cancel_text = "Back" if submenu else "Cancel"
            BlissDialog([[dlg1]], title="Error", cancel_text=cancel_text).show()
            return
        optic.optic_setup(submenu=submenu)

    def scintillator_setup(self, submenu=False):
        """
        Calls the setup (Bliss dialogue) for the scintillator from the optic object

        :param detector: Detector name or detector object
        :type: string or object
        """
        optic = self.optic
        if optic is None:
            dlg1 = UserMsg(label="No optic defined")
            cancel_text = "Back" if submenu else "Cancel"
            BlissDialog([[dlg1]], title="Error", cancel_text=cancel_text).show()
            return
        optic.scintillator_setup(submenu=submenu)

    def user_pixel_size_setup(self, submenu=False):
        """
        Calls the setup (Bliss dialog) to define the
        unbinned image pixel size manually.

        :param detector: Detector name or detector object
        :type detector: string or object
        """
        user_unbinned_pixel_size = self.user_unbinned_sample_pixel_size
        calc_unbinned_pixel_size = self.auto_unbinned_sample_pixel_size
        user_mode = self.sample_pixel_size_mode.value == "USER"

        dlg1 = UserMsg(
            label="Calculated unbinned image pixel size in um: {}".format(
                calc_unbinned_pixel_size
            )
        )
        user_unbinned_pixel_size_str = (
            "" if user_unbinned_pixel_size is None else user_unbinned_pixel_size
        )
        dlg2 = UserOptionalFloatInput(
            label="User unbinned image pixel size in um:      ",
            defval=user_unbinned_pixel_size_str,
        )
        dlg3 = UserCheckBox(label="Apply user defined pixel size?", defval=user_mode)
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[dlg1], [dlg2], [dlg3]], title="Pixel Size Setup", cancel_text=cancel_text
        ).show()

        if ret is not False:
            user_unbinned_pixel_size = dlg2.input_to_value(ret[dlg2])
            user_mode = ret[dlg3]
            self.sample_pixel_size_mode = "user" if user_mode else "auto"
            self.user_unbinned_sample_pixel_size = user_unbinned_pixel_size

    def mounted_optic_setup(self, submenu=False):
        """
        User dialog which allows to specify the associated optic for this detector.

        A user optics list in the yml configuration file can offer a list of possible optics.
        It is always possible to specifiy another optic.
        """
        mounted_optic = self.optic
        user_optics = self.tomo_detectors.user_optics
        if user_optics is None:
            dlg1 = UserMsg(label="No user optics defined")
            cancel_text = "Back" if submenu else "Cancel"
            BlissDialog([[dlg1]], title="Error", cancel_text=cancel_text).show()
            return

        from tomo.helpers.select_dialog2 import select_dialog2

        value_list = []
        for optic in user_optics:
            if isinstance(optic, str):
                optic = static.get_config().get(optic)
            label = optic.name
            if optic is mounted_optic:
                label += " (active)"
            value_list.append((optic, label))

        choice = mounted_optic
        cancel_text = "Back" if submenu else "Cancel"
        choice = select_dialog2(
            title="User Optic Setup",
            values=value_list,
            selection=choice,
            cancel_text=cancel_text,
        )
        if choice is None:
            return
        self.optic = choice
