from .flat_motion import FlatMotion
from bliss.common import deprecation


TomoRefMot = FlatMotion  # noqa
"""Deprecated in bliss-tomo 2.6.

Prefer to use FlatMotion instead.
"""

deprecation.deprecated_warning(
    "Class",
    "tomo.controllers.reference.TomoRefMot",
    replacement="tomo.controllers.flat_motion.FlatMotion",
    since_version="2.6.0",
)
