# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
import typing
import time
import enum
import numbers
import tabulate
import numpy
import contextlib
from bliss.common import event
from bliss.common.axis import Axis
from bliss.config.beacon_object import (
    BeaconObject,
    EnumProperty,
)
from bliss.common.utils import typecheck
from bliss.common.logtools import log_debug
from .tomo_detector import TomoDetectorPixelSizeMode
from bliss.common.utils import BOLD, YELLOW, RED, GREEN
from tomo.helpers.beacon_object_helper import check_unexpected_keys

if typing.TYPE_CHECKING:
    from .tomo_config import TomoConfig
    from .tomo_detector import TomoDetector


class HolotomoState(enum.Enum):
    READY = "READY"
    MOVING = "MOVING"
    STABILIZING = "STABILIZING"
    INVALID = "INVALID"
    INVALID_DETECTOR_BINNING_CHANGED = "INVALID_DETECTOR_BINNING_CHANGED"
    INVALID_DETECTOR_DISTANCE_CHANGED = "INVALID_DETECTOR_DISTANCE_CHANGED"
    INVALID_DETECTOR_CHANGED = "INVALID_DETECTOR_CHANGED"
    UNKNOWN = "UNKNOWN"


def NOOP(x):
    return x


class HolotomoDistance(typing.NamedTuple):
    """Describe an holotomo distance"""

    position: float
    """Position in the x-axis, in mm"""

    z1: float
    """Distance from the source, in mm"""

    pixel_size: float
    """Sample pixel size of this distance with the active tomo detector, in um
    """


class Holotomo(BeaconObject):
    """
    Hold the setup of the beamline related to the holotomo.

    It contains

    - The expected user pixel size
    - The related distances for the scan
    - Cached information to be exposed by Daiquiri
    """

    def __init__(self, name: str, config: dict[str, typing.Any]):
        BeaconObject.__init__(self, config=config, name=name, share_hardware=False)
        log_debug(self, "__init__() entering")

        check_unexpected_keys(self, config, ["tomo", "ratios", "offsets", "calcmode"])

        log_debug(self, "__init__() parameters")
        self.__tomo_config = config["tomo"]

        self.__ratios: list[float] = config["ratios"]
        self.__offsets: list[float] = config["offsets"]
        self.__calcmode: str = config["calcmode"]
        assert self.__calcmode in ["ratio", "offset"]

        event.connect(self, "nb_distances", self._on_nb_distances_changed)

        if self.state == HolotomoState.UNKNOWN:
            self.state = HolotomoState.READY

        self.__active_detector: TomoDetector | None = None
        detectors = self.__tomo_config.detectors
        event.connect(detectors, "active_detector", self._on_active_detector_changed)
        active_detector = detectors.active_detector
        if active_detector:
            # At startup we must not invalidate the distances if not needed
            if active_detector.name == self._detector_name:
                self.__active_detector = active_detector
                self._connect_detector(active_detector)
            else:
                self._on_active_detector_changed(active_detector)

        # Have to be called when everything was initialized
        self.__tomo_config._set_holotomo(self)

        log_debug(self, "__init__() leaving")

    def __close__(self):
        self.__tomo_config = None
        active_detector = self.__active_detector
        if active_detector is not None:
            self.__active_detector = None
            self._disconnect_detector(active_detector)

    @property
    def tomo_config(self) -> TomoConfig:
        """Returns the TomoConfig object which own this TomoDetectors"""
        if self.__tomo_config is None:
            raise RuntimeError("Tomo structure not properly setup")
        return self.__tomo_config

    state = EnumProperty(
        name="state",
        enum_type=HolotomoState,
        default=HolotomoState.UNKNOWN,
        unknown_value=HolotomoState.UNKNOWN,
        doc="State of the holotomo",
    )

    nb_distances = BeaconObject.property_setting(
        name="nb_distances", default=4, doc="Number of distances per holotomo"
    )

    _ = """Stored to invalid the cache"""
    _detector_name = BeaconObject.property_setting(
        name="_detector_name", default="", doc=_
    )

    _ = """Sleep time used after a move"""
    settle_time = BeaconObject.property_setting(name="settle_time", default=0.0, doc=_)

    _ = """Requested pixel size, um"""
    _pixel_size = BeaconObject.property_setting(name="pixel_size", default=None, doc=_)

    @property
    def pixel_size(self) -> numbers.Real | None:
        return self._pixel_size

    @pixel_size.setter
    def pixel_size(self, pixel_size: numbers.Real | None):
        if pixel_size is None:
            self.clear()
            return
        self.compute_from_pixel_size(pixel_size)

    _ = """Description of distances actually setup"""
    _distances = BeaconObject.property_setting(name="distances", default=[], doc=_)

    @property
    def distances(self) -> list[HolotomoDistance]:
        return self._distances

    def _invalidate_distances(self, new_state: HolotomoState):
        if self.distances is None:
            return
        self.state = new_state

    def _on_active_detector_changed(self, detector_name, *args, **kwargs):
        """Called when the active detector was changed"""
        detector = self.tomo_config.detectors.active_detector
        if detector is self.__active_detector:
            return
        self._invalidate_distances(HolotomoState.INVALID_DETECTOR_CHANGED)
        if self.__active_detector is not None:
            self._disconnect_detector(self.__active_detector)
        self.__active_detector = detector
        if self.__active_detector is not None:
            self._detector_name = detector.name
            self._connect_detector(detector)
        else:
            self._detector_name = ""

    def _connect_detector(self, detector: TomoDetector):
        event.connect(
            detector.detector.image._image_params,
            "binning",
            self._on_detector_binning_changed,
        )
        event.connect(detector, "source_distance", self._on_detector_distance_changed)

    def _disconnect_detector(self, detector: TomoDetector):
        event.disconnect(
            detector.detector.image._image_params,
            "binning",
            self._on_detector_binning_changed,
        )
        event.disconnect(
            detector, "source_distance", self._on_detector_distance_changed
        )

    def _on_detector_distance_changed(self, value, *args, **kwargs):
        self._invalidate_distances(HolotomoState.INVALID_DETECTOR_DISTANCE_CHANGED)

    def _on_detector_binning_changed(self, value, *args, **kwargs):
        self._invalidate_distances(HolotomoState.INVALID_DETECTOR_BINNING_CHANGED)

    def _get_image_pixel_size(self, detector: TomoDetector):
        """Returns the pixel size of the image"""
        camera_pixel_size = detector.get_camera_pixel_size()[0]
        image = detector.detector.image
        return camera_pixel_size * image.binning[0]

    def _get_sample_position_from_pixel_width(self, pixel_width: float):
        """Compute the the needed position of the sample for an expected pixel width"""
        tomo_config = self.tomo_config
        detector = tomo_config.detectors.active_detector

        image_pixel_size = self._get_image_pixel_size(detector)
        coef = pixel_width / image_pixel_size
        coef = coef * detector.optic.magnification

        distance = coef * detector.source_distance
        sx_value = distance + tomo_config.sample_stage._x_axis_focal_pos

        return sx_value

    def _on_nb_distances_changed(self, value, *args, **kwargs):
        """Called when the nb of distances was changed"""
        self.validate()

    def _get_pixel_width_from_sample_distance(self, sample_distance):
        tomo_config = self.tomo_config
        detector = tomo_config.detectors.active_detector
        coef = (
            sample_distance / detector.source_distance
        ) / detector.optic.magnification
        # calculate the pixel size
        image_pixel_size = self._get_image_pixel_size(detector)
        pixel_size = coef * image_pixel_size
        return pixel_size

    def _compute_first_z1_from_pixel_size(self, pixel_size: float) -> float:
        tomo_config = self.tomo_config
        x_axis_focal_pos = tomo_config.sample_stage._x_axis_focal_pos
        return self._get_sample_position_from_pixel_width(pixel_size) - x_axis_focal_pos

    @typecheck
    def compute_from_first_distance(self, distance: numbers.Real):
        """Update the distances based on the first distance"""
        pixel_size = self._get_pixel_width_from_sample_distance(distance)
        self.pixel_size = pixel_size

    @typecheck
    def compute_from_pixel_size(self, pixel_size: numbers.Real):
        """Update the distances based on the first distance"""
        ps = float(pixel_size)

        def iter_z1():
            z1_first = self._compute_first_z1_from_pixel_size(ps)
            calcmode = self.__calcmode
            if calcmode == "ratio":
                for i in range(self.nb_distances):
                    if i >= len(self.__ratios):
                        break
                    z1 = z1_first * self.__ratios[i]
                    yield z1
            elif calcmode == "offset":
                for i in range(self.nb_distances):
                    if i >= len(self.__offsets):
                        break
                    z1 = z1_first + self.__offsets[i]
                    yield z1
            else:
                raise ValueError(f"calcmode '{calcmode}' is unsupported")

        distances: list[HolotomoDistance] = []
        tomo_config = self.tomo_config
        x_axis_focal_pos = tomo_config.sample_stage._x_axis_focal_pos
        detector = tomo_config.detectors.active_detector
        assert detector.sample_pixel_size_mode == TomoDetectorPixelSizeMode.AUTO

        for z1 in iter_z1():
            position = z1 + x_axis_focal_pos
            px = self._get_pixel_width_from_sample_distance(z1)
            distances.append(
                HolotomoDistance(
                    z1=z1,
                    position=position,
                    pixel_size=px,
                )
            )

        self._pixel_size = pixel_size
        self._distances = distances
        self.state = HolotomoState.READY

    @property
    def first_distance(self) -> float | None:
        distances = self.distances
        if len(distances) == 0:
            return None
        return distances[0].z1

    @first_distance.setter
    def first_distance(self, distance: numbers.Real | None):
        if distance is None:
            self.clear()
            return

        self.compute_from_first_distance(distance)

    @property
    def x_axis(self) -> Axis:
        """Return the x-axis used to move the holotomo motors

        By default, is the tomo config defines a `tracked_x_axis`, then this
        axis is used. Else the `x_axis` is used.
        """
        tomo_config = self.tomo_config
        tracked = tomo_config.tracked_x_axis
        if tracked is not None:
            return tracked
        x_axis = tomo_config.x_axis
        if x_axis is None:
            raise RuntimeError(
                f"No configured x-axis found in {tomo_config.name}. Holotomo can be used"
            )
        return x_axis

    @property
    def distance(self) -> int | None:
        distances = self.distances
        if len(distances) == 0:
            return None
        x_axis = self.x_axis
        position = x_axis.position
        tolerance = 0.01
        for id, d in enumerate(distances):
            if numpy.abs(position - d.position) < tolerance:
                return id + 1
        return None

    @contextlib.contextmanager
    def _move_context(self):
        """Do everything needed before and after a move"""
        state = self.state
        if state != HolotomoState.READY:
            raise ValueError(f"{self.name} is not READY")

        try:
            self.state = HolotomoState.MOVING
            yield
            settle_time = self.settle_time
            if settle_time > 0:
                self.state = HolotomoState.STABILIZING
                time.sleep(settle_time)
        finally:
            self.state = HolotomoState.READY

    def _get_position_from_distance_id(self, distance: numbers.Integral | str) -> float:
        """
        Return the sample axis position from a named distance.

        Arguments:
            distance: The number of the distance (1 for the first distance)
                or `"dist1"`
                or `"focal"` to move to the focal position (distance = 0mm)
        """
        if distance == "focal":
            tomo_config = self.tomo_config
            return tomo_config.sample_stage._x_axis_focal_pos
        if isinstance(distance, str):
            if distance.startswith("dist"):
                distance = distance[4:]
        d = int(distance)
        if d <= 0:
            raise ValueError(f"The first distance is identified by 1. Found '{d}'.")
        return self.distances[d - 1].position

    @typecheck
    def move(self, distance: typing.Union[numbers.Integral, str]):
        """Move to a holo distance with the BLISS standard `mv` function.

        It takes care of the stabilization time.

        Arguments:
            distance: The numberof the distance (1 for the first distance)
                or `"focal"` to move to the focal position (distance = 0mm)
        """
        with self._move_context():
            x_axis = self.x_axis
            from bliss.common.standard import move

            position = self._get_position_from_distance_id(distance)
            move(x_axis, position)

    @typecheck
    def umove(self, distance: typing.Union[numbers.Integral, str]):
        """Move to a holo distance with the BLISS standard `umv` function.

        It takes care of the stabilization time.

        Arguments:
            distance: The numberof the distance (1 for the first distance)
                or `"focal"` to move to the focal position (distance = 0mm)
        """
        with self._move_context():
            x_axis = self.x_axis
            from bliss.shell.standard import umv

            position = self._get_position_from_distance_id(distance)
            umv(x_axis, position)

    @typecheck
    def clear(self):
        """Clear the actual distances"""
        self._pixel_size = None
        self._distances = []
        self.state = HolotomoState.INVALID

    @typecheck
    def validate(self):
        """Re-generate the distances with the same pixel size in case the
        distances have changed"""
        pixel_size = self.pixel_size
        self.clear()
        if pixel_size is None:
            return
        self.pixel_size = pixel_size

    def __info__(self) -> str:
        tomo_config = self.tomo_config
        detector = tomo_config.detectors.active_detector
        lima = detector.detector
        x_axis = self.x_axis

        state = self.state
        distance = self.distance

        table = []
        if state.name.startswith("INVALID"):
            table.append(["state", RED(state.name), ""])
            if state.name.startswith("INVALID_"):
                table.append(
                    ["", f"{YELLOW('TIPS')}: Use '.validate()' to regenerate", ""]
                )
                table.append(["", "      the distances with the same pixel size", ""])
                table.append([])
        elif state == HolotomoState.READY:
            table.append(["state", GREEN(state.name), ""])
        else:
            table.append(["state", YELLOW(state.name), ""])
        table.append(["pixel size", self.pixel_size, "um"])
        table.append(["settle time", self.settle_time, "s"])
        table.append(
            [
                "actual distance",
                YELLOW("misaligned") if distance is None else distance,
                "",
            ]
        )
        table.append(
            [
                f"x-axis ({x_axis.name})",
                x_axis.axis_rounder(x_axis.position),
                x_axis.unit,
            ]
        )
        table.append([])

        table.append(["distances"])
        tdistances: list[typing.Any] = []
        if len(self.distances) > 0:
            tdistances.append(
                [
                    "#",
                    "distance [mm]",
                    f"position [{x_axis.unit}]",
                    "pixel size [um]",
                    "fov [mm]",
                    "",
                ]
            )
            detector_size = lima.image.roi[2:4]

            for id, d in enumerate(self.distances):
                fov = d.pixel_size * detector_size[0] * 1e-3
                selected = id + 1 == distance
                C = BOLD if selected else NOOP
                dist = [
                    C(f"{id + 1}"),
                    C(f"{d.z1:0.3f}"),
                    C(x_axis.axis_rounder(d.position)),
                    C(f"{d.pixel_size:0.3f}"),
                    C(f"{fov:0.3f}"),
                ]
                if selected:
                    dist.append(BOLD("<--"))
                else:
                    dist.append("")
                tdistances.append(dist)
        else:
            table.append(["   No distances"])
        info_str = "Holotomo:\n\n"
        info_str += tabulate.tabulate(table, tablefmt="plain", floatfmt="0.3f")
        info_str += "\n"
        info_str += tabulate.tabulate(
            tdistances, headers="firstrow", disable_numparse=True
        )
        return info_str
