# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
from typing import TYPE_CHECKING
import math
import typing
import enum
import gevent
import weakref

from tabulate import tabulate

from bliss.common import event
from bliss.config.beacon_object import (
    BeaconObject,
    EnumProperty,
    ConfigObjListPropertySetting,
)
from bliss.common.logtools import log_debug

from tomo.parameters import TomoParameters
from bliss.controllers.lima.lima_base import Lima
from .tomo_detector import TomoDetector
from tomo.helpers.beacon_object_helper import check_unexpected_keys
from tomo.helpers import info_utils
from bliss.common.utils import RED
from bliss.common import deprecation
from bliss.common.hook import MotionHook


if TYPE_CHECKING:
    from .tomo_config import TomoConfig


class TomoDetectorsState(enum.Enum):
    UNKNOWN = "UNKNOWN"
    READY = "READY"
    MOUNTING = "MOUNTING"


class TomoDetectors(BeaconObject, TomoParameters):
    """
    Hold the list of available tomo detectors plus the actual active tomo
    detector.

    The class defines the optic installed on a detector and offers an interface to calculate the
    combined image pixel size and field of view of the detector with its optic.

    The object can hold a list of detector and their associated optics. All methods of of the object will take
    the detector name (or the detector object) as input.

    It is possible to apply the specified image flipping from the optic to the detector and to
    correct the calculated image pixel size manually.

    **Example yml file**::

        name: tomo_detectors
        plugin: bliss
        class: TomoDetectors
        package: tomo.optic
        detectors:
           - $det1
           - $det2
           - $det3
    """

    def __init__(self, name, config):
        BeaconObject.__init__(self, config=config, name=name, share_hardware=False)
        log_debug(self, "__init__() entering")

        log_debug(self, "__init__() parameters")

        check_unexpected_keys(
            self,
            config,
            ["detectors", "detector_axis", "active_detector", "user_optics"],
        )

        self._detectors = []
        self.__tomo_config = None
        # Pixel sizes defined by users and the user mode are kept as parameters
        def_val = [float("NAN"), False]
        param_defaults = {}

        self.detector_axis = config.get("detector_axis")
        if self.detector_axis is not None:
            event.connect(
                self.detector_axis, "position", self._on_detector_position_changed
            )

        for detector in config["detectors"]:
            if detector.detector_axis and self.detector_axis:
                raise RuntimeError(
                    "detector axis should be configured either on each tomo detector either on tomo detectors not both"
                )
            limadet = detector.detector
            param_defaults[limadet.name] = def_val
            self._detectors.append(detector)
        self.detectors = self._detectors

        # Get a list of optics to choose from when defined in the configuration file
        self.user_optics: list[str] | None = config.get("user_optics")

        # Initialise the TomoParameters class
        TomoParameters.__init__(self, f"optic:{name}", param_defaults)
        # print (self.parameters)

        self.motion_hooks: list[MotionHook] = []

        self.state = TomoDetectorsState.READY
        log_debug(self, "__init__() leaving")

    def _set_tomo_config(self, tomo_config: TomoConfig):
        assert self.__tomo_config is None
        self.__tomo_config = weakref.ref(tomo_config)
        for detector in self._detectors:
            detector._set_tomo_detectors(self)

    @property
    def tomo_config(self) -> TomoConfig:
        """Returns the TomoConfig object which own this TomoDetectors"""
        if self.__tomo_config is None:
            raise RuntimeError("Tomo structure not properly setup")
        return self.__tomo_config()

    _ = """State of the detector"""
    state = EnumProperty(
        name="state",
        enum_type=TomoDetectorsState,
        default=TomoDetectorsState.UNKNOWN,
        unknown_value=TomoDetectorsState.UNKNOWN,
        doc=_,
    )

    _ = """List of defined detectors"""
    detectors = ConfigObjListPropertySetting("detectors", doc=_)

    _ = """
    Active detector involved in the tomo acquisition
    """
    active_detector = BeaconObject.config_obj_property_setting(
        name="active_detector", default=None, doc=_
    )

    def _normalize_tomo_detector(self, device):
        """Returns a tomodetector object from different source"""
        if device is None:
            return None
        if isinstance(device, TomoDetector):
            return device
        if isinstance(device, Lima):
            tomodetector = self.get_tomo_detector(device)
            if tomodetector is None:
                raise TypeError(f"No TomoDetector found for device {device.name}")
            return tomodetector
        raise TypeError(f"Unsupported type {type(device)}")

    @active_detector.setter
    def set_active_detector(self, device):
        return self._normalize_tomo_detector(device)

    _detector_axis_focal_pos = BeaconObject.property_setting(
        name="detector_axis_focal_pos",
        default=None,
        doc="Position of the focal point into the detector axis",
    )

    @property
    def source_distance(self) -> float | None:
        """
        Return the distance from the source in mm
        """
        focal_pos = self._detector_axis_focal_pos
        if focal_pos is None:
            return None
        pos = self.detector_axis.position if self.detector_axis is not None else 0
        return pos - focal_pos

    @source_distance.setter
    def source_distance(self, distance: float | None):
        """
        Set the distance from the source in mm
        """
        if distance is not None and distance < 0:
            raise RuntimeError(f"Distance {distance} is not expected")
        if distance is None:
            self._detector_axis_focal_pos = None
        else:
            pos = self.detector_axis.position if self.detector_axis is not None else 0
            self._detector_axis_focal_pos = pos - distance
        self._send_source_distance(distance)

    def _on_detector_position_changed(self, position, signal=None, sender=None):
        """Callback called when the detector_axis position change"""
        focal_pos = self._detector_axis_focal_pos
        if focal_pos is not None:
            distance = position - focal_pos
        else:
            distance = None
        self._send_source_distance(distance)

    def _send_source_distance(self, distance):
        """Called to emit a new source distance"""
        event.send(self, "source_distance", distance)

    def _execute_pre_move_hooks(self):
        """
        Allows to do an action before detector mounting
        """
        for h in self.motion_hooks:
            h.pre_move([self])

    def _execute_post_move_hooks(self):
        """
        Allows to do an action after detector mounting
        """
        for h in self.motion_hooks:
            h.post_move([self])

    def mount(self, device, wait=True):
        """Procedure to mount a detector as the actual one

        Attribute:
            device: The new device to mount
            wait: If true the call will be blocking until the end of the procedure
        """
        device = self._normalize_tomo_detector(device)

        actual = self.active_detector
        if actual is device:
            return

        if self.state != TomoDetectorsState.READY:
            raise ValueError(
                f"Mount aborted. Device state not ready (found {self.state})"
            )

        def task():
            self._execute_pre_move_hooks()
            if type(self)._mount is not TomoDetectors._mount:
                # Mounting function was inherited
                self.state = TomoDetectorsState.MOUNTING
                self.active_detector = None
                unmount_detector = self.active_detector
                mount_detector = device
                if unmount_detector:
                    unmount_detector._set_unmounting(True)
                if mount_detector:
                    mount_detector._set_mounting(True)

                try:
                    self._mount(
                        unmount_detector=unmount_detector, mount_detector=mount_detector
                    )
                    self.active_detector = device
                finally:
                    if unmount_detector:
                        unmount_detector._set_unmounting(False)
                    if mount_detector:
                        mount_detector._set_mounting(False)
                    self.state = TomoDetectorsState.READY
            else:
                # Switch the detector anyway as it was mounted manually?
                self.active_detector = device
            self._execute_post_move_hooks()

        p = gevent.spawn(task)
        if wait:
            p.join()
            p.get()  # trig exception, in case

    def _mount(
        self,
        unmount_detector: typing.Optional[TomoDetector],
        mount_detector: typing.Optional[TomoDetector],
    ):
        """Inherit this method to implement the way to mount detectors

        Argument:
            unmount_detector: Detector to unmount (can be None)
            mount_detector: Detector to mount (can be None)
        """
        raise NotImplementedError("This have to be implemented")

    def __info__(self):
        info_str = "Tomo detectors:\n\n"

        table_list = []
        table_list.append(["Role", ""])
        if self.detector_axis is not None:
            table_list.append(
                [
                    "focal position",
                    info_utils.format_quantity(self._detector_axis_focal_pos, "mm"),
                ]
            )
            table_list.append(
                ["detector axis", info_utils.format_device(self.detector_axis)]
            )
        table_list.append(
            [
                "source <-> detectors",
                info_utils.format_quantity(self.source_distance, "mm"),
            ]
        )
        table_list.append(
            [
                "active detector",
                info_utils.format_device(self.active_detector),
            ]
        )
        table = tabulate(table_list, headers="firstrow", tablefmt="simple")
        info_str += table

        table_list = [
            [
                "Tomo detectors",
                "Type",
                "Pixel size [um]",
                "Mode",
                "Optics",
                "Magnification",
            ]
        ]
        for detector in self._detectors:
            limadet = detector.detector

            def format_type():
                try:
                    try:
                        limadet._proxy.ping()
                    except Exception:
                        return RED("Offline")
                    return limadet._proxy.camera_type
                except Exception:
                    return RED("Error")

            table_list.append(
                [
                    info_utils.format_active(
                        detector.name, detector is self.active_detector
                    ),
                    format_type(),
                    info_utils.format_quantity(detector.sample_pixel_size, "um"),
                    detector.sample_pixel_size_mode.name,
                    info_utils.format_device(detector.optic),
                    info_utils.format_magnification(detector.optic),
                ]
            )

        table = tabulate(
            table_list,
            headers="firstrow",
            tablefmt="simple",
        )
        info_str += "\n\n"
        info_str += table
        return info_str

    def get_detector_name(self, detector: typing.Union[str, Lima, TomoDetector]) -> str:
        """
        Returns the BLISS lima detector name from the given detector.

        Arguments:
            detector: Detector name or detector object
        """
        if isinstance(detector, str):
            return detector
        if isinstance(detector, Lima):
            return detector.name
        if isinstance(detector, TomoDetector):
            return detector.detector.name
        raise TypeError(f"Unsupported type {type(detector)}")

    def get_tomo_detector(
        self, detector: typing.Union[str, Lima, TomoDetector]
    ) -> typing.Optional[TomoDetector]:
        """Returns a `TomoDetector` from a detector object

        Arguments:
            detector: Detector name or detector object

        Returns:
            The expected tomo detector, else None if not found
        """
        limadet_name = self.get_detector_name(detector)
        for d in self._detectors:
            limadet = d.detector
            if limadet.name == limadet_name:
                return d
        return None

    def get_detector(self, detector) -> typing.Optional[Lima]:
        """
        Returns the Lima detector object associated with the given detector name

        Arguments:
            detector: Detector name or detector object

        Returns:
            The expected detector, else None if not found
        """
        detector = self.get_tomo_detector(detector)
        if detector is None:
            return None
        return detector.detector

    def get_optic(self, detector):
        """
        Returns the optic object associated with the given detector

        Arguments:
            detector: Detector name or detector object

        Returns:
            The expected optic, else None if not found
        """
        detector = self.get_tomo_detector(detector)
        if detector is None:
            return None
        return detector.optic

    def set_optic(self, detector, optic):
        """
        Sets the optic object associated with the given detector

        Arguments:
            detector: Detector name or detector object
            optic   : Optic name or optic object
                      if optic == None, resets to the default optic specified in the configuration file

        Returns:
            Runtime error if detector is not found
        """
        deprecation.deprecated_warning(
            "Function",
            "set_optic",
            replacement="tomodetector.optic=",
            since_version="2.6",
        )

        detector = self.get_tomo_detector(detector)
        if detector is None:
            raise RuntimeError("Detector not defined!\n")
        detector.optic = optic

    def get_image_pixel_size(self, detector):
        """
        Calculate the image pixel size from the unbinned pixel size and the
        current detector binning.

        Prefer using `TomoDetector.sample_pixel_size`

        :param detector: Detector name or detector object
        :type detector: string or object

        :return: Image pixel size
        :rtype: float
        """
        tomo_detector = self.get_tomo_detector(detector)
        assert tomo_detector is not None
        tomo_detector.sync_hard()
        return tomo_detector.sample_pixel_size

    def apply_image_flipping(self, detector):
        """
        Apply the defined optic image flipping to the detector

        :param detector: Detector name or detector object
        :type detector: string or object
        """
        # get detector and optic objects
        detector = self.get_tomo_detector(detector)
        det = detector.detector
        optic = detector.optic

        # if detecor and optic are defined, set image flipping from optic
        if det is not None and optic is not None:
            det.image.flip = optic.image_flipping
        else:
            raise RuntimeError("Detector or Optic not defined!\n")

    def get_field_of_view(self, detector):
        """
        Calculate the detector field of view in mm
        from the image pixel size and the image size.

        :param detector: Detector name or detector object
        :type detector: string or object
        :return: Detector field of view in mm
        :rtype: float
        """
        tomo_detector = self.get_tomo_detector(detector)
        assert tomo_detector is not None
        tomo_detector.sync_hard()
        return tomo_detector.field_of_view

    def apply_user_pixel_size(self, detector, value=None):
        """
        Switch from the calculated pixel size to the user defined pixel size.
        If set to True the user defined pixel size is applied.

        :param detector: Detector name or detector object
        :type detector: string or object
        :param value: User mode True or False. If None, returns the current user mode
        :type value: boolean
        :return: Applied user mode
        :rtype: boolean
        """
        det_name = self.get_detector_name(detector)
        try:
            p_value = getattr(self.parameters, det_name)
        except Exception:
            raise RuntimeError("Detector not defined!\n")

        if value is not None:
            if value is True and math.isnan(p_value[0]):
                raise RuntimeError("User pixel size is not yet defined!\n")
            p_value[1] = value
            setattr(self.parameters, det_name, p_value)

        return p_value[1]

    def set_user_pixel_size(self, detector, value=None):
        """
        Set the user defined unbinned image pixel size in um

        :param detector: Detector name or detector object
        :type detector: string or object
        :param value: Unbinned image pixel size. If None, returns the current user defined pixel size
        :type value: float
        :return: User defined unbinned image pixel size
        :rtype: float
        """
        tomo_detector = self.get_tomo_detector(detector)
        if tomo_detector is None:
            raise RuntimeError("Detector not defined!\n")
        if value is not None:
            if math.isnan(value):
                value = None
            tomo_detector.user_unbinned_sample_pixel_size = value
        return tomo_detector.user_unbinned_sample_pixel_size

    def setup(self, submenu=False):
        """
        Set-up detectors
        """
        from tomo.helpers.select_dialog2 import select_dialog2

        value_list = []
        for d in self.detectors:
            label = d.detector.name
            if d is self.active_detector:
                label += " (active)"
            value_list.append((d, label))

        choice = self.active_detector
        while True:
            cancel_text = "Back" if submenu else "Cancel"
            choice = select_dialog2(
                title="Detectors Setup",
                values=value_list,
                selection=choice,
                cancel_text=cancel_text,
            )
            if choice is None:
                return

            choice.setup(submenu=True)
