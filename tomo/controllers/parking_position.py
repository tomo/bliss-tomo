from __future__ import annotations

import typing
from bliss.config.beacon_object import BeaconObject
from bliss.config.static import Config
from tomo.helpers import info_utils
from tabulate import tabulate
from bliss.common.utils import BOLD

if typing.TYPE_CHECKING:
    from tomo.controllers.tomo_config import TomoConfig


class ParkingPosition(BeaconObject):
    """Holder for the parking positon.

    For now we store the translation x, y, z positions.
    """

    def __init__(self, name: str, config: Config, tomo_config: TomoConfig):
        self.name: str = name
        BeaconObject.__init__(self, config)
        self._tomo_config: TomoConfig = tomo_config

    _reference = BeaconObject.property_setting(
        name="reference",
        default=(None, None, None),
        doc="Reference position",
    )

    @property
    def reference(self) -> tuple[float | None, float | None, float | None]:
        return self._reference

    @reference.setter
    def reference(self, value: tuple[float | None, float | None, float | None]):
        self._reference = value

    def move_to_reference(self, user: bool = False):
        if user:
            from bliss.shell.standard import umv as move
        else:
            from bliss.shell.standard import mv as move

        tomo_config = self._tomo_config
        rx, ry, rz = self._reference
        if (rx, ry, rz) == (None, None, None):
            raise RuntimeError("No reference defined")
        refs = {
            tomo_config.x_axis: rx,
            tomo_config.y_axis: ry,
            tomo_config.z_axis: rz,
        }
        # Filter unused motors and positions
        refs2 = {k: v for k, v in refs.items() if k is not None and v is not None}
        # Flatten
        args = [i for k, v in refs2.items() for i in [k, v]]
        move(*args)

    def record_reference(self):
        """Record the actual position as the reference"""
        tomo_config = self._tomo_config
        value = (
            tomo_config.x_axis and tomo_config.x_axis.position,
            tomo_config.y_axis and tomo_config.y_axis.position,
            tomo_config.z_axis and tomo_config.z_axis.position,
        )
        self._reference = value

    def __info__(self):
        """
        Show information about tomo configuration
        Expose configured scan and chain presets on runners
        """
        rx, ry, rz = self._reference
        tomo_config = self._tomo_config
        x_axis = tomo_config.x_axis
        y_axis = tomo_config.y_axis
        z_axis = tomo_config.z_axis

        info_str = f"{self.name} info:\n"
        table_list = []
        table_list.append(["Role", "Ref", "Value"])
        table_list.append([BOLD("Reference"), ""])
        table_list.append(
            [
                "   X",
                info_utils.format_device(x_axis),
                info_utils.format_axis_absolute_position(x_axis, rx),
            ]
        )
        table_list.append(
            [
                "   Y",
                info_utils.format_device(y_axis),
                info_utils.format_axis_absolute_position(y_axis, ry),
            ]
        )
        table_list.append(
            [
                "   Z",
                info_utils.format_device(z_axis),
                info_utils.format_axis_absolute_position(z_axis, rz),
            ]
        )

        table = tabulate(table_list, headers="firstrow", tablefmt="simple")
        info_str += table
        return info_str
