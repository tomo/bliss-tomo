from __future__ import annotations

import typing
from bliss.config.beacon_object import BeaconObject
import weakref
from tabulate import tabulate
from tomo.scan.dark_runner import DarkRunner
from tomo.scan.flat_runner import FlatRunner
from tomo.scan.return_ref_runner import ReturnRefRunner
from tomo.scan.count_runner import CountRunner
from bliss import global_map
from bliss.common.protocols import HasMetadataForScan
from bliss.common.axis import Axis
from tomo.scan.continuous_2d_runner import Continuous2DRunner
from tomo.scan.continuous_runner import ContinuousRunner
from tomo.scan.sweep_runner import SweepRunner
from tomo.scan.sweep_2d_runner import Sweep2DRunner
from tomo.scan.interlaced_runner import InterlacedRunner
from tomo.scan.step_runner import StepRunner
from tomo.scan.helical_runner import HelicalRunner
from tomo.scan.mesh_runner import MeshRunner
from tomo.scan.multiturns_runner import MultiTurnsRunner
from tomo.scan.tiling_runner import TilingRunner
from tomo.helpers.nexuswriter_utils import hdf5_device_names
from tomo.helpers import shell_utils
from tomo.helpers.beacon_object_helper import check_unexpected_keys
from fscan.fscantools import FScanParamBase
from tomo.helpers.axis_utils import is_user_clockwise
from tomo.helpers import info_utils
from tomo.helpers import config_helper
from bliss.common.utils import RED
from bliss.config.static import Config
from .tomo_sample_stage import TomoSampleStage
from .beam_tracker import BeamTracker
from .parking_position import ParkingPosition

if typing.TYPE_CHECKING:
    from tomo.controllers.tomo_detectors import TomoDetectors
    from tomo.scan.tomo_runner import TomoRunner
    from bliss.scanning.scan import ScanPreset
    from tomo.controllers.tomo_imaging import TomoImaging
    from tomo.controllers.holotomo import Holotomo
    from tomo.controllers.auto_projection import AutoProjection


class ConfigTomoPars(FScanParamBase):
    """
    Class to create parameters common to all tomo sequences

    **Parameters**:

    start_pos: Float
        tomo start position
    range: Int
        tomo range
    flat_n: Int
        number of flat images
    dark_n: Int
        number of dark images
    tomo_n: Int
        number of projections
    energy: Float
        energy value in keV
    exposure_time: Float
        acquisition time in secondes for one projection
    comment: String (optional)
        additional info about experiment
    latency_time: Float
        extra time in secondes for one projection
    """

    DEFAULT = {
        "start_pos": 0,
        "range": 360,
        "flat_n": 21,
        "dark_n": 20,
        "tomo_n": 999,
        "energy": 0.0,
        "exposure_time": 0.5,
        "comment": "",
        "latency_time": 1e-5,
    }
    LISTVAL = {}
    NOSETTINGS = []

    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            ConfigTomoPars.DEFAULT,
            value_list=ConfigTomoPars.LISTVAL,
            no_settings=ConfigTomoPars.NOSETTINGS,
        )

    def _validate_start_pos(self, value):
        if isinstance(value, int):
            value = float(value)
        if not isinstance(value, float):
            raise ValueError("start_pos should be a float")
        return float(value)

    def _validate_range(self, value):
        if isinstance(value, int):
            value = float(value)
        if not isinstance(value, float):
            raise ValueError("range should be a float")
        return float(value)

    def _validate_flat_n(self, value):
        if not isinstance(value, int):
            raise ValueError("flat_n should be an int")
        return int(value)

    def _validate_dark_n(self, value):
        if not isinstance(value, int):
            raise ValueError("dark_n should be an int")
        return int(value)

    def _validate_exposure_time(self, value):
        if isinstance(value, int):
            value = float(value)
        if not isinstance(value, float):
            raise ValueError("exposure_time should be a float")
        return float(value)

    def _validate_latency_time(self, value):
        if isinstance(value, int):
            value = float(value)
        if not isinstance(value, float):
            raise ValueError("latency_time should be a float")
        return float(value)

    def _validate_comment(self, value):
        return str(value)

    def _validate_tomo_n(self, value):
        if not int(value) > 0:
            raise ValueError("tomo_n should be > 0")
        return int(value)

    def _validate_energy(self, value):
        if isinstance(value, int):
            value = float(value)
        if not isinstance(value, float):
            raise ValueError("energy should be a float")
        return float(value)


class TomoConfig(BeaconObject, HasMetadataForScan):
    """
    Configuration class for full field tomography.

    Read the hardware configuration for the tomo set-up used and
    instantiates all defined runners.

    **Example yml file**::

        name: mrtomo_config
        plugin: bliss
        class: TomoConfig
        package: tomo.controllers.tomo_config

        fscan_config: $fscan_mrtomo
        # object used to move sample out of the beam / in beam
        reference: $mrref_displ

        detectors: $mrdetectors_optic

        sample_stage:
            # Motor under the rotation in the direction of the beam
            translation_x: null
            # Motor under the rotation orthogonal to the beam and the vertical plan
            translation_y: $yrot_eh2
            # Motor under the rotation moving the sample vertically
            translation_z: $sz_eh2

            # Motor controlling the rotation axis
            rotation: $srot_eh2       # axis object for the tomo rotation

            # Real motor moving the sample on top of the rotation axis
            sample_u: $sx_eh2
            # Real motor moving the sample on top of the rotation axis
            sample_v: $sy_eh2

        # default chain to be used for dark, flat and return scans
        # when detector is in single mode
        default_single_chain: $MRTOMO_int_chain

        # default chain to be used for dark, flat and return scans
        # when detector is in accumulation mode
        default_accumulation_chain: $MRTOMO_int_chain_acc

        # default chain to be used for tiling and return scans
        # when detector is in single mode
        # if the detector is not part of this chain, default_single_chain is used
        default_slow_single_chain: $MRTOMO_int_chain

        # default chain to be used for tiling and return scans
        # when detector is in accumulation mode
        # if the detector is not part of this chain, default_accumulation_chain is used
        default_slow_accumulation_chain: $MRTOMO_int_chain_acc

        # object used to check for refill or beam lost (if current < 5mA)
        machinfo: $machinfo       (optional)
        # shutter to close during refill
        beam_shutter: $bsh1       (optional)
        # used to detect beam lost
        frontend: $frontend       (optional)

    """

    def __init__(self, name: str, config: Config):
        BeaconObject.__init__(self, config=config, name=name, share_hardware=False)
        self.name = name
        global_map.register(self, tag=name)

        self.__config = config
        from ..helpers.sample_stage_motion_hook import SampleStageMotionHook
        from .auto_projection import AutoProjection

        # Clone the dict in order to check the keys
        self._init_config(config)

        self._tomo_runners: dict[str, TomoRunner] = self._create_runners()

        self.pars = ConfigTomoPars(self.name)
        self._tomo_imaging: weakref.ReferenceType[TomoImaging] | None = None
        self._holotomo: weakref.ReferenceType[Holotomo] | None = None

        motionhook = SampleStageMotionHook(self, config.get("motion_hook", {}))
        motionhook.set_event_sender(self)
        self.__motionhook = motionhook

        autoprojection = AutoProjection(self)
        self.__autoprojection = autoprojection

        # Propagates the parent into the architecture
        if self.detectors is not None:
            self.detectors._set_tomo_config(self)

    def _create_runners(self) -> dict[str, TomoRunner]:
        """
        Create the default tomo runners.

        Tomo runners are used to acquire different kind of images: dark,
        flat, projections, static.
        Different runners are available for projections depending on tomo
        sequence.
        """
        from tomo.scan.presets.inhibit_auto_projection import (
            InhibitAutoProjectionPreset,
        )
        from tomo.scan.presets.disable_active_detector_processing import (
            DisableActiveDetectorProcessing,
        )

        inhibitautoproj = InhibitAutoProjectionPreset(self)

        runners: dict[str, TomoRunner] = {}

        proj = CountRunner()
        proj.set_default_chains(self.default_single_chain, self.default_acc_chain)
        runners["count"] = proj

        dark = DarkRunner()
        dark.set_default_chains(self.default_single_chain, self.default_acc_chain)
        runners["dark"] = dark

        flat = FlatRunner(self.reference, tomoconfig=self)
        flat.set_default_chains(self.default_single_chain, self.default_acc_chain)
        runners["flat"] = flat

        return_ref = ReturnRefRunner(self.rotation_axis)
        return_ref.set_default_chains(
            single_chain=self.default_slow_single_chain,
            acc_chain=self.default_slow_acc_chain,
            fallback_single_chain=self.default_single_chain,
            fallback_acc_chain=self.default_acc_chain,
        )
        return_ref.add_scan_preset(inhibitautoproj)
        runners["return_ref"] = return_ref

        in_daiquiri = not shell_utils.is_in_shell()
        if in_daiquiri:
            disablelimaprocessing = DisableActiveDetectorProcessing(self)
            dark.add_scan_preset(disablelimaprocessing)
            flat.add_scan_preset(disablelimaprocessing)
            proj.add_scan_preset(disablelimaprocessing)
            return_ref.add_scan_preset(disablelimaprocessing)

        if self.z_axis is not None:
            tiling = TilingRunner(self)
            tiling.add_scan_preset(inhibitautoproj)
            tiling.set_default_chains(
                single_chain=self.default_slow_single_chain,
                acc_chain=self.default_slow_acc_chain,
                fallback_single_chain=self.default_single_chain,
                fallback_acc_chain=self.default_acc_chain,
            )
            runners["tiling"] = tiling

        if self.fscan_config is not None:
            runners["CONTINUOUS2D"] = Continuous2DRunner(self.fscan_config)
            runners["SWEEP2D"] = Sweep2DRunner(self.fscan_config)
            runners["CONTINUOUS"] = ContinuousRunner(self.fscan_config)
            runners["SWEEP"] = SweepRunner(self.fscan_config)
            runners["INTERLACED"] = InterlacedRunner(self.fscan_config)
            runners["HELICAL"] = HelicalRunner(self.fscan_config)
            runners["MULTITURNS"] = MultiTurnsRunner(self.fscan_config)

        mesh = MeshRunner()
        mesh.set_default_chains(
            single_chain=self.default_slow_single_chain,
            acc_chain=self.default_slow_acc_chain,
            fallback_single_chain=self.default_single_chain,
            fallback_acc_chain=self.default_acc_chain,
        )
        runners["MESH"] = mesh

        step = StepRunner()
        step.set_default_chains(self.default_single_chain, self.default_acc_chain)
        runners["STEP"] = step

        return runners

    _energy = BeaconObject.property_setting(
        name="energy",
        default=999,
        doc="Energy in keV",
    )

    @property
    def sample_stage(self) -> TomoSampleStage:
        return self._sample_stage

    @property
    def energy(self) -> float:
        """
        Return energy in keV
        """
        return self._energy

    @energy.setter
    def energy(self, value):
        """
        Set energy in keV
        """
        # For compatibility, would be better to remove it
        self.pars.energy = float(value)
        self._energy = value

    _latency_time = BeaconObject.property_setting(
        name="latency_time",
        default=0,
        doc="Latency time for the active detector in second",
    )

    @property
    def latency_time(self) -> float:
        """
        Return the latency time in second.
        """
        return self._latency_time

    @latency_time.setter
    def latency_time(self, value):
        """
        Set the latency time in second
        """
        # For compatibility, would be better to remove it
        self.pars.latency_time = float(value)
        self._latency_time = value

    @property
    def detectors(self) -> TomoDetectors:
        return self._detectors

    @property
    def is_moving(self) -> bool:
        """
        True if the sample stage is moving.

        It takes into account real motors and magnification for the active
        detectors.
        """
        return self.__motionhook.is_moving

    @property
    def auto_projection(self) -> AutoProjection:
        """
        Return the object which handle auto projection
        """
        return self.__autoprojection

    def _init_config(self, config):
        """
        Read keys from config yml file and create corresponding attributes
        """
        self._detectors = config.get("detectors", None)

        if config.get("detector_x", None) is not None:
            raise RuntimeError(
                f"{self.name}: {self.name}.detector_x have to be moved into {self._detectors.name}.detector_axis"
            )

        expected_keys = [
            "sample_stage",
            "tracked_translation_x",
            "reference_position",  # compatibility with bliss-tomo == 2.6
            "parking_position",
            "beam_tracker",
            "flat_motion",
            "reference",
            "fscan_config",
            "machinfo",
            "frontend",
            "beam_shutter",
            "detectors",
            "default_single_chain",
            "default_accumulation_chain",
            "default_slow_single_chain",
            "default_slow_accumulation_chain",
            "motion_hook",
        ]
        check_unexpected_keys(self, config, expected_keys)

        def create_sample_stage() -> TomoSampleStage:
            sample_stage = config.get("sample_stage", None)
            if sample_stage is None:
                raise RuntimeError("No sample stage found")
            from bliss.config.static import ConfigNode

            if isinstance(sample_stage, ConfigNode):
                obj = TomoSampleStage(f"{self.name}__sample_stage", sample_stage)
                config_helper.register_object(obj)
                return obj
            if not isinstance(sample_stage, TomoSampleStage):
                raise TypeError(
                    f"Type {type(sample_stage)} not expected for a sample stage"
                )
            return sample_stage

        self._sample_stage: TomoSampleStage = create_sample_stage()

        def create_parking_position() -> ParkingPosition | None:
            parking_position = config.get(
                "parking_position", config.get("reference_position")
            )
            if parking_position is None:
                return None
            from bliss.config.static import ConfigNode

            if isinstance(parking_position, ConfigNode):
                obj = ParkingPosition(
                    f"{self.name}__parking_position", parking_position, self
                )
                config_helper.register_object(obj)
                return obj
            if not isinstance(parking_position, ParkingPosition):
                raise TypeError(
                    f"Type {type(parking_position)} not expected for a parking_position"
                )
            return parking_position

        self._parking_position: ParkingPosition | None = create_parking_position()

        self._flat_motion = config.get("flat_motion", config.get("reference", None))

        self.fscan_config = config.get("fscan_config", None)
        self.machinfo = config.get("machinfo", None)
        self.frontend = config.get("frontend", None)
        self.beam_shutter = config.get("beam_shutter", None)
        self._beam_tracker: BeamTracker = config.get("beam_tracker", None)

        self.default_single_chain = config.get("default_single_chain", None)
        self.default_acc_chain = config.get("default_accumulation_chain", None)
        self.default_slow_single_chain = config.get("default_slow_single_chain", None)
        self.default_slow_acc_chain = config.get(
            "default_slow_accumulation_chain", None
        )

        if self._beam_tracker is not None:
            self._beam_tracker._set_sample_stage(self._sample_stage)

        self._tracked_x_axis = config.get("tracked_translation_x", None)

    def __info__(self):
        """
        Show information about tomo configuration
        Expose configured scan and chain presets on runners
        """
        info_str = f"{self.name} configuration info:\n"

        sample_stage = self._sample_stage

        table_list = []
        table_list.append(["Role", "Ref", "Value"])
        table_list.append(["Sample stage"])
        table_list.append(
            [
                "   Rotation axis",
                info_utils.format_device(sample_stage.rotation_axis),
                info_utils.format_axis_position(sample_stage.rotation_axis),
            ]
        )
        table_list.append(
            ["      Direction", info_utils.format_direction(sample_stage.rotation_axis)]
        )
        table_list.append(
            [
                "   Translation in x",
                info_utils.format_device(sample_stage.x_axis),
                info_utils.format_axis_position(sample_stage.x_axis),
            ]
        )
        table_list.append(
            [
                "   Translation in y",
                info_utils.format_device(sample_stage.y_axis),
                info_utils.format_axis_position(sample_stage.y_axis),
            ]
        )
        table_list.append(
            [
                "   Translation in z",
                info_utils.format_device(sample_stage.z_axis),
                info_utils.format_axis_position(sample_stage.z_axis),
            ]
        )
        table_list.append(
            [
                "   Sample translation u-motor",
                info_utils.format_device(sample_stage.sample_u_axis),
                info_utils.format_axis_position(sample_stage.sample_u_axis),
            ]
        )
        table_list.append(
            [
                "   Sample translation v-motor",
                info_utils.format_device(sample_stage.sample_v_axis),
                info_utils.format_axis_position(sample_stage.sample_v_axis),
            ]
        )
        table_list.append(
            [
                "   Sample translation in x",
                info_utils.format_device(sample_stage.sample_x_axis),
                info_utils.format_axis_position(sample_stage.sample_x_axis),
            ]
        )
        table_list.append(
            [
                "   Sample translation in y",
                info_utils.format_device(sample_stage.sample_y_axis),
                info_utils.format_axis_position(sample_stage.sample_y_axis),
            ]
        )
        table_list.append([""])
        table_list.append(
            ["Fscan config object", info_utils.format_device(self.fscan_config)]
        )
        table_list.append(["machinfo object", info_utils.format_device(self.machinfo)])
        table_list.append(
            [
                "source <-> sample",
                "",
                info_utils.format_quantity(sample_stage.source_distance, "mm"),
            ]
        )
        active_detector = self.detectors.active_detector
        if active_detector is not None:
            table_list.append(
                [
                    "source <-> active detector",
                    info_utils.format_device(active_detector.detector),
                    info_utils.format_quantity(active_detector.source_distance, "mm"),
                ]
            )

        table = tabulate(table_list, headers="firstrow", tablefmt="simple")
        info_str += table

        table_list = [
            ["Lima detectors", "Type", "Pixel size [um]", "Mode", "Magnification"]
        ]
        for detector in self.detectors.detectors:
            limadet = detector.detector

            def format_type():
                try:
                    try:
                        limadet._proxy.ping()
                    except Exception:
                        return RED("Offline")
                    return limadet._proxy.camera_type
                except Exception:
                    return RED("Error")

            table_list.append(
                [
                    info_utils.format_active(limadet.name, detector is active_detector),
                    format_type(),
                    info_utils.format_quantity(detector.sample_pixel_size, "um"),
                    detector.sample_pixel_size_mode.name,
                    info_utils.format_magnification(detector.optic),
                ]
            )

        table = tabulate(
            table_list,
            headers="firstrow",
            tablefmt="simple",
        )
        info_str += "\n\n"
        info_str += table

        info_str += "\n\n"
        info_str += "PRESETS:\n\n"
        # scan presets
        table_list = [["Runners", "Scan presets", "Chain presets"]]

        def format_preset_list(presets):
            if presets == []:
                return "none"
            return ", ".join(presets)

        for runner in self._tomo_runners:
            scan_presets = self._tomo_runners[runner].get_scan_preset_list()
            chain_presets = self._tomo_runners[runner].get_chain_preset_list()
            table_list.append(
                [
                    runner,
                    format_preset_list(scan_presets),
                    format_preset_list(chain_presets),
                ]
            )
        table = tabulate(
            table_list,
            headers="firstrow",
            tablefmt="simple",
        )
        info_str += table
        return info_str

    def _set_tomo_imaging(self, tomo_imaging: TomoImaging):
        """
        Called during the initialization at the construction of tomo_imaging
        object.

        This is needed to avoid cyclic construction of objects from Yaml.
        """
        if self._tomo_imaging:
            if self._tomo_imaging() is tomo_imaging:
                # The object is already set
                return
            raise ValueError("Tomo imaging already defined")
        self._tomo_imaging = weakref.ref(tomo_imaging)

    @property
    def tomo_imaging(self) -> TomoImaging | None:
        """
        Return tomo_imaging object associated to tomoconfig if one,
        else return None.
        """
        if self._tomo_imaging is None:
            return None
        tomo_imaging = self._tomo_imaging()
        return tomo_imaging

    def _set_holotomo(self, holotomo: Holotomo):
        """
        Called during the initialization at the construction of holotomo
        object.

        This is needed to avoid cyclic construction of objects from Yaml.
        """
        if self._holotomo:
            if self._holotomo() is holotomo:
                # The object is already set
                return
            raise ValueError("Holotomo already defined")
        self._holotomo = weakref.ref(holotomo)

    @property
    def holotomo(self) -> Holotomo | None:
        """
        Return holotomo object associated to tomoconfig if one,
        else return None.
        """
        if self._holotomo is None:
            return None
        holotomo = self._holotomo()
        return holotomo

    @property
    def flat_motion(self):
        return self._flat_motion

    @property
    def reference(self):
        """Deprecated in bliss-tomo 2.6"""
        return self._flat_motion

    @typing.overload
    def get_runner(self, name: typing.Literal["dark"]) -> DarkRunner:
        ...

    @typing.overload
    def get_runner(self, name: typing.Literal["flat"]) -> FlatRunner:
        ...

    @typing.overload
    def get_runner(self, name: typing.Literal["return_ref"]) -> ReturnRefRunner:
        ...

    @typing.overload
    def get_runner(self, name: typing.Literal["tiling"]) -> TilingRunner:
        ...

    @typing.overload
    def get_runner(self, name: typing.Literal["count"]) -> CountRunner:
        ...

    def get_runner(self, name: str) -> TomoRunner:
        """
        Return runner associated to given name
        """
        return self._tomo_runners[name]

    def add_runner(self, name: str, runner: TomoRunner):
        """
        Add a new runner or replace an existing runner with given name
        """
        self._tomo_runners[name] = runner

    def add_scan_preset(
        self,
        runner_names: str | list[str],
        preset_obj: ScanPreset,
        preset_name: str | None = None,
    ):
        """
        Add a preset to a list of runners.
        Runner names must correspond to the _tomo_runners dictonary keys
        """
        if isinstance(runner_names, str):
            runner_names = [runner_names]
        for name in runner_names:
            if name in self._tomo_runners:
                self._tomo_runners[name].add_scan_preset(preset_obj, preset_name)

    def remove_scan_preset(self, runner_names: str | list[str], preset_name: str):
        """
        Remove a preset from a list of runners.
        Runner names must correspond to the _tomo_runners dictonary keys
        """
        if isinstance(runner_names, str):
            runner_names = [runner_names]
        for name in runner_names:
            if name in self._tomo_runners:
                self._tomo_runners[name].remove_scan_preset(preset_name)

    def add_chain_preset(self, runner_names, preset_obj, preset_name=None):
        """
        Add a chain preset to a list of runners.
        Runner names must correspond to the _tomo_runners dictonary keys
        """
        for name in runner_names:
            self._tomo_runners[name].add_chain_preset(preset_obj, preset_name)

    def remove_chain_preset(self, runner_names, preset_name):
        """
        Remove a preset from a list of runners.
        Runner names must correspond to the _tomo_runners dictonary keys
        """
        for name in runner_names:
            self._tomo_runners[name].remove_chain_preset(preset_name)

    def disable_presets(self):
        """
        Disable on all tomo runners defined presets
        """
        for runner in self._tomo_runners:
            self._tomo_runners[runner].disable_presets()

    def enable_presets(self):
        """
        Enable on all tomo runners defined presets
        """
        for runner in self._tomo_runners:
            self._tomo_runners[runner].enable_presets()

    def set_active(self):
        """
        Set this tomo config the active tomo config from `ACTIVE_TOMOCONFIG`
        """
        from ..globals import ACTIVE_TOMOCONFIG

        ACTIVE_TOMOCONFIG.set_active_object(self)

    @property
    def active(self) -> bool:
        """
        True if this tomo config is the one exposed by `ACTIVE_TOMOCONFIG`
        """
        from ..globals import ACTIVE_TOMOCONFIG

        return ACTIVE_TOMOCONFIG.deref_active_object() is self

    @active.setter
    def active(self, activate: bool):
        from ..globals import ACTIVE_TOMOCONFIG

        if activate:
            ACTIVE_TOMOCONFIG.set_active_object(self)
        else:
            ACTIVE_TOMOCONFIG.set_active_object(None)

    @property
    def beam_tracker(self) -> BeamTracker | None:
        """Used beam tracker."""
        return self._beam_tracker

    @property
    def parking_position(self) -> ParkingPosition | None:
        """Reference position which can be setup by the users."""
        return self._parking_position

    @property
    def reference_position(self) -> ParkingPosition | None:
        """Reference position which can be setup by the users.

        Deprecated since bliss-tomo 2.7. Prefer to use `parking_position`.
        """
        return self._parking_position

    @property
    def tracked_x_axis(self) -> Axis | None:
        """Motor under the rotation in the direction of the beam.

        When it is used to move, the sample stage move following the beam.
        See :py:attr:`break_tracker`.
        """
        return self._tracked_x_axis

    @property
    def x_axis(self) -> Axis:
        """Motor under the rotation in the direction of the beam

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.x_axis

    @property
    def y_axis(self) -> Axis:
        """Motor under the rotation orthogonal to the beam and the vertical plan

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.y_axis

    @property
    def z_axis(self) -> Axis:
        """Motor under the rotation moving the sample vertically

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.z_axis

    @property
    def rotation_axis(self) -> Axis:
        """Motor controlling the rotation axis

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.rotation_axis

    @property
    def sample_u_axis(self) -> Axis:
        """Real motor moving the sample on top of the rotation axis

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.sample_u_axis

    @property
    def sample_v_axis(self) -> Axis:
        """Real motor moving the sample on top of the rotation axis

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.sample_v_axis

    @property
    def sample_x_axis(self) -> Axis:
        """Motor moving the sample on top of the rotation axis and always aligned to the beam

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.sample_x_axis

    @property
    def sample_y_axis(self) -> Axis:
        """Motor moving the sample on top of the rotation axis and always orthogonal to the bean

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.sample_y_axis

    @property
    def sample_x(self) -> Axis:
        """Motor moving the sample on top of the rotation axis and always aligned to the beam

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.sample_x_axis

    @property
    def sample_y(self) -> Axis:
        """Motor moving the sample on top of the rotation axis and always orthogonal to the bean

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.sample_y_axis

    @property
    def sample_u(self) -> Axis:
        """Real motor moving the sample on top of the rotation axis

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.sample_u_axis

    @property
    def sample_v(self) -> Axis:
        """Real motor moving the sample on top of the rotation axis

        Deprecated, prefere to use the `.sample_stage` reference
        """
        return self._sample_stage.sample_v_axis

    def scan_metadata(self):
        """
        Create and return dictionary with tomo configuration in it.
        This will create a folder in intrument part of h5 file called
        tomoconfig
        """
        from tomo.globals import USE_METADATA_FOR_SCAN

        if not USE_METADATA_FOR_SCAN:
            return None

        # No metadata if the tomo config is not the active one
        if not self.active:
            return None

        sample_stage = self.sample_stage
        active_detector = self.detectors.active_detector
        model_mapping = {
            "translation_x": sample_stage.x_axis,
            "translation_y": sample_stage.y_axis,
            "translation_z": sample_stage.z_axis,
            "sample_x": sample_stage.sample_x_axis,
            "sample_y": sample_stage.sample_y_axis,
            "sample_u": sample_stage.sample_u_axis,
            "sample_v": sample_stage.sample_v_axis,
            "rotation": sample_stage.rotation_axis,
            "tomo_detector": active_detector,
        }
        meta: dict[str, typing.Any] = {
            k: hdf5_device_names(obj)
            for k, obj in model_mapping.items()
            if obj is not None
        }
        meta["rotation_is_clockwise"] = is_user_clockwise(sample_stage.rotation)
        detector_center = sample_stage.detector_center
        if detector_center[0]:
            meta["detector_center_y"] = detector_center[0]
            meta["detector_center_y@units"] = sample_stage.y_axis.unit
        if detector_center[1]:
            meta["detector_center_z"] = detector_center[1]
            meta["detector_center_z@units"] = sample_stage.z_axis.unit

        meta["sample_pixel_size"] = active_detector.sample_pixel_size
        meta["sample_pixel_size@units"] = "um"
        meta["optics_pixel_size"] = active_detector.optics_pixel_size
        meta["optics_pixel_size@units"] = "um"

        return meta
