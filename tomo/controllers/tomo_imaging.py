import enum
import contextlib
import logging
from bliss.common.logtools import log_error
from bliss.common import event
from bliss.config.beacon_object import BeaconObject, EnumProperty
from .tomo_config import TomoConfig
from ..helpers.beacon_object_helper import SyncProperties
from tomo.optic.base_optic import OpticState
from tomo.helpers.beacon_object_helper import check_unexpected_keys

_logger = logging.getLogger(__name__)


class TomoImagingState(enum.Enum):
    READY = "READY"
    """The device is ready to process an imaging"""

    BLOCKED = "BLOCKED"
    """The device can't be used right now cause a dependency is in use"""

    ACQUIRING = "ACQUIRING"
    """The device is acquiring data"""

    UNKNOWN = "UNKNOWN"
    """The device state is unknown"""


class _DeviceGroup:
    """Monitor events from a set of devices to check if something is moving

    Generate a `is_moving` boolean event in case something is moving.
    """

    def __init__(self):
        self._moving_devices = set()
        self._devices = []
        self._is_moving = False

    @property
    def devices(self):
        return iter(self._devices)

    def add_motor(self, motor):
        if motor is None:
            return
        event.connect(motor, "state", self._update_motor_state)
        self._devices.append(motor)

    def remove_motor(self, motor):
        if motor is None:
            return
        self._devices.remove(motor)
        event.disconnect(motor, "state", self._update_motor_state)
        self._moving_devices -= set([motor.name])
        self._set_moving(len(self._moving_devices) > 0)

    def _update_motor_state(self, state, sender):
        if state.MOVING:
            self._moving_devices.add(sender.name)
        else:
            self._moving_devices.discard(sender.name)
        self._set_moving(len(self._moving_devices) > 0)

    def add_optic(self, optic):
        if optic is None:
            return
        event.connect(optic, "state", self._update_optic_state)
        self._devices.append(optic)

    def _is_optic_moving(self, optic) -> bool:
        return optic.state == OpticState.MOVING

    def remove_optic(self, optic):
        if optic is None:
            return
        self._devices.remove(optic)
        event.disconnect(optic, "state", self._update_optic_state)
        self._moving_devices -= set([optic.name])
        self._set_moving(len(self._moving_devices) > 0)

    def _update_optic_state(self, state, sender):
        if state == OpticState.MOVING:
            self._moving_devices.add(sender.name)
        else:
            self._moving_devices.discard(sender.name)
        self._set_moving(len(self._moving_devices) > 0)

    @property
    def is_moving(self) -> bool:
        return self._is_moving

    def _set_moving(self, is_moving):
        if self._is_moving == is_moving:
            return
        self._is_moving = is_moving
        event.send(self, "is_moving", is_moving)


class TomoImaging(BeaconObject):
    """
    Define a tomo procedure to acquire simple images from a tomo detector.

    - The object is connected to a `tomoconfig` to know the activate detector
      and the configuration.
    - It provides procedures to take dark, reference, and normal projection.
      The `tomo` object is used to know how to play with the shutter and the
      motor for references.
    - The state of the sample stage is used to auto update image after a move.
      This behavior can be enabled or disabled, and an exposure time can be
      specified.
    """

    def __init__(self, name, config):
        BeaconObject.__init__(self, config=config, name=name)

        check_unexpected_keys(
            self,
            config,
            [
                "tomoconfig",
                "use_motion_event",
                "exposure_time",
                "settle_time",
                "update_on_move",
            ],
        )

        tomoconfig = config["tomoconfig"]
        assert isinstance(tomoconfig, TomoConfig)
        self._tomoconfig = tomoconfig

        use_event = config.get("use_motion_event", True)

        self._is_sample_stage_moving = False
        self._motion_hook = None
        if use_event:
            self._sample_stage = _DeviceGroup()
            self._sample_stage.add_motor(tomoconfig.x_axis)
            self._sample_stage.add_motor(tomoconfig.y_axis)
            self._sample_stage.add_motor(tomoconfig.z_axis)
            self._sample_stage.add_motor(tomoconfig.rotation_axis)
            # No need to add both couple
            # It is supposed to be linked real/calc axis
            if tomoconfig.sample_u is not None and tomoconfig.sample_v is not None:
                self._sample_stage.add_motor(tomoconfig.sample_u)
                self._sample_stage.add_motor(tomoconfig.sample_v)
            else:
                self._sample_stage.add_motor(tomoconfig.sample_x)
                self._sample_stage.add_motor(tomoconfig.sample_y)

            detectors = self._tomoconfig.detectors
            self.__active_detector = None
            event.connect(detectors, "active_detector", self.__active_detector_changed)
            active_detector = detectors.active_detector
            if active_detector:
                self.__active_detector_changed(active_detector)
            event.connect(
                self._sample_stage, "is_moving", self._on_sample_stage_state_change
            )
        else:
            event.connect(tomoconfig, "is_moving", self._on_sample_stage_state_change)

        # Have to be called when everything was initialized
        tomoconfig._set_tomo_imaging(self)

        autoproj = tomoconfig.auto_projection

        # sync properties
        self._sync1 = SyncProperties(self, "update_on_move", autoproj, "enabled")
        self._sync2 = SyncProperties(self, "exposure_time", autoproj, "exposure_time")
        self._sync3 = SyncProperties(self, "settle_time", autoproj, "settle_time")

    _ = """State of the device"""
    state = EnumProperty(
        name="state",
        enum_type=TomoImagingState,
        default=TomoImagingState.UNKNOWN,
        unknown_value=TomoImagingState.UNKNOWN,
        doc=_,
    )

    _ = """If true a projection from the actual detector is acquired at the end
    of any move of the sample stage"""
    update_on_move = BeaconObject.property_setting(
        name="update_on_move", default=False, doc=_
    )

    _ = """Exposure time to use with the dark/ref/proj"""
    exposure_time = BeaconObject.property_setting(
        name="exposure_time", default=1.0, doc=_
    )

    _ = """Settle time used after a move and before a ct"""
    settle_time = BeaconObject.property_setting(name="settle_time", default=0.0, doc=_)

    def __active_detector_changed(self, detector_name, *args, **kwargs):
        detector = self.active_detector
        if detector is self.__active_detector:
            return
        if self.__active_detector is not None:
            self._sample_stage.remove_optic(self.__active_detector.optic)
        self.__active_detector = detector
        if self.__active_detector is not None:
            self._sample_stage.add_optic(self.__active_detector.optic)

    @property
    def tomoconfig(self):
        return self._tomoconfig

    @property
    def motion_hook(self):
        return self._motion_hook

    @property
    def active_detector(self):
        return self.tomoconfig.detectors.active_detector

    def _on_sample_stage_state_change(self, is_moving, signal=None, sender=None):
        self._set_sample_stage_moving(is_moving)

    def _set_sample_stage_moving(self, is_moving):
        if self._is_sample_stage_moving == is_moving:
            return
        self._is_sample_stage_moving = is_moving
        if is_moving:
            self.state = TomoImagingState.BLOCKED
        else:
            self.state = TomoImagingState.READY

    @contextlib.contextmanager
    def inhibit_update_on_move_context(self):
        """Context to inhibit the update on move"""
        autoproj = self._tomoconfig.auto_projection
        with autoproj.inhibit():
            yield

    def take_dark(self, count_time=None, scan_hook=None):
        _logger.info("Take dark")
        if self.active_detector is None:
            log_error(self, "Error while dark requested: Active detector is None")
            return
        if count_time is None:
            count_time = self.exposure_time
        runner = self.tomoconfig.get_runner("dark")
        autoproj = self.tomoconfig.auto_projection
        autoproj.wait_for_stabilization()
        scan = runner(count_time, 1, self.active_detector.detector, save=False)
        if scan_hook:
            scan_hook(scan)

    def take_flat(self, count_time=None, scan_hook=None):
        _logger.info("Take flat")
        if self.active_detector is None:
            log_error(self, "Error while flat requested: Active detector is None")
            return
        if count_time is None:
            count_time = self.exposure_time
        runner = self.tomoconfig.get_runner("flat")
        autoproj = self.tomoconfig.auto_projection
        autoproj.wait_for_stabilization()
        scan = runner(count_time, 1, self.active_detector.detector, save=False)
        if scan_hook:
            scan_hook(scan)

    def take_proj(self, count_time=None, scan_hook=None):
        _logger.info("Take projection")
        if self.active_detector is None:
            log_error(self, "Error while proj requested: Active detector is None")
            return
        if count_time is None:
            count_time = self.exposure_time
        runner = self.tomoconfig.get_runner("count")
        autoproj = self.tomoconfig.auto_projection
        autoproj.wait_for_stabilization()
        scan = runner(count_time, self.active_detector.detector, save=False)
        if scan_hook:
            scan_hook(scan)

    def __info__(self):
        def format_device(device):
            if device is None:
                return "Undefined"
            return f"{device.name}"

        info_str = f"{self.name} tomo imaging:\n"
        info_str += "\n"
        info_str += "  Role           | Device/value\n"
        info_str += " ----------------------------------------------\n"
        info_str += f"  tomoconfig     | -> {format_device(self.tomoconfig)}\n"
        info_str += f"  detector       | -> {format_device(self.active_detector)}\n"
        info_str += f"  update_on_move | {self.update_on_move}\n"
        info_str += f"  exposure_time  | {self.exposure_time}\n"
        info_str += f"  settle_time    | {self.settle_time}\n"
        return info_str
