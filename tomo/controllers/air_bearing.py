from __future__ import annotations
import typing
import enum
from bliss.config.beacon_object import BeaconObject, EnumProperty
from tomo.helpers.beacon_object_helper import check_unexpected_keys
from bliss.common.hook import MotionHook


class AirBearingState(enum.Enum):
    UNKNOWN = "Unknown state"
    ON = "Active air cushion"
    OFF = "No air cushion"
    MOVING_ON = "About to be on"
    MOVING_OFF = "About to be off"
    FAULT = "In fault"


class AirBearing(BeaconObject):
    """
    Air cushion device.
    """

    VALID_CONFIG_KEYS: list[str] = []

    def __init__(self, name: str, config: dict[str, typing.Any]):
        BeaconObject.__init__(self, config=config, name=name)
        check_unexpected_keys(self, config, self.VALID_CONFIG_KEYS)
        self.motion_hooks: list[MotionHook] = []
        if self.state == AirBearingState.UNKNOWN:
            try:
                self.sync_hard()
            except Exception:
                # Do not break the instanciation for that
                pass

    state = EnumProperty(
        name="state",
        enum_type=AirBearingState,
        default=AirBearingState.UNKNOWN,
        unknown_value=AirBearingState.UNKNOWN,
        doc="State of the device",
    )

    def on(self):
        """
        Turn on the air bearing.

        This check first if the state is not already ON.
        Then check if it can be turned on `self._can_turn_on()`.
        And finally call `self._turn_on()` to do the action.
        """
        if self.state == AirBearingState.ON:
            return

        if not self._can_turn_on():
            return

        self.state = AirBearingState.MOVING_ON
        self._execute_pre_motion_hooks()
        try:
            self._turn_on()
        except Exception:
            self.state = AirBearingState.FAULT
            raise
        else:
            self.state = AirBearingState.ON
        finally:
            self._execute_post_motion_hooks()

    def _can_turn_on(self) -> bool:
        """
        Can be reimplemented to early cancel the action.
        """
        return True

    def _turn_on(self):
        """
        Do the action of turning on the air bearing.

        It is only called if `_can_turn_on` was returning True.
        """
        raise NotImplementedError()

    def off(self):
        """Turn off the air bearing.

        This check first if the state is not already OFF.
        Then check if it can be turned on `self._can_turn_off()`.
        And finally call `self._turn_off()` to do the action.
        """
        if self.state == AirBearingState.OFF:
            return

        if not self._can_turn_off():
            return

        self.state = AirBearingState.MOVING_OFF
        self._execute_pre_motion_hooks()
        try:
            self._turn_off()
        except Exception:
            self.state = AirBearingState.FAULT
            raise
        else:
            self.state = AirBearingState.OFF
        finally:
            self._execute_post_motion_hooks()

    def _can_turn_off(self) -> bool:
        """
        Can be reimplemented to early cancel the action.
        """
        return True

    def _turn_off(self):
        raise NotImplementedError()

    def sync_hard(self):
        """Synchronize the device with the real location of the controller"""
        pass

    def _execute_pre_motion_hooks(self):
        """
        Allows to do an action before the air bearing motion
        """
        for h in self.motion_hooks:
            h.pre_move([self])

    def _execute_post_motion_hooks(self):
        """
        Allows to do an action after air bearing motion
        """
        for h in self.motion_hooks:
            h.post_move([self])

    def __info__(self):
        state = self.state
        result = f"{self.name}\n"
        result += f"   state:            {state.name}\n"

        return result
