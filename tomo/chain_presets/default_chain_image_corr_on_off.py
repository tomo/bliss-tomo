from bliss.scanning.chain import ChainPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.common.logtools import log_warning


class DefaultChainImageCorrOnOffPreset(ChainPreset):
    """
    Class used to deactivate image correction during common bliss scans
    in saving mode only and reactivate it after.

    Also deactivate image correction, if current image roi differs from
    flatfield or dark image.

    Attributes:
        image_corr: object handling image correction and containing image
        roi information
        detectors: list of detectors presents in active measurement group
        image_corr_was_on: dictionary containing flag set to True if image
                           correction is activated
    """

    def __init__(self, image_corr):
        self.image_corr = image_corr

    def prepare(self, acq_chain):
        """
        Read detectors presents in active measurement group.

        Check if flatfield or background correction is active on detector.

        Set image_corr_was_on flag accordingly
        """
        lima_acq_obj = [
            acq_obj
            for acq_obj in acq_chain.nodes_list
            if isinstance(acq_obj, LimaAcquisitionMaster)
        ]

        self.image_corr_was_on = {}
        self.detectors = []

        for acq_obj in lima_acq_obj:
            if acq_obj.acq_params["saving_mode"] != "NOSAVING":
                self.detectors.append(acq_obj.device)
                self.image_corr_was_on[acq_obj.device] = False
            else:
                if (
                    acq_obj.device.processing.use_background
                    or acq_obj.device.processing.use_flatfield
                ) and acq_obj.device.image.roi != self.image_corr.image_roi:
                    log_warning(
                        self,
                        f"deactivate background substraction and flatfield on {acq_obj.device.name} due to image roi incoherency\n",
                    )
                    acq_obj.device.processing.use_background = False
                    acq_obj.device.processing.use_flatfield = False
                    ctrl_params = acq_obj.device.get_current_parameters()
                    ctrl_params["saving_frame_per_file"] = (
                        1
                        if ctrl_params["saving_frame_per_file"] == -1
                        else ctrl_params["saving_frame_per_file"]
                    )
                    acq_obj.device.apply_parameters(ctrl_params)
                if acq_obj.device.processing.use_background:
                    log_warning(
                        self,
                        f"background_substraction is activated on {acq_obj.device.name}\n",
                    )
                if acq_obj.device.processing.use_flatfield:
                    log_warning(
                        self, f"flatfield is activated on {acq_obj.device.name}\n"
                    )

    def start(self, acq_chain):
        """
        Deactivate flatfield and background correction on detectors where flag
        image_corr_was_on is set to True
        """
        for detector in self.detectors:
            if detector.processing.use_background or detector.processing.use_flatfield:
                self.image_corr_was_on[detector] = True
                detector.processing.use_background = False
                detector.processing.use_flatfield = False

    def stop(self, acq_chain):
        """
        Reactivate flatfield and background correction on detectors where flag
        image_corr_was_on is set to True
        """
        for detector in self.detectors:
            if self.image_corr_was_on[detector]:
                detector.processing.use_background = True
                detector.processing.use_flatfield = True
