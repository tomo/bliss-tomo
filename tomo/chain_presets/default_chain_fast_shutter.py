from bliss.scanning.chain import ChainPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster


class DefaultChainFastShutterPreset(ChainPreset):
    """
    Open fast shutter before taking each image and close it after acquisition.

    This class used for all bliss common scans (ct, ascan, timescan...)

    Attributes:
        shutter: Tomo shutter object
            contains methods to control shutters
        close_shutter: flag used to specify if shutter must be closed or not
            after image taking
    """

    def __init__(self, shutter=None, close_shutter=True):
        self.shutter = shutter
        self.close_shutter = close_shutter

    def prepare(self, acq_chain):
        """
        Open shutter if shutter controlled by detector
        """
        self.soft_shutter = False

        dets = [
            node.device
            for node in acq_chain.nodes_list
            if isinstance(node, LimaAcquisitionMaster)
        ]
        det = dets[0] if len(dets) == 1 else None

        if det is not None and det.camera_type == "Frelon":
            if det.camera.image_mode == "FULL FRAME":
                det.shutter.mode = "AUTO_FRAME"
                det.shutter.close_time = self.shutter.closing_time
            else:
                det.shutter.mode = "MANUAL"
                det.shutter.close_time = 0.0
        elif self.shutter is not None:
            self.soft_shutter = True

    def start(self, acq_chain):
        """
        Open shutter if shutter controlled by soft
        """
        if self.soft_shutter:
            self.shutter.open()

    def stop(self, acq_chain):
        """
        Close shutter if close_shutter attribute set to True and if shutter
        controlled by soft
        """
        if self.soft_shutter and self.close_shutter:
            self.shutter.close()
