from __future__ import annotations

import typing
import numpy
import time
import contextlib

from blissdata.lima.image_utils import image_from_server
from blissdata.lima.image_utils import NoImageAvailable


from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from bliss.scanning.toolbox import DefaultAcquisitionChain

if typing.TYPE_CHECKING:
    from bliss.controllers.lima.lima_base import Lima


@contextlib.contextmanager
def use_image_no_saving(
    chain_builder: DefaultAcquisitionChain,
    detector: Lima,
    enabled: bool = False,
):
    """Context to use a ImageNoSavingPreset to a chain builder"""
    try:
        if enabled:
            preset = ImageNoSavingPreset(detector)
            chain_builder.add_preset(preset)
        else:
            preset = None
        yield preset
    finally:
        if enabled:
            chain_builder.remove_preset(preset)


class ImageNoSavingPreset(ChainPreset):
    """
    Preset which fetch the detector data at the end of the scan.

    This can be used when a scan is not saved, and then the data is not sent
    to the nexuswriter.

    Use :meth:`get_images` to access to the images from this preset at the
    end of the scan.
    """

    def __init__(self, detector: Lima):
        self._images: list[numpy.ndarray] = []
        self._detector = detector

    class Iterator(ChainIterationPreset):
        def __init__(self, images, detector):
            self._images = images
            self._detector = detector

        def prepare(self):
            pass

        def start(self):
            pass

        def stop(self):
            nb_retry = 4
            for i in range(nb_retry + 1):
                try:
                    image = image_from_server(self._detector.proxy, -1).array
                    break
                except (IndexError, NoImageAvailable):
                    # Image not yet ready
                    # Try a couple of times
                    if i >= nb_retry:
                        raise
                    time.sleep(0.05)
            self._images.append(image)

    def get_iterator(self, acq_chain):
        while True:
            yield ImageNoSavingPreset.Iterator(self._images, self._detector)

    def _get_images(self) -> list[numpy.ndarray]:
        """Deprecated, prefer to use the public API"""
        return self.get_images()

    def get_images(self) -> list[numpy.ndarray]:
        return self._images
