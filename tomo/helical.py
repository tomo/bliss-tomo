from bliss import setup_globals
from bliss.common.logtools import log_error
from tomo.sequencebasic import (
    SequenceBasic,
    SequenceBasicPars,
    ScanType,
    cleanup_sequence,
)
from tomo.helpers.proxy_param import ProxyParam
from tomo.scan.presets.common_header import CommonHeaderPreset
from .utils import setup
from bliss.shell.dialog.helpers import dialog
from bliss.common.cleanup import cleanup, axis as cleanup_axis, capture_exceptions
from bliss.common.standard import mv


class HeliTomoPars(SequenceBasicPars):
    DEFAULT = dict(
        **SequenceBasicPars.DEFAULT,
        **{
            "step_size": 0.1,
            "step_time": 0,
            "slave_start_pos": -450.0,
            "slave_step_size": 0.8,
            "acq_time": 0.8,
            "nturns": 3,
            "acc_margin": 0.0,
            "slave_acc_margin": 0.0,
            "sampling_time": 0.5,
            "back_and_forth": False,
        },
    )
    LISTVAL = dict(**SequenceBasicPars.LISTVAL)
    OBJKEYS = [*SequenceBasicPars.OBJKEYS, "motor", "slave_motor"]
    NOSETTINGS = [*SequenceBasicPars.NOSETTINGS, "save_flag"]

    def _validate_tomo_n(self, value):
        if not int(value) > 0:
            raise ValueError("tomo_n should be > 0")
        return int(value)

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False


class HelicalTomo(SequenceBasic):
    def __init__(self, name, config):
        self.name = name + "_sequence"
        super().__init__(name, config)
        self._slave_motor = self._z_axis

    def _create_parameters(self):
        tomo_config = self.tomo_config
        sequence_pars = HeliTomoPars(self.name)
        return ProxyParam(tomo_config.pars, sequence_pars)

    def add_metadata(self, scan_info):
        super().add_metadata(scan_info)
        scan_info["technique"]["scan"]["sequence"] = "tomo:helical"
        scan_info["title"] = "tomo:helical"

    def check_coverage(self):
        fov = (
            self._detectors.get_image_pixel_size(self._detector)
            * self._detector.image.height
            / 1000
        )
        if self._inpars.field_of_view == "Half":
            z_step = self.pars.slave_step_size
            if z_step > fov:
                raise AttributeError(f"Z_step should be inferior to {fov}")
        else:
            z_step = self.pars.slave_step_size
            if z_step > (fov * 2):
                raise AttributeError(f"Z_step should be inferior to {fov * 2}")

    def validate(self):
        self._inpars.end_pos = self.pars.start_pos + self.pars.range

        pixel_size = self._detectors.get_image_pixel_size(self._detector)
        yrot_px = self._y_axis.position / pixel_size

        if abs(yrot_px) > 20:
            self._inpars.field_of_view = "Half"
        else:
            self._inpars.field_of_view = "Full"

        self.check_coverage()

        darks_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "dark"
        ]
        flats_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "flat"
        ]
        returns_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name == "return_ref"
        ]
        projs_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name in ScanType.values
        ]

        nb_proj_scan = len(projs_in_sequence)
        nb_dark_scan = len(darks_in_sequence)
        nb_flat_scan = len(flats_in_sequence)
        nb_return_scan = len(returns_in_sequence)

        proj_time = nb_proj_scan * self._inpars.proj_time
        dark_time = nb_dark_scan * self.pars.dark_n * self.pars.exposure_time
        flat_runner = self.get_runner("flat")
        flat_time = nb_flat_scan * flat_runner._estimate_scan_duration(self._inpars)

        if self.pars.return_images_aligned_to_flats:
            disp = self.pars.flat_on * self.pars.range / self.pars.tomo_n
        else:
            disp = 90
        return_runner = self.get_runner("return_ref")
        return_time = nb_return_scan * return_runner._estimate_scan_duration(
            self._inpars
        )

        self._inpars.scan_time = dark_time + flat_time + proj_time + return_time

    def add_proj(
        self,
        start_pos1,
        end_pos1,
        start_pos2,
        end_pos2,
        tomo_n,
        expo_time,
        proj_scan=None,
    ):
        detector = self._detector
        header = dict()
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)

        title = f"projections {int(start_pos1)} - {int(end_pos1)}"
        self._inpars.projection = end_pos1

        if proj_scan is not None:
            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan(
                start_pos1, end_pos1, start_pos2, end_pos2, tomo_n, expo_time, run=False
            )

            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)

        proj_scan.scan_info.update({"title": title})

    def add_projections_group(
        self,
        start_pos1=None,
        end_pos1=None,
        start_pos2=None,
        end_pos2=None,
        tomo_n=None,
        expo_time=None,
        flat_on=None,
        proj_scan=None,
    ):
        """
        Divide tomo acquisition into several projection groups according
        to flat_on parameter
        One projection group is one projection scan followed by flat
        images
        flat_on indicates the number of projections after which flat
        images must be taken
        If number of projection groups deduced from tomo_n and flat_on
        is not an integer, an additional group with a reduced number of
        projections is created
        """
        if flat_on is None:
            flat_on = self.pars.flat_on

        if tomo_n is None:
            tomo_n = self.pars.tomo_n

        if tomo_n < flat_on:
            flat_on = tomo_n

        nb_groups = int(tomo_n / flat_on)
        groups1 = [start_pos1]
        groups2 = [start_pos2]
        step1 = (end_pos1 - start_pos1) / tomo_n
        step2 = (end_pos2 - start_pos2) / tomo_n

        for i in range(nb_groups):
            groups1.append(groups1[-1] + step1 * flat_on)
            groups2.append(groups2[-1] + step2 * flat_on)

        for i in range(0, len(groups1) - 1):
            self.add_proj_group(
                start_pos1=groups1[i],
                end_pos1=groups1[i + 1],
                start_pos2=groups2[i],
                end_pos2=groups2[i + 1],
                tomo_n=flat_on,
                expo_time=expo_time,
                proj_scan=proj_scan,
            )

        if tomo_n % flat_on != 0:
            self.add_proj_group(
                start_pos1=groups1[-1],
                end_pos1=end_pos1,
                start_pos2=groups2[-1],
                end_pos2=end_pos2,
                tomo_n=tomo_n % flat_on,
                expo_time=expo_time,
                proj_scan=proj_scan,
            )

    def add_proj_group(
        self,
        start_pos1=None,
        end_pos1=None,
        start_pos2=None,
        end_pos2=None,
        tomo_n=None,
        expo_time=None,
        proj_scan=None,
    ):
        """
        Set the runner that sequence must use to acquire projection
        images of one projection group
        If proj_scan is not specified, default projection runner
        corresponding to scan type parameter will be used
        Add projection scan to the sequence
        Add image common header preset to projection scan
        Add flat scan to the sequence
        """
        "Add one group of projections"
        if proj_scan is not None:
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan(
                self._rotation_axis,
                start_pos1,
                end_pos1,
                self._slave_motor,
                start_pos2,
                end_pos2,
                tomo_n,
                expo_time,
                run=False,
            )
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        detector = self._detector
        header = {}
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)
        proj_scan.add_preset(header_preset)
        self._inpars.nb_proj_groups += 1
        self._inpars.start_proj_groups.append([start_pos1, start_pos2])
        self._inpars.end_proj_groups.append([end_pos1, end_pos2])
        self._inpars.points_proj_groups.append(tomo_n)
        end_proj = sum(self._inpars.points_proj_groups)
        start_proj = end_proj - self._inpars.points_proj_groups[-1]
        title = f"projections {int(start_proj)} - {int(end_proj)}"
        proj_scan.scan_info.update({"title": title})
        self._inpars.projection = end_proj
        self.add_flat()

    def projection_scan(
        self,
        start_pos1,
        end_pos1,
        start_pos2,
        end_pos2,
        tomo_n,
        expo_time,
        scan_info=None,
        run=True,
    ):
        # if self.pars.scan_type != "HELICAL":
        #     raise ValueError(
        #         f"{self.pars.scan_type} is not recognized, "
        #         f"this sequence only works for HELICAL scans"
        #     )

        runner = self.get_runner("HELICAL")

        # step_size1 = (end_pos1 - start_pos1) * 1.0 / tomo_n
        # step_size2 = (end_pos2 - start_pos2) * 1.0 / tomo_n

        latency_time = self.pars.latency_time
        tomo_scan = runner(
            self._rotation_axis,
            start_pos1,
            end_pos1,
            self._slave_motor,
            start_pos2,
            end_pos2,
            tomo_n * self.pars.nturns,
            expo_time,
            latency_time=latency_time,
            scan_info=scan_info,
            run=False,
        )

        self._inpars.proj_time = runner._estimate_scan_duration(self._inpars)

        if run:
            tomo_scan.run()

        return tomo_scan

    def full_turn_scan(
        self,
        dataset_name=None,
        start_pos=None,
        start_pos2=None,
        end_pos2=None,
        tomo_n=None,
        run=True,
    ):
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if start_pos2 is not None:
            self.pars.slave_start_pos = start_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if end_pos2 is not None:
            self.pars.slave_step_size = (
                end_pos2 - self.pars.start_pos
            ) / self.pars.nturns

        self.pars.range = 360
        self._setup_sequence(run=run)

    def half_turn_scan(
        self,
        dataset_name=None,
        start_pos=None,
        start_pos2=None,
        end_pos2=None,
        tomo_n=None,
        run=True,
    ):
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if start_pos2 is not None:
            self.pars.slave_start_pos = start_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if end_pos2 is not None:
            self.pars.slave_step_size = (
                end_pos2 - self.pars.start_pos
            ) / self.pars.nturns
        self.pars.range = 180
        self._setup_sequence(run=run)

    def basic_scan(
        self,
        start_pos,
        nb_turn,
        start_pos2,
        z_step,
        tomo_n,
        expo_time,
        collection_name=None,
        dataset_name=None,
        run=True,
    ):
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        else:
            dataset_name = setup_globals.SCAN_SAVING.dataset_name
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        else:
            collection_name = setup_globals.SCAN_SAVING.collection_name

        if start_pos is not None:
            self.pars.start_pos = start_pos
        if nb_turn is not None:
            self.pars.nturns = nb_turn
            end_pos = start_pos + nb_turn * 360
            self.pars.nturns = nb_turn
            self.pars.range = end_pos - self.pars.start_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time
        if start_pos2 is not None:
            self.pars.slave_start_pos = start_pos2
        if z_step is not None:
            self.pars.slave_step_size = z_step
        self._setup_sequence(run=run)

    def backforth_scan(
        self,
        nb_turn,
        z_step,
        tomo_n,
        expo_time=None,
        collection_name=None,
        dataset_name=None,
        run=True,
    ):
        self.pars.back_and_forth = True
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        else:
            dataset_name = setup_globals.SCAN_SAVING.dataset_name
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        else:
            collection_name = setup_globals.SCAN_SAVING.collection_name

        self.pars.start_pos = self._rotation_axis.position
        start_pos = self.pars.start_pos
        if nb_turn is not None:
            self.pars.nturns = nb_turn
            end_pos = start_pos + nb_turn * 360
            self.pars.nturns = nb_turn
            self.pars.range = end_pos - self.pars.start_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time
        self.pars.slave_start_pos = self._z_axis.position
        if z_step is not None:
            self.pars.slave_step_size = z_step
        self._setup_sequence(run=run)
        self.pars.back_and_forth = False

    def build_sequence(self):
        self.init_sequence()

        if self.pars.dark_at_start:
            self.add_dark()
        if self.pars.flat_at_start:
            self.add_flat()

        start_pos = self.pars.start_pos
        tomo_range = self.pars.range
        self._inpars.init_start_pos = start_pos
        self._inpars.init_range = tomo_range / self.pars.nturns
        scandir = 1 if tomo_range > 0 else -1
        if self.pars.back_and_forth:
            #    self.pars.return_to_start_pos = False
            #    if (
            #        scandir > 0
            #        and self._rotation_axis.position
            #        > start_pos + tomo_range / 2
            #        or scandir < 0
            #        and self._rotation_axis.position
            #        < start_pos + tomo_range / 2
            #    ):
            if self._rotation_axis.position > 360:
                #        self.pars.start_pos = start_pos + tomo_range
                self.pars.range = -tomo_range
                scandir = -1
            else:
                self.pars.start_pos = start_pos
                self.pars.range = tomo_range
                scandir = 1

        slave_end_pos = (
            self.pars.slave_start_pos
            + (self.pars.nturns * self.pars.slave_step_size) * scandir
        )
        end_pos = self.pars.start_pos + 360 * self.pars.nturns * scandir
        if self.pars.projection_groups:
            self.add_projections_group(
                self.pars.start_pos,
                end_pos,
                self.pars.slave_start_pos,
                slave_end_pos,
                self.pars.tomo_n,
                self.pars.exposure_time,
                flat_on=self.pars.flat_on,
            )
        else:
            self.add_proj(
                start_pos1=self.pars.start_pos,
                end_pos1=end_pos,
                start_pos2=self.pars.slave_start_pos,
                end_pos2=slave_end_pos,
                tomo_n=self.pars.tomo_n,
                expo_time=self.pars.exposure_time,
            )

        if self.pars.images_on_return:
            self.add_return()
        if self.pars.flat_at_end:
            self.add_flat()
        if self.pars.dark_at_end:
            self.add_dark()

    @cleanup_sequence
    def run(self, user_info=None):
        """
        Prepare and execute the sequence
        Start acquisition presets
        Add and run all the scans contained in the sequence
        Stop acquisition presets
        Move back rotation axis to initial position if
        return_to_start_pos parameter is set to True
        """
        if user_info is not None:
            if self._prepare_done:
                raise RuntimeError(
                    "Scan prepare was already done. Use user_info at creation or at run, not at both."
                )
        if not self._prepare_done:
            self.prepare(user_info)

        if self.pars.return_to_start_pos:
            restore_list = (cleanup_axis.POS, cleanup_axis.VEL)
        else:
            restore_list = (cleanup_axis.VEL,)

        with capture_exceptions(raise_index=0) as capture:
            with self._run_context(capture):
                with cleanup(
                    self._rotation_axis, self._z_axis, restore_list=restore_list
                ):
                    self._start_presets()
                    mv(self._rotation_axis, self.pars.start_pos)
                    with self._sequence.sequence_context() as scan_seq:
                        self._current_start = (
                            self._machinfo.counters.current.value
                            if self._machinfo is not None
                            else None
                        )
                        for scan in self._scans:
                            scan_seq.add(scan)
                            scan.run()
                        self._current_stop = (
                            self._machinfo.counters.current.value
                            if self._machinfo is not None
                            else None
                        )
                        self._send_icat_metadata()
                        self._stop_presets()

            if len(capture.failed) > 0:
                print("\n")
                log_error(
                    self,
                    f"A problem occured during {self.name.split(':')[-1]} sequence, sequence aborted",
                )
                log_error(self, capture.exception_infos)
                log_error(self, "\n")
                self._prepare_done = False
                self._stop_presets()
                self._sino = None
                self.pars.start_pos = self._inpars.init_start_pos
                self.pars.range = self._inpars.init_range
                return

            self._sino = None

        self._prepare_done = False
        self.pars.start_pos = self._inpars.init_start_pos
        self.pars.range = self._inpars.init_range

        self._check_beam(user_info)


# Setup the standard BLISS `menu`
@dialog(HelicalTomo.__name__, "setup")
def _setup(sequence):
    setup(sequence)
