import enum


class NxTomoImageKey(enum.IntEnum):
    PROJECTION = 0
    FLAT_FIELD = 1
    DARK_FIELD = 2
    INVALID = 3
    ESRF_RETURN = -1  # ESRF specific key for alignment check
