from __future__ import annotations

import tango
import numbers
import typing
import pint
from .globals import ACTIVE_TOMOCONFIG
from bliss import current_session

try:
    # This is not working with pytest env
    from bliss.setup_globals import *
except ImportError:
    pass
from bliss.common.logtools import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.shell.standard import goto_peak
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.common.measurementgroup import get_active as get_active_mg
from bliss.common.plot import plotselect
from bliss.common.utils import typecheck
from tomo.helpers.locking_helper import lslock, force_unlock  # noqa


def _active_tomoconfig():
    """
    Return active tomoconfig in current session if exists
    """
    tomoconfig = ACTIVE_TOMOCONFIG.deref_active_object()
    if tomoconfig is None:
        raise RuntimeError("No active tomo config was setup for this session")
    return tomoconfig


def update_on():
    """
    Activate image taking after a motor movement
    Image taking uses tomo exposure time
    """
    _active_tomoconfig().auto_projection.enabled = True


def update_off():
    """
    Deactivate image taking after a motor movement
    """
    _active_tomoconfig().auto_projection.enabled = False


def update_is_on():
    """
    Return True if image taking after a motor movement is enabled
    """
    return _active_tomoconfig().auto_projection.enabled


def reset_detector(*detectors):
    """
    For all detectors enabled in ACTIVE_MG:
        - remove image roi
        - remove image binning
        - remove image rotation
        - remove image flipping
        - remove all roi counters
        - remove all roi prpfiles
        - erase image used for background substraction
        - erase image used for flatflied correction
        - remove background substraction
        - remove flatflied correction
    """
    if len(detectors) == 0:
        list_det = []

        # analyse the active measurement group (ACTIVE_MG)
        builder = ChainBuilder([])

        for node in builder.get_nodes_by_controller_type(Lima):
            list_det.append(node.controller)

        if len(list_det) == 0:
            raise RuntimeError(
                "DETECTOR ERROR : No detector defined in the active measurement group!\n"
            )

    for detector in detectors:
        detector.image.roi = [0, 0, 0, 0]
        detector.image.binning = [1, 1]
        detector.image.rotation = 0
        detector.image.flip = False, False
        detector.roi_counters.clear()
        detector.roi_profiles.clear()
        detector.processing.background = ""
        detector.processing.flatfield = ""
        detector.processing.use_background = False
        detector.processing.use_flatfield = False


def get_controller_lima():
    """
    Return all lima controllers enabled in ACTIVE_MG
    """
    try:
        builder = ChainBuilder([])
    except ValueError as e:
        if "all counters disabled" in str(e.args):
            return None
        else:
            raise e

    for node in builder.get_nodes_by_controller_type(Lima):
        return node.controller


def _add_roi_counter(active_detector):
    """
    Define a roi counter named "full" on current image roi of
    active detector (enabled in ACTIVE_MG)
    """
    active_detector.roi_counters["full"] = (
        0,
        0,
        active_detector.image.width,
        active_detector.image.height,
    )


def _remove_roi_counter(active_detector):
    """
    Remove roi counter named "full" on current image roi of
    active detector (enabled in ACTIVE_MG)
    """
    active_detector.roi_counters.remove("full")


def std_on():
    """
    Define roi counter named "full" on current image roi of
    active detector (enabled in ACTIVE_MG)
    Select "full_std" counter in flint
    """
    active_detector = get_controller_lima()
    _add_roi_counter(active_detector)
    plotselect(active_detector.roi_counters.counters.full_std)


def std_off():
    """
    Remove roi counter named "full" on current image roi of
    active detector (enabled in ACTIVE_MG)
    """
    active_detector = get_controller_lima()
    _remove_roi_counter(active_detector)


def avg_on():
    """
    Define roi counter named "full" on current image roi of
    active detector (enabled in ACTIVE_MG)
    Select "full_avg" counter in flint
    """
    active_detector = get_controller_lima()
    _add_roi_counter(active_detector)
    plotselect(active_detector.roi_counters.counters.full_avg)


def avg_off():
    """
    Remove roi counter named "full" defined on current image
    roi of active detector (enabled in ACTIVE_MG)
    """
    active_detector = get_controller_lima()
    _remove_roi_counter(active_detector)


def roi_cen(width=None, height=None):
    """
    Define a centered roi on active detector (enabled in ACTIVE_MG)
    width is roi width
    height is roi height
    """
    active_detector = get_controller_lima()
    if width is None and height is None:
        elog_print(f'roi: {active_detector.image.roi}')
        return (active_detector.image.width, active_detector.image.height)
    curr_bin = active_detector.image.binning
    active_detector.image.binning = 1, 1
    active_detector.image.roi = (
        (active_detector.image.fullsize[0] - width) / 2,
        (active_detector.image.fullsize[1] - height) / 2,
        width,
        height,
    )
    active_detector.image.binning = curr_bin


def roi(x=None, y=None, width=None, height=None):
    """
    Define a roi on active detector (enabled in ACTIVE_MG) with
    given parameters
    x is x coordinates starting point
    y is y coordinates starting point
    width is roi width
    height is roi height
    """
    active_detector = get_controller_lima()
    if x is None and y is None and width is None and height is None:
        elog_print(f'roi: {active_detector.image.roi}')
        return active_detector.image.roi
    active_detector.image.roi = x, y, width, height


def TOMO_N(value=None):
    """
    Read/write number of projections
    """
    if value is None:
        elog_print(f'TOMO_N: {_active_tomoconfig().pars.tomo_n}')
        return _active_tomoconfig().pars.tomo_n
    _active_tomoconfig().pars.tomo_n = value


def EXP_TIME(value=None):
    """
    Read/write exposure time per frame in s
    """
    if value is None:
        elog_print(f'EXP_TIME: {_active_tomoconfig().pars.exposure_time} s')
        return _active_tomoconfig().pars.exposure_time
    _active_tomoconfig().pars.exposure_time = value

def LATENCY_TIME(value=None):
    """
    Read/write latency time in s
    """
    if value is None:
        elog_print(f'LATENCY_TIME: {_active_tomoconfig().pars.latency_time} s')
        return _active_tomoconfig().pars.latency_time
    _active_tomoconfig().pars.latency_time = value


@typecheck
def Y_STEP(value: typing.Union[numbers.Real, None] = None):
    """
    Read/write relative lateral axis displacement in mm for flat images
    """
    tomoconfig = _active_tomoconfig()
    flat_motion = tomoconfig.reference
    y_axis = tomoconfig.y_axis
    if value is None:
        elog_print(f'Y_STEP: {flat_motion.get_relative_motion(y_axis)} mm')
        return flat_motion.get_relative_motion(y_axis)

    # This will raise an exception if the value is out of the range
    tomoconfig.y_axis.get_motion(value)

    flat_motion.set_out_of_beam_displacement(y_axis, value)


def COMMENT(value=None):
    """
    Read/write comment tomo parameter
    """
    if value is None:
        elog_print(f'COMMENT: {_active_tomoconfig().pars.comment}')
        return _active_tomoconfig().pars.comment
    _active_tomoconfig().pars.comment = value


def RANGE(value=None):
    """
    Read/write tomo range
    """
    if value is None:
        elog_print(f'RANGE: {_active_tomoconfig().pars.range} deg')
        return _active_tomoconfig().pars.range
    _active_tomoconfig().pars.range = value


def DISTANCE(value: float | None = None):
    """
    Read/write sample detector distance
    """
    active_detector = get_controller_lima()
    tomoconfig = _active_tomoconfig()
    tomodetector = tomoconfig.detectors.get_tomo_detector(active_detector)
    if value is None:
        elog_print(f'DISTANCE: {tomodetector.sample_detector_distance} mm')
        return tomodetector.sample_detector_distance
    tomodetector.sample_detector_distance = value




def SUB_FRAME(value):
    """
    Define exposure time of one subframe in case of accumulation.

    Set latency time as 10% of exposure time.
    Use active detector (enabled in ACTIVE_MG).
    """
    active_detector = get_controller_lima()
    active_detector.accumulation.max_expo_time = value
    LATENCY_TIME(float(value) * 0.1)

def REF_N(value=None):
    """
    Read/write number of flat images
    """
    if value is None:
        elog_print(f'REF_N: {_active_tomoconfig().pars.flat_n}')
        return _active_tomoconfig().pars.flat_n
    _active_tomoconfig().pars.flat_n = value


def DARK_N(value=None):
    """
    Read/write number of dark images
    """
    if value is None:
        elog_print(f'DARK_N: {_active_tomoconfig().pars.dark_n}')
        return _active_tomoconfig().pars.dark_n
    _active_tomoconfig().pars.dark_n = value


def ENERGY(value=None):
    """
    Read/write energy tomo parameter
    """
    if value is None:
        elog_print(f'ENERGY: {_active_tomoconfig().energy} keV')
        return _active_tomoconfig().energy
    _active_tomoconfig().energy = float(value)


def SCINTILLATOR(value=None):
    """
    Read/write scintillator type
    """
    active_detector = get_controller_lima()
    if value is None:
        elog_print(f'SCINTILLATOR: {_active_tomoconfig().detectors.get_optic(active_detector).scintillator}')
        return _active_tomoconfig().detectors.get_optic(active_detector).scintillator
    _active_tomoconfig().detectors.get_optic(active_detector).scintillator = str(value)


def pixel_size(value=None, unit="um"):
    """
    Read/write the pixel size value (in um) of active detector.

    If parameter `value` is filled, set a user pixel size value in um
    otherwise return th user pixel size.
    """
    active_detector = get_controller_lima()
    tomodet = _active_tomoconfig().detectors.get_tomo_detector(active_detector)
    if value is not None:
        value = pint.Quantity(value, unit).to("um").magnitude
        tomodet.sample_pixel_size_mode = "USER"
        tomodet.user_unbinned_sample_pixel_size = value
        return
    current_pixel_size = _active_tomoconfig().detectors.get_image_pixel_size(
        active_detector
    )
    if current_pixel_size is None:
        current_pixel_size = tomodet.auto_sample_pixel_size
        tomodet.sample_pixel_size_mode = "AUTO"
    if active_detector.image.binning != [1, 1]:
        print("The returned value is binned pixel size")
    elog_print(f'pixel size: {current_pixel_size} um')
    return current_pixel_size


def print_pixel_size(detector=None):
    """
    Display pixel size value in um of given detector
    """
    current_pixel_size = pixel_size(detector)
    print(f"you are currently using a pixel size of {current_pixel_size:.3f} um")


def pic():
    """
    Wrap goto_peak() standard bliss function
    """
    goto_peak()


def reset_saving():
    """
    Force hdf5 saving format and one_file_per_scan saving mode on
    active detector (enabled in ACTIVE_MG)
    """
    active_detector = get_controller_lima()
    active_detector.saving.file_format = "HDF5"
    active_detector.saving.mode = 1


def tomoccdselect(detector=None):
    """
    Helper to enable a detector in ACTIVE_MG
    Display list of detectors defined in session if no detector specified
    Enable also machinfo counters if machinfo object is defined in tomoconfig
    """
    active_mg = get_active_mg()
    active_detector = get_controller_lima()
    # free memory taken by lima
    try:
        active_detector.camera_type
        for i in range(2):
            active_detector.prepareAcq()
    except Exception:
        # lima server is not running
        pass
    if detector is None:
        from bliss.shell.cli.user_dialog import UserChoice

        values = []
        i = 0
        active_index = i
        ldetectors = [
            current_session.config.get(counters.split(':')[0])
            for counters in active_mg.available
            if 'image' in counters
        ]
        for detector in ldetectors:
            try:
                detector.proxy.ping()
                if detector == active_detector:
                    active_index = i
                values.append((i, detector.name))
                i += 1
            except tango.DevFailed:
                print(f"{detector.name} seems to be disconnected")
        if len(values) == 0:
            return
        dlg = UserChoice(
            label="Select tomo detector", values=values, defval=active_index
        )
        ret = BlissDialog([[dlg]], title="Tomo Ccd Select").show()
        if ret:
            detector_name = values[ret[dlg]][1]
        else:
            return
    else:
        detector_name = detector.name
    _active_tomoconfig().detectors.active_detector = detector_name

    active_mg = get_active_mg()

    if active_detector is not None:
        active_mg.disable(f"*{active_detector.name}:*")
    if getattr(_active_tomoconfig(), "machinfo"):
        active_mg.enable(_active_tomoconfig().machinfo.name + ":*")
    image_cnt = f"{detector_name}:image"
    if image_cnt not in active_mg.available:
        active_mg.add(detector.image)
    active_mg.enable(detector_name + ":*")
    nb_bpm = len(
        [
            counter_name
            for counter_name in active_mg.available
            if counter_name.startswith(detector_name + ":bpm")
        ]
    )
    if nb_bpm > 0:
        active_mg.disable(detector_name + ":bpm*")
