from bliss import current_session


# example of full field tomo sequence
# in this example, the sequence is composed of,
# one dark scan and one flat scan at the beginning,
# followed by several projection + flat scans if option is activated
# or one projection scan if not,
# and additional images are taken on motor return
def fasttomo(
    start_pos=None,
    trange=None,
    exposure_time=None,
    tomo_n=None,
    projection_groups=False,
    flat_on=None,
    run=True,
):
    fast_tomo = current_session.config.get("fast_tomo")

    # update fasttomo parameters according to given values
    if start_pos is not None:
        fast_tomo.pars.start_pos = start_pos
    if trange is not None:
        fast_tomo.pars.range = trange
    if exposure_time is not None:
        fast_tomo.pars.exposure_time = exposure_time
    if tomo_n is not None:
        fast_tomo.pars.tomo_n = tomo_n
    if projection_groups is not None:
        fast_tomo.pars.projection_groups = projection_groups
    if flat_on is not None:
        fast_tomo.pars.flat_on = flat_on

    start_pos = fast_tomo.pars.start_pos
    end_pos = fast_tomo.pars.start_pos + fast_tomo.pars.range
    tomo_n = fast_tomo.pars.tomo_n
    projection_groups = fast_tomo.pars.projection_groups
    flat_on = fast_tomo.pars.flat_on
    exposure_time = fast_tomo.pars.exposure_time

    # select detector enabled in active measurement group
    # initialize sequence with an empty scans list
    # initialize sequence metadata
    fast_tomo.init_sequence()

    # create dark scan and add it in scans list of the sequence
    fast_tomo.add_dark()
    # create flat scan and add it in scans list of the sequence
    fast_tomo.add_flat()
    # if user wants several projection groups,
    # create for each group projection scan + flat scan
    # and add it in scans list of the sequence
    if projection_groups:
        fast_tomo.add_projections_group(
            start_pos, end_pos, tomo_n, exposure_time, flat_on=flat_on
        )
    # otherwise
    # create one projection scan
    # and add it in scans list of the sequence
    else:
        fast_tomo.add_proj(start_pos, end_pos, tomo_n, exposure_time)

    # create return scan and add it in scans list of the sequence
    fast_tomo.add_return()

    if run:
        fast_tomo.run()
    else:
        # build the sequence
        # add metadata
        # add sinogram if roi profile is defined on active detector
        fast_tomo.prepare()

    return fast_tomo


def zseries(
    delta_pos, nb_scans, sleep=0.0, start_pos=None, start_nb=1, tomo_range=360, run=True
):
    zseries = current_session.config.get("z_series")

    zseries.delta_pos = delta_pos
    zseries.nb_scans = nb_scans
    zseries.sleep = sleep
    zseries.start_pos = start_pos
    zseries.start_nb = start_nb
    zseries.pars.range = tomo_range

    zseries._setup_sequence(run=run)
    return zseries


def fast_z_series(
    delta_pos, nb_scans, sleep=0.0, start_pos=None, start_nb=1, tomo_range=360, run=True
):
    zseries = current_session.config.get("fast_z_series")

    zseries.delta_pos = delta_pos
    zseries.nb_scans = nb_scans
    zseries.sleep = sleep
    zseries.start_pos = start_pos
    zseries.start_nb = start_nb
    zseries.pars.range = tomo_range

    zseries._setup_sequence(run=run)
    return zseries
