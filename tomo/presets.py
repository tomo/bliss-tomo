# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.image_spectrum import ImageSpectrumPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.projection_spectrum import ProjectionSpectrumPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.common_header import CommonHeaderPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.reference_motor import ReferenceMotorPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.dark_shutter import DarkShutterPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.image_corr_on_off import ImageCorrOnOffPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.fast_shutter import FastShutterPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.demo_shutter import DemoShutterPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.wait_for_refill import WaitForRefillPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.wait_for_beam import WaitForBeamPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.detector_safety import DetectorSafetyPreset  # noqa

# Compatibility with tomo <= 2.3.1
from tomo.scan.presets.musst_timer_config import MusstTimerConfigPreset  # noqa

# Compatibility with tomo < 2.5.0
from tomo.chain_presets.default_chain_image_corr_on_off import (  # noqa
    DefaultChainImageCorrOnOffPreset,
)

# Compatibility with tomo < 2.5.0
from tomo.chain_presets.default_chain_fast_shutter import (  # noqa
    DefaultChainFastShutterPreset,
)

from bliss.common import deprecation

deprecation.deprecated_warning(
    "Module",
    "tomo.presets",
    replacement="tomo.scan.presets.{preset_name} OR tomo.chain_presets.{preset_name}",
    since_version="2.5.0",
)
