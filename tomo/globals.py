from __future__ import annotations
from .helpers.active_object import ActiveObject as _ActiveObject
from .controllers.tomo_config import TomoConfig as _TomoConfig

ACTIVE_TOMOCONFIG = _ActiveObject(
    object_class=_TomoConfig,
    object_key="active_tomoconfig",
    object_label="tomo config",
)
"""Share a TomoConfig instance to the BLISS session"""


USE_METADATA_FOR_SCAN = True
"""If true, the virtual controllers will stores themself the metadata"""


def get_active_tomo_config() -> _TomoConfig:
    config = ACTIVE_TOMOCONFIG.deref_active_object()
    if config is None:
        raise RuntimeError("No active tomo config setup yet")
    return config
