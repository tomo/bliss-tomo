#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Literal, Optional

from bliss.rest_service.typedef import (
    Field,
    ObjectType,
    CallableSchema,
    Callable1Arg,
    HardwareSchema,
)

logger = logging.getLogger(__name__)

TomoFlatMotionStates = ["READY"]


class _MotionSchema(HardwareSchema):
    axisRef: str
    relative_position: float | None = None
    absolute_position: float | None = None


class _UpdateParamSchema(HardwareSchema):
    axisRef: Optional[str] = None
    position: Optional[float] = None  # can be None


class _PropertiesSchema(HardwareSchema):
    state: Optional[Literal[tuple(TomoFlatMotionStates)]] = Field(None, read_only=True)
    settle_time: Optional[int | float] = Field(None, read_only=True)
    power_onoff: Optional[bool] = Field(None, read_only=True)
    move_in_parallel: Optional[bool] = Field(None, read_only=True)
    available_axes: Optional[list[str]] = Field(None, read_only=True)
    motion: Optional[list[_MotionSchema]] = Field(None, read_only=True)


class _CallablesSchema(CallableSchema):
    update_axis: Callable1Arg[dict[str, int | float]]


class TomoFlatMotionType(ObjectType):
    NAME = "tomoflatmotion"
    PROPERTIES = _PropertiesSchema
    CALLABLES = _CallablesSchema


Default = TomoFlatMotionType
