#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Literal, Optional
from pydantic import BaseModel

from bliss.rest_service.typedef import (
    Field,
    ObjectType,
    CallableSchema,
    Callable1Arg,
    HardwareSchema,
)

logger = logging.getLogger(__name__)

TomoHoloStates = [
    "READY",
    "MOVING",
    "STABILIZING",
    "INVALID",
    "INVALID_DETECTOR_BINNING_CHANGED",
    "INVALID_DETECTOR_DISTANCE_CHANGED",
    "INVALID_DETECTOR_CHANGED",
    "UNKNOWN",
]


class _DistanceSchema(BaseModel):
    position: int | float
    z1: int | float
    pixel_size: int | float


class _PropertiesSchema(HardwareSchema):
    state: Literal[tuple(TomoHoloStates)] = Field("UNKNOWN", read_only=True)
    pixel_size: Optional[float]
    """None if not defined"""
    nb_distances: int
    settle_time: float
    distances: list[_DistanceSchema] = Field(read_only=True)


class _CallablesSchema(CallableSchema):
    move: Callable1Arg


class TomoHoloType(ObjectType):
    NAME = "tomoholo"
    PROPERTIES = _PropertiesSchema
    CALLABLES = _CallablesSchema


Default = TomoHoloType
