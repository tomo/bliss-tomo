#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Optional

from bliss.rest_service.typedef import (
    Field,
    HardwareRefField,
    ObjectType,
    CallableSchema,
    HardwareSchema,
)

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    sx: Optional[str] = HardwareRefField(read_only=True)
    sy: str = HardwareRefField(read_only=True)
    sz: str = HardwareRefField(read_only=True)
    somega: str = HardwareRefField(read_only=True)
    sampx: Optional[str] = HardwareRefField(read_only=True)
    sampy: Optional[str] = HardwareRefField(read_only=True)
    sampu: Optional[str] = HardwareRefField(read_only=True)
    sampv: Optional[str] = HardwareRefField(read_only=True)
    pusher: Optional[str] = HardwareRefField(read_only=True)
    air_bearing_x: Optional[str] = HardwareRefField(read_only=True)
    x_axis_focal_pos: Optional[float] = Field(read_only=True)
    detector_center: Optional[list[float | None]] = Field(read_only=True, length=2)


class _CallablesSchema(CallableSchema):
    pass


class TomoSampleStageType(ObjectType):
    NAME = "tomosamplestage"
    PROPERTIES = _PropertiesSchema
    CALLABLES = _CallablesSchema


Default = TomoSampleStageType
