#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging

from bliss.rest_service.typedef import (
    ObjectType,
    CallableSchema,
    EmptyCallable,
    HardwareSchema,
)

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    pass


class _CallablesSchema(CallableSchema):
    move_to_reference: EmptyCallable


class TomoReferencePositionType(ObjectType):
    NAME = "tomoreferenceposition"
    PROPERTIES = _PropertiesSchema
    CALLABLES = _CallablesSchema


Default = TomoReferencePositionType
