#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Literal

from bliss.rest_service.typedef import (
    Field,
    ObjectType,
    CallableSchema,
    EmptyCallable,
    HardwareSchema,
)

logger = logging.getLogger(__name__)

AirBearingStates = ["UNKNOWN", "ON", "OFF", "MOVING_ON", "MOVING_OFF", "FAULT"]


class AirBearingPropertiesSchema(HardwareSchema):
    state: Literal[tuple(AirBearingStates)] = Field("UNKNOWN", read_only=True)


class AirBearingCallablesSchema(CallableSchema):
    sync_hard: EmptyCallable
    on: EmptyCallable
    off: EmptyCallable


class AirBearingType(ObjectType):
    NAME = "airbearing"

    STATE_OK = [AirBearingStates[1], AirBearingStates[2]]

    PROPERTIES = AirBearingPropertiesSchema

    CALLABLES = AirBearingCallablesSchema


Default = AirBearingType
