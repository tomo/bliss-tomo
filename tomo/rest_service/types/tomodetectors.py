#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Optional

from bliss.rest_service.typedef import (
    Field,
    HardwareRefField,
    HardwareRefListField,
    ObjectType,
    CallableSchema,
    Callable1Arg,
    HardwareSchema,
)

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    state: Optional[str] = Field("UNKNOWN", read_only=True)
    detectors: list[str] = HardwareRefListField(read_only=True)
    active_detector: Optional[str] = HardwareRefField(None, read_only=True)


class _CallablesSchema(CallableSchema):
    mount: Callable1Arg[str]


class TomoDetectorsType(ObjectType):
    NAME = "tomodetectors"
    PROPERTIES = _PropertiesSchema
    CALLABLES = _CallablesSchema


Default = TomoDetectorsType
