#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Optional

from bliss.rest_service.typedef import (
    Field,
    ObjectType,
    HardwareRefField,
    CallableSchema,
    HardwareSchema,
)

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    state: Optional[str] = Field("UNKNOWN", read_only=True)
    camera_pixel_size: list[float] = Field(length=2, read_only=True)
    actual_size: Optional[list[int]] = Field(None, length=2, read_only=True)
    sample_pixel_size_mode: Optional[str] = None
    user_sample_pixel_size: Optional[float] = None
    sample_pixel_size: Optional[float] = Field(None, read_only=True)
    field_of_view: Optional[float] = Field(None, read_only=True)
    detector: Optional[str] = HardwareRefField(None, read_only=True)
    optic: Optional[str] = HardwareRefField(None, read_only=True)
    source_distance: Optional[float] = Field(None, read_only=True)
    acq_mode: Optional[str] = None


class _CallablesSchema(CallableSchema):
    pass


class TomoDetectorType(ObjectType):
    NAME = "tomodetector"
    PROPERTIES = _PropertiesSchema
    CALLABLES = _CallablesSchema


Default = TomoDetectorType
