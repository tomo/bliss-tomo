#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Optional

from bliss.rest_service.typedef import (
    Field,
    ObjectType,
    CallableSchema,
    Callable1Arg,
    HardwareSchema,
)

logger = logging.getLogger(__name__)


class OpticPropertiesSchema(HardwareSchema):
    state: Optional[str]
    magnification: Optional[float]
    available_magnifications: Optional[list[float]]
    target_magnification: Optional[float]
    magnification_range: Optional[list[float]] = Field(length=2)


class OpticCallablesSchema(CallableSchema):
    move: Callable1Arg[float]


class OpticType(ObjectType):
    NAME = "optic"

    PROPERTIES = OpticPropertiesSchema

    CALLABLES = OpticCallablesSchema


Default = OpticType
