#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Optional

from bliss.rest_service.typedef import (
    ObjectType,
    CallableSchema,
    HardwareSchema,
    HardwareRefField,
)

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    imaging: Optional[str] = HardwareRefField(None, read_only=True)
    sample_stage: str = HardwareRefField(read_only=True)
    sxbeam: Optional[str] = HardwareRefField(None, read_only=True)
    detectors: str = HardwareRefField(read_only=True)
    reference_position: Optional[str] = HardwareRefField(None, read_only=True)
    energy: Optional[float] = None
    flat_motion: str = HardwareRefField(read_only=True)
    holotomo: Optional[str] = HardwareRefField(None, read_only=True)
    latency_time: float


class _CallablesSchema(CallableSchema):
    pass


class TomoConfigType(ObjectType):
    NAME = "tomoconfig"
    PROPERTIES = _PropertiesSchema
    CALLABLES = _CallablesSchema


Default = TomoConfigType
