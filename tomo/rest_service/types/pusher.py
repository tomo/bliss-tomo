#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Literal, Optional

from bliss.rest_service.typedef import (
    Field,
    ObjectType,
    CallableSchema,
    Callable1Arg,
    EmptyCallable,
    HardwareSchema,
)

logger = logging.getLogger(__name__)

PusherStates = ["UNKNOWN", "RETRACTED", "IN_TOUCH", "MOVING_IN", "MOVING_OUT"]


class PusherPropertiesSchema(HardwareSchema):
    state: Optional[Literal[tuple(PusherStates)]] = Field(None, read_only=True)


class PusherCallablesSchema(CallableSchema):
    sync_hard: EmptyCallable
    move_in: EmptyCallable
    move_out: EmptyCallable
    push: Callable1Arg[float]


class PusherType(ObjectType):
    NAME = "pusher"
    STATE_OK = [PusherStates[1], PusherStates[2]]

    PROPERTIES = PusherPropertiesSchema
    CALLABLES = PusherCallablesSchema


Default = PusherType
