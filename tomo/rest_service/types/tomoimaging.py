#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
from typing import Literal

from bliss.rest_service.typedef import (
    Field,
    ObjectType,
    CallableSchema,
    EmptyCallable,
    HardwareSchema,
)

logger = logging.getLogger(__name__)

TomoImagingStates = ["READY", "BLOCKED", "ACQUIRING", "UNKNOWN"]


class _PropertiesSchema(HardwareSchema):
    state: Literal[tuple(TomoImagingStates)] = Field("UNKNOWN", read_only=True)
    update_on_move: bool
    exposure_time: float
    settle_time: float


class _CallablesSchema(CallableSchema):
    take_proj: EmptyCallable
    take_dark: EmptyCallable
    take_flat: EmptyCallable


class TomoImagingType(ObjectType):
    NAME = "tomoimaging"
    PROPERTIES = _PropertiesSchema
    CALLABLES = _CallablesSchema


Default = TomoImagingType
