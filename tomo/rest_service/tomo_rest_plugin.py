# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

import importlib
import logging
from typing import Any
from bliss.rest_service.rest_plugin import RestPlugin
from bliss.rest_service.typedef import ObjectType
from bliss.rest_service.mappingdef import ObjectMapping


_logger = logging.getLogger(__name__)


BLISS_TOMO_CLASSNAMES = {
    "tomo.controllers.pusher.Pusher": "pusher",
    "tomo.controllers.air_bearing.AirBearing": "airbearing",
    "tomo.controllers.tomo_config.TomoConfig": "tomoconfig",
    "tomo.controllers.tomo_detector.TomoDetector": "tomodetector",
    "tomo.controllers.tomo_detectors.TomoDetectors": "tomodetectors",
    "tomo.controllers.tomo_imaging.TomoImaging": "tomoimaging",
    "tomo.controllers.tomo_sample_stage.TomoSampleStage": "tomosamplestage",
    "tomo.controllers.flat_motion.FlatMotion": "tomoflatmotion",
    "tomo.controllers.holotomo.Holotomo": "tomoholo",
    "tomo.controllers.flat_motion.FlatMotion": "tomoflatmotion",
    "tomo.controllers.parking_position.ParkingPosition": "tomoreferenceposition",
    "tomo.optic.base_optic.BaseOptic": "optic",
    # Special objects
    "bliss.rest_service.dummies.DummyActiveTomo": "activetomoconfig",
}
"""
Mapping from `bliss-tomo` fully qualified class name to mapped object name located in
`bliss.rest_service.hardware.mapped`.

This have to be removed at some point.
"""


class TomoRestPlugin(RestPlugin):

    def get_object_type_names(self) -> list[str]:
        from tomo.rest_service import types

        return self.get_module_names_from_package(types)

    def get_object_type(self, obj_type: str) -> type[ObjectType]:
        from tomo.rest_service import types

        return self.get_object_type_from_package(obj_type, types)

    def get_mapping_class_from_obj(self, obj: Any) -> type[ObjectMapping] | None:
        from tomo.rest_service import mappings

        mapped_name = self.get_mapping_name_from_object(obj)
        if mapped_name is None:
            return None
        return self.get_mapping_class_from_package(mapped_name, mappings)

    def get_mapping_name_from_object(self, obj: Any) -> str | None:
        """
        Get a mapped class from an object.
        """
        for bliss_mapping, mapped_class_name in BLISS_TOMO_CLASSNAMES.items():
            bliss_file, bliss_class_name = bliss_mapping.rsplit(".", 1)
            # Some classes may not be available depending on the bliss version
            try:
                bliss_module = importlib.import_module(bliss_file)
                bliss_class = getattr(bliss_module, bliss_class_name)
            except ModuleNotFoundError:
                _logger.warning("Could not find bliss module %s", bliss_mapping)
                continue
            if isinstance(obj, bliss_class):
                return mapped_class_name

        return None
