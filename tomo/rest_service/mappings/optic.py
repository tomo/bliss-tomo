#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ..types.optic import OpticType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    HardwareProperty,
    EnumProperty,
)

from tomo.optic.base_optic import OpticState

logger = logging.getLogger(__name__)


class Optic(ObjectMapping):
    TYPE = OpticType

    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=OpticState),
        "magnification": HardwareProperty("magnification"),
        "available_magnifications": HardwareProperty("available_magnifications"),
        "target_magnification": HardwareProperty("target_magnification"),
        "magnification_range": HardwareProperty("magnification_range"),
    }

    # Call moves with wait=False
    def _call_move(self, value, **kwargs):
        logger.debug("_call_move %s %s %s", self.name(), value, kwargs)

        try:
            self._object.move_magnification(value, wait=False)
        except Exception:
            logger.error(
                "Error while moving magnification %s to %s",
                self._object.name,
                value,
                exc_info=True,
            )
            raise


Default = Optic
