#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ..types.airbearing import AirBearingType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    EnumProperty,
)
from tomo.controllers.air_bearing import AirBearingState

logger = logging.getLogger(__name__)


class AirBearing(ObjectMapping):
    TYPE = AirBearingType

    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=AirBearingState),
    }

    CALLABLE_MAP = {
        "on": "on",
        "off": "off",
        "sync_hard": "sync_hard",
    }


Default = AirBearing
