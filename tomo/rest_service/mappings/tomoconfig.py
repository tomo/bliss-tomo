#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ..types.tomoconfig import TomoConfigType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    ObjectRefProperty,
    HardwareProperty,
)

logger = logging.getLogger(__name__)


class TomoConfig(ObjectMapping):
    TYPE = TomoConfigType

    PROPERTY_MAP = {
        "sample_stage": ObjectRefProperty("sample_stage", compose=True),
        "imaging": ObjectRefProperty("tomo_imaging", compose=True),
        "sxbeam": ObjectRefProperty("tracked_x_axis", compose=True),
        "detectors": ObjectRefProperty("detectors", compose=True),
        "reference_position": ObjectRefProperty("reference_position", compose=True),
        "energy": HardwareProperty("energy"),
        "flat_motion": ObjectRefProperty("reference", compose=True),
        "holotomo": ObjectRefProperty("holotomo", compose=True),
        "latency_time": HardwareProperty("latency_time"),
    }


Default = TomoConfig
