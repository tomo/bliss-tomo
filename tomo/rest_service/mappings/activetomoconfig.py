#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from bliss.rest_service.types.objectref import ObjectrefType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    ObjectRefProperty,
)

from tomo.globals import ACTIVE_TOMOCONFIG

logger = logging.getLogger(__name__)


class _ActiveTomoConfigRefProperty(ObjectRefProperty):
    def __init__(self):
        ObjectRefProperty.__init__(self, "ACTIVE_TOMOCONFIG", compose=True)

    @property
    def getter(self):
        return _ActiveTomoConfigRefProperty._get_ref

    def _get_ref(self):
        try:
            active_obj = ACTIVE_TOMOCONFIG.deref_active_object()
        except Exception:
            return None
        if active_obj is None:
            return None
        return active_obj.name


class Activetomoconfig(ObjectMapping):
    TYPE = ObjectrefType

    PROPERTY_MAP = {"ref": _ActiveTomoConfigRefProperty()}


Default = Activetomoconfig
