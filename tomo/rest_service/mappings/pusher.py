#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ..types.pusher import PusherType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    EnumProperty,
)
from tomo.controllers.pusher import PusherState

logger = logging.getLogger(__name__)


class Pusher(ObjectMapping):
    TYPE = PusherType

    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=PusherState),
    }

    CALLABLE_MAP = {
        "move_in": "move_in",
        "move_out": "move_out",
        "sync_hard": "sync_hard",
        "push": "push",
    }


Default = Pusher
