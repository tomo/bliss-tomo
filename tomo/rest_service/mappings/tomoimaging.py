#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ..types.tomoimaging import TomoImagingType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    EnumProperty,
    HardwareProperty,
)
from tomo.controllers.tomo_imaging import TomoImagingState

logger = logging.getLogger(__name__)


class TomoImaging(ObjectMapping):
    TYPE = TomoImagingType

    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=TomoImagingState),
        "update_on_move": HardwareProperty("update_on_move"),
        "exposure_time": HardwareProperty("exposure_time"),
        "settle_time": HardwareProperty("settle_time"),
    }

    CALLABLE_MAP = {
        "take_proj": "take_proj",
        "take_dark": "take_dark",
        "take_flat": "take_flat",
    }


Default = TomoImaging
