#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from bliss.config.static import get_config
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    EnumProperty,
    ObjectRefProperty,
    ObjectRefListProperty,
)

from tomo.controllers.tomo_detectors import TomoDetectorsState
from ..types.tomodetectors import TomoDetectorsType


logger = logging.getLogger(__name__)


class TomoDetectors(ObjectMapping):
    TYPE = TomoDetectorsType

    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=TomoDetectorsState),
        "active_detector": ObjectRefProperty("active_detector"),
        "detectors": ObjectRefListProperty("detectors", compose=True),
    }

    # Call moves with wait=False
    def _call_mount(self, value, **kwargs):
        logger.debug(f"_call_mount {self.name()} {value} {kwargs}")

        if value.startswith("hardware:"):
            device_name = value[9:]
        else:
            device_name = value

        config = get_config()
        device = config.get(device_name)
        if device is not None:
            self._object.mount(device, wait=False)


Default = TomoDetectors
