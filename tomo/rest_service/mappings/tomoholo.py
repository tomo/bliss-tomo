#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ..types.tomoholo import TomoHoloType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    EnumProperty,
    HardwareProperty,
)
from tomo.controllers.holotomo import HolotomoState, HolotomoDistance

logger = logging.getLogger(__name__)


class _DistancesProperty(HardwareProperty):
    def translate_from(self, values: list[HolotomoDistance]):
        if values is None:
            return None
        result = []
        for v in values:
            result.append(v._asdict())
        return result


class TomoHolo(ObjectMapping):
    TYPE = TomoHoloType

    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=HolotomoState),
        "pixel_size": HardwareProperty("pixel_size"),
        "nb_distances": HardwareProperty("nb_distances"),
        "settle_time": HardwareProperty("settle_time"),
        "distances": _DistancesProperty("distances"),
    }

    CALLABLE_MAP = {
        "move": "move",
    }


Default = TomoHolo
