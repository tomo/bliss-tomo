#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ..types.tomoreferenceposition import TomoReferencePositionType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
)

logger = logging.getLogger(__name__)


class TomoReferencePosition(ObjectMapping):
    TYPE = TomoReferencePositionType

    PROPERTY_MAP = {}

    CALLABLE_MAP = {
        "move_to_reference": "move_to_reference",
    }


Default = TomoReferencePosition
