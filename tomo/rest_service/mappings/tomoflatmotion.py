#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ..types.tomoflatmotion import TomoFlatMotionType
from bliss.rest_service.mappingdef import (
    ObjectMapping,
    HardwareProperty,
    ObjectRefListProperty,
)
from tomo.controllers.flat_motion import FlatMotion
from bliss.common.axis import Axis

logger = logging.getLogger(__name__)


class _MotionProperty(HardwareProperty):
    def translate_from(self, values: list[FlatMotion]):
        if values is None:
            return None
        result = []
        for v in values:
            meta = v._asdict()
            name = meta.pop("axis_name")
            meta["axisRef"] = f"hardware:{name}"
            result.append(meta)
        return result


class _AvailableAxesProperty(ObjectRefListProperty):
    def translate_from(self, values: list[Axis]):
        if values is None:
            return None
        result = []
        for v in values:
            result.append(f"hardware:{v.name}")
        return result


class TomoFlatMotion(ObjectMapping):
    TYPE = TomoFlatMotionType

    def _get_state(self):
        return "READY"

    def _get_motion(self):
        return self._object._out_beam_motion

    PROPERTY_MAP = {
        "state": HardwareProperty("state", getter=_get_state),
        "settle_time": HardwareProperty("settle_time"),
        "power_onoff": HardwareProperty("power_onoff"),
        "move_in_parallel": HardwareProperty("move_in_parallel"),
        "available_axes": _AvailableAxesProperty("_available_motors", compose=True),
        "motion": _MotionProperty("out_beam_motion", getter=_get_motion),
    }

    # Call moves with wait=False
    def _call_update_axis(self, value: dict, **kwargs):
        logger.debug(f"_call_update_axis {self.name} {value} {kwargs}")
        axis_name = value.pop("axis_ref")[9:]
        absolute_position = value.pop("absolute_position", None)
        relative_position = value.pop("relative_position", None)
        if value != {}:
            raise ValueError(f"Unused keys: {value}")
        self._object.update_axis(
            axis_name=axis_name,
            absolute_position=absolute_position,
            relative_position=relative_position,
        )


Default = TomoFlatMotion
