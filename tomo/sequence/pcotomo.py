import datetime
import gevent
import contextlib
import math
from bliss import setup_globals, current_session
from bliss.scanning.scan import ScanPreset, ScanState
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.group import Sequence
from bliss.common.cleanup import cleanup, axis as cleanup_axis, capture_exceptions
from bliss.common.logtools import log_error, log_warning
from bliss.controllers.lima.lima_base import Lima

from fscan.fscantools import FScanParamStruct
from fscan.fscantools import FScanParamBase
from fscan.fscantools import FScanModeBase

from tomo.scan.presets.common_header import CommonHeaderPreset
from tomo.scan.presets.wait_for_beam import WaitForBeamPreset
from tomo.scan.presets.wait_for_refill import WaitForRefillPreset
from tomo.scan.presets.image_spectrum import ImageSpectrumPreset
from tomo.scan.presets.projection_spectrum import ProjectionSpectrumPreset
from tomo.metadata import TomoMetaData
from tomo.sinogram import TomoSinogram
from tomo.sequence.presets.base import SequencePreset
from tomo.helpers import string_utils
from tomo.scan.image_runner import ImageRunner
from bliss import current_session
from bliss import setup_globals
from bliss.common.logtools import log_info, log_debug, log_error
from fscan.fscantools import FScanParamBase, FScanModeBase
from tomo.sequencebasic import SequenceBasic, ScanType, cleanup_sequence
from bliss.scanning.scan_info import ScanInfo
from bliss.controllers.motors.elmo import Elmo
from bliss.controllers.motors.elmo_whistle import Elmo_whistle
from bliss.controllers.motors.icepap import Icepap
from tomo.sequence.presets.inhibit_auto_projection import InhibitAutoProjection
from tomo.sequence.multitomo import MultiTurnsTomo, MultiTurnsTomoPars
from tomo.helpers.proxy_param import ProxyParam
from bliss.common.cleanup import cleanup, axis as cleanup_axis, capture_exceptions
from bliss.shell.dialog.helpers import dialog
from tomo.sequence.presets.send_trigger_preset import SendTriggerPreset
from tomo.sequence.presets.user_info_preset import UserInfoPreset

LoopMode = FScanModeBase("LoopMode", "FILL_MEMORY", "DOWNLOADING")
SequenceTriggerMode = FScanModeBase(
    "SequenceTriggerMode", "EXTERNAL", "INTERNAL", "EXTSTART"
)
ShutterMode = FScanModeBase("ShutterMode", "SYNCHRONIZED", "SOFT")


class PcoTomoPars(MultiTurnsTomoPars):
    DEFAULT = MultiTurnsTomoPars.DEFAULT.copy()
    DEFAULT.update(
        {
            "sequence_trigger_mode": SequenceTriggerMode.INTERNAL,
            "loop_mode": LoopMode.FILL_MEMORY,
            "shutter_mode": ShutterMode.SYNCHRONIZED,
            "shutter_time": 0,
            "download_after_ntomo": 1,
        }
    )
    LISTVAL = MultiTurnsTomoPars.LISTVAL.copy()
    LISTVAL.update(
        {
            "loop_mode": LoopMode.values,
            "shutter_mode": ShutterMode.values,
            "sequence_trigger_mode": SequenceTriggerMode.values,
        }
    )

    def _validate_loop_mode(self, value):
        return LoopMode.get(value, "loop_mode")

    def _validate_shutter_mode(self, value):
        return ShutterMode.get(value, "shutter_mode")


class PcoTomo(MultiTurnsTomo):
    """
    Class to handle multiple turns tomo acquisition with dimax

    **Attributes**

    name : str
        The Bliss object name
    """

    def __init__(self, name, config):
        self.name = name + "_sequence"
        super(MultiTurnsTomo, self).__init__(name, config)
        self._sequence_name = "pcotomo:basic"

        inhibitautoproj = InhibitAutoProjection(self._tomo_config)
        self.add_sequence_preset(inhibitautoproj)
        self._inttrigger_func = None
        self._exttrigger_func = None

    def _create_parameters(self):
        tomo_config = self.tomo_config
        sequence_pars = PcoTomoPars(self.name)
        return ProxyParam(tomo_config.pars, sequence_pars)

    def add_metadata(self, scan_info):
        """
        Fill the scan_info dictionary with the basic tomo meta data
        Add lateral motor name to tomo meta data
        """
        super().add_metadata(scan_info)
        scan_info["technique"]["scan"]["sequence"] = "tomo:pcotomo"
        scan_info["technique"]["scan"]["ntomo"] = self.pars.ntomo
        scan_info["technique"]["scan"]["loop_mode"] = self.pars.loop_mode
        scan_info["technique"]["scan"]["shutter_mode"] = self.pars.shutter_mode

    def projection_scan(
        self, motor, start_pos, end_pos, tomo_n, expo_time, scan_info=None, run=True
    ):
        """
        Get runner and build scan
        Return projection scan object
        """
        runner = self.get_runner("MULTITURNS")
        latency_time = self.pars.latency_time
        tomo_loop = self.pars.tomo_loop
        waiting_angle = self.pars.waiting_turns * (end_pos - start_pos)
        start_turns = self.pars.start_turns * 360
        start_pos += start_turns
        end_pos = start_pos + self.pars.range * self.pars.ntomo
        tomo_n = self.pars.tomo_n * self.pars.ntomo
        runner.pars.shutter_time = self.pars.shutter_time
        loop_mode = self.pars.loop_mode
        download_after_ntomo = tomo_n
        fscanloop = runner._fscanloop
        if self.pars.shutter_mode == "SOFT":
            runner.pars.shutter_time = 0
        else:
            runner.pars.shutter_time = self.pars.shutter_time
        if (
            self.pars.sequence_trigger_mode == "EXTSTART"
            or self.pars.sequence_trigger_mode == "EXTERNAL"
        ):
            fscanloop.pars.start_trig_mode = "EXTERNAL"
        else:
            fscanloop.pars.start_trig_mode = "INTERNAL"
        if loop_mode == "FILL_MEMORY":
            loop_mode = "INTERNAL"
            lima_read_mode = "PER_SCAN"
            if self.pars.waiting_turns > 0:
                if self.pars.sequence_trigger_mode == "EXTERNAL":
                    loop_mode = "EXTERNAL"
        elif loop_mode == "DOWNLOADING":
            loop_mode = "LIMA_READY"
            lima_read_mode = "PER_LOOP"
            waiting_angle = 360
            if self.pars.waiting_turns > 0:
                tomo_loop *= self.pars.ntomo
                end_pos = start_pos + self.pars.range
                tomo_n = self.pars.tomo_n
                download_after_ntomo = self.pars.download_after_ntomo * tomo_n
            else:
                waiting_angle = 360
                download_after_ntomo = tomo_n

        if (
            self.pars.sequence_trigger_mode == "INTERNAL"
            or self.pars.sequence_trigger_mode == "EXTSTART"
        ):
            fscanloop = runner._fscanloop
            fscanloop.set_lima_ready_trigger_func(self._inttrigger_func)
        elif self.pars.sequence_trigger_mode == "EXTERNAL":
            fscanloop = runner._fscanloop
            fscanloop.set_lima_ready_trigger_func(self._exttrigger_func)

        tomo_scan = runner(
            motor,
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            tomo_loop,
            self.pars.range,
            waiting_angle=waiting_angle,
            loop_mode=loop_mode,
            latency_time=latency_time,
            lima_read_mode=lima_read_mode,
            images_per_loop=download_after_ntomo,
            scan_info=scan_info,
            run=False,
        )

        if self.pars.loop_mode == "DOWNLOADING" and self.pars.waiting_turns > 0:
            iter_nimages = [
                i
                for i in range(tomo_n, tomo_n * tomo_loop, tomo_n)
                if i
                not in range(
                    download_after_ntomo,
                    download_after_ntomo * tomo_loop,
                    download_after_ntomo,
                )
            ]
            fscanloop = runner._fscanloop
            sleep_time = 360 / fscanloop.inpars.speed * (self.pars.waiting_turns - 1)
            musst = fscanloop.master.musst_list.find_musst_for_motors(
                fscanloop.pars.motor
            )

            send_trigger_preset = SendTriggerPreset(
                musst, motor, sleep_time, iter_nimages, self._inttrigger_func
            )
            tomo_scan.add_preset(send_trigger_preset)

        userinfopreset = UserInfoPreset(fscanloop.pars.start_trig_mode)
        tomo_scan.add_preset(userinfopreset)

        self._inpars.proj_time = runner._estimate_scan_duration(self._inpars)

        if run:
            tomo_scan.run()

        return tomo_scan

    def full_turn_scan(
        self,
        ntomo=None,
        tomo_loop=None,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        waiting_turns=None,
        loop_mode=None,
        start_turns=None,
        download_after_ntomo=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo of 360 angular range
        """
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if ntomo is not None:
            self.pars.ntomo = ntomo
        if tomo_loop is not None:
            self.pars.tomo_loop = tomo_loop
        if waiting_turns is not None:
            self.pars.waiting_turns = waiting_turns
        if loop_mode is not None:
            self.pars.loop_mode = loop_mode
        if start_turns is not None:
            self.pars.start_turns = start_turns
        if download_after_ntomo is not None:
            self.pars.download_after_ntomo = download_after_ntomo
        self.pars.range = 360

        self._setup_sequence("pcotomo:fullturn", run=run, scan_info=scan_info)

    def half_turn_scan(
        self,
        ntomo=None,
        tomo_loop=None,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        waiting_turns=None,
        loop_mode=None,
        start_turns=None,
        download_after_ntomo=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo of 180 angular range
        """
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if ntomo is not None:
            self.pars.ntomo = ntomo
        if tomo_loop is not None:
            self.pars.tomo_loop = tomo_loop
        if waiting_turns is not None:
            self.pars.waiting_turns = waiting_turns
        if loop_mode is not None:
            self.pars.loop_mode = loop_mode
        if start_turns is not None:
            self.pars.start_turns = start_turns
        if download_after_ntomo is not None:
            self.pars.download_after_ntomo = download_after_ntomo
        self.pars.range = 180

        self._setup_sequence("pcotomo:halfturn", run=run, scan_info=scan_info)

    def basic_scan(
        self,
        ntomo=None,
        tomo_loop=None,
        start_pos=None,
        end_pos=None,
        tomo_n=None,
        expo_time=None,
        dataset_name=None,
        collection_name=None,
        waiting_turns=None,
        loop_mode=None,
        start_turns=None,
        download_after_ntomo=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo using given parameters
        """

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if end_pos is not None:
            self.pars.range = self.pars.start_pos + end_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time
        if ntomo is not None:
            self.pars.ntomo = ntomo
        if tomo_loop is not None:
            self.pars.tomo_loop = tomo_loop
        if waiting_turns is not None:
            self.pars.waiting_turns = waiting_turns
        if loop_mode is not None:
            self.pars.loop_mode = loop_mode
        if start_turns is not None:
            self.pars.start_turns = start_turns
        if download_after_ntomo is not None:
            self.pars.download_after_ntomo = download_after_ntomo

        self._setup_sequence("pcotomo:basic", run=run, scan_info=scan_info)

    def _check_max_images(self):
        pars = self.pars
        detector = self._detector
        max_images = detector.camera.max_nb_images
        max_scans = max_images / pars.tomo_n
        return int(max_scans)

    def show_info(self):
        runner = self.get_runner("MULTITURNS")
        fscanloop = runner._fscanloop
        info_str = self.__info__()
        one_scan_time = self.pars.range / fscanloop.inpars.speed
        waiting_time = 360 / fscanloop.inpars.speed * self.pars.waiting_turns
        frequency = one_scan_time + waiting_time
        sizeofoneimage = (
            self._detector.image.depth
            * self._detector.image.width
            * self._detector.image.height
            / 1024
            / 1024
        )
        sizeofonescan = sizeofoneimage * self.pars.tomo_n / 1024
        numberofscans = self.pars.download_after_ntomo
        self._estimate_saving_rate()
        transferrate = round(
            self._detector.proxy.saving_statistics[-1] / 1024 / 1024, 2
        )
        if self.pars.loop_mode == "DOWNLOADING":
            if self.pars.waiting_turns > 0:
                nb_loop = self.pars.tomo_loop / self.pars.download_after_ntomo
            else:
                nb_loop = self.pars.tomo_loop
            info_str += (
                "\n\n"
                + f"{nb_loop}"
                + " times "
                + f"{self.pars.download_after_ntomo}"
                + " scans of "
                + f"{one_scan_time:.2f}"
                + " secondes "
            )
        else:
            info_str += (
                "\n\n"
                + f"{self.pars.tomo_loop*self.pars.ntomo}"
                + " scans of "
                + f"{one_scan_time:.2f}"
                + " secondes "
            )
        if self.pars.waiting_turns:
            info_str += "\n every " + f"{frequency:.2f}" + " secondes "
        info_str += (
            "\n\n"
            f"Time to download film: {round((sizeofonescan*numberofscans)/(transferrate/1024),2)} s ({round((sizeofonescan*numberofscans)/(transferrate/1024)/60,2)} min)\n"
        )
        print(info_str)

    def _estimate_saving_rate(self):
        if self._detector.proxy.saving_statistics[-1] == 0.0:
            scan_saving = current_session.scan_saving
            prev_collection = scan_saving.collection_name
            prev_dataset = scan_saving.dataset_name
            scan_saving.newcollection("tmp")
            scan_saving.newdataset("dimax_estimation")
            scan = ImageRunner()
            scan(self.pars.exposure_time, 50)
            scan_saving.newcollection(prev_collection)
            scan_saving.newdataset(prev_dataset)


# Setup the standard BLISS `menu`
@dialog(PcoTomo.__name__, "setup")
def _setup(sequence):
    from ..utils import setup

    setup(sequence)
