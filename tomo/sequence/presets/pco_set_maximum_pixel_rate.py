from .base import SequencePreset
from tomo.standard import get_controller_lima
from bliss.common.logtools import log_warning


class PcoSetMaximumPixelRatePreset(SequencePreset):
    """
    Class used to set maximum pixel rate on pco camera
    """

    def prepare(self):

        # get detector for sequence
        detector = get_controller_lima()
        if detector is not None:
            try:
                # set pixel rate to maximum
                if detector.camera_type.lower() == "pco":
                    detector.camera.pixel_rate = (
                        detector.camera.pixel_rate_valid_values[-1]
                    )
            except AttributeError:
                log_warning(self, f"{detector.name} server is not running")
                return
        else:
            log_warning(self, "No detector found")
            return
