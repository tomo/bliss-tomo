import weakref
from .base import SequencePreset
from tomo.controllers.tomo_config import TomoConfig


class InhibitAutoProjection(SequencePreset):
    """Inhibit the auto projection provided by the specified TomoConfig"""

    def __init__(self, tomoconfig: TomoConfig):
        assert isinstance(tomoconfig, TomoConfig)
        super(InhibitAutoProjection, self).__init__(
            "inhibit_auto_projection", {"tomo": tomoconfig.name}
        )
        self.__tomoconfig = weakref.ref(tomoconfig)
        self.__context = None

    @property
    def _tomoconfig(self):
        return self.__tomoconfig()

    def start(self):
        assert self.__context is None
        tomoconfig = self._tomoconfig
        self.__context = tomoconfig.auto_projection.inhibit()
        self.__context.__enter__()

    def stop(self):
        self.cleanup()

    def cleanup(self):
        if self.__context is not None:
            self.__context.__exit__(None, None, None)
            self.__context = None
