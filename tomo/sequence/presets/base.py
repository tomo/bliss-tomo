from bliss import global_map
from bliss.common.logtools import log_info


class SequencePreset:
    """
    Abstract class to define a preset to a sequence.

    The class implements methods which allow to do some actions during tomo
    acquisition prepare, before running tomo acquisition and at the end of
    tomo acquisition.

    Attributes:
        name: Name of the object
        config: configuration provided by the yaml file
    """

    def __init__(self, name: str, config: dict):
        # init logging
        self.log_name = name + ".sequence_preset"
        global_map.register(self, tag=self.log_name)
        log_info(self, "__init__() entering")

        self.name = name
        self.config = config

        log_info(self, "__init__() leaving")

    def prepare(self):
        """
        This methods is called during tomo acquisition prepare.
        """
        pass

    def start(self):
        """
        This methods is called before running tomo acquisition.
        """
        pass

    def stop(self):
        """
        This methods is called at the end of tomo acquisition.
        """
        pass

    def cleanup(self):
        """
        This methods is always called at the end of tomo acquisition.
        """
        pass
