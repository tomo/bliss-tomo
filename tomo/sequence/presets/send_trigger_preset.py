from bliss.scanning.scan import ScanPreset
import time


class SendTriggerPreset(ScanPreset):
    """ """

    def __init__(self, musst, rotation, sleep_time, iter_images, trigger_func):
        super().__init__()
        self.musst = musst
        self.rotation = rotation
        self.sleep_time = sleep_time
        self.iter_images = iter_images
        self.trigger_func = trigger_func
        self._data = []

    def prepare(self, scan):
        self.connect_data_channels([self.musst, ...], self.send_trigger)

    def send_trigger(self, counter, channel_name, data):
        if channel_name == f"{self.musst.name}:{self.rotation.name}_raw":
            self._data.extend(data.tolist())
            if len(self._data) / 2 in self.iter_images:
                time.sleep(self.sleep_time)
                self.trigger_func()
