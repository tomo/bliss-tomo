"""Sequence presets

.. autosummary::
   :toctree:

   base
   inhibit_auto_projection
   send_trigger_preset
   user_info_preset
"""
