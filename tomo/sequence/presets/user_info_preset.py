from bliss.scanning.scan import ScanPreset
from bliss.common.utils import BOLD


class UserInfoPreset(ScanPreset):
    def __init__(self, start_mode):
        super().__init__()
        self.start_mode = start_mode

    def start(self, scan):
        if self.start_mode == "EXTERNAL":
            print(
                BOLD(
                    "\n\nYou can send trigger when you want. Acquisition will not start before rotation has reached scan speed\n\n"
                )
            )
        else:
            print(
                BOLD(
                    "\n\nAcquisition will start as soon as rotation has reached scan speed\n\n"
                )
            )
