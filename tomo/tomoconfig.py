# Compatibility with tomo <= 2.3.1

from tomo.controllers.tomo_config import TomoConfig  # noqa
from tomo.controllers.tomo_config import ConfigTomoPars  # noqa
from bliss.common import deprecation

deprecation.deprecated_warning(
    "Module",
    "tomo.tomoconfig",
    replacement="tomo.controllers.tomo_config",
    since_version="2.3.1",
)
