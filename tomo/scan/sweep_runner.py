import logging
from bliss.common.logtools import log_info
from bliss.scanning.scan_info import ScanInfo
from bliss import current_session
from tomo.scan.tomo_runner import TomoRunner
from tomo.constants import NxTomoImageKey
from fscan.fscantools import FScanParamBase

_logger = logging.getLogger(__name__)


class SweepRunner(TomoRunner):
    """
    Class used to build and run scan covering all angular space by
    rewinding motor for each image acquisition
    Built as a layer of fsweep
    """

    def __init__(self, config):
        super().__init__()
        self._projection_scan_name = "fsweep"
        self._fscan_config = config
        self._fsweep = config.get_runner(self._projection_scan_name)
        self.pars: FScanParamBase = self._fsweep.pars
        """fsweep parameters"""
        self._scan = None

    def __call__(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        scan_info=None,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like fsweep or
        bliss common scans
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        self.scan_info = ScanInfo.normalize(scan_info)
        if header is None:
            header = {}

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            motor, start_pos, end_pos, expo_time, tomo_n, scan_info
        )

        self._setup_detectors_from_active_mg()

        self._scan = self._setup_scan(
            motor,
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build and return fsweep
        """
        log_info("tomo", "projection_scan() entering")

        fsweep = self._fsweep
        fsweep.pars.motor = motor
        fsweep.pars.start_pos = start_pos
        fsweep.pars.acq_size = (end_pos - start_pos) / tomo_n
        fsweep.pars.acq_time = expo_time
        fsweep.pars.npoints = tomo_n
        fsweep.prepare(scan_info)
        proj_scan = fsweep.scan

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Get scan time already estimated in fsweep.validate()
        """
        fsweep = self._fsweep
        fsweep.validate()
        return fsweep.inpars.scan_time

    def _add_metadata(
        self, motor, start_pos, end_pos, expo_time, proj_n, scan_info=None
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        alias_name = "None"
        ALIASES = current_session.env_dict["ALIASES"]

        if ALIASES.get_alias(motor.name) is not None:
            alias_name = ALIASES.get_alias(motor.name)
        technique = scan_info.setdefault("technique", {})
        technique["image_key"] = NxTomoImageKey.PROJECTION.value
        technique["proj"] = {
            "motor": ["rotation", motor.name, alias_name],
            "scan_type": "SWEEP",
            "scan_range": end_pos - start_pos,
            "proj_n": proj_n,
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        return scan_info

    def get_scan_preset_list(self):
        """
        Return list of configured scan presets
        """
        # get the scan presets on the tomo runner
        preset_list = super().get_scan_preset_list()
        # get the scan presets on the fscan
        for i in self._fsweep._scan_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list

    def get_chain_preset_list(self):
        """
        Return list of configured chain presets
        """
        # get the chain presets on the tomo runner
        preset_list = super().get_chain_preset_list()
        # get the chain presets on the fscan
        for i in self._fsweep._chain_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list
