import logging

from bliss.common.logtools import log_info
from bliss.scanning.scan_info import ScanInfo
from bliss import current_session
from tomo.scan.tomo_runner import TomoRunner
from tomo.constants import NxTomoImageKey
from fscan.fscantools import FScanParamBase

_logger = logging.getLogger(__name__)


class Continuous2DRunner(TomoRunner):
    """
    Class used to build and run continuous scan with two motors
    One motor moves continuously and another one moves step by step
    Built as a layer of fscan2d
    """

    def __init__(self, config):
        super().__init__()
        self._projection_scan_name = "fscan2d"
        self._fscan_config = config
        self._fscan2d = config.get_runner(self._projection_scan_name)
        self.pars: FScanParamBase = self._fscan2d.pars
        """fscan2d parameters"""
        self._scan = None

    def __call__(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        scan_info=None,
        header=None,
        latency_time=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like fscan2d or
        bliss common scans
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        self.scan_info = ScanInfo.normalize(scan_info)
        if header is None:
            header = {}
        if latency_time is None:
            default_latency = self._fscan2d.pars.latency_time
            latency_time = default_latency

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            fast_motor,
            fast_start_pos,
            fast_end_pos,
            fast_npoints,
            slow_motor,
            slow_start_pos,
            slow_step_size,
            slow_npoints,
            expo_time,
            latency_time,
            scan_info,
        )

        self._setup_detectors_from_active_mg()

        self._scan = self._setup_scan(
            fast_motor,
            fast_start_pos,
            fast_end_pos,
            fast_npoints,
            slow_motor,
            slow_start_pos,
            slow_step_size,
            slow_npoints,
            expo_time,
            latency_time,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        latency_time,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build and return fscan2d
        """
        log_info("tomo", "projection_scan() entering")

        fscan2d = self._fscan2d
        fscan2d.pars.fast_motor = fast_motor
        fscan2d.pars.fast_start_pos = fast_start_pos
        fscan2d.pars.fast_step_size = (fast_end_pos - fast_start_pos) / fast_npoints
        fscan2d.pars.fast_step_time = fscan2d.pars.acq_time
        fscan2d.pars.fast_npoints = fast_npoints
        fscan2d.pars.slow_motor = slow_motor
        fscan2d.pars.slow_start_pos = slow_start_pos
        fscan2d.pars.slow_step_size = slow_step_size
        fscan2d.pars.slow_npoints = slow_npoints
        fscan2d.pars.acq_time = expo_time

        fscan2d.pars.latency_time = latency_time

        fscan2d.prepare(scan_info)
        proj_scan = fscan2d.scan

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Get scan time already estimated in fscan2d.validate()
        """
        fscan2d = self._fscan2d
        fscan2d.validate()
        return fscan2d.inpars.scan_time

    def _add_metadata(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        latency_time,
        scan_info=None,
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        motor = list()
        alias_name = "None"
        ALIASES = current_session.env_dict["ALIASES"]

        if ALIASES.get_alias(fast_motor.name) is not None:
            alias_name = ALIASES.get_alias(fast_motor.name)
        motor.extend(["fast_motor", fast_motor.name, alias_name])
        alias_name = "None"
        if ALIASES.get_alias(slow_motor.name) is not None:
            alias_name = ALIASES.get_alias(slow_motor.name)
        motor.extend(["slow_motor", slow_motor.name, alias_name])
        technique = scan_info.setdefault("technique", {})
        technique["image_key"] = NxTomoImageKey.PROJECTION.value
        technique["proj"] = {
            "motor": motor,
            "scan_type": "CONTINUOUS",
            "scan_range": fast_end_pos - fast_start_pos,
            "proj_n": fast_npoints,
            "nb_scans": slow_npoints,
            "latency_time": latency_time,
            "latency_time@units": "s",
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        return scan_info

    def get_scan_preset_list(self):
        """
        Return list of configured scan presets
        """
        # get the scan presets on the tomo runner
        preset_list = super().get_scan_preset_list()
        # get the scan presets on the fscan
        for i in self._fscan2d._scan_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list

    def get_chain_preset_list(self):
        """
        Return list of configured chain presets
        """
        # get the chain presets on the tomo runner
        preset_list = super().get_chain_preset_list()
        # get the chain presets on the fscan
        for i in self._fscan2d._chain_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list
