from bliss.common.logtools import log_info, log_error
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.scan_info import ScanInfo
from bliss.common.scans import pointscan
from bliss.common.cleanup import (
    error_cleanup,
    capture_exceptions,
    axis as cleanup_axis,
)
from tomo.scan.tomo_runner import TomoRunner
from tomo.scan.presets.common_header import CommonHeaderPreset
from tomo.constants import NxTomoImageKey


class ReturnRefRunner(TomoRunner):
    """
    Class used to build and run scan to acquire static images on return
    Built as a layer of pointscan
    """

    def __init__(self, motor, single_chain=None, acc_chain=None):
        super().__init__(single_chain=single_chain, acc_chain=acc_chain)
        self._motor = motor
        self._detectors = None

    def get_motor_positions(self, pars):
        """
        Return list of motor positions where to each image
        Deduce it according to tomo range and option return_images_aligned_to_flats
        """
        rot_direction = 1
        if pars.range < 0:
            rot_direction = -1

        end_pos = pars.start_pos + pars.range

        # when scan range > 360, only take return images for the last 360 deg.
        if abs(pars.range) > 360:
            limit_pos = end_pos - (360 * rot_direction)
        else:
            limit_pos = pars.start_pos

        i = len(pars.points_proj_groups) - 1

        rot_position = end_pos
        positions = list()
        while (rot_position - limit_pos) * rot_direction >= 0:
            positions.append(rot_position)

            if pars.return_images_aligned_to_flats and pars.projection_groups:
                step_size = abs(pars.range / pars.tomo_n)
                shift_angle = step_size * pars.points_proj_groups[i]
                rot_position = rot_position - (shift_angle * rot_direction)
                i -= 1
            else:
                rot_position = rot_position - (90.0 * rot_direction)

        return positions

    def __call__(
        self,
        positions,
        expo_time,
        *counters,
        scan_info=None,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like
        pointscan
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        if header is None:
            header = {}

        # Add the flat scan meta data
        scan_info = self._add_metadata(scan_info)

        # add image identification to the common image header
        header["image_key"] = str(NxTomoImageKey.ESRF_RETURN.value)

        self._scan = self._setup_scan(
            self._motor,
            positions,
            expo_time,
            *counters,
            header=header,
            save=save,
            scan_info=scan_info,
        )

        self._setup_presets()
        if run:
            # on error go back to initial position
            restore_list = (cleanup_axis.POS,)
            with error_cleanup(self._motor, restore_list=restore_list):
                with capture_exceptions(raise_index=0) as capture:
                    with capture():
                        self._scan.run()
                    # test if an error has occured
                    if len(capture.failed) > 0:
                        log_error(
                            "tomo",
                            "A problem occured during the return_ref scan, scan aborted",
                        )
                        log_error("tomo", capture.exception_infos)
                        log_error("tomo", "\n")
        return self._scan

    def _setup_scan(
        self,
        motor,
        rot_pos_list,
        expo_time,
        *counters,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build and return scan
        Take a list of given counters by user or use those which are
        enabled in ACTIVE_MG
        Handle accumulation mode on detectors
        By default, images are taken every 90 degrees and aligned to
        reference groups if the option 'return_images_aligned_to_flats'
        is checked.
        """
        log_info("tomo", "return_ref_scan() entering")

        # scan title
        title = "static images"

        # add image identification to the common image header
        header["image_key"] = "0"  # projection NXtomo definition

        builder = ChainBuilder(list(counters))

        detectors = list()
        for node in builder.get_nodes_by_controller_type(Lima):
            detectors.append(node.controller)
        self._detectors = detectors

        with self._setup_detectors_in_default_chain(detectors):
            # scan for static return_ref images
            ret_scan = pointscan(
                motor,
                rot_pos_list,
                expo_time,
                *counters,
                name=title,
                title=title,
                scan_info=scan_info,
                save=save,
                run=False,
            )

        # add common header preset
        header_preset = CommonHeaderPreset(*detectors, header=header)
        ret_scan.add_preset(header_preset)

        log_info("tomo", "ret_scan() leaving")

        return ret_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Estimate scan time by taking into account image acquisition time
        for each image and motor displacement between each image
        """
        if tomo_pars.return_images_aligned_to_flats:
            disp = tomo_pars.flat_on * tomo_pars.range / tomo_pars.tomo_n
        else:
            disp = 90
        nmoves = int(tomo_pars.range / disp) + 1
        return_time = (
            self.mot_disp_time(self._motor, disp, self._motor.velocity) * nmoves
            + nmoves * tomo_pars.exposure_time
        )

        return return_time

    def _add_metadata(self, scan_info=None):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        technique = scan_info.setdefault("technique", {})
        technique["image_key"] = NxTomoImageKey.ESRF_RETURN.value
        return scan_info
