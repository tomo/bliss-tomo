from __future__ import annotations
import contextlib
import tabulate
from typing import Optional, List
from bliss.scanning.chain import AcquisitionChain, ChainPreset
from bliss.common.scans import DEFAULT_CHAIN
from bliss.scanning.scan import ScanPreset
from bliss.common.utils import autocomplete_property
from bliss.scanning.scan_info import ScanInfo
from bliss.physics.trajectory import LinearTrajectory
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from tomo.helpers.locking_helper import lock_context
from tomo.scan.presets.locked_lima_detectors import LockedLimaDetectorsPreset


class TomoRunner:
    """
    Base class for tomo runners.

    Arguments:
        single_chain: If define, this chain will be used for detectors not setup
                      with accumulation mode.
        acc_chain: This chain will be used for detectors setup with accumulation
                   mode. If a detector is in accumulation and this chain is not
                   set, the acquisition will fail.
    """

    def __init__(
        self, single_chain: AcquisitionChain = None, acc_chain: AcquisitionChain = None
    ):
        self._chain = None
        self._scan = None
        self._scan_presets: dict[str, ScanPreset] = dict()
        self._chain_presets: dict[str, ChainPreset] = dict()
        self._use_presets = True
        self._lock_detector_enabled = True
        self._single_chain: AcquisitionChain = single_chain
        self._acc_chain: AcquisitionChain = acc_chain
        self._fallback_single_chain: AcquisitionChain = None
        self._fallback_acc_chain: AcquisitionChain = None
        self._lima_locking = LockedLimaDetectorsPreset(type(self).__name__)

    def set_default_chains(
        self,
        single_chain: AcquisitionChain = None,
        acc_chain: AcquisitionChain = None,
        fallback_single_chain: AcquisitionChain = None,
        fallback_acc_chain: AcquisitionChain = None,
    ):
        """Define common default chains to use in specific context.

        It only makes sense if the runner use a step scan.

        Arguments:
            single_chain: Chain which will be used to patch the `DEFAULT_CHAIN` if
                          the Lima detector is setup in `SINGLE` mode
            acc_chain: Chain which will be used to patch the `DEFAULT_CHAIN` if
                       the Lima detector is setup in `ACCUMULATION` mode
            fallback_single_chain: Chain which will be used to patch the `DEFAULT_CHAIN` if
                                   the Lima detector is setup in `SINGLE` mode
                                   and not already part of the `single_chain`
            fallback_acc_chain: Chain which will be used to patch the `DEFAULT_CHAIN` if
                                the Lima detector is setup in `ACCUMULATION` mode
                                and not already part of the `acc_chain`
        """
        self._single_chain = single_chain
        self._acc_chain = acc_chain
        self._fallback_single_chain = fallback_single_chain
        self._fallback_acc_chain = fallback_acc_chain

    @contextlib.contextmanager
    def disabled_locking_detector_context(self):
        """This context disable the detector locking during the runner.

        The actual implementation skip adding a locking preset during the
        building phase of the scan.
        """
        old = self._lock_detector_enabled
        self._lock_detector_enabled = False
        try:
            yield
        finally:
            self._lock_detector_enabled = old

    def _setup_presets(self):
        if self._lock_detector_enabled:
            self._scan.add_preset(self._lima_locking)
        if self._use_presets:
            # --- chain presets
            for preset in self._chain_presets.values():
                self._chain.add_preset(preset)
            # --- scan presets
            for preset in self._scan_presets.values():
                self._scan.add_preset(preset)

    def add_scan_preset(self, preset, name=None):
        if not isinstance(preset, ScanPreset):
            raise ValueError("preset should be an instance of ScanPreset !!")
        preset_name = preset.__class__.__name__ if name is None else name
        self._scan_presets[preset_name] = preset

    def _apply_acq_mode(self, detector: Lima):
        from ..globals import ACTIVE_TOMOCONFIG

        tomoconfig = ACTIVE_TOMOCONFIG.deref_active_object()
        if tomoconfig is None:
            return None
        tomo_detector = tomoconfig.detectors.get_tomo_detector(detector)
        tomo_detector.apply_acq_mode()

    def _setup_detectors_from_active_mg(self):
        """Apply acq_mode to the Lima detectors part of the ACTIVE_MG"""
        builder = ChainBuilder([])
        detectors = []
        for node in builder.get_nodes_by_controller_type(Lima):
            detectors.append(node.controller)
        for d in detectors:
            self._apply_acq_mode(d)

    @contextlib.contextmanager
    def _setup_detectors_in_default_chain(self, detectors: List[Lima]):
        """
        Context to setup the default chain based on default settings of this runner.

        The default chain is restored at the termination.
        """
        previous_settings = DEFAULT_CHAIN._settings

        def get_patch_setting(device, chains) -> Optional[dict]:
            for chain in chains:
                if chain is None:
                    continue
                for config in chain["chain_config"]:
                    if config["device"] is device:
                        return config
            return None

        patches = []
        for detector in detectors:
            self._apply_acq_mode(detector)
            if detector.acquisition.mode == "ACCUMULATION":
                patch = get_patch_setting(
                    detector, [self._acc_chain, self._fallback_acc_chain]
                )
                if patch is None:
                    raise ValueError(
                        "Detector '%s' is in accumulation mode, but no accumulation settings was found in any chain settings",
                        detector.name,
                    )
            else:
                patch = get_patch_setting(
                    detector, [self._single_chain, self._fallback_single_chain]
                )
            if patch is not None:
                patches.append(patch)
        if patches != []:
            DEFAULT_CHAIN.set_settings(patches)

        try:
            yield
        finally:
            DEFAULT_CHAIN._settings = previous_settings

    def remove_scan_preset(self, name):
        if name in self._scan_presets:
            self._scan_presets.pop(name)

    def add_chain_preset(self, preset, name=None):
        if not isinstance(preset, ChainPreset):
            raise ValueError("preset should be an instance of ChainPreset !!")
        preset_name = preset.__class__.__name__ if name is None else name
        self._chain_presets[preset_name] = preset

    def remove_chain_preset(self, name):
        if name in self._chain_presets:
            self._chain_presets.pop(name)

    def disable_presets(self):
        self._use_presets = False

    def enable_presets(self):
        self._use_presets = True

    def get_scan_preset_list(self):
        preset_list = [*self._scan_presets.keys()]
        return preset_list

    def get_chain_preset_list(self):
        preset_list = [*self._chain_presets.keys()]
        return preset_list

    def _estimate_scan_duration(self, tomo_pars):
        raise NotImplementedError

    def _add_metadata(self, scan_info=None):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        return scan_info

    @autocomplete_property
    def scan(self):
        if self._scan is None:
            raise RuntimeError("No scan performed yet !!")
        return self._scan

    @autocomplete_property
    def chain(self):
        if self._chain is None:
            raise RuntimeError("No chain setup yet !!")
        return self._chain

    def chain_show(self):
        self.chain._tree.show()

    def get_data(self, dataname=None):
        if dataname is None:
            return self.scan.get_data()
        if dataname in self.get_data().keys():
            return self.scan.get_data()[dataname]
        for name in self.get_data().keys():
            if name.endswith(f":{dataname}"):
                return self.scan.get_data()[name]
        raise KeyError(
            f"Invalid dataname !! use {self._scan_name}.list_data() to get a list of valid data."
        )

    def list_data(self):
        heads = ["Data names", "Sizes"]
        infos = list()
        for name, val in self.scan.get_data().items():
            if not name.endswith("image"):
                infos.append([name, val.shape])
        print("\n" + tabulate.tabulate(infos, headers=heads) + "\n")

    @property
    def data_names(self):
        return list(self.scan.get_data().keys())

    def mot_disp_time(self, mot, disp, speed):
        """
        Estimates motor displacement time.
        """
        lt = LinearTrajectory(
            pi=0,
            pf=disp,
            velocity=speed,
            acceleration=mot.acceleration,
            ti=0,
        )
        return lt.instant(disp)
