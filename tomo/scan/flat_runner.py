from __future__ import annotations
import weakref
import contextlib
from bliss.common.logtools import log_info, log_error
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.scan_info import ScanInfo
from bliss.common.scans import loopscan
from bliss.common.cleanup import (
    error_cleanup,
    capture_exceptions,
    axis as cleanup_axis,
)
from tomo.scan.tomo_runner import TomoRunner
from tomo.scan.presets.common_header import CommonHeaderPreset
from tomo.constants import NxTomoImageKey
from tomo.controllers.flat_motion import FlatMotion


class FlatRunner(TomoRunner):
    """
    Class used to build and run scan to acquire images with the sample
    out of the beam (flat images)
    Built as a layer of loopscan
    """

    def __init__(
        self, reference: FlatMotion, single_chain=None, acc_chain=None, tomoconfig=None
    ):
        super().__init__(single_chain=single_chain, acc_chain=acc_chain)
        self._flat_motion: FlatMotion = reference
        self.__tomoconfig = None
        if tomoconfig:
            self.__tomoconfig = weakref.ref(tomoconfig)
        self._detectors = None

    @property
    def _tomoconfig(self):
        # dereference the weakref
        return self.__tomoconfig()

    def __call__(
        self,
        expo_time,
        flat_n,
        *counters,
        scan_info=None,
        projection=1,
        turn=0,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like
        loopscan
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        if header is None:
            header = {}

        # Add the flat scan meta data
        scan_info = self._add_metadata(expo_time, flat_n, scan_info)

        self._scan = self._setup_scan(
            expo_time,
            flat_n,
            *counters,
            flat_motion=self._flat_motion,
            projection=projection,
            turn=turn,
            header=header,
            save=save,
            scan_info=scan_info,
        )

        self._setup_presets()
        if run:
            # on error go back to initial position
            restore_list = (cleanup_axis.POS,)
            motors = self._flat_motion.involved_axes
            with error_cleanup(*motors, restore_list=restore_list):
                with capture_exceptions(raise_index=0) as capture:
                    with capture():
                        with contextlib.ExitStack() as stack:
                            if self._tomoconfig is not None:
                                stack.enter_context(
                                    self._tomoconfig.auto_projection.inhibit()
                                )
                            self._scan.run()
                    # test if an error has occured
                    if len(capture.failed) > 0:
                        log_error(
                            "tomo",
                            "A problem occured during the flat scan, scan aborted",
                        )
                        log_error("tomo", capture.exception_infos)
                        log_error("tomo", "\n")
        return self._scan

    def _setup_scan(
        self,
        expo_time,
        flat_n,
        *counters,
        flat_motion,
        projection=1,
        turn=0,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build and return scan
        Take a list of given counters by user or use those which are
        enabled in ACTIVE_MG
        Handle accumulation mode on detectors
        """
        log_info("tomo", "flat_scan() entering")

        # scan title
        if turn > 0:
            title = "flat images " + str(projection) + "_" + str(turn)
        else:
            title = "flat images " + str(projection)

        # add image identification to the common image header
        header["image_key"] = str(NxTomoImageKey.FLAT_FIELD.value)

        builder = ChainBuilder(list(counters))

        detectors = list()
        for node in builder.get_nodes_by_controller_type(Lima):
            detectors.append(node.controller)
        self._detectors = detectors

        with self._setup_detectors_in_default_chain(detectors):
            # scan for flat images
            flat_scan = loopscan(
                flat_n,
                expo_time,
                *counters,
                name=title,
                title=title,
                save=save,
                run=False,
                scan_info=scan_info,
            )

        # add common header preset
        header_preset = CommonHeaderPreset(*detectors, header=header)
        flat_scan.add_preset(header_preset)

        log_info("tomo", "flat_scan() leaving")

        return flat_scan

    def _estimate_scan_duration(self, tomo_pars):
        return self.estimation_time(
            expo_time=tomo_pars.exposure_time, flat_n=tomo_pars.flat_n
        )

    def estimation_time(self, expo_time: float, flat_n: int):
        """
        Estimate scan time by taking into account image acquisition time
        for each image and two times slowest reference motor displacement
        """
        flat_motion = self._flat_motion
        scan_time = flat_motion.estimation_time() + flat_n * expo_time
        return scan_time

    def _add_metadata(self, expo_time, flat_n, scan_info=None):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        technique = scan_info.setdefault("technique", {})
        flat_motion = self._flat_motion
        technique["flat"] = {
            "flat_n": flat_n,
            "displacement": flat_motion.get_nx_motion(),
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        technique["image_key"] = NxTomoImageKey.FLAT_FIELD.value
        return scan_info
