import logging

from bliss.common.logtools import log_info
from bliss.scanning.scan_info import ScanInfo
from tomo.scan.tomo_runner import TomoRunner

_logger = logging.getLogger(__name__)


class HelicalRunner(TomoRunner):
    def __init__(self, config):
        super().__init__()
        self._projection_scan_name = "f2scan"
        self._f2scan_config = config
        self._f2scan = config.get_runner(self._projection_scan_name)
        self.pars = self._f2scan.pars
        self._scan = None

    def __call__(
        self,
        motor1,
        start_pos1,
        end_pos1,
        motor2,
        start_pos2,
        end_pos2,
        tomo_n,
        expo_time,
        scan_info=None,
        header=None,
        latency_time=None,
        run=True,
        save=True,
    ):
        self.scan_info = ScanInfo.normalize(scan_info)
        if header is None:
            header = {}
        if latency_time is None:
            default_latency = self._f2scan.pars.latency_time
            latency_time = default_latency

        # Add the user and scan meta data
        scan_info = self._add_metadata(scan_info)

        self._setup_detectors_from_active_mg()

        self._scan = self._setup_scan(
            motor1,
            start_pos1,
            end_pos1,
            motor2,
            start_pos2,
            end_pos2,
            tomo_n,
            expo_time,
            latency_time,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._setup_presets()

        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        motor1,
        start_pos1,
        end_pos1,
        motor2,
        start_pos2,
        end_pos2,
        tomo_n,
        expo_time,
        latency_time,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Acquires images with the sample out of the beam.
        """
        log_info("tomo", "projection_scan() entering")

        f2scan = self._f2scan
        f2scan.pars.motor = motor1
        f2scan.pars.start_pos = start_pos1
        f2scan.pars.start_pos = start_pos1
        f2scan.pars.step_size = (end_pos1 - start_pos1) / tomo_n

        f2scan.pars.slave_motor = motor2
        f2scan.pars.slave_start_pos = start_pos2
        f2scan.pars.slave_step_size = (end_pos2 - start_pos2) / tomo_n

        f2scan.pars.acq_time = expo_time
        f2scan.pars.latency_time = latency_time
        f2scan.pars.npoints = tomo_n
        f2scan.prepare(scan_info)
        proj_scan = f2scan.scan

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        f2scan = self._f2scan
        f2scan.validate()
        return f2scan.inpars.scan_time

    def get_scan_preset_list(self):
        # get the scan presets on the tomo runner
        preset_list = super().get_scan_preset_list()
        # get the scan presets on the f2scan
        for i in self._f2scan._scan_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list

    def get_chain_preset_list(self):
        # get the chain presets on the tomo runner
        preset_list = super().get_chain_preset_list()
        # get the chain presets on the f2scan
        for i in self._f2scan._chain_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list
