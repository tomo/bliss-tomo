import logging

from bliss.common.logtools import log_info
from bliss.scanning.scan_info import ScanInfo
from bliss import current_session
from tomo.scan.tomo_runner import TomoRunner
from tomo.constants import NxTomoImageKey
from fscan.fscantools import FScanParamBase

_logger = logging.getLogger(__name__)


class ContinuousRunner(TomoRunner):
    """
    Class used to build and run continuous scan with one motor
    Built as a layer of fscan
    """

    def __init__(self, config):
        super().__init__()
        self._projection_scan_name = "fscan"
        self._fscan_config = config
        self._fscan = config.get_runner(self._projection_scan_name)
        self.pars: FScanParamBase = self._fscan.pars
        """fscan parameters"""
        self._scan = None

    def __call__(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        scan_info=None,
        header=None,
        latency_time=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like fscan or
        bliss common scans
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        self.scan_info = ScanInfo.normalize(scan_info)
        if header is None:
            header = {}
        if latency_time is None:
            default_latency = self._fscan.pars.latency_time
            latency_time = default_latency

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            motor, start_pos, end_pos, expo_time, tomo_n, latency_time, scan_info
        )

        self._setup_detectors_from_active_mg()

        self._scan = self._setup_scan(
            motor,
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            latency_time,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        latency_time,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build and return fscan
        """
        log_info("tomo", "projection_scan() entering")

        fscan = self._fscan
        fscan.pars.motor = motor
        fscan.pars.start_pos = start_pos
        fscan.pars.step_size = (end_pos - start_pos) / tomo_n
        fscan.pars.acq_time = expo_time
        fscan.pars.step_time = fscan.pars.acq_time
        fscan.pars.latency_time = latency_time
        fscan.pars.npoints = tomo_n
        fscan.prepare(scan_info)
        proj_scan = fscan.scan

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Get scan time already estimated in fscan.validate()
        """
        fscan = self._fscan
        fscan.validate()
        return fscan.inpars.scan_time

    def _add_metadata(
        self, motor, start_pos, end_pos, expo_time, proj_n, latency_time, scan_info=None
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        ALIASES = current_session.env_dict["ALIASES"]
        # alias_name = str(setup_globals.ALIASES.get_alias(motor.name))
        alias_name = str(ALIASES.get_alias(motor.name))
        technique = scan_info.setdefault("technique", {})
        technique["image_key"] = NxTomoImageKey.PROJECTION.value
        technique["proj"] = {
            "motor": ["rotation", motor.name, alias_name],
            "scan_type": "CONTINUOUS",
            "scan_range": end_pos - start_pos,
            "proj_n": proj_n,
            "latency_time": latency_time,
            "latency_time@units": "s",
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        return scan_info

    def get_scan_preset_list(self):
        """
        Return list of configured scan presets
        """
        # get the scan presets on the tomo runner
        preset_list = super().get_scan_preset_list()
        # get the scan presets on the fscan
        for i in self._fscan._scan_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list

    def get_chain_preset_list(self):
        """
        Return list of configured chain presets
        """
        # get the chain presets on the tomo runner
        preset_list = super().get_chain_preset_list()
        # get the chain presets on the fscan
        for i in self._fscan._chain_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list
