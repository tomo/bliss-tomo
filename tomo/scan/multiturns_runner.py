import logging

from bliss.common.logtools import log_info
from bliss.scanning.scan_info import ScanInfo
from bliss import current_session
from tomo.scan.tomo_runner import TomoRunner

_logger = logging.getLogger(__name__)


class MultiTurnsRunner(TomoRunner):
    """Multiple turns of continuous scan with one motor"""

    def __init__(self, config):
        super().__init__()
        self._projection_scan_name = "fscanloop"
        self._fscan_config = config
        self._fscanloop = config.get_runner(self._projection_scan_name)
        self.pars = self._fscanloop.pars
        self._scan = None

    def __call__(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        nloop,
        trange,
        scan_info=None,
        header=None,
        waiting_angle=0,
        loop_mode="INTERNAL",
        latency_time=None,
        lima_read_mode="PER_SCAN",
        images_per_loop=None,
        run=True,
        save=True,
    ):
        self.scan_info = ScanInfo.normalize(scan_info)

        if header is None:
            header = {}
        if latency_time is None:
            default_latency = self._fscanloop.pars.latency_time
            latency_time = default_latency
        if images_per_loop is None:
            images_per_loop = tomo_n
        loop_trig_mode = loop_mode
        nbscans = nloop
        ntomo = int((end_pos - start_pos) / trange)
        if ntomo > 1 and nloop == 1:
            nbscans = ntomo

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            motor,
            start_pos,
            end_pos,
            expo_time,
            tomo_n,
            latency_time,
            nbscans,
            waiting_angle,
            loop_trig_mode,
            lima_read_mode,
            scan_info,
        )

        self._setup_detectors_from_active_mg()

        self._scan = self._setup_scan(
            motor,
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            latency_time,
            nloop,
            waiting_angle,
            loop_trig_mode,
            lima_read_mode,
            images_per_loop,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        latency_time,
        nloop,
        waiting_angle,
        loop_trig_mode,
        lima_read_mode,
        images_per_loop,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Acquires images with the sample out of the beam.
        """
        log_info("tomo", "projection_scan() entering")

        fscanloop = self._fscanloop
        fscanloop.pars.motor = motor
        fscanloop.pars.start_pos = start_pos
        fscanloop.pars.step_size = (end_pos - start_pos) / tomo_n
        fscanloop.pars.acq_time = expo_time
        fscanloop.pars.step_time = fscanloop.pars.acq_time
        fscanloop.pars.latency_time = latency_time
        fscanloop.pars.nloop = nloop
        fscanloop.pars.loop_trig_mode = loop_trig_mode
        fscanloop.pars.loop_delta = waiting_angle
        fscanloop.pars.loop_range = end_pos - start_pos
        fscanloop.pars.lima_read_mode = lima_read_mode
        fscanloop.pars.images_per_loop = images_per_loop
        fscanloop.prepare(scan_info)
        proj_scan = fscanloop.scan

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        fscanloop = self._fscanloop
        fscanloop.validate()
        return fscanloop.inpars.scan_time

    def _add_metadata(
        self,
        motor,
        start_pos,
        end_pos,
        expo_time,
        proj_n,
        latency_time,
        nbscans,
        waiting_angle,
        loop_trig_mode,
        lima_read_mode,
        scan_info=None,
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        ALIASES = current_session.env_dict["ALIASES"]
        alias_name = str(ALIASES.get_alias(motor.name))
        technique = scan_info.setdefault("technique", {})
        technique["proj"] = {
            "motor": ["rotation", motor.name, alias_name],
            "scan_type": "CONTINUOUS",
            "scan_range": end_pos - start_pos,
            "proj_n": proj_n,
            "nb_turns": nbscans,
            "waiting_angle": waiting_angle,
            "loop_trig_mode": loop_trig_mode,
            "lima_read_mode": lima_read_mode,
            "latency_time": latency_time,
            "latency_time@units": "s",
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        return scan_info

    def get_scan_preset_list(self):
        # get the scan presets on the tomo runner
        preset_list = super().get_scan_preset_list()
        # get the scan presets on the fscan
        for i in self._fscanloop._scan_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list

    def get_chain_preset_list(self):
        # get the chain presets on the tomo runner
        preset_list = super().get_chain_preset_list()
        # get the chain presets on the fscan
        for i in self._fscanloop._chain_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list
