import logging
from bliss.scanning.scan import ScanPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster

_logger = logging.getLogger(__name__)


class FastShutterPreset(ScanPreset):
    """
    Class used to open fast shutter before taking reference, static or projection images
    and close it after acquisition.

    **Attributes**

    shutter : Tomo shutter object
        contains methods to control shutters
    close_shutter : flag
        Used to specify if shutter must be closed or not
        after image taking
    """

    def __init__(self, shutter=None, close_shutter=True):
        super().__init__()
        self.shutter = shutter
        self.close_shutter = close_shutter

    def prepare(self, scan):
        """
        Open shutter if shutter controlled by detector
        """
        self.soft_shutter = False
        self.restore_shutter_mode = None

        dets = [
            node.device
            for node in scan.acq_chain.nodes_list
            if isinstance(node, LimaAcquisitionMaster)
        ]
        det = dets[0] if len(dets) == 1 else None

        if det is not None and det.proxy.lima_type == "Frelon":
            if det.camera.image_mode == "FULL FRAME":
                det.shutter.mode = "AUTO_FRAME"
                det.shutter.close_time = self.shutter.closing_time
            else:
                self.soft_shutter = True
                det.shutter.mode = "MANUAL"
                det.shutter.close_time = 0.0

        elif det is not None and det.proxy.lima_type == "Ximea":
            proxy = det._get_proxy("Ximea")
            if proxy.gpo_mode == "OFF":
                self.soft_shutter = True
                self.restore_shutter_mode = self.shutter.mode
                self.shutter.mode = self.shutter.MANUAL

        elif self.shutter is not None:
            self.soft_shutter = True

    def start(self, scan):
        """
        Open shutter if shutter controlled by soft
        """
        if self.soft_shutter:
            self.shutter.open()

    def stop(self, scan):
        """
        Close shutter if close_shutter attribute set to True and if shutter
        controlled by soft
        """
        if self.soft_shutter and self.close_shutter:
            self.shutter.close()

        if self.restore_shutter_mode is not None:
            self.shutter.mode = self.restore_shutter_mode
