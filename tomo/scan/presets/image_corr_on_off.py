import logging
from bliss.scanning.scan import ScanPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster

_logger = logging.getLogger(__name__)


class ImageCorrOnOffPreset(ScanPreset):
    """
    Class used to deactivate image correction during tomo acquisition
    and reactivate it after

    **Attributes**

    detectors: list of detectors presents in active measurement group
    image_corr_was_on: dictionary containing flag set to True if image
    correction is activated
    """

    def prepare(self, scan):
        """
        Read detectors presents in active measurement group
        Check if flatfield or background correction is active on detector
        Deactivate flatfield and background correction
        Set image_corr_was_on flag accordingly
        """

        self.detectors = [
            acq_obj.device
            for acq_obj in scan.acq_chain.nodes_list
            if type(acq_obj) == LimaAcquisitionMaster
        ]

        self.image_corr_was_on = {}

        for detector in self.detectors:
            if detector.processing.use_background or detector.processing.use_flatfield:
                self.image_corr_was_on[detector] = True
                detector.processing.use_background = False
                detector.processing.use_flatfield = False
                ctrl_params = detector.get_current_parameters()
                ctrl_params["saving_frame_per_file"] = (
                    1
                    if ctrl_params["saving_frame_per_file"] == -1
                    else ctrl_params["saving_frame_per_file"]
                )
                detector.apply_parameters(ctrl_params)
            else:
                self.image_corr_was_on[detector] = False

    def stop(self, scan):
        """
        Reactivate flatfield and background correction on detectors where flag
        image_corr_was_on is set to True
        """
        for detector in self.detectors:
            if self.image_corr_was_on[detector]:
                detector.processing.use_background = True
                detector.processing.use_flatfield = True
                ctrl_params = detector.get_current_parameters()
                ctrl_params["saving_frame_per_file"] = (
                    1
                    if ctrl_params["saving_frame_per_file"] == -1
                    else ctrl_params["saving_frame_per_file"]
                )
                detector.apply_parameters(ctrl_params)
