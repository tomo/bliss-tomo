import numpy as np
import logging
from bliss.scanning.scan import ScanPreset

_logger = logging.getLogger(__name__)


class ProjectionSpectrumPreset(ScanPreset):
    """
    Class used in case of projection images to:

    * reemit roi profile data towards tomo sequence
    * reformat and emit rotation position data corresponding to x axis
      for sinogram plot
    * reformat and emit roi pixel coordinates data corresponding to y axis
      for sinogram plot

    **Attributes**

    detector : Bliss controller object
        detector on which roi profile is defined
    sequence : Bliss sequence object
        sequence containing the channel where data must be reemitted
    spectrum_channel_name : string
        sequence channel name where roi profile data must be reemitted
    translation_channel_name : string
        sequence channel name where roi pixel coordinates data must be emitted
    rotation_channel_name : string
        sequence channel name where rotation position data must be emitted
    tomo_range : float
        angular range of tomo scan
    """

    def __init__(
        self,
        detector,
        motor,
        sequence,
        spectrum_channel_name,
        translation_channel_name,
        rotation_channel_name,
    ):
        super().__init__()

        self.detector = detector
        self.motor = motor
        self.sequence = sequence
        self.spectrum_channel_name = spectrum_channel_name
        self.translation_channel_name = translation_channel_name
        self.rotation_channel_name = rotation_channel_name
        self.tomo_range = None

    def prepare(self, scan):
        """
        Link roi profile data channel from projection scan to
        reemit_spectrum function
        """
        self.connect_data_channels(
            [self.detector.roi_profiles, self.motor], self.reemit_spectrum
        )
        self.tomo_range = scan.scan_info.get("technique").get("proj").get("scan_range")

    def reemit_spectrum(self, counter, channel_name, data):
        """
        Reemit roi profile data towards sequence
        Reformat and emit rotation position data corresponding to x axis
        for sinogram plot
        Reformat and emit roi pixel coordinates data corresponding to y axis
        for sinogram plot
        """
        npixels = self.detector.roi_profiles.get_rois()[0].width
        if counter == self.detector.roi_profiles:
            if data is None:
                return
            data = data.astype(np.float32)
            self.sequence.custom_channels[self.spectrum_channel_name].emit(data)
            data = np.array([np.arange(npixels)] * len(data)).flatten()
            data = data.astype(np.float32)
            self.sequence.custom_channels[self.translation_channel_name].emit(data)
        if counter == self.motor:
            if data is None:
                return
            if (data > self.tomo_range).any():
                data = np.where(data > self.tomo_range, data % self.tomo_range, data)
            data = np.repeat(data, npixels)
            data = data.astype(np.float32)
            self.sequence.custom_channels[self.rotation_channel_name].emit(data)
