from bliss.scanning.scan import WatchdogCallback
import time


class BeamLost(BaseException):
    pass


class BeamCheckWatchdog(WatchdogCallback):
    """
    Class used to check if there is beam and stop scan in case of beam lost

    **Attributes**

    machinfo: Bliss object used to read info from machine (current, etc...)
    """

    def __init__(self, machinfo, frontend):
        super().__init__()
        self.machinfo = machinfo
        self.frontend = frontend
        self.i = 0

    def on_scan_data(self, data_events, scan_info):
        if time.time() - self.i > 1:
            if not self.frontend.is_open or self.machinfo.counters.current.value < 5:
                raise BeamLost
            self.i = time.time()
