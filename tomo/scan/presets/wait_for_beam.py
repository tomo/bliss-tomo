import logging
import time
from bliss.scanning.scan import ScanPreset

_logger = logging.getLogger(__name__)


class WaitForBeamPreset(ScanPreset):
    """
    Class used to check if beam can be taken and open the frontend if it
    is closed

    **Attributes**

    machinfo: Bliss object used to read info from machine (current, etc...)
    frontend: Bliss object used to control frontend
    """

    def __init__(self, machinfo, frontend):
        super().__init__()
        self.machinfo = machinfo
        self.frontend = frontend

    def prepare(self, scan):
        """
        Check if beam can be taken
        If injection mode, wait until beam is back
        Open the frontend is it is closed
        """
        print(f"the SR current is {self.machinfo.counters.current.value:.2f} mA")

        beam_status = self.machinfo.proxy.mode
        delivery_mode = (
            "delivery" in self.machinfo.all_information.get("SR_Operator_Mesg").lower()
        )

        while (
            not (beam_status == 2 and delivery_mode)
            or self.machinfo.counters.current.value < 5
        ):
            print("beam is off, checking every 30 seconds")
            time.sleep(30)
            beam_status = self.machinfo.proxy.mode
            delivery_mode = (
                "delivery"
                in self.machinfo.all_information.get("SR_Operator_Mesg").lower()
            )

        if beam_status == 2 and delivery_mode:
            print("beam is on, ckecking for front end status")

            while not self.frontend.is_open:
                print("front end closed, trying to open it every minute")
                self.frontend.open(timeout=0)
                time.sleep(60)

            print("front end is open, the experiment can continue")
