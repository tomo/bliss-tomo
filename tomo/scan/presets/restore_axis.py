import logging
import gevent
from bliss.scanning.scan import ScanPreset
from bliss.common.standard import move
from bliss.common.protocols import Scannable
from bliss import is_bliss_shell
from bliss.shell.standard import umv

_logger = logging.getLogger(__name__)


class RestoreAxisPreset(ScanPreset):
    """
    Preset to move back axis at there original position after the scan.

    Arguments:
        sleep_time: Time to wait after the move, for hardware stabilization
    """

    def __init__(self, axis1=None, axis2=None, axis3=None, sleep_time=None):
        self._restore_args = []

        def _define_axis(axis):
            if axis is None:
                return
            if not isinstance(axis, Scannable):
                raise TypeError("A scannable is expected")
            self._restore_args.extend([axis, axis.position])

        _define_axis(axis1)
        _define_axis(axis2)
        _define_axis(axis3)
        self._sleep_time = sleep_time

    def prepare(self, scan):
        pass

    def start(self, scan):
        pass

    def stop(self, scan):
        if is_bliss_shell():
            umv(*self._restore_args)
        else:
            move(*self._restore_args)
        if self._sleep_time is not None:
            gevent.sleep(self._sleep_time)
