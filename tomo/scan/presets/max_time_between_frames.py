from bliss.scanning.scan import Scan, ScanPreset
from bliss.controllers.lima.lima_base import Lima


class MaxTimeBetweenFramesPreset(ScanPreset):
    """
    Setup lima to avoid timeout.

    Right now some Lima detectors have a timeout between frames,
    which stops the acquisition.

    - Some are hardcoded (pco)
    - Some can be setup (iris)

    This context setup the lima detector if the feature if available.

    If the future it would be good to setup such information directly
    as scan params or in fscan param.

    Arguments:
        detector: A lima detector
        max_time: The max expected time between 2 consecutive frames
    """

    def __init__(self, detector: Lima, max_time: float):
        self._camera = detector.camera
        self._has_configurable_timeout = hasattr(self._camera, "acq_timeout")
        # Assume `acq_timeout` is writtable, we could add another check if it's not always the case
        self._max_time: float = max_time

    def prepare(self, scan: Scan):
        if self._has_configurable_timeout:
            self._prev = self._camera.acq_timeout
            self._camera.acq_timeout = self._max_time

    def start(self, scan: Scan):
        pass

    def stop(self, scan: Scan):
        if self._has_configurable_timeout:
            self._camera.acq_timeout = self._prev
