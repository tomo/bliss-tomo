import logging
from bliss.scanning.scan import ScanPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster

_logger = logging.getLogger(__name__)


class DetectorSafetyPreset(ScanPreset):
    """
    Class used to close shutter before save operation is complete in case
    save operation takes some time (windows detector)

    **Attributes**

    shutter: Bliss object used to control shutter
    """

    def __init__(self, shutter):
        super().__init__()
        self.shutter = shutter

    def prepare(self, scan):
        """
        Get detector device from tomoconfig
        Connect detector acquisition data with protect_my_detector() function
        """
        self.detector = [
            node.device
            for node in scan.acq_chain.nodes_list
            if isinstance(node, LimaAcquisitionMaster)
        ][0]
        self.connect_data_channels([self.detector, ...], self.protect_my_detector)

    def start(self, scan):
        """
        Open shutter
        """
        self.shutter.open()

    def protect_my_detector(self, counter, channel_name, data):
        """
        Close shutter when detector acquired last projection
        """
        if counter == self.detector and channel_name == f"{self.detector.name}:image":
            if (
                data.get("last_image_acquired")
                == self.detector.acquisition.nb_frames - 1
            ):
                self.shutter.close()
