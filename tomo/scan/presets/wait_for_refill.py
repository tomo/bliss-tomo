import logging
import gevent
from bliss.scanning.scan import ScanPreset
from bliss.common.user_status_info import status_message

_logger = logging.getLogger(__name__)


class WaitForRefillPreset(ScanPreset):
    """
    This preset will pause a scan during the refill
    and if the **checktime** is greater than the time to refill.
    If **checktime** is set to None then we try to find **count_time**
    on the top master of the chain.

    Do not forget to intialize MachInfo object in session's setup.

    **Attributes**

    machinfo: Bliss object used to read info from machine (current, etc...)
    checktime: time in seconds to be compared with remaining time before next refill
    waittime: time in seconds to wait after beam is back
    polling_time: time in seconds to wait before checking refill state again
    shutter: Bliss object used to close shutter during refill and then
    protect sample
    """

    def __init__(
        self, machinfo, shutter=None, checktime=None, waittime=10.0, polling_time=1.0
    ):
        super().__init__()
        self.machinfo = machinfo
        self.checktime = checktime
        self.waittime = waittime
        self.polling_time = polling_time
        self.shutter = shutter

    def prepare(self, scan):
        """
        Check if checktime is greater than the time to refill and if not,
        close the shutter and pause the scan during refill
        If waittime is not None, wait for this given time after beam is
        back and open the shutter
        """
        ok = self.machinfo.check_for_refill(self.checktime)
        with status_message() as p:
            while not ok:
                if self.shutter is not None and self.shutter.is_open:
                    self.shutter.close()
                p("Waiting for refill...")
                gevent.sleep(self.polling_time)
                ok = self.machinfo.check_for_refill(self.checktime)
                if ok and self.waittime:
                    p(f"Waiting {self.waittime} after Beam is back")
                    gevent.sleep(self.waittime)
                    ok = self.machinfo.check_for_refill(self.checktime)
                    if self.shutter is not None:
                        self.shutter.open()
