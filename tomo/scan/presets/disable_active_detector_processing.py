import weakref
from bliss.scanning.scan import ScanPreset


class DisableActiveDetectorProcessing(ScanPreset):
    """Disable active detector processing

    And restore it in the end.
    """

    def __init__(self, tomoconfig):
        from tomo.controllers.tomo_config import TomoConfig

        assert isinstance(tomoconfig, TomoConfig)
        super(DisableActiveDetectorProcessing, self).__init__()
        self.__tomoconfig = weakref.ref(tomoconfig)
        self.__was_using_background: bool = None
        self.__was_using_flatfield: bool = None

    @property
    def _tomoconfig(self):
        return self.__tomoconfig()

    @property
    def _active_detector(self):
        tomoconfig = self._tomoconfig
        detetors = tomoconfig.detectors
        active_detector = detetors.active_detector
        return active_detector

    def prepare(self, scan):
        active_detector = self._active_detector
        if active_detector is None:
            return
        assert self.__was_using_background is None
        assert self.__was_using_flatfield is None

        self.__was_using_background: bool = (
            active_detector.detector.processing.use_background
        )
        self.__was_using_flatfield: bool = (
            active_detector.detector.processing.use_flatfield
        )
        active_detector.detector.processing.use_background = False
        active_detector.detector.processing.use_flatfield = False

    def stop(self, scan):
        active_detector = self._active_detector
        if active_detector is None:
            return
        active_detector.detector.processing.use_background = self.__was_using_background
        active_detector.detector.processing.use_flatfield = self.__was_using_flatfield
        self.__was_using_background = None
        self.__was_using_flatfield = None
