import logging
from bliss.scanning.scan import ScanPreset

_logger = logging.getLogger(__name__)


class ReferenceMotorPreset(ScanPreset):
    """
    Class used to move sample out of the beam before taking reference images
    and move sample back in beam after acquisition.

    **Attributes**

    reference : reference object
        contains methods to control reference motors
    """

    def __init__(self, reference):
        super().__init__()
        self.reference = reference

    def prepare(self, scan):
        """
        Move sample out of the beam
        """
        print(f"\n\nMove sample out of beam for {scan.name}")
        self.reference.move_out()

    def start(self, scan):
        pass

    def stop(self, scan):
        """
        Move sample back in beam
        """
        print(f"\n\nMove sample back in beam")
        self.reference.move_in()
