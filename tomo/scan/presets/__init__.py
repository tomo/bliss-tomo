"""Scan presets

.. autosummary::
   :toctree:

   common_header
   dark_shutter
   demo_shutter
   detector_safety
   disable_active_detector_processing
   fast_shutter
   image_corr_on_off
   image_spectrum
   inhibit_auto_projection
   move_axis
   musst_timer_config
   ordered_presets
   projection_spectrum
   reference_motor
   restore_axis
   round_position_preset
   wait_for_beam
   wait_for_refill
"""
