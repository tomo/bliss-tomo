import logging
import numpy
from bliss.scanning.scan import ScanPreset

_logger = logging.getLogger(__name__)


class ImageSpectrumPreset(ScanPreset):
    """
    Class used in case of dark or flat images to reemit roi profile data
    towards tomo sequence

    **Attributes**

    detector : Bliss controller object
        detector on which roi profile is defined
    sequence : Bliss sequence object
        sequence containing the channel where data must be reemitted
    spectrum_channel_name : string
        sequence channel name where roi profile data must be reemitted
    """

    def __init__(self, detector, sequence, spectrum_channel_name):
        super().__init__()

        self.detector = detector
        self.sequence = sequence
        self.spectrum_channel_name = spectrum_channel_name

    def prepare(self, scan):
        """
        Link roi profile data channel from dark or flat scan to
        reemit_spectrum function
        """
        self.connect_data_channels(
            [self.detector.roi_profiles, ...], self.reemit_spectrum
        )

    def reemit_spectrum(self, counter, channel_name, data):
        """
        Reemit roi profile data towards sequence
        """
        if counter == self.detector.roi_profiles:
            if data is None:
                return
            data = data.astype(numpy.float32)
            self.sequence.custom_channels[self.spectrum_channel_name].emit(data)
