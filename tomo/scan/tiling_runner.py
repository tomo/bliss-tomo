import numpy
import pint
from typing import Optional, Tuple
import logging
import enum

from bliss.common.scans import ascan, sct
from bliss.common.scans.meshes import amesh
from bliss.scanning.scan_info import ScanInfo
from bliss.controllers.lima.lima_base import Lima
from bliss.common.axis import Axis
from bliss.physics.trajectory import LinearTrajectory

from tomo.scan.tomo_runner import TomoRunner
from tomo.controllers.tomo_detector import TomoDetector
from tomo.scan.presets.move_axis import MoveAxisPreset
from tomo.scan.presets.ordered_presets import OrderedPresetsPreset
from tomo.scan.presets.restore_axis import RestoreAxisPreset
from tomo.helpers.nexuswriter_utils import hdf5_axis_name
from tomo.helpers.fscan_helper import fscan_as_bliss_scan
from tomo.scan.presets.max_time_between_frames import MaxTimeBetweenFramesPreset


_logger = logging.getLogger(__name__)


class TilingLateralMotorMode(enum.Enum):
    UNDER_ROT = "UNDER_ROT"
    OVER_ROT = "OVER_ROT"


def _compute_time_between_slow_step(
    fast_motor: Axis,
    fast_velocity: float,
    slow_motor: Axis,
    slow_step_size: float,
) -> float:
    """
    Time taken to move between 2 frames when the slow step is involved.

    The procedure have to
    - Decelerate the `fast_motor` from `fast_velocity` to 0
    - Move `slow_motor` relative to `slow_step_size`
    - Accelerate the `fast_motor` from 0 to `fast_velocity`

    Here we neglecte the repositioning of the fast motor.
    """
    try:
        fast_accel_time = fast_velocity / fast_motor.acceleration
    except ZeroDivisionError:
        # piezo motors have 0 acceleration
        fast_accel_time = 0
    slow_t = LinearTrajectory(
        0, slow_step_size, slow_motor.velocity, slow_motor.acceleration
    )
    return slow_t.duration + fast_accel_time * 2


class TilingRunner(TomoRunner):
    """Tiling scan using a vertical and an horizontal scan based on the tomo
    config geometry.

    The scan steps are automatically computed based on the field of view of the
    detector.

    The fast axis can be either the z axis or the sample-y axis.

    The lateral motor can be set to use the motor over or under the rotation.

    When the motor under the rotation is used, the motors on top of the rotation
    are first centered (and will be restored. Finally the motor under the
    rotation is also restored at the termination of the scan.
    """

    def __init__(self, tomoconfig):
        TomoRunner.__init__(self)
        self._tomoconfig = tomoconfig
        self.y_axis = tomoconfig.y_axis
        self.z_axis = tomoconfig.z_axis
        self.sample_y_axis = tomoconfig.sample_y_axis
        self.sample_u_axis = tomoconfig.sample_u_axis
        self.rotation_axis = tomoconfig.rotation_axis

        # FIXME: Would be good to release this constraint
        assert self.y_axis.unit == self.z_axis.unit
        if self.sample_y_axis is not None:
            assert self.y_axis.unit == self.sample_y_axis.unit

    def __call__(
        self,
        expo_time: float,
        zstart: float,
        zstop: float,
        ystart: float,
        ystop: float,
        tomo_detector: TomoDetector,
        sleep_time: Optional[float] = None,
        scan_info: Optional[dict] = None,
        run: bool = True,
        vertical_first: bool = True,
        lateral_motor_mode: TilingLateralMotorMode = TilingLateralMotorMode.OVER_ROT,
        presets=None,
        continuous: bool = False,
        speed_ratio: float = 1.0,
    ):
        """
        Create the scan.

        Arguments:
            vertical_first: If true, the vertical axis is scan first (nested into the horizontal)
            lateral_motor_mode: One of 'under_rot' or 'over_rot', this define which motor will be
                                involved to scan laterally
        """
        if continuous and lateral_motor_mode != TilingLateralMotorMode.UNDER_ROT:
            # Theoretically we could move it. But the motor have to be part of the musst
            # cause of fscan, and it's a pseudo motor
            raise ValueError(
                "For continuous scan the translation have to be under the rotation"
            )
        if continuous and not vertical_first:
            raise ValueError(
                "For continuous scan the fast scan have to be the vertical one"
            )

        self._scan = self._setup_scan(
            expo_time=expo_time,
            zstart=zstart,
            zstop=zstop,
            ystart=ystart,
            ystop=ystop,
            tomo_detector=tomo_detector,
            sleep_time=sleep_time,
            scan_info=scan_info,
            vertical_first=vertical_first,
            lateral_motor_mode=lateral_motor_mode,
            presets=presets,
            continuous=continuous,
            speed_ratio=speed_ratio,
        )
        self._setup_presets()
        if run:
            self._scan.run()

        return self._scan

    @staticmethod
    def _motor_trajectory(axis, start, stop, fov):
        """
        Compute the expected trajectory for a given axis.

        Arguments:
            axis: Bliss axis
            start: Start of the region corner for the tiled image
            stop: Stop of the region corner for the tiled image
            fov: field of view of the camera in this axis

        Returns:
            The trajectory for the motor (start, stop, intervals)
        """
        sign = 1 if start < stop else -1
        vfrom = pint.Quantity(start, axis.unit) + 0.5 * fov * sign
        vto = pint.Quantity(stop, axis.unit) - 0.5 * fov * sign
        vrange = (vto - vfrom) * sign
        if vrange <= 0:
            # The camera is wider than the requested location
            vfrom = (vfrom + vto) * 0.5
            vto = vfrom
            interval = 0
        else:
            interval = (vrange / fov).to("dimensionless")
            interval = int(numpy.ceil(interval))
        return vfrom, vto, interval

    @staticmethod
    def _get_fov_shape(
        tomo_detector: TomoDetector,
    ) -> Tuple[pint.Quantity, pint.Quantity]:
        """Compute Field-of-View shape for given tomo detector"""
        lima: Lima = tomo_detector.detector
        pixel_size = tomo_detector.sample_pixel_size
        if pixel_size is None:
            raise ValueError("Detector sample pixel size is not defined")
        if pixel_size is None or pixel_size <= 0:
            raise ValueError("Detector sample pixel size is inconsistent")
        pixel_size = pint.Quantity(pixel_size, "um")
        width = lima.image.width
        height = lima.image.height
        if width <= 0 or height <= 0:
            raise ValueError("Detector size is inconsistent")

        return height * pixel_size, width * pixel_size

    def get_y_trajectory_info(
        self,
        tomo_detector: TomoDetector,
        start: float,
        stop: float,
        lateral_motor_mode: TilingLateralMotorMode,
        real_motor=False,
    ):
        """
        Arguments:
            real_motor: If true try not to use a pseudo motor
                        This only have meaning for estimation time
                        Else pseudo will not have velocity/acceleration
        """
        _, fov_width = self._get_fov_shape(tomo_detector)

        if lateral_motor_mode == TilingLateralMotorMode.OVER_ROT:
            ref = pint.Quantity(self.y_axis.position, self.y_axis.unit)
            if not real_motor:
                axis = self.sample_y_axis
            else:
                # We can conpute estimation time with that motor
                axis = self.sample_u_axis
        elif lateral_motor_mode == TilingLateralMotorMode.UNDER_ROT:
            # FIXME: maybe handle it in a different way
            # For now the motors on top of the rotation will be centered
            ref = pint.Quantity(0, "mm")
            axis = self.y_axis

        motor_start, motor_stop, interval = self._motor_trajectory(
            axis, start, stop, fov_width
        )
        return axis, motor_start - ref, motor_stop - ref, interval

    def get_z_trajectory_info(
        self, tomo_detector: TomoDetector, start: float, stop: float
    ):
        fov_height, _ = self._get_fov_shape(tomo_detector)
        motor_start, motor_stop, interval = self._motor_trajectory(
            self.z_axis, start, stop, fov_height
        )
        return self.z_axis, motor_start, motor_stop, interval

    def _setup_scan(
        self,
        expo_time: float,
        zstart: float,
        zstop: float,
        ystart: float,
        ystop: float,
        tomo_detector: TomoDetector,
        lateral_motor_mode,
        sleep_time: Optional[float] = None,
        scan_info: Optional[dict] = None,
        vertical_first: bool = True,
        presets=None,
        continuous: bool = False,
        speed_ratio: float = 1.0,
    ):
        _, start_z, stop_z, interval_z = self.get_z_trajectory_info(
            tomo_detector, zstart, zstop
        )

        y_axis, start_y, stop_y, interval_y = self.get_y_trajectory_info(
            tomo_detector, ystart, ystop, lateral_motor_mode
        )
        _logger.debug("Planed projections: %s", interval_y * interval_z)

        def to_motor_position(axis, quantity):
            return quantity.to(axis.unit).magnitude

        scan_info = self._add_metadata(
            expo_time,
            tomo_detector,
            scan_info,
            scan_sample_y_axis=(
                interval_y != 0
                and lateral_motor_mode == TilingLateralMotorMode.OVER_ROT
            ),
            scan_y_axis=(
                interval_y != 0
                and lateral_motor_mode == TilingLateralMotorMode.UNDER_ROT
            ),
            scan_z_axis=interval_z != 0,
        )

        def get_continuous_velocity(z_axis):
            px = tomo_detector.sample_pixel_size
            vmax = z_axis.velocity
            v1px = 1000 * px / expo_time
            if v1px < vmax:
                vmin = v1px
            else:
                vmin = vmax * 0.1
            return vmin + (vmax - vmin) * speed_ratio

        preset_list = OrderedPresetsPreset(presets)

        if interval_y == 0 and interval_z == 0:
            # Move at the right place and take a ct
            preset = MoveAxisPreset(
                y_axis, to_motor_position(y_axis, start_y), sleep_time=sleep_time
            )
            preset_list.append_preset(preset)
            with self._setup_detectors_in_default_chain([tomo_detector.detector]):
                scan = sct(
                    expo_time,
                    tomo_detector.detector.image,
                    run=False,
                    scan_info=scan_info,
                    title="tiling",
                )
        elif interval_y == 0:
            preset = MoveAxisPreset(
                y_axis, to_motor_position(y_axis, start_y), sleep_time=sleep_time
            )
            preset_list.append_preset(preset)
            with self._setup_detectors_in_default_chain([tomo_detector.detector]):
                mz = self.z_axis
                mzstart = to_motor_position(self.z_axis, start_z)
                mzstop = to_motor_position(self.z_axis, stop_z)
                mzinterval = interval_z
                if continuous:
                    fscan = self._tomoconfig.fscan_config.get_runner("fscan")
                    fscan.pars.motor = mz
                    fscan.pars.start_pos = mzstart
                    fscan.pars.npoints = mzinterval + 1
                    fscan.pars.step_size = (mzstop - mzstart) / mzinterval
                    fscan.pars.step_time = (
                        fscan.pars.step_size / get_continuous_velocity(mz)
                    )

                    fscan.pars.acq_time = expo_time
                    fscan.pars.scan_mode = "TIME"
                    try:
                        fscan.show()
                    except Exception:
                        fscan._master.show()
                        raise

                    scan = fscan_as_bliss_scan(
                        fscan,
                        tomo_detector.detector.image,
                        run=False,
                        scan_info=scan_info,
                        title="tiling",
                    )
                else:
                    scan = ascan(
                        mz,
                        mzstart,
                        mzstop,
                        mzinterval,
                        expo_time,
                        tomo_detector.detector.image,
                        run=False,
                        scan_info=scan_info,
                        title="tiling",
                    )
        elif interval_z == 0:
            preset = MoveAxisPreset(
                self.z_axis,
                to_motor_position(self.z_axis, start_z),
                sleep_time=sleep_time,
            )
            preset_list.append_preset(preset)
            with self._setup_detectors_in_default_chain([tomo_detector.detector]):
                scan = ascan(
                    y_axis,
                    to_motor_position(y_axis, start_y),
                    to_motor_position(y_axis, stop_y),
                    interval_y,
                    expo_time,
                    tomo_detector.detector.image,
                    run=False,
                    scan_info=scan_info,
                    title="tiling",
                )
        else:

            def get_motor(vertical: bool):
                if vertical:
                    return [
                        self.z_axis,
                        to_motor_position(self.z_axis, start_z),
                        to_motor_position(self.z_axis, stop_z),
                        interval_z,
                    ]
                else:
                    return [
                        y_axis,
                        to_motor_position(y_axis, start_y),
                        to_motor_position(y_axis, stop_y),
                        interval_y,
                    ]

            m1, m1start, m1stop, m1interval = get_motor(vertical=vertical_first)
            m2, m2start, m2stop, m2interval = get_motor(vertical=not vertical_first)

            with self._setup_detectors_in_default_chain([tomo_detector.detector]):
                if continuous:
                    m1_velocity = get_continuous_velocity(m1)
                    fscan = self._tomoconfig.fscan_config.get_runner("fscan2d")

                    fscan.pars.fast_motor = m1
                    fscan.pars.fast_start_pos = m1start
                    fscan.pars.fast_npoints = m1interval + 1
                    fscan.pars.fast_step_size = (m1stop - m1start) / m1interval
                    fscan.pars.fast_step_time = fscan.pars.fast_step_size / m1_velocity

                    fscan.pars.slow_motor = m2
                    fscan.pars.slow_start_pos = m2start
                    fscan.pars.slow_npoints = m2interval + 1
                    fscan.pars.slow_step_size = (m2stop - m2start) / m2interval

                    fscan.pars.acq_time = expo_time
                    fscan.pars.scan_mode = "TIME"
                    fscan.pars.fast_motor_mode = "ZIGZAG"
                    try:
                        fscan.show()
                    except Exception:
                        fscan._master.show()
                        raise

                    time_between_slow_step = _compute_time_between_slow_step(
                        fast_motor=m1,
                        fast_velocity=m1_velocity,
                        slow_motor=m2,
                        slow_step_size=(m2stop - m2start) / m2interval,
                    )

                    # Could be some software overhead
                    TIMEOUT_MARGIN = 2.0

                    max_time_preset = MaxTimeBetweenFramesPreset(
                        detector=tomo_detector.detector,
                        max_time=max(time_between_slow_step, fscan.pars.fast_step_time)
                        + TIMEOUT_MARGIN,
                    )
                    preset_list.append_preset(max_time_preset)

                    scan = fscan_as_bliss_scan(
                        fscan,
                        tomo_detector.detector.image,
                        run=False,
                        scan_info=scan_info,
                        title="tiling",
                    )
                else:
                    scan = amesh(
                        m1,
                        m1start,
                        m1stop,
                        m1interval,
                        m2,
                        m2start,
                        m2stop,
                        m2interval,
                        expo_time,
                        tomo_detector.detector.image,
                        backnforth=True,
                        sleep_time=sleep_time,
                        run=False,
                        scan_info=scan_info,
                        title="tiling",
                    )

        if lateral_motor_mode == TilingLateralMotorMode.UNDER_ROT:
            # Center the motors
            if self._tomoconfig.sample_u_axis is not None:
                preset = MoveAxisPreset(
                    self._tomoconfig.sample_u_axis,
                    0,
                    self._tomoconfig.sample_v_axis,
                    0,
                    sleep_time=sleep_time,
                    restore_position=True,
                )
                preset_list.append_preset(preset)

            # Move back the rotation axis at the expected place
            preset = RestoreAxisPreset(y_axis, sleep_time=sleep_time)
            preset_list.append_preset(preset)

        # FIXME: This have to be handled in a better way
        scan._independent_presets = preset_list
        return scan

    def _add_metadata(
        self,
        expo_time,
        detector,
        scan_info=None,
        scan_sample_y_axis=True,
        scan_y_axis=False,
        scan_z_axis=True,
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        pixel_size = detector.sample_pixel_size  # it's micro meter

        scan_info = ScanInfo.normalize(scan_info)
        technique = scan_info.setdefault("technique", {})
        tiling = {
            "y_axis_name": hdf5_axis_name(self.y_axis, scan_y_axis),
            "z_axis_name": hdf5_axis_name(self.z_axis, scan_z_axis),
            "sample_y_axis_name": hdf5_axis_name(
                self.sample_y_axis, scan_sample_y_axis
            ),
            "rotation_axis_name": hdf5_axis_name(self.rotation_axis, False),
            "exposure_time": expo_time,
            "exposure_time@units": "s",
            "sample_pixel_size": pixel_size,
            "sample_pixel_size@units": "um",
        }
        technique["tiling"] = tiling
        return scan_info
