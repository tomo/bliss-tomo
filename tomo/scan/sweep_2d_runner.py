import logging

from bliss.common.logtools import log_info
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.scan import Scan
from bliss.scanning.acquisition.motor import LinearStepTriggerMaster
from bliss.scanning.chain import AcquisitionChain
from bliss import current_session
from tomo.scan.sweep_runner import SweepRunner
from fscan.fscantools import FScanDisplay
from fscan.fscaninfo import FScanInfo
from tomo.constants import NxTomoImageKey

_logger = logging.getLogger(__name__)


class Sweep2DRunner(SweepRunner):
    """
    Class used to build and run the following 2d scan:
    one step motor moves between each sweep scan
    """

    def __init__(self, config):
        super().__init__(config)
        self._projection_scan_name = "fsweep2d"
        self._slow_motor = None
        self._slow_start_pos = None
        self._slow_end_pos = None
        self._slow_npoints = None

    def __call__(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        scan_info=None,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like fscan2d or
        bliss common scans
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        self.scan_info = ScanInfo.normalize(scan_info)
        if header is None:
            header = {}

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            fast_motor,
            fast_start_pos,
            fast_end_pos,
            fast_npoints,
            slow_motor,
            slow_start_pos,
            slow_step_size,
            slow_npoints,
            expo_time,
            scan_info,
        )

        self._setup_detectors_from_active_mg()

        self._scan = self._setup_scan(
            fast_motor,
            fast_start_pos,
            fast_end_pos,
            fast_npoints,
            slow_motor,
            slow_start_pos,
            slow_step_size,
            slow_npoints,
            expo_time,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build and return scan
        Use some methods from fsweep object to build fsweep scan part and
        add a LinearStepTriggerMaster on top of it to control slow motor
        """
        log_info("tomo", "projection_scan() entering")

        self._slow_motor = slow_motor
        self._slow_start_pos = slow_start_pos
        self._slow_end_pos = slow_start_pos + slow_step_size * (slow_npoints - 1)
        self._slow_npoints = slow_npoints

        fsweep = self._fsweep
        fsweep.pars.motor = fast_motor
        fsweep.pars.start_pos = fast_start_pos
        fsweep.pars.acq_size = (fast_end_pos - fast_start_pos) / fast_npoints
        fsweep.pars.acq_time = expo_time
        fsweep.pars.npoints = fast_npoints
        fsweep.validate()
        fsweep.master.init_scan()

        self._chain = AcquisitionChain(parallel_prepare=True)
        slow_mot_master = LinearStepTriggerMaster(
            slow_npoints, slow_motor, slow_start_pos, self._slow_end_pos
        )
        mot_master = fsweep.master.setup_motor_master(self._chain)
        self._chain.add(slow_mot_master, mot_master)
        acq_master = fsweep.master.setup_acq_master(self._chain, mot_master)
        # --- lima devices
        lima_params = {
            "acq_trigger_mode": "EXTERNAL_GATE",
            "acq_expo_time": expo_time,
            "acq_nb_frames": fast_npoints * slow_npoints,
            "wait_frame_id": [
                (i * fast_npoints - 1) for i in range(1, slow_npoints + 1)
            ],
            "prepare_once": True,
            "start_once": True,
            "scan_frame_per_file": fast_npoints,
        }
        fsweep.master.mgchain.setup("lima", self._chain, acq_master, lima_params)
        fsweep.master.setup_acq_slaves(self._chain, acq_master)

        inpars = fsweep.inpars
        if inpars.display_flag:
            fsweep.master.show()

        fscan_info = FScanInfo(fsweep._scan_name, "fsweep", inpars.npoints)
        fscan_info.set_fscan_pars(fsweep.pars)
        fscan_info.set_fscan_title(
            "{motor_name} {start_pos:g} {acq_size:g} {acq_time:g} {npoints:d}", inpars
        )
        fscan_info.set_fscan_info(scan_info)

        proj_scan = Scan(
            self._chain,
            name=fsweep._scan_name,
            save=inpars.save_flag,
            scan_info=fscan_info,
            data_watch_callback=FScanDisplay(
                fsweep.master.musst,
                trigger_name="timer",
                motors=(slow_motor, fast_motor),
                limas=fsweep.master.lima_used,
                mcas=fsweep.master.mca_used,
            ),
        )

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Estimate scan time using fsweep.validate() to get scan time of
        fast acquisition and add slow motor displacement for each step
        """
        slow_step = (self._slow_end_pos - self._slow_start_pos) / self._slow_npoints
        slow_motor_time = self.mot_disp_time(
            self._slow_motor, slow_step, self._slow_motor.velocity
        ) * (self._slow_npoints - 1)
        fsweep = self._fsweep
        fsweep.validate()
        return fsweep.inpars.scan_time * self._slow_npoints + slow_motor_time

    def _add_metadata(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        scan_info=None,
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        motor = list()
        alias_name = "None"
        ALIASES = current_session.env_dict["ALIASES"]

        if ALIASES.get_alias(fast_motor.name) is not None:
            alias_name = ALIASES.get_alias(fast_motor.name)
        motor.extend(["fast_motor", fast_motor.name, alias_name])
        alias_name = "None"
        if ALIASES.get_alias(slow_motor.name) is not None:
            alias_name = ALIASES.get_alias(slow_motor.name)
        motor.extend(["slow_motor", slow_motor.name, alias_name])
        technique = scan_info.setdefault("technique", {})
        technique["image_key"] = NxTomoImageKey.PROJECTION.value
        technique["proj"] = {
            "motor": motor,
            "scan_type": "SWEEP",
            "scan_range": fast_end_pos - fast_start_pos,
            "proj_n": fast_npoints,
            "nb_scans": slow_npoints,
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        return scan_info

    def get_scan_preset_list(self):
        """
        Return list of configured scan presets
        """
        # get the scan presets on the tomo runner
        preset_list = super().get_scan_preset_list()
        # get the scan presets on the fscan
        for i in self._fsweep._scan_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list

    def get_chain_preset_list(self):
        """
        Return list of configured chain presets
        """
        # get the chain presets on the tomo runner
        preset_list = super().get_chain_preset_list()
        # get the chain presets on the fscan
        for i in self._fsweep._chain_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list
