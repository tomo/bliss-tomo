import logging

from bliss.common.logtools import log_info, log_error
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.common.scans.meshes import amesh
from bliss.scanning.scan_info import ScanInfo
from bliss import current_session
from tomo.scan.tomo_runner import TomoRunner
from tomo.constants import NxTomoImageKey
from bliss.common.cleanup import error_cleanup, capture_exceptions
from bliss.common.cleanup import axis as cleanup_axis

_logger = logging.getLogger(__name__)


class MeshRunner(TomoRunner):
    """
    Class used to build and run step by step scan with two motors
    Built as a layer of bliss standard amesh
    """

    def __init__(self, default_single_chain=None, default_accumulation_chain=None):
        super().__init__(
            single_chain=default_single_chain, acc_chain=default_accumulation_chain
        )
        self._projection_scan_name = "amesh"
        self._fast_motor = None
        self._fast_start_pos = None
        self._fast_end_pos = None
        self._fast_npoints = None
        self._slow_motor = None
        self._slow_start_pos = None
        self._slow_end_pos = None
        self._slow_npoints = None
        self._expo_time = None
        self._detectors = None

    def __call__(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        scan_info=None,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like bliss
        standard amesh
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        self.scan_info = ScanInfo.normalize(scan_info)
        if header is None:
            header = {}

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            fast_motor,
            fast_start_pos,
            fast_end_pos,
            fast_npoints,
            slow_motor,
            slow_start_pos,
            slow_step_size,
            slow_npoints,
            expo_time,
            scan_info,
        )

        self._scan = self._setup_scan(
            fast_motor,
            fast_start_pos,
            fast_end_pos,
            fast_npoints,
            slow_motor,
            slow_start_pos,
            slow_step_size,
            slow_npoints,
            expo_time,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            # on error go back to initial position
            restore_list = (cleanup_axis.POS,)
            with error_cleanup(fast_motor, slow_motor, restore_list=restore_list):
                with capture_exceptions(raise_index=0) as capture:
                    with capture():
                        self._scan.run()
                    # test if an error has occured
                    if len(capture.failed) > 0:
                        log_error(
                            "tomo",
                            "A problem occured during the step scan, scan aborted",
                        )
                        log_error("tomo", capture.exception_infos)
                        log_error("tomo", "\n")

        return self._scan

    def _setup_scan(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build and return amesh
        """
        log_info("tomo", "projection_scan() entering")

        self._fast_motor = fast_motor
        self._fast_start_pos = fast_start_pos
        self._fast_end_pos = fast_end_pos
        self._fast_npoints = fast_npoints
        self._slow_motor = slow_motor
        self._slow_start_pos = slow_start_pos
        self._slow_end_pos = slow_start_pos + slow_step_size * slow_npoints
        self._slow_npoints = slow_npoints
        self._expo_time = expo_time

        builder = ChainBuilder([])
        self._detectors = []
        for node in builder.get_nodes_by_controller_type(Lima):
            self._detectors.append(node.controller)

        with self._setup_detectors_in_default_chain(self._detectors):
            proj_scan = amesh(
                fast_motor,
                fast_start_pos,
                fast_end_pos,
                fast_npoints,
                slow_motor,
                slow_start_pos,
                self._slow_end_pos,
                slow_npoints,
                expo_time,
                run=False,
                scan_info=scan_info,
            )

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Estimate scan time by taking into account fast motor displacement
        and image acquisition time for each angle of each repetition and
        slow motor displacement for each repetition
        """
        fast_step = (self._fast_end_pos - self._fast_start_pos) / self._fast_npoints
        slow_step = (self._slow_end_pos - self._slow_start_pos) / self._slow_npoints
        return (
            self.mot_disp_time(self._fast_motor, fast_step, self._fast_motor.velocity)
            * self._fast_npoints
            * (self._slow_npoints + 1)
            + self.mot_disp_time(self._slow_motor, slow_step, self._slow_motor.velocity)
            * self._slow_npoints
            + (self._fast_npoints + 1) * (self._slow_npoints + 1) * self._expo_time
        )

    def _add_metadata(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        scan_info=None,
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        motor = list()
        alias_name = "None"
        ALIASES = current_session.env_dict["ALIASES"]

        if ALIASES.get_alias(fast_motor.name) is not None:
            alias_name = ALIASES.get_alias(fast_motor.name)
        motor.extend(["fast_motor", fast_motor.name, alias_name])
        alias_name = "None"
        if ALIASES.get_alias(slow_motor.name) is not None:
            alias_name = ALIASES.get_alias(slow_motor.name)
        motor.extend(["slow_motor", slow_motor.name, alias_name])
        technique = scan_info.setdefault("technique", {})
        technique["image_key"] = NxTomoImageKey.PROJECTION.value
        technique["proj"] = {
            "motor": motor,
            "scan_type": "STEP",
            "scan_range": fast_end_pos - fast_start_pos,
            "proj_n": fast_npoints + 1,
            "nb_scans": slow_npoints + 1,
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        return scan_info
