import typing
from .base_scintillator import Correction, BaseScintillator

try:
    # BLISS >= 2.1
    from bliss.shell.formatters.ansi import BOLD, BLUE, YELLOW, GREEN

    USE_PT = True
    COLORS = ["class:color1", "class:color2", "class:color3"]
except ImportError:
    # BLISS <= 2.0
    from bliss.common.utils import BOLD, BLUE, YELLOW, GREEN

    USE_PT = False
    COLORS = [BLUE, YELLOW, GREEN]


class HumanReadableTurns(typing.NamedTuple):
    full_turns: int
    half_turns: int
    quarter_turns: int
    eighth_turns: int
    sixteenth_turns: int
    orientation: typing.Literal["clockwise", "anticlockwise", ""]


def human_readable_turns(num_turns: float) -> HumanReadableTurns:
    """
    Calculate a human readable number of turns.

    Broken down the `num_turns` into full turns, half turns, quarter turns, eighth turns,
    and sixteenth turns. The orientation (anticlockwise or clockwise) is determined based
    on the sign of the number of turns and the orientation factor from the geometry.

    Parameters:
        num_turns: Number or turns as signed floating number. A positive value means
                   clockwise direction.
    """
    num_sixteenth_turns = abs(num_turns) * 16

    # Calculate full, half, quarter, and eighth turns as positive values
    full_turns = int(num_sixteenth_turns // 16)
    remaining = num_sixteenth_turns % 16

    half_turns = int(remaining // 8)
    remaining %= 8

    quarter_turns = int(remaining // 4)
    remaining %= 4

    eighth_turns = int(remaining // 2)
    remaining %= 2

    sixteenth_turns = int(remaining)

    return HumanReadableTurns(
        full_turns=full_turns,
        half_turns=half_turns,
        quarter_turns=quarter_turns,
        eighth_turns=eighth_turns,
        sixteenth_turns=sixteenth_turns,
        orientation=(
            "anticlockwise" if num_turns < 0 else ("clockwise" if num_turns > 0 else "")
        ),
    )


def create_scintillator_geometry(config: dict) -> BaseScintillator:
    if "model" not in config:
        raise RuntimeError("No 'model' defined for the scintillator geometry")

    # FIXME: We could replace that with python plugin if beamline want to tune
    #        the algorithm on there own
    model = config["model"]
    if model == "right_triangle":
        from .right_triangle import RightTriangleScintillator

        return RightTriangleScintillator(config)
    elif model == "optique_peter_ttss":
        from .optique_peter_ttss import OptiquePeterTtss

        return OptiquePeterTtss()
    raise RuntimeError(f"Unsupported model '{model}' for scintillator geometry")


def print_tilt_results(
    geometry: BaseScintillator,
    tilt_corrections: list[Correction],
):
    """
    Print the tilt results in number of screw turns, including which screw to turn.

    Parameters:
    - tilt_results: A dictionary containing the displacement distances, required turns, and screw position for each axis.
    """

    def format_turns(turns: HumanReadableTurns):
        # Prepare the turns string
        turns_list = []
        if turns.full_turns > 0:
            turns_list.append(f"{turns.full_turns}")
        if turns.half_turns > 0:
            turns_list.append(f"{turns.half_turns}/2")
        if turns.quarter_turns > 0:
            turns_list.append(f"{turns.quarter_turns}/4")
        if turns.eighth_turns > 0:
            turns_list.append(f"{turns.eighth_turns}/8")
        if turns.sixteenth_turns > 0:
            turns_list.append(f"{turns.sixteenth_turns}/16")

        # Combine the turns into a single string
        if turns_list == []:
            turns_string = ""
        else:
            turns_string = "  +  ".join(turns_list)
        return turns_string

    for c in tilt_corrections:
        print()
        print(c.description)
        if USE_PT:
            from bliss.shell.formatters.tabulate import tabulate

            header = [
                "    Screw",
                "    Nb turn",
                "    Decomposed turn",
                "    Orientation",
            ]
            rows = [header]
            for index, sc in enumerate(c.corrections):
                num_turns = sc.turns
                turns = human_readable_turns(num_turns)
                turns_string = format_turns(turns)
                rows += [
                    [
                        (COLORS[index], f"{sc.role}"),
                        num_turns,
                        ("class:header", turns_string),
                        ("class:header", turns.orientation),
                    ]
                ]
            rows = list(map(list, zip(*rows)))  # transpose
            print(tabulate(rows))
        else:
            for index, sc in enumerate(c.corrections):
                color = COLORS[index % len(COLORS)]
                num_turns = sc.turns
                turns = human_readable_turns(num_turns)
                turns_string = format_turns(turns)

                print(BOLD(f"- Screw '{color(sc.role)}':"))
                if num_turns != 0.0:
                    if turns_string != "":
                        print(f"   - {turns_string}  turns {turns.orientation}")
                    else:
                        print("   - No turns needed")
                    print(f"     ({num_turns:.4f} turns)")
                else:
                    print("   - No turns needed")
    print()
