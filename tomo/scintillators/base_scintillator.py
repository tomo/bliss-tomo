from abc import ABC, abstractmethod
import typing


class ScrewCorrection(typing.NamedTuple):
    """Correction to apply to the scintillator"""

    role: str
    """Identifier of the screw"""

    turns: float
    """Number of turns for the correction.

    The number is signed, with the positive value in clockwise direction.
    """

    distance: float
    """Distance to move."""


class Correction(typing.NamedTuple):
    """Correction to apply to the scintillator"""

    description: str
    """Description of this set of correction"""

    corrections: list[ScrewCorrection]
    """List of screw corrections."""


class BaseScintillator(ABC):
    """Base class for scintillator geometry

    Provides helper to deal with connection.
    """

    @abstractmethod
    def tilt_hardware_correction(
        self,
        tilt_corr_v_rad: float,
        tilt_corr_h_rad: float,
    ) -> list[Correction]:
        """Convert a physical correction into the a hardware based correction.

        For now it s set of screws to turn.

        Arguments:
            tilt_corr_v_rad: Tilt angle around the vertical axes (in radian).
            tilt_corr_h_rad: Tilt angle around the horizontal axes (in radian).

        Returns:
            A list of screws to tune.
        """
        ...
