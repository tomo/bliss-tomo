from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.chain import AcquisitionMaster, AcquisitionChannel, ChainNode
from bliss.controllers.musst import Musst, MusstIntegratingCounterController
import gevent
from gevent import event
import numpy
import time


class SoftTriggerMusstAcquisitionMaster(MusstAcquisitionMaster):
    """
    Class to generate by musst one or several triggers per point
    """

    def __init__(self, musst_device):
        super().__init__(
            musst_device,
            program="soft_trig.mprg",
            program_start_name="SOFT_TRIG",
            program_abort_name="SOFT_TRIG_CLEAN",
        )
        self._AcquisitionObject__trigger_type = AcquisitionMaster.SOFTWARE
        # self.musst.CLEAR

    def prepare(self):
        if self._iter_index == 0:
            self.musst.CLEAR
        lima_slave = self.slaves[0]

        self.wait_slaves_prepare()

        timer_factor = self.musst.get_timer_factor()
        npulses = 1
        expo_time = lima_slave.device.acquisition.expo_time
        deadtime = 0
        if lima_slave.device.accumulation.nb_frames > 1:
            npulses = int(lima_slave.device.accumulation.nb_frames)
            expo_time = lima_slave.device.accumulation.expo_time
        if lima_slave.device.camera_type.lower() == "pco":
            lattime = 0
            if (
                lima_slave.device.accumulation.expo_time > -1
                and lima_slave.device.accumulation.expo_time < 0.012
            ):
                lattime = 200e-6
            if (
                lima_slave.device.accumulation.expo_time > -1
                and lima_slave.device.accumulation.expo_time >= 0.012
            ):
                lattime = 50e-6
            deadtime = int(
                ((lima_slave.device.camera.coc_run_time - expo_time) + lattime)
                * timer_factor
            )

        if lima_slave.device.camera_type.lower() == "iris_15":
            lattime = 0.0
            readout_time = lima_slave.device.camera.readout_time * 1.0e-6
            deadtime = int((readout_time + lattime) * timer_factor)

        expotime = int(expo_time * timer_factor)
        trigdelta = expotime + deadtime

        self.next_vars = {
            "NPULSES": npulses,
            "EXPOTIME": expotime,
            "TRIGDELTA": trigdelta,
        }

        return MusstAcquisitionMaster.prepare(self)

    def start(self):
        pass

    def trigger(self):
        self.musst.run(self.program_abort_name)
        while self.musst.STATE == self.musst.RUN_STATE:
            gevent.idle()
        self.musst.run(self.program_start_name)
        self._start_epoch = time.time()
        self._running_state = True
        self._event.set()

    def stop(self):
        if self.musst.STATE == self.musst.RUN_STATE:
            self.musst.ABORT
            if self.program_abort_name:
                self.musst.run(self.program_abort_name)
                self.wait_ready()


class SoftTriggerIntegratingCounterController(MusstIntegratingCounterController):
    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        return SoftTriggerMusstAcquisitionMaster(self.musst)


class musstacc(Musst):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._counter_controllers["icc"] = SoftTriggerIntegratingCounterController(self)
