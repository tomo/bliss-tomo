from tomo.fulltomo import FullFieldTomo
from tomo.sequencebasic import SequenceBasic
from bliss import setup_globals
import numpy as np


class TomoLookup(FullFieldTomo):
    """
    Class to handle full field tomo loop acquisition
    """

    def __init__(self, name, config):
        SequenceBasic.__init__(self, name, config)
        self.lmotors = config.get("step_axis")
        """List of axis moving between each tomo acquisition"""
        lmotpos = list()
        lmotpos = [(motor, []) for motor in self.lmotors]
        self.lmotpos: dict = dict(lmotpos)
        """List of motor positions for each step axis"""

    def validate(self):
        """
        Check parameters coherency
        Estimate sequence acquisition time
        For each step axis, check if total displacement during acquisition
        goes beyond axis limits
        """
        # FullFieldTomo validate
        super().validate()

        for motor in self.lmotors:
            limit = min(motor.low_limit, motor.high_limit)
            if self.lmotpos[motor][0] < limit:
                raise ValueError(
                    f"{motor.name} start position is beyond axis limit ({limit})! \n"
                )
            limit = max(motor.low_limit, motor.high_limit)
            if self.lmotpos[motor][-1] > limit:
                raise ValueError(
                    f"{motor.name} end position is beyond axis limit ({limit})! \n"
                )

    def add_metadata(self, scan_info):
        """
        Fill the scan_info dictionary with full field tomo meta data
        Add additional information:

            * number of tomo repetitions
            * step axis full name and alias name (if exists)
        """
        super().add_metadata(scan_info)
        nloop = len(self.lmotpos[self.lmotors[0]])
        scan_info["technique"]["scan"]["nb_scans"] = nloop
        for motor in self.lmotors:
            motor_name = motor.name
            alias_name = "None"
            if setup_globals.ALIASES.get(motor.name) is not None:
                alias_name = motor.name
                motor_name = motor.original_name
            scan_info["technique"]["scan"]["motor"].extend(
                ["step_axis", motor_name, alias_name]
            )

    def run(self, user_info=None):
        """
        For each step axis position, move motor to the desired position
        and run full field tomo
        """
        nloop = len(self.lmotpos[self.lmotors[0]])
        for loop in range(nloop):
            motpos = list()
            for motor in self.lmotors:
                motpos.append(motor)
                motpos.append(self.lmotpos[motor][loop])
            mv(motpos)
            super().run(user_info)

    def full_turn_scan(
        self,
        *pos_mot,
        nb_scans,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        run=True,
    ):
        """
        Loop on full field tomo of 360 angular range for each step motor
        position
        """
        assert len(pos_mot) == len(self.lmotors)
        for motor, pos_mot in zip(self.lmotors, list(pos_mot)):
            self.lmotpos[motor] = np.linspace(pos_mot[0], pos_mot[1], nb_scans)
        super().full_turn_scan(dataset_name, collection_name, start_pos, run=run)

    def half_turn_scan(
        self,
        *pos_mot,
        nb_scans,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        run=True,
    ):
        """
        Loop on full field tomo of 180 angular range for each step motor
        position
        """
        assert len(pos_mot) == len(self.lmotors)
        for motor, pos_mot in zip(self.lmotors, list(pos_mot)):
            self.lmotpos[motor] = np.linspace(pos_mot[0], pos_mot[1], nb_scans)
        super().half_turn_scan(dataset_name, collection_name, start_pos, run=run)

    def basic_scan(
        self,
        *pos_mot,
        nb_scans,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        dataset_name=None,
        collection_name=None,
        run=True,
    ):
        """
        Loop on full field tomo using given parameters for each step motor
        position
        """
        assert len(pos_mot) == len(self.lmotors)
        for motor, pos_mot in zip(self.lmotors, list(pos_mot)):
            self.lmotpos[motor] = np.linspace(pos_mot[0], pos_mot[1], nb_scans)
        super().basic_scan(
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            dataset_name,
            collection_name,
            run=run,
        )
