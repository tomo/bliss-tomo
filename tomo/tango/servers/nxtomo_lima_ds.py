# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import sys
import logging
from bliss.common.greenlet_utils.gevent_monkey import unpatch
from Lima.Server import LimaCCDs
from . import helper
from . import nxtomo_simulator


def main():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d %(levelname)s %(threadName)s %(module)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    helper.register_lima_camera(nxtomo_simulator)
    unpatch(subprocess=True)
    result = LimaCCDs.main()
    sys.exit(result)


if __name__ == "__main__":
    main()
