# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import sys


def register_lima_camera(camera_module):
    """
    Register a python module as a Tango Lima camera.

    The module have to expose a `get_tango_specific_class_n_device`
    method returning the Tango device and deviceclass classes.

    Argument:
        camera_module: A python module containing the Tango Lima device classes
    """
    _tangoclassclass, tangoclass = camera_module.get_tango_specific_class_n_device()
    name = tangoclass.__name__
    sys.modules[f"Lima.Server.camera.{name}"] = camera_module

    from Lima.Server import camera

    camera.__all__.append(name)


def register_lima_plugin(camera_module):
    """
    Register a python module as a Tango Lima plugin.

    The module have to expose a `get_tango_specific_class_n_device`
    method returning the Tango device and deviceclass classes.

    Argument:
        camera_module: A python module containing the Tango Lima device classes
    """
    _tangoclassclass, tangoclass = camera_module.get_tango_specific_class_n_device()
    name = tangoclass.__name__
    sys.modules[f"Lima.Server.plugins.{name}"] = camera_module

    from Lima.Server import plugins

    plugins.__all__.append(name)
