import os.path
import h5py
import numpy
import logging
import datetime
import contextlib
import gevent
import threading
from silx.gui import qt
from silx.gui.plot3d.ScalarFieldView import ScalarFieldView
from silx.gui.plot3d.scene import transform
from silx.gui.utils import concurrent


try:
    from skimage.transform import resize
except ImportError:
    resize = None

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


class ScalarFieldView2(ScalarFieldView):
    autoResetZoom = True

    def centerScene(self):
        if self.autoResetZoom:
            ScalarFieldView.centerScene(self)

    def forceCenterScene(self):
        ScalarFieldView.centerScene(self)

    def grabArray(self):
        """Renders the OpenGL scene into a numpy array

        :returns: OpenGL scene RGB rasterization
        :rtype: numpy.ndarray
        """
        plot3D = self.getPlot3DWidget()
        if not plot3D.isValid():
            _logger.error("OpenGL 2.1 not available, cannot save OpenGL image")
            height, width = plot3D._window.shape
            image = numpy.zeros((height, width, 3), dtype=numpy.uint8)
        else:
            self.getPlot3DWidget().makeCurrent()
            image = plot3D._window.grab(plot3D.context())

        return image

    def setTransformMatrix4x4(self, matrix):
        """Set the transform matrix applied to the data.

        :param numpy.ndarray matrix: 4x4 transform matrix
        """
        matrix = numpy.array(matrix, copy=True, dtype=numpy.float32)
        self._dataTransform.setMatrix(matrix)
        self.sigTransformChanged.emit()
        self.centerScene()  # Reset viewpoint


class VisibleSampleStage:
    def __init__(self):
        self._qapp = qt.QApplication.instance()
        _logger.info("Thread: %s", threading.current_thread().name)
        if self._qapp is None:
            _logger.info("Creating QApplication...")
            self._qapp = qt.QApplication([])
            _logger.info("QApplication created")

        self._updater = gevent.spawn(self._updateQt)

        self._model = None
        self.mode = "visible"

        self.glSize = 256, 256
        self.qtParent = None
        self._qtwindow = None

        self._background = numpy.zeros(self.glSize + (3,), dtype=numpy.uint8)
        layer = numpy.arange(numpy.product(self.glSize), dtype=numpy.uint8) // 4
        layer.shape = self.glSize
        self._background[..., 0] = layer
        self.dtype_rgb = numpy.dtype(
            [("r", numpy.uint8), ("g", numpy.uint8), ("b", numpy.uint8)]
        )
        self._background = self._background.view(self.dtype_rgb)
        self._background = numpy.swapaxes(self._background, 0, 1)
        self._camera_position = None
        self._camera_direction = None
        self._camera_up = None

        # Sample stage status
        self.sx = 0
        self.sy = 0
        self.sz = 0
        self.srot = 0
        self.sampu = 0
        self.sampv = 0

        # Detector status
        self.detector_y = 0
        self.detector_size = 256, 256
        self.detector_magnification = 1

        # Shutter status
        self.is_shutter_open = True

        self._gain = 1
        self._dark = None
        self._beam = None

    @property
    def zoom(self):
        """Normalize the zoom to a power of 2"""
        zoom = self.detector_magnification
        if zoom <= 0:
            return 0.1
        if zoom < 1:
            return zoom
        return int(zoom)

    @contextlib.contextmanager
    def _open_dataset(self, filename, info):
        if filename is None or filename == "":
            yield None
            return
        if "::" in filename:
            filename, path = filename.split("::", 1)
        else:
            path = None
        if not os.path.isfile(filename):
            _logger.error("Filename %s not found", filename)
            yield None
            return

        with h5py.File(filename, "r") as h5obj:
            if path and path not in h5obj:
                _logger.error("Dataset %s::%s not found", filename, path)
                yield None
                return
            _logger.info("%s from filename %s loaded", info.capitalize(), filename)
            if path:
                yield h5obj[path]
            else:
                yield h5obj

    def load_filenames(self, filename, scene_filename=None):
        with self._open_dataset(f"{filename}::model", "Model") as data:
            if data is None:
                model = None
            else:
                model = data[...]
        model = model[::2, ::2, ::2]
        model = model[::2, ::2, ::2]
        self._model = model

        if scene_filename:
            with self._open_dataset(scene_filename, "Scene") as data:
                if "::" not in scene_filename:
                    path = f"{datetime.datetime.now().month:02d}"
                    if path not in data:
                        path = "default"
                    data = data[path]

                background = numpy.ascontiguousarray(data["background"][:, :, 0:3])
                self._background = background.view(self.dtype_rgb)
                self._camera_position = data["camera_position"][...]
                self._camera_direction = data["camera_direction"][...]
                self._camera_up = data["camera_up"][...]

    def camera_updated(self, source):
        plot3D = self._qtwindow.getPlot3DWidget()
        extrinsic = plot3D.viewport.camera.extrinsic
        _logger.error(
            "Camera position %s; direction %s; up %s",
            extrinsic.position,
            extrinsic.direction,
            extrinsic.up,
        )

    def get_window(self):
        if self._qtwindow is not None:
            return self._qtwindow
        window = ScalarFieldView2(parent=self.qtParent)
        window.setWindowFlag(qt.Qt.Window)
        window.setData(self._model)
        self._qtwindow = window
        window.setProjection("perspective")
        window.setAxesLabels("X", "Y", "Z")
        window.addIsosurface(1, "#00A0A0FF")
        window.setBackgroundColor([1, 1, 1])
        window.autoResetZoom = False
        data = self._model

        centerModelTransform = transform.Translate()
        centerModelTransform.setTranslate(
            -data.shape[2] // 2, -data.shape[1] // 2, -data.shape[0] // 2
        )

        self._sTransform = transform.Translate()
        self._sampTransform = transform.Translate()

        modelOrientation = transform.Rotate()
        modelOrientation.setAngleAxis(-90, (1, 0, 0))

        self._srotTransform = transform.Rotate()
        self._sampleStageTransform = transform.TransformList(
            [
                modelOrientation,
                self._sTransform,
                self._srotTransform,
                self._sampTransform,
                centerModelTransform,
            ]
        )

        # It have to be displayed to be able to grab it
        size = qt.QSize(*self.glSize)
        plot3D = window.getPlot3DWidget()
        plot3D.setMinimumSize(size)
        plot3D.setMaximumSize(size)
        plot3D.setOrientationIndicatorVisible(False)
        window.setVisible(True)
        self.sync_transformation()
        window.setTransformMatrix4x4(self._sampleStageTransform.matrix)
        window.forceCenterScene()

        extrinsic = plot3D.viewport.camera.extrinsic
        extrinsic.addListener(self.camera_updated)
        if self._camera_position is not None:
            extrinsic.position = self._camera_position
        if self._camera_direction is not None:
            extrinsic.direction = self._camera_direction
        if self._camera_up is not None:
            extrinsic.up = self._camera_up

        return window

    def sync_transformation(self):
        self._sampTransform.setTranslate(-self.sampv, -self.sampu, 0)
        self._sTransform.setTranslate(-self.sy, -self.sx, self.sz)
        self._srotTransform.setAngleAxis(self.srot, (0, 0, 1))

    def _updateQt(self):
        i = 0
        while True:
            if i % 20 == 0:
                _logger.debug("Update Qt...")
            self._qtProcessEvents()
            gevent.sleep(0.1)
            i += 1

    def _qtProcessEvents(self):
        self._qapp.processEvents()

    def _qtComputeImage(self):
        _logger.info("Computing image in Qt...")
        window = self.get_window()
        self.sync_transformation()
        window.setTransformMatrix4x4(self._sampleStageTransform.matrix)
        self._qtProcessEvents()
        window.repaint()
        self._qtProcessEvents()
        image = window.grabArray()
        transparent = numpy.array((1, 1, 1), dtype=self.dtype_rgb)
        rgb_image = image.view(self.dtype_rgb)
        mask = rgb_image == transparent
        rgb_image[mask] = self._background[mask]
        _logger.info("Image computed in Qt")
        return image

    def compute_image(self):
        _logger.info("Computing image...")
        future = concurrent.submitToQtMainThread(self._qtComputeImage)
        image = future.result(2)
        _logger.info("Image computed")
        return image
