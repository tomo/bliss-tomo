from __future__ import annotations

import os
import tango
import logging
from skimage.color import rgb2gray

from Lima import Core
from Lima import Simulator as LimaSimuMod
import Lima.Server.camera.Simulator as TangoSimuMod
from bliss.common.shutter import BaseShutterState

from . import visible_sample_stage
from . import remote_bliss


_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


class VisibleTomoCamera(LimaSimuMod.Camera):
    def set_device_server(self, deviceServer):
        super().__init__()
        _logger.info("Loading config...")

        self._sx_name = deviceServer.sx
        self._sy_name = deviceServer.sy
        self._sz_name = deviceServer.sz
        self._srot_name = deviceServer.srot
        self._sampu_name = deviceServer.sampu
        self._sampv_name = deviceServer.sampv
        self._det_y_name = deviceServer.detector_y
        self._det_magn_name = deviceServer.detector_magnification
        self._beam_shutter_name = deviceServer.beam_shutter
        self._visible_size = float(deviceServer.voxel_size)

        host, port = remote_bliss.find_address()
        self.remote_bliss = remote_bliss.RemoteBliss(host=host, port=port)

        axis_names = [
            self._sx_name,
            self._sy_name,
            self._sz_name,
            self._srot_name,
            self._sampu_name,
            self._sampv_name,
            self._det_y_name,
        ]
        axis_names = [a for a in axis_names if a != ""]
        for axis_name in axis_names:
            self.remote_bliss.subscribe_axis(axis_name, dial_position=True)
        if self._beam_shutter_name:
            self.remote_bliss.subscribe_shutter(self._beam_shutter_name, state=True)
        if self._det_magn_name:
            self.remote_bliss.subscribe_optic(self._det_magn_name, magnification=True)

        self.remote_bliss.start()

        def normalize_filename(f):
            if f is None:
                return None
            if not isinstance(f, str):
                return None
            if f == "":
                return None
            return os.path.abspath(f)

        _logger.info("Loading data files...")
        filename = normalize_filename(deviceServer.filename)
        self._sample_stage: visible_sample_stage.VisibleSampleStage | None
        if filename is not None and os.path.exists(filename):
            scene_filename = normalize_filename(deviceServer.scene_filename)

            self._sample_stage = visible_sample_stage.VisibleSampleStage()
            self._sample_stage.load_filenames(filename, scene_filename)
        else:
            self._sample_stage = None
            _logger.error("No data loaded")

        hwInterface = deviceServer._SimuInterface
        detinfo = hwInterface.getHwCtrlObj(Core.HwCap.DetInfo)
        pixel_size = detinfo.getPixelSize()
        self._detector_pixel_size = pixel_size

    def sync_sample_stage(self):
        """Synchronize the simulated sample stage with devices from the BLISS session"""
        ss = self._sample_stage
        assert ss is not None
        bliss = self.remote_bliss
        ss.sx = bliss.get_axis_dial_position(self._sx_name, 0)
        ss.sy = bliss.get_axis_dial_position(self._sy_name, 0)
        ss.sz = bliss.get_axis_dial_position(self._sz_name, 0)
        ss.srot = bliss.get_axis_dial_position(self._srot_name, 0)
        ss.sampu = bliss.get_axis_dial_position(self._sampu_name, 0)
        ss.sampv = bliss.get_axis_dial_position(self._sampv_name, 0)
        ss.detector_y = bliss.get_axis_dial_position(self._det_y_name, 0)
        ss.detector_magnification = bliss.get_optic_magnification(
            self._det_magn_name, 1
        )

        beam_shutter = bliss.get_shutter_state(
            self._beam_shutter_name, BaseShutterState.OPEN
        )
        ss.is_shutter_open = beam_shutter == BaseShutterState.OPEN

    def fillData(self, data):
        """
        Process the data

        Called for every frame in a different C++ thread.
        """
        _logger.info("Process")

        try:
            expo_time = self.getExpTime()
            if self._sample_stage is None:
                _logger.error("No data loaded")
                return data
            self.sync_sample_stage()
            ss = self._sample_stage
            ss.detector_size = data.buffer.shape

            _logger.info(
                "xyz %s rot %s samp %s dety %s",
                (ss.sx, ss.sy, ss.sz),
                ss.srot,
                (ss.sampu, ss.sampv),
                ss.detector_y,
            )
            _logger.info(
                "detector: mag %s size %s exp %s",
                ss.detector_magnification,
                ss.detector_size,
                expo_time,
            )
            _logger.info("beam shut %s", ss.is_shutter_open)
            image = ss.compute_image() * expo_time

            data.buffer[...] = rgb2gray(image)
        except Exception:
            _logger.error("Error during process", exc_info=True)


class VisibleTomoSimulator(TangoSimuMod.Simulator):
    def init_device(self):
        TangoSimuMod.Simulator.init_device(self)
        self._SimuCamera.set_device_server(self)


class VisibleTomoSimulatorClass(TangoSimuMod.SimulatorClass):
    device_property_list = {
        "filename": [tango.DevString, "Filename of the data", ""],
        "scene_filename": [
            tango.DevString,
            "Filename::path containing a description of the scene",
            "",
        ],
        "sx": [tango.DevString, "Name of the x-axis motor under the rotation", ""],
        "sy": [tango.DevString, "Name of the y-axis motor under the rotation", ""],
        "sz": [tango.DevString, "Name of the z-axis motor under the rotation", ""],
        "srot": [tango.DevString, "Name of the rotation motor", ""],
        "voxel_size": [tango.DevFloat, "Size of a single voxel in meter", 0.000001],
        "sampu": [
            tango.DevString,
            "Name of the u-vector motor on top of the rotation",
            "",
        ],
        "sampv": [
            tango.DevString,
            "Name of the v-vector motor on top of the rotation",
            "",
        ],
        "detector_y": [
            tango.DevString,
            "Name of the y-axis motor moving the detector",
            "",
        ],
        "detector_magnification": [tango.DevString, "Name of the detector optic", ""],
        "beam_shutter": [tango.DevString, "Name of the beam shutter", ""],
    }
    device_property_list.update(TangoSimuMod.SimulatorClass.device_property_list)


def get_control(**kwargs):
    return TangoSimuMod.get_control(
        **kwargs, _Simulator=VisibleTomoSimulator, _Camera=VisibleTomoCamera
    )


def get_tango_specific_class_n_device():
    return VisibleTomoSimulatorClass, VisibleTomoSimulator
