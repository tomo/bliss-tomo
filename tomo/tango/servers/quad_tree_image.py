import os
import numpy
import typing


class Layer(typing.NamedTuple):
    data: numpy.ndarray
    pixel_size: int
    pixel_index_x: numpy.ndarray
    pixel_index_y: numpy.ndarray
    pixel_center_x: numpy.ndarray
    pixel_center_y: numpy.ndarray
    values: numpy.ndarray

    @property
    def nb_pixels(self):
        return len(self.pixel_index_x)

    @property
    def surface(self):
        return self.pixel_size * self.pixel_size * self.nb_pixels

    def __str__(self):
        return (
            f"<Layer pixel size: {self.pixel_size}, "
            f"nb pixels: {self.nb_pixels}, "
            f"surface: {self.surface}>"
        )


class QuadTreeImage:
    def __init__(self, image, filter_zero=True):
        self._blocks: typing.List[Layer] = []
        self._filter_zero = filter_zero
        self._shape = image.shape
        self._dtype = image.dtype

        current = image
        delta = 0.5
        while current.shape != (1, 1):
            xx, yy, values, reduced = self.reduce(current)
            current = reduced
            if current.size == 0:
                break
            layer = Layer(
                pixel_size=int(delta * 2),
                data=current,
                pixel_index_x=xx,
                pixel_index_y=yy,
                pixel_center_x=xx * (delta * 2) + delta,
                pixel_center_y=yy * (delta * 2) + delta,
                values=values,
            )
            self._blocks.append(layer)
            delta *= 2

    @property
    def dtype(self):
        return self._dtype

    @property
    def shape(self):
        return self._shape

    def reduce(self, image):
        # assert image.shape[0] == image.shape[1]
        desty, destx = image.shape[0] // 2, image.shape[1] // 2

        a0 = image[0::2, 0::2]
        a1 = image[0::2, 1::2]
        a2 = image[1::2, 0::2]
        a3 = image[1::2, 1::2]

        is_same = numpy.logical_and(
            (a0 == a2), numpy.logical_and((a0 == a1), (a2 == a3))
        )
        is_same = numpy.logical_and(is_same, numpy.isfinite(a0))
        reduced = numpy.empty((desty, destx), numpy.float32)
        reduced[...] = numpy.nan
        reduced[is_same] = a0[is_same]

        is_not_same = numpy.logical_not(is_same)

        adef = [(a0, 0, 0), (a1, 0, 1), (a2, 1, 0), (a3, 1, 1)]
        xx = []
        yy = []
        values = []
        for a, dy, dx in adef:
            mask = numpy.logical_and(is_not_same, numpy.isfinite(a))
            if self._filter_zero:
                mask = numpy.logical_and(mask, a != 0)
            y, x = numpy.where(mask)
            values.append(a[y, x])
            xx.append(x * 2 + dx)
            yy.append(y * 2 + dy)

        xx = numpy.concatenate(xx)
        yy = numpy.concatenate(yy)
        values = numpy.concatenate(values)
        return xx, yy, values, reduced

    @property
    def blocks(self):
        return self._blocks

    @property
    def nb_layers(self):
        return len(self._blocks)

    @property
    def nb_pixels(self):
        pixels = [b.nb_pixels for b in self._blocks]
        return sum(pixels)

    def densify(self):
        image = numpy.zeros(self.shape, dtype=self.dtype)
        for block in self._blocks:
            xx = block.pixel_center_x
            yy = block.pixel_center_y
            values = block.values
            s = block.pixel_size
            for x, y, v in zip(xx, yy, values):
                x_start = int(x - s / 2)
                x_stop = int(x + s / 2)
                y_start = int(y - s / 2)
                y_stop = int(y + s / 2)
                image[y_start:y_stop, x_start:x_stop] = v

        return image

    def __str__(self):
        return (
            f"<QuadTreeImage nb layers: {self.nb_layers}, "
            f"nb pixels: {self.nb_pixels}"
            ">"
        )


class SparseVoxel:
    def __init__(self, data):
        self._data = []
        for z in range(data.shape[0]):
            image = data[z, ...]
            bt = QuadTreeImage(image)
            self._data.append(bt)

    @property
    def dtype(self):
        return self._data[0].dtype

    @property
    def shape(self):
        return (len(self),) + self._data[0].shape

    def __len__(self):
        return len(self._data)

    def __getitem__(self, key):
        return self._data[key]


def test_single_pixel():
    image = numpy.zeros((2, 2), dtype=numpy.uint8)
    image[0, 0] = 1
    bt = QuadTreeImage(image)
    assert bt.nb_pixels == 1
    result = bt.densify()
    numpy.testing.assert_array_equal(image, result)


def test_two_pixels():
    image = numpy.zeros((8, 8), dtype=numpy.uint8)
    image[2, 3] = 1
    image[0, 0] = 2
    image[0, 1] = 2
    image[1, 0] = 2
    image[1, 1] = 2
    bt = QuadTreeImage(image)
    assert bt.nb_pixels == 2
    result = bt.densify()
    numpy.testing.assert_array_equal(image, result)


def test_real_case():
    import h5py

    root = os.path.dirname(__file__)
    project_root = os.path.join(root, "../../..")
    filename = f"{project_root}/demo/data/dragon.h5"
    with h5py.File(filename, "r") as f:
        data = f["model"][...]
    for z in range(data.shape[0]):
        image = data[z]
        quad = QuadTreeImage(image)
        result = quad.densify()
        numpy.testing.assert_array_equal(image, result)
