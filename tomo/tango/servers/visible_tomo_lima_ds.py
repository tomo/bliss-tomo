# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import sys
import logging
import gevent
from bliss.common.greenlet_utils.gevent_monkey import unpatch
from Lima.Server import LimaCCDs
from . import helper
from . import visible_tomo_simulator

try:
    import pyvirtualdisplay
except ImportError:
    pyvirtualdisplay = None


_logger = logging.getLogger(__name__)


def process_gevent():
    try:
        gevent.sleep(0.01)
    except Exception:
        _logger.critical("Uncaught exception from gevent", exc_info=True)


def main():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d %(levelname)s %(threadName)s %(module)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    unpatch(subprocess=True)
    helper.register_lima_camera(visible_tomo_simulator)

    if pyvirtualdisplay:
        vdisplay = pyvirtualdisplay.Display()
        vdisplay.start()
    else:
        _logger.warning(
            "pyvirtualdisplay is not installed. Use can use it to hide the Qt application"
        )
        vdisplay = None

    try:
        result = LimaCCDs.main(event_loop=process_gevent)
    finally:
        if pyvirtualdisplay:
            vdisplay.stop()

    sys.exit(result)


if __name__ == "__main__":
    main()
