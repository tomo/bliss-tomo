import os
import tango
import logging

from Lima import Core
from Lima import Simulator as LimaSimuMod
import Lima.Server.camera.Simulator as TangoSimuMod
from bliss.common.shutter import BaseShutterState

from . import nxtomo_generator
from . import remote_bliss


_logger = logging.getLogger(__name__)


class NxTomoCamera(LimaSimuMod.Camera):
    Core.DEB_CLASS(Core.DebModApplication, "NxTomoGeneratorTask")

    def set_device_server(self, deviceServer):
        self._expo_time = 0
        self._srot_name = deviceServer.srot
        self._sample_y_name = deviceServer.sample_y
        self._sample_y_in = float(deviceServer.sample_y_in)
        self._sample_y_out = float(deviceServer.sample_y_out)
        self._beam_shutter_name = deviceServer.beam_shutter

        host, port = remote_bliss.find_address()
        self.remote_bliss = remote_bliss.RemoteBliss(host=host, port=port)

        if self._srot_name:
            self.remote_bliss.subscribe_axis(self._srot_name, dial_position=True)
        if self._sample_y_name:
            self.remote_bliss.subscribe_axis(self._sample_y_name, dial_position=True)
        if self._beam_shutter_name:
            self.remote_bliss.subscribe_shutter(self._beam_shutter_name, state=True)

        self.remote_bliss.start()

        filenames = [deviceServer.filename, deviceServer.alt_filename]
        for filename in filenames:
            if filename != "" and os.path.exists(filename):
                break
        else:
            msg = "', '".join(filenames)
            raise RuntimeError(f"Datafiles '{msg}' not found")
        filename = os.path.abspath(filename)

        self._data = nxtomo_generator.GeneratorFromNxTomo()
        self._data.load_nxtomo_filename(filename, entry_name="entry0000")
        print(f"Loaded '{filename}'")

        # Update the camera size according to the loaded image
        height, width = self._data.detector_shape
        self.setFrameDim(Core.FrameDim(width, height, Core.Bpp32))

    def set_expo_time(self, expo_time):
        self._expo_time = expo_time

    def _is_sample_in(self, pos):
        mid = (self._sample_y_in + self._sample_y_out) * 0.5
        if self._sample_y_in < self._sample_y_out:
            return pos < mid
        else:
            return pos > mid

    def fillData(self, data):
        """
        Process the data

        Called for every frame in a different C++ thread.
        """
        # Detector configuration
        exposure = self.getExpTime()

        # Sync the sample stage with BLISS state
        bliss = self.remote_bliss
        srot = bliss.get_axis_dial_position(self._srot_name, 0)
        sample_y = bliss.get_axis_dial_position(self._sample_y_name, 0)
        beam_shutter = bliss.get_shutter_state(
            self._beam_shutter_name, BaseShutterState.OPEN
        )

        is_beam_shutter_open = beam_shutter == BaseShutterState.OPEN
        is_sample_in = self._is_sample_in(sample_y)
        print(srot, sample_y, is_sample_in, beam_shutter, exposure)

        if is_beam_shutter_open:
            if is_sample_in:
                image = self._data.get_projection(angle=srot, exposure=exposure)
            else:
                image = self._data.get_flat(exposure)
        else:
            image = self._data.get_dark(exposure)

        # FIXME: It would be good to request lima to use the image size
        # Clamp the image to the result data
        width = min(data.buffer.shape[0], image.shape[0])
        height = min(data.buffer.shape[1], image.shape[1])
        data.buffer[...] = 0
        data.buffer[0:width, 0:height] = image[0:width, 0:height]


class NxTomoSimulator(TangoSimuMod.Simulator):
    def init_device(self):
        TangoSimuMod.Simulator.init_device(self)
        self._SimuCamera.set_device_server(self)


class NxTomoSimulatorClass(TangoSimuMod.SimulatorClass):
    device_property_list = {
        "srot": [tango.DevString, "BLISS name of the rotation motor", ""],
        "sample_y": [tango.DevString, "BLISS name of the motor moving the sample", ""],
        "sample_y_in": [tango.DevFloat, "Position when the sample is in the beam", 0.0],
        "sample_y_out": [
            tango.DevFloat,
            "Position when the sample is out of the beam",
            1.0,
        ],
        "beam_shutter": [
            tango.DevString,
            "BLISS name of the shutter object blocking the beam",
            "",
        ],
        "filename": [tango.DevString, "NXtomo filename to use as resource", ""],
        "alt_filename": [
            tango.DevString,
            "Alternative NXtomo filename if the default one is missing",
            "",
        ],
    }
    device_property_list.update(TangoSimuMod.SimulatorClass.device_property_list)


def get_control(**kwargs):
    return TangoSimuMod.get_control(
        **kwargs, _Simulator=NxTomoSimulator, _Camera=NxTomoCamera
    )


def get_tango_specific_class_n_device():
    return NxTomoSimulatorClass, NxTomoSimulator
