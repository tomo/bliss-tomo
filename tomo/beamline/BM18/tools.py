import os
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.static import get_config
from bliss.shell.standard import mv as bliss_mv
from bliss.shell.standard import umv as bliss_umv
from bliss.shell.standard import mvr as bliss_mvr
from bliss.shell.standard import umvr as bliss_umvr
from bliss.common.scans.ct import ct as bliss_ct
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.static import get_config
from bliss import setup_globals
from bliss.common.logtools import *
from tomo.standard import update_is_on
from bliss.config.settings import SimpleSetting


_TOMO = None


def init_tomo(tomo):
    global _TOMO
    _TOMO = tomo


def _select_tomo():
    dlg_tomo = UserInput(label="Enter name of tomo object you want to use (ex: hrtomo)")
    ret = BlissDialog([[dlg_tomo]], title="Tomo Setup").show()
    if ret != False:
        tomo = get_config().get(ret[dlg_tomo])
        init_tomo(tomo)


def mv(*args):
    bliss_mv(*args)
    if update_is_on():
        ct()


def mvr(*args):
    bliss_mvr(*args)
    if update_is_on():
        ct()


def umv(*args):
    bliss_umv(*args)
    if update_is_on():
        ct()


def umvr(*args):
    bliss_umvr(*args)
    if update_is_on():
        ct()


def shopen():
    setup_globals.bsh1.open()


def shclose():
    setup_globals.bsh1.close()


def ct(exposure_time=None, *args):
    if update_is_on():
        global _TOMO
        if _TOMO is not None:
            if exposure_time is not None:
                return bliss_ct(exposure_time, *args)
            else:
                if _TOMO.pars.exposure_time != 0:
                    return bliss_ct(_TOMO.pars.exposure_time, *args)
                else:
                    print(f"WARNING {_TOMO.pars.exposure_time} is 0")
        else:
            print(
                "No tomo exists in the session. Please use init_tomo(full_tomo) to define one"
            )
    else:
        if exposure_time is None:
            exposure_time = 1
        return bliss_ct(exposure_time, *args)
