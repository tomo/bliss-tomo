from bliss import setup_globals
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.static import get_config


class OpiomSetup:
    """
    Class to switch opiom outputs manually on ID19
    """

    def __init__(self, name, config):
        self.mux = config.get("multiplexer")

    def __call__(self):
        values = [(0, "MUSST_TRIG"), (1, "MUSST_GATE")]
        defval = self.mux.getPossibleValues("SOURCE").index(
            self.mux.getOutputStat("SOURCE")
        )
        dlg_source = UserChoice(label="SOURCE OPIOM", values=values, defval=defval)
        if self.mux.name == "multiplexer2":
            values = [
                (0, "DET9_START"),
                (1, "DET10_START"),
                (2, "DET11_START"),
                (3, "DET12_START"),
                (4, "DET13_START"),
                (5, "DET14_START"),
            ]
        else:
            values = [
                (0, "DET1_START"),
                (1, "DET2_START"),
                (2, "DET3_START"),
                (3, "DET4_START"),
                (4, "DET5_START"),
                (5, "DET6_START"),
                (6, "DET7_START"),
                (7, "DET8_START"),
            ]
        defval = self.mux.getPossibleValues("CAMERA").index(
            self.mux.getOutputStat("CAMERA")
        )
        dlg_camera = UserChoice(label="CAMERA", values=values, defval=defval)

        values = [
            (0, "ORIGINAL_SIGNAL"),
            (1, "1_MICROSECONDS"),
            (2, "16_MICROSECONDS"),
            (3, "256_MICROSECONDS"),
        ]
        if self.mux.name == "multiplexer2":
           values = [
            (0, "ORIGINAL_SIGNAL"),
            (1, "16_MICROSECONDS"),
            (2, "256_MICROSECONDS"),
            (3, "512_MICROSECONDS"),
            ] 
        defval = self.mux.getPossibleValues("LENGTH_MUSSTA").index(
            self.mux.getOutputStat("LENGTH_MUSSTA")
        )
        dlg_length = UserChoice(label="LENGTH MUSSTA", values=values, defval=defval)

        if self.mux.name == "multiplexer2":
            
            values = [
            (0, "DISABLE"),
            (1, "ENABLE"),
            ]
            
            defval = self.mux.getPossibleValues("LAB_TRIGGER").index(
            self.mux.getOutputStat("LAB_TRIGGER")
            )
            
            dlg_trigger = UserChoice(label="LAB TRIGGER", values=values, defval=defval)

        if self.mux.name == "multiplexer2":
            ret = BlissDialog(
                [[dlg_source], [dlg_camera], [dlg_length], [dlg_trigger]], title="Opiom Setup"
            ).show()
        else:
            ret = BlissDialog(
                [[dlg_source], [dlg_camera], [dlg_length]], title="Opiom Setup"
            ).show()

        # returns False on cancel
        if ret != False:
            self.mux.switch(
                "SOURCE", self.mux.getPossibleValues("SOURCE")[ret[dlg_source]]
            )
            self.mux.switch(
                "CAMERA", self.mux.getPossibleValues("CAMERA")[ret[dlg_camera]]
            )
            self.mux.switch(
                "LENGTH_MUSSTA",
                self.mux.getPossibleValues("LENGTH_MUSSTA")[ret[dlg_length]],
            )
            if self.mux.name == "multiplexer2":
                self.mux.switch(
                "LAB_TRIGGER",
                self.mux.getPossibleValues("LAB_TRIGGER")[ret[dlg_trigger]],
                )
