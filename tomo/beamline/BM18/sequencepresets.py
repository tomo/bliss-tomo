from tomo.sequence.presets.base import SequencePreset
from tomo.standard import *
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from tomo.sequencebasic import ScanType
from bliss import setup_globals, current_session


class UpdateOnOffPreset(SequencePreset):
    def __init__(self, name, config):
        self.update_was_on = None
        super().__init__(name, config)

    def start(self):
        self.update_was_on = False
        if update_is_on():
            self.update_was_on = True
            update_off()

    def stop(self):
        if self.update_was_on:
            update_on()


class ImageCorrOnOffPreset(SequencePreset):
    def __init__(self, name, config):
        super().__init__(name, config)
        self.image_corr = config.get("image_corr")

    def prepare(self):
        self.image_corr_was_on = {}
        self.detectors = []

        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            self.detectors.append(node.controller)
            self.image_corr_was_on[node.controller] = False
            if (
                node.controller.processing.use_background
                or node.controller.processing.use_flatfield
            ):
                self.image_corr_was_on[node.controller] = True
                self.image_corr.flat_off()
                self.image_corr.dark_off()

    def stop(self):
        for detector in self.detectors:
            if self.image_corr_was_on[detector]:
                self.image_corr.flat_on()
                self.image_corr.dark_on()


class HDF5SavingCheckPreset(SequencePreset):
    def __init__(self, name, config):
        super().__init__(name, config)

    def prepare(self):
        self.saving_format_was_edf = {}
        self.detectors = []

        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            self.detectors.append(node.controller)
            self.saving_format_was_edf[node.controller] = False
            if node.controller.saving.file_format == "EDF":
                self.saving_format_was_edf[node.controller] = True
                node.controller.saving.file_format = "HDF5"

    def stop(self):
        for detector in self.detectors:
            if self.saving_format_was_edf[detector]:
                detector.saving.file_format = "EDF"


class LatencyCheckPreset(SequencePreset):
    def __init__(self, name, config):
        super().__init__(name, config)
        self.tomo = config.get("tomo")

    def prepare(self):
        if (
            self.tomo.pars.scan_type == ScanType.CONTINUOUS
            and self.tomo._detector.acquisition.mode == "SINGLE"
        ):
            if self.tomo.pars.latency_time < 0.1 * self.tomo.pars.exposure_time:
                self.tomo.pars.latency_time = 0.1 * self.tomo.pars.exposure_time
        if (
            self.tomo.pars.scan_type == ScanType.CONTINUOUS
            and self.tomo._detector.acquisition.mode == "ACCUMULATION"
        ):
            if (
                self.tomo.pars.latency_time
                < 0.1 * self.tomo._detector.accumulation.max_expo_time
            ):
                self.tomo.pars.latency_time = (
                    0.1 * self.tomo._detector.accumulation.max_expo_time
                )


class FastShutterSequencePreset(SequencePreset):
    """
    Class used to open fast shutter before taking reference, static or projection images
    and close it after acquisition.

    ***Attributes***
    shutter : Tomo shutter object
        contains methods to control shutters
    """

    def __init__(self, tomo, shutter):
        # self.tomo = config.get("tomo")
        self.tomo = tomo
        # self.shutter = config.get("shutter")
        self.shutter = shutter

    def prepare(self):
        """
        Opens shutter if shutter controlled by detector
        """
        self.soft_shutter = False

        det = self.tomo._detector

        if det is not None and det.camera_type == "Frelon":
            if det.camera.image_mode == "FULL FRAME":
                det.shutter.mode = "AUTO_FRAME"
                det.shutter.close_time = self.shutter.closing_time
            else:
                det.shutter.mode = "MANUAL"
                det.shutter.close_time = 0.0
        elif self.shutter is not None:
            self.soft_shutter = True

    def start(self):
        """
        Opens shutter if shutter controlled by soft
        """
        if self.soft_shutter:
            self.shutter.open()

    def stop(self):
        """
        Closes shutter if shutter controlled by soft
        """
        if self.soft_shutter:
            self.shutter.close()


class SzStatusCheckPreset(SequencePreset):
    def __init__(self, name, config):
        self.axis = config.get("axis")
        super().__init__(name, config)

    def start(self):
        if not self.axis.state.READY:
            self.axis.rmove(-0.001)
            self.axis.rmove(0.001)


class AccMarginPreset(SequencePreset):
    def __init__(self, fscan_config, sequence):
        self._fscan = fscan_config.get_runner("fscan")
        self._sequence = sequence
    def prepare(self):
        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            det = node.controller
        self._fscan.pars.motor = self._sequence.tomo_config.rotation_axis
        self._fscan.pars.start_pos = self._sequence.pars.start_pos
        self._fscan.pars.acq_time = self._sequence.pars.exposure_time
        self._fscan.pars.npoints = self._sequence.pars.tomo_n
        self._fscan.pars.step_size = self._sequence.pars.range / self._sequence.pars.tomo_n
        self._fscan.pars.step_time = 0
        self._fscan.pars.latency_time = self._sequence.pars.latency_time 
        if det.camera_type.lower() == "pco":
            self._fscan.validate()
            if self._fscan.inpars.acc_time >= 3:
                self._fscan.pars.acc_margin = round(
                    (
                        (
                            2.9
                            - self._fscan.inpars.speed
                            / self._fscan.inpars.motor.acceleration
                        )
                        * self._fscan.inpars.speed
                    ),
                    3,
                )
        if det.camera_type.lower() == "iris_15":
            self._fscan.pars.acc_margin = self._fscan.pars.step_size
            self._fscan.validate()
            det.camera.acq_timeout = self._fscan.inpars.acc_time+5


class AccMarginHelicalPreset(SequencePreset):
    def __init__(self, fscan_config, sequence):
        self._fscan = fscan_config.get_runner("fscan")
        self._f2scan = fscan_config.get_runner("f2scan")
        self._sequence = sequence

    def prepare(self):
        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            det = node.controller
        self._f2scan.pars.motor = self._sequence.tomo_config.rotation_axis
        self._f2scan.pars.slave_motor = self._sequence.tomo_config.z_axis
        self._f2scan.pars.start_pos = self._sequence.pars.start_pos
        self._f2scan.pars.acq_time = self._sequence.pars.exposure_time
        self._f2scan.pars.npoints = self._sequence.pars.tomo_n
        self._f2scan.pars.step_size = self._sequence.pars.range / self._sequence.pars.tomo_n
        self._f2scan.pars.step_time = 0
        self._f2scan.pars.slave_start_pos = self._sequence.pars.slave_start_pos
        self._f2scan.pars.slave_step_size = self._sequence.pars.slave_step_size / self._sequence.pars.tomo_n
        self._fscan.pars.acc_margin = self._f2scan.pars.step_size 
        self._f2scan.pars.acc_margin = self._f2scan.pars.step_size 
        self._f2scan.pars.latency_time = self._sequence.pars.latency_time 
        self._f2scan.validate()
        acc_margin = self._f2scan.master.mot_master.master["undershoot_start_margin"]
        acc_time = self._f2scan.inpars.speed / self._f2scan.pars.motor.acceleration + acc_margin / self._f2scan.inpars.speed
        if det.camera_type.lower() == "pco":
            if acc_time >= 3:
                self._fscan.pars.acc_margin = 0
                self._f2scan.pars.acc_margin = 0
                self._f2scan.pars.slave_acc_margin = 0
        if det.camera_type.lower() == "iris_15":
            det.camera.acq_timeout = acc_time+5

class MetadataPreset(SequencePreset):
    def __init__(self):
        self.scan_saving = current_session.scan_saving

    def prepare(self):
        current_session.icat_metadata.resetup()
        setup_globals.att_metadata()

    def stop(self):
        if (
            "_" in self.scan_saving.dataset_name
            and self.scan_saving.dataset_name.split("_")[-1].isnumeric()
        ):
            dataset_name = "_".join(self.scan_saving.dataset_name.split("_")[:-1])
        else:
            dataset_name = self.scan_saving.dataset_name

        setup_globals.enddataset()

        setup_globals.newdataset(dataset_name)


class DirectoriesMappingPreset(SequencePreset):
    def __init__(self):
        self.scan_saving = current_session.scan_saving

    def prepare(self):
        saving_space = self.scan_saving.base_path.split("/")[-1]
        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            det = node.controller
            if (
                det.camera_type.lower() == "iris_15" or det.name == "edgewin1"
            ) and saving_space in det.directories_mapping_names:
                det.select_directories_mapping(saving_space)


class MusstMaxAccuracySequencePreset(SequencePreset):
    def __init__(self, musst):
        self.musst = musst

    def prepare(self):
        self.musst.TMRCFG = "50MHZ"


class HalfAcquiPreset(SequencePreset):
    def __init__(self, sequence):
        self.sequence = sequence

    def prepare(self):
        if self.sequence._y_axis.position != 0 and self.sequence.pars.range >= 360:
            self.sequence.pars.half_acquisition = True
        else:
            self.sequence.pars.half_acquisition = False


class FrontEndPreset(SequencePreset):
    def __init__(self, machinfo, frontend):
        self.frontend = frontend
        self.machinfo = machinfo

    def prepare(self):
        beam_status = self.machinfo.proxy.mode
        delivery_mode = (
            "delivery" in self.machinfo.all_information.get("SR_Operator_Mesg").lower()
        )
        if not self.frontend.is_open and beam_status == 2 and delivery_mode and self.machinfo.counters.current.value > 5:
            self.frontend.open()


class DeactivateRoiCountersPreset(SequencePreset):
    def prepare(self):
        builder = ChainBuilder([])
        self.ldet = []
        self.lroicounters = []
        for node in builder.get_nodes_by_controller_type(Lima):
            det = node.controller
            self.ldet.append(det)
            for i, roi in enumerate(list(det.roi_counters.get_rois())):
                name = list(det.roi_counters.keys())[i]
                self.lroicounters.append((name, roi))
            det.roi_counters.clear()

    def stop(self):
        for i, det in enumerate(self.ldet):
            for roi_counters in self.lroicounters:
                det.roi_counters[roi_counters[0]] = roi_counters[1]
