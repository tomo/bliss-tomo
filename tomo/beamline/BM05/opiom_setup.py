from bliss import setup_globals
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.static import get_config


class OpiomSetup:
    """
    Class to switch opiom outputs manually on ID19
    """

    def __init__(self, name, config):
        self.mux = get_config().get("multiplexer")
        self.mux2 = get_config().get("multiplexer2")

    def __call__(self):
        values = [(0, "MUSST1A"), (1, "MUSST1B"), (2, "MUSST2A"), (3, "MUSST2B")]
        defval = self.mux.getPossibleValues("SOURCE").index(
            self.mux.getOutputStat("SOURCE")
        )
        dlg_source = UserChoice(label="SOURCE OPIOM", values=values, defval=defval)
        values = [
            (0, "EH1_VISIBLE_START"),
            (1, "EH1_SINGLEMIC_START"),
            (2, "EH1_SPARE1_START"),
            (3, "EH1_SPARE2_START"),
            (4, "EH2_10X_START"),
            (5, "EH2_DZOOM_START"),
            (6, "EH2_ZOOM_START"),
            (7, "EH2_TWINMIC_START"),
        ]
        defval = self.mux.getPossibleValues("CAMERA").index(
            self.mux.getOutputStat("CAMERA")
        )
        dlg_camera = UserChoice(label="CAMERA", values=values, defval=defval)

        ret = BlissDialog([[dlg_source], [dlg_camera]], title="Opiom Setup").show()

        # returns False on cancel
        if ret != False:
            self.mux.switch(
                "SOURCE", self.mux.getPossibleValues("SOURCE")[ret[dlg_source]]
            )
            if ret[dlg_source] == 2:
                self.mux2.switch("SOURCE", "MUSST")
            self.mux.switch(
                "CAMERA", self.mux.getPossibleValues("CAMERA")[ret[dlg_camera]]
            )
