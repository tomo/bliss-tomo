from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset, ChainIterationPreset


class OpiomPreset(ScanPreset):
    """
    Class used to handle opiom during tomo acquisition

    ***Attributes***
    mux : Bliss controller object
        multiplexer based on opiom card
    tomo_station : string
        name of tomo station used to retrieve related opiom mode
    mode : string
        name of opiom mode associated to tomo station and a given
    """

    def __init__(self, *multiplexer, detectors_optic, tomo_station, sequence):
        self.mux = multiplexer[0]
        self.mux2 = multiplexer[1]
        self.detectors_optic = detectors_optic
        self.tomo_station = tomo_station
        self.sequence = sequence

    def prepare(self, scan):
        self.prev_val_mux = self.mux.getGlobalStat()
        self.prev_val_mux2 = self.mux2.getGlobalStat()
        scan_type = self.sequence.pars.scan_type
        camera = [
            node.device
            for node in scan.acq_chain.nodes_list
            if type(node) == LimaAcquisitionMaster
        ]
        if len(camera) == 0:
            raise RuntimeError(
                "no detector is enabled in measurement group, please activate one with tomoccdselect()"
            )
        if len(camera) > 1:
            raise RuntimeError(
                "several detectors are enabled in measurement group, please make sure only one is active"
            )
        camera = camera[0]
        optic = self.detectors_optic.get_optic(camera)

        if (
            scan_type in ("SWEEP", "INTERLACED")
            or camera.camera_type.lower() == "basler"
        ):
            if self.tomo_station == "mr":
                self.mux.switch("SOURCE", "MUSST2B")
            elif self.tomo_station == "hr":
                self.mux.switch("SOURCE", "MUSST1B")
        else:
            if self.tomo_station == "mr":
                self.mux.switch("SOURCE", "OPIOM2_1")
                self.mux2.switch("SOURCE", "MUSST")
            elif self.tomo_station == "hr":
                self.mux.switch("SOURCE", "MUSST1A")

        if self.tomo_station == "mr":
            #if optic.name.lower() not in camera.name.lower():
            #    self.mux.switch("CAMERA", "EH1_SPARE1_START")
            if "visible" in optic.name.lower():
                self.mux.switch("CAMERA", "EH2_VISIBLE_START")
            elif "dzoom" in str(type(optic)).lower():
                self.mux.switch("CAMERA", "EH2_DZOOM_START")
            elif "zoom" in str(type(optic)).lower():
                self.mux.switch("CAMERA", "EH2_ZOOM_START")
            elif "twinmic" in str(type(optic)).lower():
                self.mux.switch("CAMERA", "EH2_TWINMIC_START")
            elif "10x" in optic.name.lower():
                self.mux.switch("CAMERA", "EH2_10X_START")
        elif self.tomo_station == "hr":
            if "visible" in optic.name.lower():
                self.mux.switch("CAMERA", "EH1_VISIBLE_START")
            elif "twinmic" in str(type(optic)).lower():
                self.mux.switch("CAMERA", "EH1_SINGLEMIC_START")

    def stop(self, scan):
        """
        Switches back multiplexer ouputs to previous configuration.
        """

        self.mux.switch("SOURCE", self.prev_val_mux["SOURCE"])
        self.mux2.switch("SOURCE", self.prev_val_mux2["SOURCE"])
        self.mux.switch("CAMERA", self.prev_val_mux["CAMERA"])


class DefaultChainOpiomPreset(ChainPreset):
    """
    Class used for all bliss common scans (ct, ascan, timescan,...) to switch opiom output according to detector

    ***Attributes***
    mux : Bliss controller object
        multiplexer based on opiom card
    detectors_optic : tomo controller object
        links each tomo detector to its optic
    tomo_station : string
        name of tomo station used to retrieve related musst card
    """

    def __init__(self, *multiplexer, detectors_optic, tomo_station):
        self.mux = multiplexer[0]
        self.mux2 = multiplexer[1]
        self.detectors_optic = detectors_optic
        self.tomo_station = tomo_station

    def prepare(self, acq_chain):
        self.prev_val_mux = self.mux.getGlobalStat()
        self.prev_val_mux2 = self.mux2.getGlobalStat()
        camera = [
            node.device
            for node in acq_chain.nodes_list
            if type(node) == LimaAcquisitionMaster
        ]
        if len(camera) == 0:
            raise RuntimeError(
                "no detector is enabled in measurement group, please activate one with tomoccdselect()"
            )
        if len(camera) > 1:
            raise RuntimeError(
                "several detectors are enabled in measurement group, please make sure only one is active"
            )

        camera = camera[0]
        optic = self.detectors_optic.get_optic(camera)
        if optic is None:
            return

        if camera.camera_type.lower() == "basler":
            if self.tomo_station == "mr":
                self.mux.switch("SOURCE", "MUSST2B")
            elif self.tomo_station == "hr":
                self.mux.switch("SOURCE", "MUSST1B")
        else:
            if camera.acquisition.mode == "ACCUMULATION" or (
                camera.camera_type.lower() == "pco"
                and "dimax" in getattr(camera.camera, "cam_type").lower()
            ):
                if self.tomo_station == "mr":
                    self.mux.switch("SOURCE", "OPIOM2_1")
                    self.mux2.switch("SOURCE", "MUSST")
                elif self.tomo_station == "hr":
                    self.mux.switch("SOURCE", "MUSST1A")
            else:
                if self.tomo_station == "mr":
                    self.mux.switch("SOURCE", "MUSST2B")
                elif self.tomo_station == "hr":
                    self.mux.switch("SOURCE", "MUSST1B")

        if self.tomo_station == "mr":
            #if optic.name.lower() not in camera.name.lower():
            #    self.mux.switch("CAMERA", "EH1_SPARE1_START")
            if "visible" in optic.name.lower():
                self.mux.switch("CAMERA", "EH2_VISIBLE_START")
            elif "dzoom" in str(type(optic)).lower():
                self.mux.switch("CAMERA", "EH2_DZOOM_START")
            elif "zoom" in str(type(optic)).lower():
                self.mux.switch("CAMERA", "EH2_ZOOM_START")
            elif "twinmic" in str(type(optic)).lower():
                self.mux.switch("CAMERA", "EH2_TWINMIC_START")
            elif "10x" in optic.name.lower():
                self.mux.switch("CAMERA", "EH2_10X_START")
        elif self.tomo_station == "hr":
            if "visible" in optic.name.lower():
                self.mux.switch("CAMERA", "EH1_VISIBLE_START")
            elif "twinmic" in str(type(optic)).lower():
                self.mux.switch("CAMERA", "EH1_SINGLEMIC_START")

    def stop(self, acq_chain):
        """
        Switches back multiplexer ouputs to previous configuration.
        """

        self.mux.switch("SOURCE", self.prev_val_mux["SOURCE"])
        self.mux2.switch("SOURCE", self.prev_val_mux2["SOURCE"])
        self.mux.switch("CAMERA", self.prev_val_mux["CAMERA"])


class ReferenceMotorMoveOutPreset(ScanPreset):
    """
    Class used to move sample out of the beam before taking images
    ***Attributes***
    reference : reference object
        contains methods to control reference motors
    """

    def __init__(self, reference):
        self.reference = reference

    def prepare(self, scan):
        """
        Moves sample out of the beam
        """
        print(f"Move sample out of beam for {scan.name}")
        self.reference.move_out()
        print("moved out")

    def start(self, scan):
        pass

    def stop(self, scan):
        pass


class ReferenceMotorMoveInPreset(ScanPreset):
    """
    Class used to move sample in beam after taking images

    ***Attributes***
    reference : reference object
        contains methods to control reference motors
    """

    def __init__(self, reference):
        self.reference = reference

    def prepare(self, scan):
        pass

    def start(self, scan):
        pass

    def stop(self, scan):
        """
        Moves sample back in beam
        """
        print(f"Move sample back in beam")
        self.reference.move_in()
        print("moved in")


class ShutterPreset(ChainPreset):
    def __init__(self, shutter):
        self.shutter = shutter
        super().__init__()

    class Iterator(ChainIterationPreset):
        def __init__(self, shutter, iteration_nb):
            self.shutter = shutter
            self.iteration_nb = iteration_nb

        def prepare(self):
            if self.iteration_nb > 0:
                self.shutter.close()

        def start(self):
            self.shutter.open()

        def stop(self):
            self.shutter.close()

    def get_iterator(self, acq_chain):
        iteration_nb = 0
        while True:
            yield ShutterPreset.Iterator(self.shutter, iteration_nb)
            iteration_nb += 1


class CloseShutterDuringImageTransferPreset(ScanPreset):
    """
    Class used to close shutter before save operation is complete in case
    save operation takes some time (windows detector)

    **Attributes**

    shutter: Bliss object used to control shutter
    """

    def __init__(self, shutter, tomo_config):
        super().__init__()
        self.shutter = shutter
        self.i = 1
        self.tomo_config = tomo_config
        self.detectors_optic = tomo_config.detectors

    def prepare(self, scan):
        """
        Open shutter
        Get detector device from tomoconfig
        Connect detector acquisition data with protect_my_detector() function
        """
        self.i = 1
        camera = [
            node.device
            for node in scan.acq_chain.nodes_list
            if isinstance(node, LimaAcquisitionMaster)
        ]
        if len(camera) == 0:
            raise RuntimeError(
                "no detector is enabled in measurement group, please activate one with tomoccdselect()"
            )
        if len(camera) > 1:
            raise RuntimeError(
                "several detectors are enabled in measurement group, please make sure only one is active"
            )
        self.detector = camera[0]
        optic = self.detectors_optic.get_optic(self.detector)
        self.connect_data_channels([self.detector, ...], self.protect_my_detector)
        self.acq_nb_frames = self.tomo_config.pars.tomo_n

    def protect_my_detector(self, counter, channel_name, data):
        """
        Close shutter when detector acquired last projection
        """
        if (
            counter == self.detector
            #and "dimax" in self.detector.camera.cam_name.lower()
            and channel_name == f"{self.detector.name}:image"
        ):
            if (
                data.get("last_index") is not None
                and data.get("last_index") == self.acq_nb_frames * self.i - 1
                and self.shutter.is_open
            ):
                self.i += 1
                self.shutter.close()


class LaserPreset(ChainPreset):
    def __init__(self, focus_mot, laser, psho):
        self.focus_mot = focus_mot
        self.laser = laser
        self.psho = psho

    def prepare(self, chain):

        nodes = list(chain.get_node_from_devices(self.focus_mot))
        if len(nodes) > 0 and nodes[0] is not None:
            if self.psho.position > 10.0:
                print("PS horizontal offset is %0.2f so I assume we\'re working in the side beam." % self.psho.position)
                self.laser.paper_side()
            else:
                print("PS horizontal offset is %0.2f so I assume we\'re working in the central beam." % self.psho.position)
                self.laser.paper_central()


    def stop(self, chain):
        nodes = list(chain.get_node_from_devices(self.focus_mot))
        if len(nodes) > 0 and nodes[0] is not None:
            self.laser.OUT()
