from tomo.sequencepreset import SequencePreset
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.config.settings import SimpleSetting
from tomo.sequencebasic import ScanType
from tomo.standard import update_off, update_on, update_is_on
from bliss import setup_globals
from bliss.shell.standard import umv


class UpdateOnOffPreset(SequencePreset):
    def __init__(self, name, config):
        self.update_was_on = None
        super().__init__(name, config)

    def start(self):
        self.update_was_on = False
        if update_is_on():
            self.update_was_on = True
            update_off()

    def stop(self):
        if self.update_was_on:
            update_on()


class ImageCorrOnOffPreset(SequencePreset):
    def __init__(self, name, config):
        super().__init__(name, config)
        self.image_corr = config.get("image_corr")

    def prepare(self):
        self.image_corr_was_on = {}
        self.detectors = []

        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            self.detectors.append(node.controller)
            self.image_corr_was_on[node.controller] = False
            if (
                node.controller.processing.use_background
                or node.controller.processing.use_flatfield
            ):
                self.image_corr_was_on[node.controller] = True
                self.image_corr.flat_off()
                self.image_corr.dark_off()

    def stop(self):
        for detector in self.detectors:
            if self.image_corr_was_on[detector]:
                self.image_corr.dark_on()
                self.image_corr.flat_on()


class FastShutterSequencePreset(SequencePreset):
    """
    Class used to open fast shutter before taking reference, static or projection images
    and close it after acquisition.

    ***Attributes***
    shutter : Tomo shutter object
        contains methods to control shutters
    """

    def __init__(self, tomo, shutter):
        self.tomo = tomo
        self.shutter = shutter

    def prepare(self):
        """
        Opens shutter if shutter controlled by detector
        """
        self.soft_shutter = False
        det = self.tomo.get_controller_lima()

        if det is not None and det.camera_type == "Frelon":
            if det.camera.image_mode == "FULL FRAME":
                det.shutter.mode = "AUTO_FRAME"
                det.shutter.close_time = self.shutter.closing_time
            else:
                self.soft_shutter = True
                det.shutter.mode = "MANUAL"
                det.shutter.close_time = 0.0
        elif self.shutter is not None:
            self.soft_shutter = True

    def start(self):
        """
        Opens shutter if shutter controlled by soft
        """
        if self.soft_shutter:
            self.shutter.open()

    def stop(self):
        """
        Closes shutter if shutter controlled by soft
        """
        if self.soft_shutter:
            self.shutter.close()


class HalfAcquiPreset(SequencePreset):
    def __init__(self, name, config):
        self._full_tomo = config.get("full_tomo")
        super().__init__(name, config)
        self._half_acqui_was_on = SimpleSetting(
            "half_acqui_was_on", default_value=False
        )

    def start(self):
        if self._full_tomo.pars.half_acquisition:
            umv(self._full_tomo._y_axis, self._full_tomo.pars.acquisition_position)
            self._half_acqui_was_on.set(True)
        if not self._full_tomo.pars.half_acquisition and self._half_acqui_was_on.get():
            umv(self._full_tomo._y_axis, self._full_tomo.pars.full_frame_position)
            self._half_acqui_was_on.set(False)


class MusstMaxAccuracySequencePreset(SequencePreset):
    """
    Class used to open fast shutter before taking reference, static or projection images
    and close it after acquisition.

    ***Attributes***
    shutter : Tomo shutter object
        contains methods to control shutters
    """

    def __init__(self, musst):
        self.musst = musst

    def prepare(self):
        """
        Opens shutter if shutter controlled by detector
        """
        self.musst.TMRCFG = "50MHZ"


class RemoveBackwardDispPreset(SequencePreset):
    def __init__(self, sequence):
        self.fscanloop = sequence.get_runner("MULTITURNS")._fscanloop
        self.pars = sequence.pars
        self.motor = sequence._rotation_axis

    def prepare(self):
        fscanloop = self.fscanloop
        fscanloop.pars.motor = self.motor
        fscanloop.pars.step_size = self.pars.range / self.pars.tomo_n
        fscanloop.pars.acq_time = self.pars.exposure_time
        fscanloop.pars.step_time = fscanloop.pars.acq_time
        fscanloop.pars.acc_margin = 0
        fscanloop.validate()
        fscanloop.pars.acc_margin = (
            self.pars.start_pos
            + self.pars.start_turns * 360
            - fscanloop.inpars.acc_disp
        )
