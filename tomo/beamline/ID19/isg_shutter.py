# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import Timeout, sleep
import time
import PyTango


# BaseShutter
from bliss.common.shutter import Shutter, BaseShutter, BaseShutterState

# Needed for logging
from bliss.common import session
from bliss import global_map
from bliss.common.logtools import log_info, log_debug

# Needed for creating a serialline
from bliss.comm.util import get_comm

"""
Handle the experiment fast shutter via the Serial Line

example yml file:

#fast shutter configuration
- name isg_shutter
  plugin: bliss
  class: IsgShutter
  package: id19.tomo.beamline.ID19
  # delay time after a close in ms (when issuing close())
  closing_time: 7.5 
  # delay time after an open in ms (when issuing open())
  opening_time: 0.2
  # information on the serial line
  serial: 
    url: tango://id19/serialRP_192/26   
"""


class IsgShutter(BaseShutter):
    """
    I need to inherit from Shutter since this is a shutter
    """

    def __init__(self, name, config):
        self.__name = name
        self.__config = config

        global_map.register(self, tag=self.name)
        log_info(self, "_init() entering")

        self.__closing_time = self.config["closing_time"] / 1000.0
        self.__opening_time = self.config["opening_time"] / 1000.0

        # Open serial connection to the shutter box
        self._serial = get_comm(self.config)

        # Always set to external mode
        self.fsexton()

        # Always set ISGMODE OFF
        self.ISGMODE = "OFF"
        self.__isgmode = "OFF"

        log_info(self, "__init__() leaving")

    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    def __info__(self):
        info_str = f"{self.name} info:\n"
        info_str += self._query("?VER")
        info_str += self._query("?INFO")

        return info_str

    @property
    def state(self):
        """From BaseShutterState - To generate Verbose message of the shutter state"""
        """ It is either BaseShutterState.OPEN, BaseShutterState.CLOSED or BaseShutterState.UNKNOWN """

        log_info(self, "_state entering")
        reply = self._query("?STATE")
        print(reply)

        fsstate = BaseShutterState.UNKNOWN
        if reply == "ADOWN_BDOWN":
            fsstate = BaseShutterState.CLOSED
        else:
            if reply == "ALOW_BDOWN":
                fsstate = BaseShutterState.OPEN
            else:
                fsstate = BaseShutterState.UNKNOWN

        log_info(self, "_state leaving")
        return fsstate

    @property
    def state_string(self):
        s = self.state
        return s.value

    @property
    def mode(self):
        _mode = self.fsgetext()
        if _mode == 1:
            return Shutter.EXTERNAL
        else:
            return Shutter.MANUAL

    def fsreset(self):
        """Reset the shutter box"""
        log_info(self, "fsreset entering")
        self.RESET
        log_info(self, "fsreset leaving")

    def fsexton(self):
        """Enables the synchronization of the shutter by ext TTL signals (from OPIOM)"""
        log_info(self, "fsexton entering")
        self.EXT = "ON"
        log_info(self, "fsexton leaving")

    def fsextoff(self):
        """Disables the synchronization of the shutter by ext TTL signals (from OPIOM)"""
        log_info(self, "fsextoff entering")
        self.EXT = "OFF"
        log_info(self, "fsextoff leaving")

    def fsgetext(self):
        """
        This returns 0 if synchronization by external signals are off (no TTL signal when doing a scan)
          and 1 if synchronization by external signals are on (The scan is controlling the shutter)
        """
        log_info(self, "fsgetext entering")
        reply = self.EXT
        # reply is "ON" or "OFF"
        if reply == "ON":
            return 1
        else:
            if reply == "OFF":
                return 0
            else:
                raise RuntimeError(
                    "Unexpected reply from shutter = %s. Raised by instance %s"
                    % (reply, self.name)
                )
        log_info(self, "fsgetext leaving")
        return reply

    def open(self):
        """
        This opens the shutter
        """
        log_info(self, "open entering")

        # command only possible when in MANUAL mode
        if self.mode == Shutter.EXTERNAL:
            raise RuntimeError(
                "Shutter cannot be opened in EXTERNAL trigger mode. Switch to MANUAL mode!"
            )

        # "OPEN <X>" is seen in the spec macro and it closes after <X> millisecond.
        # "OPEN 0"  will open the shutter and you need to close it with close()
        self._query("OPEN 0")

        # now sleep self.opening_time seconds
        time.sleep(self.opening_time)

        log_info(self, "open leaving")

    def close(self):
        """
        This closes the shutter
        """
        log_info(self, "close entering")

        # command only possible when in MANUAL mode
        if self.mode == Shutter.EXTERNAL:
            raise RuntimeError(
                "Shutter cannot be closed in EXTERNAL trigger mode. Switch to MANUAL mode!"
            )

        self._query("CLOSE")

        # now sleep self.closing_time seconds
        time.sleep(self.closing_time)

        log_info(self, "close leaving")

    def is_open(self):
        if self.state == BaseShutterState.OPEN:
            return True
        else:
            return False

    def is_close(self):
        if self.state == BaseShutterState.CLOSE:
            return True
        else:
            return False

    @property
    def closing_time(self):
        return self.__closing_time

    @property
    def opening_time(self):
        return self.__opening_time

    def measure_open_close_time(self):
        """
        This method can be overloaded if needed.
        Basic timing on. No timeout to wait opening/closing.
        """
        self.close()  # ensure it's closed
        start_time = time.time()
        self.open()
        opening_time = time.time() - start_time

        start_time = time.time()
        self.close()
        closing_time = time.time() - start_time
        return opening_time, closing_time

    # Serial line protocol

    def _query(self, cmd, timeout=None):
        """
        write_readline to the device
        gives back a formatted str stripped of the EOL character
        """

        try:
            self._serial.flush()

            self._serial._write((cmd + "\r\n").encode())
            if cmd.startswith("?") or cmd.startswith("#"):
                msg = self._serial._readline(timeout=timeout)
                cmd = cmd.strip("#").split(" ")[0]
                msg = msg.replace((cmd + " ").encode(), "".encode())
                # print (msg)
                if msg.startswith("$".encode()):
                    msg = self._serial._readline(eol="$\r\n", timeout=timeout)
                    return msg.strip("$\r\n".encode()).decode()

                elif msg.startswith("ERROR".encode()):
                    raise RuntimeError(msg.decode())

                return (msg.strip("\r\n".encode())).decode()

        except PyTango.ConnectionFailed:
            ## most likely that the Tango Server died
            raise RuntimeError(
                "Tango Serial server is not running anymore. Raised by instance %s"
                % self.name
            )
        except Exception as e:
            raise RuntimeError(
                "Error when trying to communicate with %s. Command %s returned %s"
                % (self.name, cmd, type(e))
            )

    def RESET(self):
        """Reset the module"""
        return self._query("RESET")

    @property
    def EXT(self):
        """Read external trigger mode"""
        return self._query("?EXT")

    @EXT.setter
    def EXT(self, mode):
        """Set the external trigger mode. The mode can be ON or OFF"""

        if mode == "ON" or mode == "OFF":
            cmd = "EXT " + mode
            self._query(cmd)
        else:
            print("\nThe external trigger mode can be set to ON or OFF\n")

    @property
    def LOWPWR(self):
        """Read the low power mode"""
        return self._query("?LOWPWR")

    @LOWPWR.setter
    def LOWPWR(self, mode):
        """Set the low power mode"""

        if mode == "ON" or mode == "OFF":
            cmd = "LOWPWR " + mode
            self._query(cmd)
        else:
            print("\nThe low power mode can be set to ON or OFF\n")

    @property
    def ISGMODE(self):
        """Read ISGMODE (protected mode")"""
        return self.__isgmode

    @ISGMODE.setter
    def ISGMODE(self, mode):
        """Enter/exit protected mode. Setting ISGMODE ON allows to change the shutter protected parameters."""

        if mode == "ON" or mode == "OFF":
            cmd = "ISGMODE " + mode
            self._query(cmd)
            self.__isgmode = mode
        else:
            print("\nThe protected mode can be set to ON or OFF\n")

    #
    # Protected parameters!
    # ISGMODE must be ON
    #

    @property
    def SHTYPE(self):
        """Read the shutter type. Can be ID19 or ID17"""
        return self._query("?SHTYPE")

    @SHTYPE.setter
    def SHTYPE(self, sh_type):
        """Set the shutter type. Can be ID19 or ID17"""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify sutter type! ISGMODE is OFF!")

        if sh_type == "ID19" or sh_type == "ID17":
            # apply shutter type
            cmd = "SHTYPE " + sh_type
            self._query(cmd)

            # get the shutter type configuration
            conf_dict = self.config["shtype"][sh_type]
            # apply timing configuration
            for key in conf_dict:
                cmd = key + " " + str(conf_dict[key])
                self._query(cmd)

            # display config
            print(self.__info__())
        else:
            print("\nThe shutter type must be ID19 or ID17\n")

    @property
    def TUP(self):
        """Read the TUP parameter in ms"""
        return self._query("?TUP")

    @TUP.setter
    def TUP(self, timeval):
        """Set the TUP parameter in ms. Limits 4 < timeval < 60ms"""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify TUP parameter! ISGMODE is OFF!")

        timeval = int(timeval)
        if timeval < 4 or timeval > 60:
            print("\nTUP parameter limits: 4 < timeval < 60ms\n")
        else:
            cmd = "TUP " + str(timeval)
            self._query(cmd)

    @property
    def TDOWN(self):
        """Read the TDOWN parameter in ms"""
        return self._query("?TDOWN")

    @TDOWN.setter
    def TDOWN(self, timeval):
        """Set the TDOWN parameter in ms. Limits 0 < timeval < 30ms"""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify TDOWN parameter! ISGMODE is OFF!")

        timeval = int(timeval)
        if timeval < 0 or timeval > 30:
            print("\nTDOWN parameter limits: 0 < timeval < 30ms\n")
        else:
            cmd = "TDOWN " + str(timeval)
            self._query(cmd)

    @property
    def VMAX(self):
        """Read the VMAX parameter in Volt"""
        return self._query("?VMAX")

    @VMAX.setter
    def VMAX(self, voltval):
        """Set the VMAX parameter in Volt."""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify VMAX parameter! ISGMODE is OFF!")

        voltval = int(voltval)
        # Maximum voltage not in documentation, cannot check!
        cmd = "VMAX " + str(voltval)
        self._query(cmd)

    @property
    def VUP(self):
        """Read the VUP parameter in Volt"""
        return self._query("?VUP")

    @VUP.setter
    def VUP(self, voltval):
        """Set the VUP parameter in Volt. Limits 0 < voltval < Vmax"""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify VUP parameter! ISGMODE is OFF!")

        voltval = int(voltval)
        if voltval < 0 or voltval > self.VMAX:
            print("\nVUP parameter limits: 0 < voltval < Vmax\n")
        else:
            cmd = "VUP " + str(voltval)
            self._query(cmd)

    @property
    def VDOWN(self):
        """Read the VDOWN parameter in Volt"""
        return self._query("?VDOWN")

    @VDOWN.setter
    def VDOWN(self, voltval):
        """Set the VDOWN parameter in Volt. Limits -Vmax < voltval < 0"""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify VDOWN parameter! ISGMODE is OFF!")

        voltval = int(voltval)
        if voltval < (-1 * self.VMAX) or voltval > 0:
            print("\nVDOWN parameter limits: -Vmax < voltval < 0\n")
        else:
            cmd = "VDOWN " + str(voltval)
            self._query(cmd)

    @property
    def VLOW(self):
        """Read the VLOW parameter in Volt"""
        return self._query("?VLOW")

    @VLOW.setter
    def VLOW(self, voltval):
        """Set the VLOW parameter in Volt. Limits 0 < voltval < 24"""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify VLOW parameter! ISGMODE is OFF!")

        voltval = int(voltval)
        if voltval < 0 or voltval > 24:
            print("\nVLOW parameter limits: 0 < voltval < 24\n")
        else:
            cmd = "VLOW " + str(voltval)
            self._query(cmd)

    @property
    def PWRDEL(self):
        """Read the PWRDEL parameter in ms"""
        return self._query("?PWRDEL")

    @PWRDEL.setter
    def PWRDEL(self, timeval):
        """Set the PWRDEL parameter in ms. Limits 0 < timeval < TUP"""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify PWRDEL parameter! ISGMODE is OFF!")

        timeval = int(timeval)
        if timeval < 0 or timeval > self.TUP:
            print("\nVPWRDEL parameter limits: 0 < timeval < TUP\n")
        else:
            cmd = "PWRDEL " + str(timeval)
            self._query(cmd)

    @property
    def DOWNDEL(self):
        """Read the DOWNDEL parameter in ms"""
        return self._query("?DOWNDEL")

    @DOWNDEL.setter
    def DOWNDEL(self, timeval):
        """Set the DOWNDEL parameter in ms. Limits not ducumented"""
        if self.__isgmode == "OFF":
            raise RuntimeError("Cannot modify DOWNDEL parameter! ISGMODE is OFF!")

        timeval = int(timeval)
        # if timeval < 0 or timeval > self.TDOWN:
        #    print ("\nVDOWNDEL parameter limits: 0 < timeval < TDOWN\n")
        # else:
        #    cmd = "DOWNDEL " + str(timeval)
        #    self._query(cmd)
        cmd = "DOWNDEL " + str(timeval)
        self._query(cmd)
