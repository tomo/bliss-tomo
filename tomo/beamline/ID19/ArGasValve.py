# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import PyTango

from gevent import Timeout, sleep

from bliss.shell.cli.user_dialog import UserChoice, Container
from bliss.shell.cli.pt_widgets import BlissDialog

from bliss.common.logtools import log_info, log_debug

from enum import Enum


class TomoStation(Enum):
    UNKNOWN = 0
    HR = 1
    MR = 2


class ArGasValve:
    def __init__(self, name, config):
        self.__name = name
        self.__config = config

        self.__valve_shutter_device = config["valve_shutter_device"]
        self.__tomo_station = config["tomo_station"]

        self.__valve_to_use = TomoStation.UNKNOWN
        self.__unkown_msg = "No tomo station selected, should be HR or MR. Don't know which valve to use!"

        if self.__tomo_station == "HR":
            self.__valve_to_use = TomoStation.HR
        else:
            if self.__tomo_station == "MR":
                self.__valve_to_use = TomoStation.MR

        self.__valve_proxy = PyTango.DeviceProxy(self.__valve_shutter_device)

    @property
    def name(self):
        """
        A unique name for the Bliss object
        """
        return self.__name

    @property
    def config(self):
        """
        The configuration of Ar Gas Valve
        """
        return self.__config

    def status(self):
        if self.__valve_to_use == TomoStation.UNKNOWN:
            print(self.__unkown_msg)
            return

        state = self._valve_state()
        if self.__valve_to_use == TomoStation.HR:
            print("HR valve state: enbled = %s     state = %s" % (state[0], state[1]))
        else:
            print("MR valve state: enbled = %s     state = %s" % (state[0], state[1]))

    def enable(self):
        if self.__valve_to_use == TomoStation.UNKNOWN:
            raise ValueError(self.__unkown_msg)

        if self.__valve_to_use == TomoStation.HR:
            self.__valve_proxy.HrValveEnabled = True
        else:
            self.__valve_proxy.MrValveEnabled = True

    def disable(self):
        if self.__valve_to_use == TomoStation.UNKNOWN:
            raise ValueError(self.__unkown_msg)

        if self.__valve_to_use == TomoStation.HR:
            self.__valve_proxy.HrValveEnabled = False
        else:
            self.__valve_proxy.MrValveEnabled = False

    def open(self):
        self.__valve_proxy.ValveOpen()

    def close(self):
        self.__valve_proxy.ValveClose()

    def _valve_state(self):
        if self.__valve_to_use == TomoStation.UNKNOWN:
            raise ValueError(self.__unkown_msg)

        if self.__valve_to_use == TomoStation.HR:
            enabled = self.__valve_proxy.HrValveEnabled
            state = self.__valve_proxy.HrValveState
        else:
            enabled = self.__valve_proxy.MrValveEnabled
            state = self.__valve_proxy.MrValveState

        return [enabled, state]

    def __info__(self):
        info_str = f"{self.name} info:\n"
        info_str += f"  valve_shutter_device = {self.__valve_shutter_device} \n"
        info_str += f"  tomo_station         = {self.__tomo_station}\n"
        state = self._valve_state()
        info_str += f"  valve enabled        = {state[0]} \n"
        info_str += f"  valve state          = {state[1]} \n"
        sh_state = self.__valve_proxy.State()
        info_str += f"  shutter state        = {sh_state} \n"
        return info_str
