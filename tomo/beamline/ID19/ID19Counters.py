import time
import logging

from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info, log_debug
from bliss.scanning.acquisition.timer import SoftwareTimerMaster

from tomo.TomoCounters import TomoCounters


class ID19Counters(TomoCounters):
    """
    Class to handle additional counters for tomography on ID19

    **Attributes**:

    name : str
        The Bliss object name
    config : Bliss configuration oject
        The configuration of the object
    """

    def __init__(self, name, config):
        super().__init__(name, config)

    def add_counters(
        self, fast_master, chain, tomo_detector_name, count_time, npoints, timer=None
    ):
        """
        Adds p201, sampling and integrating counters into tomo acquisition.
        """
        # Add fast counter channels from P201 card
        super().add_p201_counters(fast_master, chain, count_time, npoints)

        # use an external timer master or create a slow time master
        slow_master = timer
        if slow_master == None:
            # create slow master (SotwareTimerMaster) with a frequency of 0.5Hz
            slow_master = SoftwareTimerMaster(count_time=2.0, npoints=0, sleep_time=0)

        super().add_sampling_counters(slow_master, chain, count_time, npoints)
        super().add_integrating_counters(slow_master, chain, count_time, npoints)

        # return the chain
        return chain
