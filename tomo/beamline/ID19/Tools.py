import os
from bliss.setup_globals import *
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.static import get_config
from bliss.shell.standard import mv as bliss_mv
from bliss.shell.standard import umv as bliss_umv
from bliss.shell.standard import mvr as bliss_mvr
from bliss.shell.standard import umvr as bliss_umvr
from bliss.common.scans.ct import ct as bliss_ct
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.static import get_config
from bliss import setup_globals
from bliss.common.logtools import *


_TOMO = None


def init_tomo(tomo):
    global _TOMO
    _TOMO = tomo


_UPDATE_ON = False


def update_on():
    global _UPDATE_ON
    _UPDATE_ON = True


def update_off():
    global _UPDATE_ON
    _UPDATE_ON = False


def update_is_on():
    global _UPDATE_ON
    return _UPDATE_ON == True


def select_tomo():
    dlg_tomo = UserInput(label="Enter name of tomo object you want to use (ex: hrtomo)")
    ret = BlissDialog([[dlg_tomo]], title="Tomo Setup").show()
    if ret != False:
        tomo = get_config().get(ret[dlg_tomo])
        init_tomo(tomo)


def mv(*args):
    bliss_mv(*args)
    if _UPDATE_ON:
        ct()


def mvr(*args):
    bliss_mvr(*args)
    if _UPDATE_ON:
        ct()


def umv(*args):
    bliss_umv(*args)
    if _UPDATE_ON:
        ct()


def umvr(*args):
    bliss_umvr(*args)
    if _UPDATE_ON:
        ct()


def ct(exposure_time=None, *args):
    if _UPDATE_ON:
        global _TOMO
        if _TOMO is not None:
            if exposure_time is not None:
                bliss_ct(exposure_time, *args)
            else:
                if _TOMO.parameters.exposure_time != 0:
                    bliss_ct(_TOMO.parameters.exposure_time, *args)
                else:
                    print(f"WARNING TOMO.parameters.exposure_time is 0")
        else:
            print(
                "No tomo exists in the session. Please use select_tomo() to define one"
            )
    else:
        if exposure_time is None:
            exposure_time = 1
        bliss_ct(exposure_time, *args)


def set_accumulation(detector):
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.default_acq_chain_acc["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        setup_globals.DEFAULT_CHAIN.set_settings(
            setup_globals.default_acq_chain["chain_config"]
        )
        raise Exception(
            f"{detector.name} is not configured in default chain for accumulation"
        )


def unset_accumulation(detector):
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.default_acq_chain["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f"{detector.name} is not configured in default chain")


def shopen():
    _TOMO.shutter.beam_shutter_open()


def shclose():
    _TOMO.shutter.beam_shutter_close()


def feopen():
    setup_globals.frontend.open()


def feclose():
    setup_globals.frontend.close()


def psp():
    print(
        f"sx {setup_globals.sx.position}  sy {setup_globals.sy.position}  sz {setup_globals.sz.position}   yrot {setup_globals.yrot.position}"
    )


def start_synchro_lbs():
    os.system("ssh lbs191 sudo /root/dockerStart.sh")


def stop_synchro_lbs():
    os.system("ssh lbs191 sudo /root/dockerStop.sh")


def lbs_free_space():
    process = os.popen("df -h /lbsram")
    result = process.readlines()
    process.close()
    mem_use = [item[:-1] for item in result[1].split(" ") if "%" in item]
    mem_use = int(mem_use[0])

    mem_available = 100 - mem_use
    msg = f"\n\033[1mIt remains {mem_available}% of free space on lbs\033[0m\n"
    print(msg)


def restart_mr_station():
    yrot = setup_globals.yrot
    srot = setup_globals.srot
    sz = setup_globals.sz

    yrot.controller.initialize()
    yrot.controller.initialize_hardware()
    yrot.controller.initialize_axis(yrot)
    yrot.controller._set_power(yrot, True)
    yrot.controller.initialize_hardware_axis(yrot)
    yrot.sync_hard()
    while not "READY" in yrot.state:
        yrot.sync_hard()
    yrot.controller.set_velocity(yrot, yrot.velocity * yrot.steps_per_unit)

    sz.controller.initialize()
    sz.controller.initialize_hardware()
    sz.controller.initialize_axis(sz)
    sz.controller._set_power(sz, True)
    sz.controller.initialize_hardware_axis(sz)
    sz.sync_hard()
    while not "READY" in sz.state:
        sz.sync_hard()
    sz.controller.set_velocity(sz, sz.velocity * sz.steps_per_unit)

    srot.controller.initialize()
    srot.controller.initialize_hardware()
    srot.controller.initialize_axis(srot)
    srot.controller._set_power(srot, True)
    srot.controller.initialize_hardware_axis(srot)
    srot.sync_hard()
    while not "READY" in srot.state:
        srot.sync_hard()
    srot.controller.set_velocity(srot, srot.velocity * srot.steps_per_unit)
    setup_globals.init_mr_srot()


def reset_detector(*detectors):
    if len(detectors) == 0:
        list_det = []

        # analyse the active measurement group (ACTIVE_MG)
        builder = ChainBuilder([])

        for node in builder.get_nodes_by_controller_type(Lima):
            list_det.append(node.controller)

        if len(list_det) == 0:
            raise RuntimeError(
                "DETECTOR ERROR : No detector defined in the active measurement group!\n"
            )

    for detector in detectors:
        detector.image.roi = [0, 0, 0, 0]
        detector.image.binning = [1, 1]
        detector.image.rotation = 0
        detector.roi_counters.clear()
        detector.roi_profiles.clear()
        detector.processing.background = None
        detector.processing.flatfield = None
        detector.processing.use_background_substraction = "disable"
        detector.processing.use_flatfield = False
