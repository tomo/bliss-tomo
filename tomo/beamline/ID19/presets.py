from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.acquisition.mca import McaAcquisitionSlave
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset


class OpiomPreset(ScanPreset):
    """
    Class used to handle opiom during tomo acquisition

    ***Attributes***
    mux : Bliss controller object
        multiplexer based on opiom card
    tomo_station : string
        name of tomo station used to retrieve related opiom mode
    mode : string
        name of opiom mode associated to tomo station and a given
    """

    def __init__(self, multiplexer, detectors_optic, tomo_station):
        self.mux = multiplexer
        self.detectors_optic = detectors_optic
        self.tomo_station = tomo_station

    def prepare(self, scan):
        self.prev_val_mux = self.mux.getGlobalStat()
        mca = [
            node.device
            for node in scan.acq_chain.nodes_list
            if type(node) == McaAcquisitionSlave
        ]
        if len(mca) > 0:
            self.mux.switch("TRIGGER", "MUSST_GATE")
        else:
            detectors = [
                node.device
                for node in scan.acq_chain.nodes_list
                if type(node) == LimaAcquisitionMaster
            ]
            basler_in_detectors = False
            for det in detectors:
                if det.camera_type.lower() == "basler":
                    basler_in_detectors = True
                    break

            if (
                scan.scan_info.get("type") in ("fsweep", "finterlaced")
                or basler_in_detectors
            ):
                self.mux.switch("TRIGGER", "MUSST_GATE")
            else:
                self.mux.switch("TRIGGER", "MUSST_TRIG")

        # if self.tomo_station == "mr":
        # self.mux.switch("CAMERA","CAM_MR")
        # elif self.tomo_station == "hr":
        # self.mux.switch("CAMERA","CAM_HR")

    def stop(self, scan):
        """
        Switches back multiplexer ouputs to previous configuration.
        """

        self.mux.switch("TRIGGER", self.prev_val_mux["TRIGGER"])
        self.mux.switch("CAMERA", self.prev_val_mux["CAMERA"])


class DefaultChainOpiomPreset(ChainPreset):
    """
    Class used for all bliss common scans (ct, ascan, timescan,...) to switch opiom output according to detector

    ***Attributes***
    mux : Bliss controller object
        multiplexer based on opiom card
    detectors_optic : tomo controller object
        links each tomo detector to its optic
    tomo_station : string
        name of tomo station used to retrieve related musst card
    """

    def __init__(self, multiplexer, detectors_optic, tomo_station):
        self.mux = multiplexer
        self.detectors_optic = detectors_optic
        self.tomo_station = tomo_station

    def prepare(self, acq_chain):
        dets = [
            node.device
            for node in acq_chain.nodes_list
            if isinstance(node, LimaAcquisitionMaster)
        ]
        det = dets[0] if len(dets) == 1 else None

        (camera,) = [
            node.device
            for node in acq_chain.nodes_list
            if type(node) == LimaAcquisitionMaster
        ]
        optic = self.detectors_optic.get_optic(camera)

        if self.mux.getOutputStat("CAMERA") != "CAM_LAM":
            if (
                camera.camera_type.lower() == "basler"
                or camera.acquisition.mode == "SINGLE"
            ):
                self.mux.switch("TRIGGER", "MUSST_GATE")
            elif camera.acquisition.mode == "ACCUMULATION":
                self.mux.switch("TRIGGER", "MUSST_TRIG")

            if self.tomo_station == "mr":
                self.mux.switch("CAMERA", "CAM_MR")
            elif self.tomo_station == "hr":
                self.mux.switch("CAMERA", "CAM_HR")


class PcoTomoOpiomPreset(ScanPreset):
    """
    Class used to handle opiom during tomo acquisition

    ***Attributes***
    mux : Bliss controller object
        multiplexer based on opiom card
    tomo_station : string
        name of tomo station used to retrieve related opiom mode
    mode : string
        name of opiom mode associated to tomo station and a given
    """

    def __init__(self, multiplexer):
        self.mux = multiplexer

    def prepare(self, scan):
        self.mux.switch("SHMODE", "MUSST")

    def stop(self, scan):
        """
        Switches back multiplexer ouputs to previous configuration.
        """

        self.mux.switch("SHMODE", "SOFT")


class ReferenceMotorMoveOutPreset(ScanPreset):
    """
    Class used to move sample out of the beam before taking images
    ***Attributes***
    reference : reference object
        contains methods to control reference motors
    """

    def __init__(self, reference):
        self.reference = reference

    def prepare(self, scan):
        """
        Moves sample out of the beam
        """
        print(f"Move sample out of beam for {scan.name}")
        self.reference.move_out()
        print("moved out")

    def start(self, scan):
        pass

    def stop(self, scan):
        pass


class ReferenceMotorMoveInPreset(ScanPreset):
    """
    Class used to move sample in beam after taking images

    ***Attributes***
    reference : reference object
        contains methods to control reference motors
    """

    def __init__(self, reference):
        self.reference = reference

    def prepare(self, scan):
        pass

    def start(self, scan):
        pass

    def stop(self, scan):
        """
        Moves sample back in beam
        """
        print(f"Move sample back in beam")
        self.reference.move_in()
        print("moved in")
