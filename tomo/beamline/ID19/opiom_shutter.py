# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import time

from gevent import Timeout, sleep

from bliss import current_session
from bliss.common.shutter import BaseShutter, BaseShutterState

"""
Handle the experiment beam shutter via the OPIOM multiplexer

example yml file:

#fast shutter configuration
- name 
  plugin: bliss
  class: opiom_shutter
  package: id19.tomo.beamline.ID19
  
  opiom: $opiom
  shutter_choice: SHTHIRD          # SHNONE, SHMONO, SHTHIRD, SHBIG
  closing_times: {SHNONE:0.0, SHMONO:7.0, SHTHIRD:8.0, SHBIG:15.0}   # closing times in ms
  
"""


class OpiomShutter(BaseShutter):
    def __init__(self, name, config):
        self.__name = name
        self.__config = config

        self.opiom = config["opiom"]
        self.shutter = config["shutter_choice"]
        self.closing_times = config["closing_times"]
        for name, value in self.closing_times.items():
            self.closing_times[name] = value / 1000.0

        possible_shutters = self.opiom.getPossibleValues("SHUTTER")
        if self.shutter not in possible_shutters:
            raise RuntimeError(
                "{} : Shutter choice is not available in the OPIOM {}, check the possible choices".format(
                    self.__name, self.opiom.name
                )
            )

        self.opiom.switch("SHUTTER", self.shutter)

        # the SHMODE on the OPIOM musst be set to SOFT!

    @property
    def name(self):
        """A unique name"""
        return self.__name

    @property
    def config(self):
        """Config of shutter"""
        return self.__config

    @property
    def state(self):
        try:
            if self.opiom.getOutputStat("SOFTSHUT") == "OPEN":
                return BaseShutterState.OPEN
            else:
                return BaseShutterState.CLOSED

        except:
            raise RuntimeError(
                "{}: Communication error with {}".format(self.__name, self.opiom.name)
            )

    @property
    def state_string(self):
        s = self.state
        if s in [
            BaseShutterState.OPEN,
            BaseShutterState.CLOSED,
            BaseShutterState.UNKNOWN,
        ]:
            return s.value
        else:
            return (self._tango_state, self._tango_status)

    def close(self):
        self.opiom.switch("SOFTSHUT", "CLOSE")

        # sleep for closing time
        time.sleep(self.closing_time)

    def open(self):
        self.opiom.switch("SOFTSHUT", "OPEN")

        # sleep for opening time
        time.sleep(self.opening_time)

    @property
    def closing_time(self):
        return self.closing_times[self.shutter]

    @property
    def opening_time(self):
        return self.closing_times[self.shutter]

    def __enter__(self):
        self.open()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
