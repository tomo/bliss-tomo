# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import time

from bliss.common.logtools import log_info, log_debug
from bliss.config.settings import SimpleSetting

from bliss.shell.cli.user_dialog import UserChoice, Container, UserFloatInput
from bliss.shell.cli.pt_widgets import BlissDialog

from tomo.TomoOptic import TomoOptic


class PeterOptic(TomoOptic):
    """
    Class to handle three objectives and four eyepieces of type OptiquePeter.
    The class has three parts:
    The standart methods implemented for every optic,
    methods to handle the three objectives and four eypieces and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:

    magnifications : list of float
        An ordered list with magnifications of the three objectives
    eyepiece_magnifications : list of float
        An ordered list of with the magnifications of the four eyepieces
    wago_device : string
        The name of the Wago server Tango device to use
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective

    rotc_motor : Bliss Axis object
        The camera rotation motor used for all three objectives
    focus_motor : Bliss Axis object
        The focus motor used for all three objectives
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor


    **Example yml file**::

        name:  peter
        plugin: bliss
        class: PeterOptics
        package: id19.tomo.beamline.ID19.PeterOptics

        magnifications: [2, 10 , 20]
        eyepiece_magnifications: [2.0, 2.5, 3.3, 4.0]
        wago_device: "id19/wcid19c/tg"
        image_flipping_hor:  False
        image_flipping_vert: False

        rotc_motor: $rotc
        focus_motors: $focrev
        focus_type: "translation"     # translation or rotation
        focus_scan_steps: 20
        focus_lim_pos:  0.5
        focus_lim_neg: -0.5"""

    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        config : dictionary
            The Bliss configuration dictionary
        """

        self.__name = name
        self.__config = config

        self.magnifications = config["magnifications"]
        self.wago_device = config["wago_device"]

        self.current_objective = None
        self.__eyepiece = SimpleSetting(f"{self.__name}:eyepiece", default_value=1.0)

        # Initialise the TomoOptic class
        super().__init__(name, config)

    @property
    def description(self):
        """
        The name string the current optics
        """
        name = "OptiquePeter_" + str(self.magnifications[self.objective - 1]) + "_"
        #                       str(self.eyepiece_magnifications[self.current_eyepiece])
        return name

    #
    # standart otics methods every otics has to implement
    #

    @property
    def type(self):
        """
        Returns the class name as the optics type
        """
        return self.__class__.__name__

    @property
    def magnification(self):
        """
        Returns the magnification of the current objective used
        """
        if self.current_objective is None:
            self._objective_state()
        return self.magnifications[self.current_objective] * self.eyepiece_magnification

    def setup(self, tomo_ccd):
        objvals = list()
        for idx, val in enumerate(self.magnifications):
            objvals.append((idx + 1, f"X{val:d}"))
        dlg_optic = UserChoice(values=objvals, defval=self.objective - 1)
        ct1 = Container([dlg_optic], title="Revolver Optic")

        dlg_eyepiece = UserFloatInput(
            label="Magnification ?", defval=self.eyepiece_magnification
        )
        ct2 = Container([dlg_eyepiece], title="Eyepiece")

        ret = BlissDialog([[ct1, ct2]], title="Revolver Optic Setup").show()

        if ret is not False:
            self.objective = int(ret[dlg_optic])
            self.eyepiece_magnification = float(ret[dlg_eyepiece])

    #
    # Specific objective handling
    #

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        try:
            magnification = self.magnification
            print(
                "Objective %d selected : magnification = X%s [eyepiece= %.1f]"
                % (
                    (self.current_objective + 1),
                    str(magnification),
                    self.eyepiece_magnification,
                )
            )

        except ValueError as err:
            print("Optics indicates a problem:\n", err)

    @property
    def objective(self):
        """
        Reads and sets the current objective (1, 2 or 3)
        """
        return self._objective_state()

    @objective.setter
    def objective(self, value):
        """
        Moves to the objective 1, 2 or 3
        """
        if value not in (1, 2, 3):
            raise ValueError("Only the objectives 1, 2 and 3 can be chosen!")

        try:
            currobj = self.objective
        except:
            currobj = 1
        if (currobj + 1) % 3 == value:
            # --- move positive
            log_debug(self, "move positive")
            self.wago_device.set("oprot_pos", 1)
            gevent.sleep(0.2)
            self.wago_device.set("oprot_pos", 0)
        else:
            # --- move negative
            log_debug(self, "move negative")
            self.wago_device.set("oprot_neg", 1)
            gevent.sleep(0.2)
            self.wago_device.set("oprot_neg", 0)

        # --- wait obj moves
        start_time = time.time()
        done = False
        while not done:
            gevent.sleep(0.25)
            try:
                if self._objective_state() == value:
                    done = True
            except:
                pass
            if not done and (time.time() - start_time) > 3.0:
                raise RuntimeError("Timeout waiting objective peter")

    def _objective_state(self):
        """
        Evaluates which obective is currently used and returns its value (1, 2 or 3)
        """
        opobj = self.wago_device.get("opobj")
        log_debug(self, f"wago read opobj = {opobj}")
        if sum(opobj) != 2:
            raise ValueError(
                "No objective selected\nThe wago key opobj does not indicate a selected opjective!"
            )

        self.current_objective = opobj.index(0)
        log_debug(self, f"current objective = {self.current_objective}")

        return self.current_objective + 1

    @property
    def eyepiece_magnification(self):
        return self.__eyepiece.get()

    @eyepiece_magnification.setter
    def eyepiece_magnification(self, value):
        self.__eyepiece.set(float(value))

    #
    # Focus related methods
    #

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """

        # the focus scan range is dependent on the magnigication
        if self.objective == 1:
            self.focus_scan_range = 0.3
        else:
            self.focus_scan_range = 0.025

        scan_params = super().focus_scan_parameters()
        return scan_params
