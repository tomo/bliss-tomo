from tomo.Tomo import ScanType
from tomo.TomoSequencePreset import TomoSequencePreset


class OpiomPreset(TomoSequencePreset):
    """
    Class to handle opiom setting
    """

    CAM2MUX = {
        "frelon1": ("CAM1", "CAM1"),
        "frelon2": ("CAM2", "CAM2"),
        "frelon3": ("CAM3", "CAM3"),
        "marana": ("MARANA", None),
        "frelon16": ("CAM1", "CAM1"),
    }

    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        mux : controller
            multiplexer Bliss object
        mode : str
            configuration mode of multiplexer
        musst_gate : boolean
            returns True if multiplexer is configured to receive gates from musst
        prev_val : list
            allows to store multiplexer configuration before tomo and reapply it after
        """

        self.mux = config["multiplexer"]

        self.use_gate = False
        self.cam_name = None

        super().__init__(name, config)

    def prepare(self, tomo):
        limadev = tomo.tomo_ccd.detector
        self.cam_name = limadev.name.lower()
        if self.cam_name not in self.CAM2MUX:
            raise ValueError(f"Unkown camera name [{det_name}] in tomo.OpiomPreset")
        if tomo.parameters.scan_type == ScanType.SWEEP:
            self.use_gate = True
        else:
            self.use_gate = False
        if self.cam_name.startswith("frelon"):
            if limadev.camera.image_mode == "FRAME TRANSFER":
                limadev.shutter.mode = "MANUAL"
                limadev.shutter.close_time = 0.0

    def start(self):
        """
        Switches multiplexer outputs according to desired mode
        """
        (cam_mux, shut_mux) = self.CAM2MUX[self.cam_name]
        self.mux.switch("TRIGGER_MODE", "MUSST")
        self.mux.switch(cam_mux, "ON")
        if self.use_gate:
            self.mux.switch("MUSST", "BTRIG")
        else:
            self.mux.switch("MUSST", "ATRIG")
        if shut_mux is not None:
            self.mux.switch("SHUTTER", shut_mux)

    def stop(self):
        """
        Switches back multiplexer ouputs to previous configuration
        """
        pass
