from bliss.common.shutter import BaseShutterState


class FastShutter(object):
    def __init__(self, name, config):
        self.name = name
        self.fsh = config.get("fsh")

    def open(self):
        self.fsh.open(wait=True)

    def close(self):
        self.fsh.close(wait=True)

    @property
    def closing_time(self):
        return self.fsh.shutter_time

    @property
    def state(self):
        state = self.fsh.state
        if state == "CLOSE":
            return BaseShutterState.CLOSED
        else:
            return BaseShutterState.OPEN
