from __future__ import annotations
import pint
from bliss import current_session
from bliss import setup_globals
from tomo.sequencebasic import SequenceBasic, SequenceBasicPars, ScanType
from tomo.sequence.presets.inhibit_auto_projection import InhibitAutoProjection
from .utils import setup
from bliss.shell.dialog.helpers import dialog
from bliss.common.axis import Axis
from tomo.helpers.proxy_param import ProxyParam


class FullTomoPars(SequenceBasicPars):
    """
    Class to create fulltomo sequence parameters

    This sequence has no specific parameters
    """

    DEFAULT = dict(
        **SequenceBasicPars.DEFAULT,
        **{
            # Dispersion for the horizontal randomization (in detector pixel unit)
            "rand_disp_y": 0,
            # Dispersion for the vertical randomization (in detector pixel unit)
            "rand_disp_z": 0,
        },
    )
    LISTVAL = dict(**SequenceBasicPars.LISTVAL)
    NOSETTINGS = [*SequenceBasicPars.NOSETTINGS]


class FullFieldTomo(SequenceBasic):
    """
    Class to handle standard tomo acquisition

    .. code-block:: yaml

        name: mrfull_tomo
        plugin: bliss
        class: FullFieldTomo
        package: tomo.fulltomo

        tomo_config: $mrtomo_config
    """

    def __init__(self, name, config):
        self.name = name + "_sequence"
        """Bliss object name"""
        self._config = config

        # randomization axes
        self._rand_y_axis = config.get("rand_y_axis")
        """Optional horizontal axis use for randomization"""
        self._rand_z_axis = config.get("rand_z_axis")
        """Optional vertical axis use for randomization"""

        SequenceBasic.__init__(self, name, config)

        inhibitautoproj = InhibitAutoProjection(self._tomo_config)
        self.add_sequence_preset(inhibitautoproj)

    def _create_parameters(self):
        """
        Create pars attribute containing sequence, basic and config pars
        """
        tomo_config = self.tomo_config
        sequence_pars = FullTomoPars(self.name)
        return ProxyParam(tomo_config.pars, sequence_pars)

    def validate(self):
        """
        Update some internal parameters
        Estimate sequence acquisition time
        """
        self._inpars.end_pos = self.pars.start_pos + self.pars.range
        self._inpars.field_of_view = "Full"

        if self.pars.half_acquisition:
            self._inpars.field_of_view = "Half"

        self._inpars.scan_time = self.estimation_time()

    def estimation_time(self) -> float:
        not_initialized = self._scans == []
        if not_initialized:
            # If called from outside
            self.init_sequence()
            self.build_sequence()

        darks_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "dark"
        ]
        flats_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "flat"
        ]
        returns_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name == "return_ref"
        ]
        projs_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name in ScanType.values
        ]

        nb_proj_scan = len(projs_in_sequence)
        nb_dark_scan = len(darks_in_sequence)
        nb_flat_scan = len(flats_in_sequence)
        nb_return_scan = len(returns_in_sequence)

        proj_time = nb_proj_scan * self._inpars.proj_time
        dark_time = nb_dark_scan * self.pars.dark_n * self.pars.exposure_time
        flat_runner = self.get_runner("flat")
        flat_time = nb_flat_scan * flat_runner._estimate_scan_duration(self._inpars)

        return_runner = self.get_runner("return_ref")
        return_time = nb_return_scan * return_runner._estimate_scan_duration(
            self._inpars
        )

        if not_initialized:
            self.init_sequence()

        return dark_time + flat_time + proj_time + return_time

    def projection_scan(
        self, motor, start_pos, end_pos, tomo_n, expo_time, scan_info=None, run=True
    ):
        """
        Get runner and build scan corresponding to tomo scan type parameter
        Estimate execution time of projection scan
        Return projection scan object
        """
        scan_type = self.pars.scan_type

        if scan_type not in ScanType.values:
            raise ValueError(f"{scan_type} is not a recognized type of scan")

        runner = self.get_runner(scan_type)

        if scan_type == ScanType.CONTINUOUS:
            if self._rand_y_axis is not None:
                print(
                    f"Axis {self._rand_y_axis.name} is ignored: Scan setup in {scan_type}"
                )
            if self._rand_z_axis is not None:
                print(
                    f"Axis {self._rand_z_axis.name} is ignored: Scan setup in {scan_type}"
                )
            latency_time = self.pars.latency_time
            tomo_scan = runner(
                motor,
                start_pos,
                end_pos,
                tomo_n,
                expo_time,
                latency_time=latency_time,
                scan_info=scan_info,
                run=False,
            )

        elif scan_type == ScanType.STEP:

            def pixel_to_axis_position(axis: Axis | None, value: float) -> float:
                if axis is None:
                    return 0
                pixel_size_um = (
                    self.tomo_config.detectors.active_detector.sample_pixel_size
                )
                px = pint.Quantity(pixel_size_um, "um").to(axis.unit).magnitude
                return value * px

            latency_time = self.pars.latency_time
            tomo_scan = runner(
                motor,
                start_pos,
                end_pos,
                tomo_n,
                expo_time,
                rand_y_axis=self._rand_y_axis,
                rand_disp_y=pixel_to_axis_position(
                    self._rand_y_axis, self.pars.rand_disp_y
                ),
                rand_disp_y_px=self.pars.rand_disp_y,
                rand_z_axis=self._rand_z_axis,
                rand_disp_z=pixel_to_axis_position(
                    self._rand_z_axis, self.pars.rand_disp_z
                ),
                rand_disp_z_px=self.pars.rand_disp_z,
                latency_time=latency_time,
                scan_info=scan_info,
                run=False,
            )
        else:
            if self._rand_y_axis is not None:
                print(
                    f"Axis {self._rand_y_axis.name} is ignored: Scan setup in {scan_type}"
                )
            if self._rand_z_axis is not None:
                print(
                    f"Axis {self._rand_z_axis.name} is ignored: Scan setup in {scan_type}"
                )
            tomo_scan = runner(
                motor,
                start_pos,
                end_pos,
                tomo_n,
                expo_time,
                scan_info=scan_info,
                run=False,
            )

        self._inpars.proj_time = runner._estimate_scan_duration(self._inpars)

        if run:
            tomo_scan.run()

        return tomo_scan

    def full_turn_scan(
        self,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        direction=1,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo of 360 angular range
        """
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        self.pars.range = 360 * direction

        self._setup_sequence("tomo:fullturn", run=run, scan_info=scan_info)

    def half_turn_scan(
        self,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        direction=1,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo of 180 angular range
        """
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        self.pars.range = 180 * direction

        self._setup_sequence("tomo:halfturn", run=run, scan_info=scan_info)

    def basic_scan(
        self,
        start_pos=None,
        end_pos=None,
        tomo_n=None,
        expo_time=None,
        dataset_name=None,
        collection_name=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo using given parameters
        """

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if end_pos is not None:
            self.pars.range = end_pos - self.pars.start_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time

        self._setup_sequence("tomo:basic", run=run, scan_info=scan_info)

    def build_sequence(self):
        """
        Build full field tomo sequence according to scan option
        parameters (dark/flat at start/end, images on return, etc...)
        """
        if self.pars.dark_at_start:
            self.add_dark()
        if self.pars.flat_at_start:
            self.add_flat()
        if self.pars.projection_groups:
            self.add_projections_group(
                self.pars.start_pos,
                self.pars.start_pos + self.pars.range,
                self.pars.tomo_n,
                self.pars.exposure_time,
                flat_on=self.pars.flat_on,
            )
        else:
            self.add_proj(
                self.pars.start_pos,
                self.pars.start_pos + self.pars.range,
                self.pars.tomo_n,
                self.pars.exposure_time,
            )

        if self.pars.images_on_return:
            self.add_return()
        if self.pars.flat_at_end:
            self.add_flat()
        if self.pars.dark_at_end:
            self.add_dark()

    def _send_icat_metadata(self):
        """
        Fill metadata fields related to standard tomo
        Send them to ICAT
        """
        scan_type = self.pars.scan_type
        scan_saving = current_session.scan_saving
        if scan_type == "STEP":
            acc_disp = 0
        elif scan_type == "CONTINUOUS":
            scan = self.get_runner(scan_type)._fscan
            acc_disp = scan.inpars.acc_disp
        elif scan_type == "SWEEP":
            scan = self.get_runner(scan_type)._fsweep
            acc_disp = scan.inpars.acc_disp
        elif scan_type == "INTERLACED":
            scan = self.get_runner(scan_type)._finterlaced
            acc_disp = scan.inpars.acc_disp
        scan_saving.dataset.write_metadata_field("TOMOAcquisition_accel_disp", str(acc_disp))
        super()._send_icat_metadata()


# Setup the standard BLISS `menu`
@dialog(FullFieldTomo.__name__, "setup")
def _setup(sequence):
    setup(sequence)
