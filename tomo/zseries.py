import datetime
import os
import time
import numpy as np
import logging

from bliss import current_session
from bliss import setup_globals
from bliss.common.cleanup import axis as cleanup_axis
from bliss.common.cleanup import cleanup, capture_exceptions
from bliss.common.axis import Axis
from bliss.common.standard import mv
from bliss.common.utils import BOLD
from bliss.scanning.scan import ScanAbort
from bliss.scanning.group import ScanSequenceError
from bliss.config.static import ConfigList
from bliss.physics.trajectory import LinearTrajectory
from bliss.controllers.motors.icepap.ancillary import IceAncillaryHook
from fscan.fscantools import FScanParamBase

from tomo.sequence.presets.inhibit_auto_projection import InhibitAutoProjection
from tomo.sequencebasic import cleanup_sequence
from tomo.fulltomo import FullFieldTomo
from tomo.helpers.proxy_param import ProxyParam

_logger = logging.getLogger(__name__)


class ZSeriesPars(FScanParamBase):
    """
    Class to create zseries sequence parameters

    **Parameters**:

    nb_digits: Int
        number of digits in stage number
    step_start_pos: Float
        z axis starting position
    start_nb: Int
        starting scan number, useful if you want to launch a zseries
        from a specific scan number in the series
    delta_pos: Float
        z axis displacement value
    sleep: Float
        time to wait in seconds after z axis movement
    one_collection_per_z: Boolean
        option to activate if you want to create one collection per tomo
    one_dataset_per_z: Boolean
        option to activate if you want to create one dataset per tomo
    back_and_forth: Boolean
        option to activate if you want to invert scan direction
        at each stage (ex: 0-180/180-0)
    """

    DEFAULT = {
        "nb_digits": 3,
        "nb_scans": 1,
        "step_start_pos": [0.0],
        "start_nb": 1,
        "delta_pos": 1,
        "sleep": 0.0,
        "one_collection_per_z": True,
        "one_dataset_per_z": False,
        "back_and_forth": False,
        "iter_nb_in_scan_name": False,
        "check_field_of_view": True,
        "dark_flat_for_each_scan": False,
    }

    LISTVAL = {}
    NOSETTINGS = []

    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            ZSeriesPars.DEFAULT,
            value_list=ZSeriesPars.LISTVAL,
            no_settings=ZSeriesPars.NOSETTINGS,
        )

    def _validate_back_and_forth(self, value):
        if type(value) is not bool:
            raise ValueError("back_and_forth should be a boolean")
        return bool(value)

    def _validate_iter_nb_in_scan_name(self, value):
        if type(value) is not bool:
            raise ValueError("iter_nb_in_scan_name should be a boolean")
        return bool(value)


class ZSeries(FullFieldTomo):
    """
    Class to acquire a series of tomo scans with z axis displacement
    between each tomo

    .. code-block:: yaml

        name: mrz_series
        plugin: bliss
        class: ZSeries
        package: tomo.zseries

        tomo_config: $mrtomo_config
        tomo: $mrfull_tomo
        z_axis: $sz_eh2
    """

    def __init__(self, name, config):
        self.name: str = name + "_sequence"
        """Bliss object name"""
        self.tomo = config.get("tomo")
        """
        Sequence executed after each z axis movement
        """
        z_axis = config.get("z_axis")
        if not isinstance(z_axis, ConfigList):
            z_axis = [z_axis]
        self.z_axis: list[Axis] = z_axis
        """
        Motors moved between each tomo scan
        """

        FullFieldTomo.__init__(self, name, config)

        self._scan_info = {}
        self._trust_data_policy_location = False
        self._estimated_end_time = None
        self._loop = None
        self._collection_name = None
        self._dataset_name = None
        self._start_pos = None
        self._range = None
        self._scanfailed = False
        self._beamlost = False
        self._nbtries = None
        self._start_time = None
        self._real_scan_time = 0
        self._run = False

    def _create_parameters(self):
        """
        Create pars attribute containing zseriesm fulltomo and config pars
        """
        zseries_pars = ZSeriesPars(self.name)
        # NOTE: tomo_config.pars is already part of self.tomo.pars
        return ProxyParam(self.tomo.pars, zseries_pars)

    def add_metadata(self, scan_info):
        """
        Fill the scan_info dictionary with the basic tomo meta data
        and update it with specific meta data related to zseries
        """
        super().add_metadata(scan_info)
        nloop = self.pars.nb_scans
        scan_info["technique"]["scan"]["nb_scans"] = nloop
        for i, motor in enumerate(self.z_axis):
            motor_name = motor.name
            alias_name = setup_globals.ALIASES.get_alias(motor)
            if alias_name is None:
                alias_name = "None"
            scan_info["technique"]["scan"]["motor"].extend(
                [f"step_axis{i}", motor_name, alias_name]
            )
        scan_info["technique"]["scan"]["sequence"] = "tomo:zseries"
        scan_info["technique"]["scan"]["start_nb"] = self.pars.start_nb
        scan_info["technique"]["scan"]["delta_pos"] = self.pars.delta_pos

    def init_sequence(self):
        """
        Adapt z axis start position according to start_nb value
        Check attributes value to avoid to hit z axis limits
        Calculate z axis positions according to delta_pos value
        """
        if self.pars.activate_sinogram and not self.pars.dark_flat_for_each_scan:
            raise ValueError(
                "activate_sinogram can't be used with dark_flat_for_each_scan=False"
            )

        super().init_sequence()
        if not self._scanfailed and not self._beamlost:

            start_pos = self.pars.start_pos
            tomo_range = self.pars.range

            if self.pars.back_and_forth:
                self._start_pos = start_pos
                self._range = tomo_range
                self.pars.return_to_start_pos = False
                scandir = 1 if tomo_range > 0 else -1
                if (
                    scandir > 0
                    and self._rotation_axis.position > start_pos + tomo_range / 2
                    or scandir < 0
                    and self._rotation_axis.position < start_pos + tomo_range / 2
                ):
                    self.pars.start_pos = start_pos + tomo_range
                    self.pars.range = -tomo_range
                else:
                    self.pars.start_pos = start_pos
                    self.pars.range = tomo_range

            if self._loop == self.pars.start_nb - 1:
                step_start_pos = self.pars.step_start_pos
                nb_scans = self.pars.nb_scans
                delta_pos = self.pars.delta_pos

                self.lmotpos = {}
                if step_start_pos is None:
                    step_start_pos = []
                    for motor in self.z_axis:
                        step_start_pos.append(motor.position)
                        start_mot = motor.position
                        stop_mot = start_mot + (nb_scans - 1) * delta_pos
                        self.lmotpos[motor.name] = np.linspace(
                            start_mot, stop_mot, nb_scans
                        )
                else:
                    if not isinstance(step_start_pos, list):
                        step_start_pos = [step_start_pos]
                    for i, motor in enumerate(self.z_axis):
                        start_mot = step_start_pos[i]
                        stop_mot = start_mot + (nb_scans - 1) * delta_pos
                        self.lmotpos[motor.name] = np.linspace(
                            start_mot, stop_mot, nb_scans
                        )

                # Calculate start position and number of scans when
                # not starting from the first one
                if self.pars.start_nb > 1:
                    for motor in self.z_axis:
                        self.lmotpos[motor.name][0] = (
                            self.lmotpos[motor.name][0]
                            + (self.pars.start_nb - 1) * self.pars.delta_pos
                        )

                nloop = self.pars.nb_scans
                if self.pars.start_nb < 1 or self.pars.start_nb > nloop:
                    raise ValueError("Invalid start scan value!")

                for motor in self.z_axis:
                    limit = min(motor.low_limit, motor.high_limit)
                    if self.lmotpos[motor.name][0] < limit:
                        raise ValueError(
                            f"{motor.name} start position "
                            "is beyond axis limit ({limit})! \n"
                        )
                    limit = max(motor.low_limit, motor.high_limit)
                    if self.lmotpos[motor.name][-1] > limit:
                        raise ValueError(
                            f"{motor.name} end position is "
                            "beyond axis limit ({limit})! \n"
                        )

                pixel_size_in_um = self._detectors.get_tomo_detector(
                    self._detector
                ).sample_pixel_size
                pixel_size_in_mm = pixel_size_in_um * 10**-3
                image_height = self._detector.image.height
                field_of_view = image_height * pixel_size_in_mm
                check_field_of_view = self.pars.check_field_of_view
                if self.pars.delta_pos > field_of_view and check_field_of_view:
                    raise ValueError(
                        f"Z axis displacement {self.pars.delta_pos} is "
                        f"higher than actual field of view {field_of_view})! \n"
                    )

    def _estimate_end_time_series(self) -> float:
        nb_scans = (self.pars.nb_scans - (self.pars.start_nb - 1))
        fullfield_duration = super().estimation_time()
        if self.pars.dark_flat_for_each_scan:
            scan_time = fullfield_duration * nb_scans
        else:
            scan_time = self._inpars.proj_time
            if self.pars.dark_at_start:
                scan_time += self.pars.dark_n * self.pars.exposure_time
            if self.pars.flat_at_start:
                flat_runner = self.get_runner("flat")
                scan_time += flat_runner._estimate_scan_duration(self._inpars)
            if self.pars.dark_at_end:
                scan_time += self.pars.dark_n * self.pars.exposure_time
            if self.pars.flat_at_end:
                flat_runner = self.get_runner("flat")
                scan_time += flat_runner._estimate_scan_duration(self._inpars)
            if self.pars.images_on_return:
                return_runner = self.get_runner("return_ref")
                scan_time += (nb_scans - 1) * return_runner._estimate_scan_duration(self._inpars)
            scan_time += (nb_scans - 1) * self._inpars.proj_time
        if not self.pars.images_on_return and not self.pars.round_position:
            axis = self.tomo_config.sample_stage.rotation_axis
            disp = abs(self.pars.range)
            lt = LinearTrajectory(
                pi=0,
                pf=disp,
                velocity=axis.velocity,
                acceleration=axis.acceleration,
                ti=0,
            )
            scan_time += lt.instant(disp) * (nb_scans - 1)
            if self.pars.return_to_start_pos:
                scan_time += lt.instant(disp)
                lmove_sz_time = []
                for motor in self.z_axis:
                    disp = abs(self.lmotpos[motor.name][-1] - self.lmotpos[motor.name][0])
                    lt = LinearTrajectory(
                    pi=0,
                    pf=disp,
                    velocity=motor.velocity,
                    acceleration=motor.acceleration,
                    ti=0,
                    )
                    move_sz_time = lt.instant(disp)
                    for motion_hook in motor.motion_hooks:
                        if isinstance(motion_hook, IceAncillaryHook):
                            move_sz_time += motion_hook.status_timeout * 2
                    lmove_sz_time.append(move_sz_time)
                move_sz_time = max(lmove_sz_time)
                scan_time += move_sz_time
        estimated_end_time = scan_time
        lmove_sz_time = []
        for motor in self.z_axis:
            disp = self.pars.delta_pos
            lt = LinearTrajectory(
                pi=0,
                pf=disp,
                velocity=motor.velocity,
                acceleration=motor.acceleration,
                ti=0,
            )
            move_sz_time = lt.instant(disp)
            for motion_hook in motor.motion_hooks:
                if isinstance(motion_hook, IceAncillaryHook):
                    move_sz_time += motion_hook.status_timeout * 2
            lmove_sz_time.append(move_sz_time)
        move_sz_time = max(lmove_sz_time)
        estimated_end_time += move_sz_time * (nb_scans - 1)
        return estimated_end_time

    def estimation_time(self) -> float:
        """
        Estimation time of the sequence (in seconds).
        """
        not_initialized = self._inpars is None
        if not_initialized:
            # If called from outside
            self.init_sequence()
            self.build_sequence()
        try:
            return self._estimate_end_time_series()
        finally:
            if not_initialized:
                self.init_sequence()

    def _setup_saving_title(self, user_info):
        scan_saving = current_session.scan_saving
        if self._collection_name is None and self._collection_name is None:
            self._collection_name = scan_saving.collection_name
            self._dataset_name = scan_saving.dataset_name
        nloop = self.pars.nb_scans
        if self._loop is None:
            self._loop = self.pars.start_nb - 1
        loop = self._loop
        nbdigits = self.pars.nb_digits
        if self.pars.delta_pos < 0:
            zstage_name = "_" + str(nloop - 1 - loop).zfill(nbdigits)
        else:
            zstage_name = "_" + str(loop).zfill(nbdigits)
        if self.pars.one_collection_per_z:
            if self.pars.iter_nb_in_scan_name:
                if self._nbtries is None:
                    root_path = scan_saving.get_path().split("/")[:-2]
                    root_path = "/".join(root_path)
                    nbtries = len(
                        [
                            directory
                            for directory in os.listdir(root_path)
                            if os.path.isdir(root_path + "/" + directory)
                            and "_" + directory.split("_")[-1] == zstage_name
                            and self._collection_name in directory
                        ]
                    )
                    nbtries = "_" + str(nbtries).zfill(self.pars.nb_digits)
                    self._nbtries = nbtries
                collection_name = self._collection_name + self._nbtries + zstage_name
            else:
                collection_name = self._collection_name + zstage_name
            if self._beamlost:
                collection_name += "_fc_"
            if self._scanfailed:
                collection_name += "_cs_"
            scan_saving.newcollection(collection_name)
        elif self.pars.one_dataset_per_z:
            if self.pars.iter_nb_in_scan_name:
                if self._nbtries is None:
                    collection_path = scan_saving.get_path().split("/")[:-1]
                    collection_path = "/".join(collection_path)
                    if os.path.isdir(collection_path):
                        ldir = [
                            directory
                            for directory in os.listdir(collection_path)
                            if os.path.isdir(collection_path + "/" + directory)
                            and "_" + directory.split("_")[-1] == zstage_name
                            and self._dataset_name in directory
                        ]
                        nbtries = len(ldir)
                        nbtries = "_" + str(nbtries).zfill(nbdigits)
                    else:
                        nbtries = "_" + str(0).zfill(nbdigits)
                    self._nbtries = nbtries
                dataset_name = self._dataset_name + self._nbtries + zstage_name
            else:
                dataset_name = self._dataset_name + zstage_name
            if self._beamlost:
                dataset_name += "_fc_"
            if self._scanfailed:
                dataset_name += "_cs_"
            scan_saving.newdataset(dataset_name)
        if user_info is None:
            user_info = {}
        user_info["title"] = "tomo:zseries" + zstage_name

    @cleanup_sequence
    def prepare(self, user_info=None):
        if self._loop is None:
            self._loop = self.pars.start_nb - 1
        if self._run:
            self._setup_saving_title(user_info)
        super().prepare(user_info)
        if self._run:
            cmd = []
            for motor in self.z_axis:
                cmd.append(motor)
                cmd.append(self.lmotpos[motor.name][self._loop])
            mv(*cmd)
            time.sleep(self.pars.sleep)

    @cleanup_sequence
    def run(self, user_info=None):
        """
        Prepare and execute the sequence
        Start acquisition presets
        For each stage:
        - Add stage number in collection name or dataset name
        - Move z axis at the right position
        - Run tomo scan
        Stop acquisition presets
        Move back z axis and rotation axis to initial position if
        return_to_start_pos parameter is set to True
        """
        self._run = True
        collection_name = current_session.scan_saving.collection_name
        dataset_name = current_session.scan_saving.dataset_name
        if self.pars.return_to_start_pos:
            restore_list = (cleanup_axis.POS, cleanup_axis.VEL)
        else:
            restore_list = (cleanup_axis.VEL,)
        with capture_exceptions(raise_index=0) as capture:
            with self._run_context(capture):
                with cleanup(
                    *self.z_axis,
                    self._cleanup,
                    collection_name=collection_name,
                    dataset_name=dataset_name,
                    restore_list=restore_list,
                ):
                    self._loop = self.pars.start_nb - 1
                    while self._loop < self.pars.nb_scans:
                        self._scanfailed = False
                        self._beamlost = False
                        try:
                            self._start_time = time.time()
                            super().run(user_info, _lock=False)
                            self._real_scan_time += time.time() - self._start_time
                            self._loop += 1
                        except (
                            KeyboardInterrupt,
                            ScanSequenceError,
                            ScanAbort,
                            ValueError,
                        ):
                            self._run = False
                            raise
                        except BaseException:
                            _logger.error(
                                "Exception during loop %s", self._loop, exc_info=True
                            )
                            print("SCAN CRASHED WE REDO IT")
                            self._scanfailed = True
                            detector = self._detector
                            if hasattr(setup_globals, "restart_server"):
                                setup_globals.restart_server(detector)
                            self._start_time = time.time()
                            super().run(user_info, _lock=False)
                            self._real_scan_time += time.time() - self._start_time
                            self._loop += 1
        self._run = False

    def _beam_lost(self, user_info):
        """
        Check after tomo scan if frontend is closed or machine current
        is below 5 mA.
        Redo tomo scan if it is the case.
        """
        self._beamlost = True
        curr = self._machinfo.counters.current.value
        print(f"the SR current is {curr:.2f} mA")
        if self._frontend.is_closed:
            print("beam was probably lost during the scan")
        elif self._machinfo.counters.current.value < 5:
            print(
                "The SR current was lower than 5mA at the end of the scan,"
                "it probably corresponds to a beam loss without closing of "
                "the front-end"
            )
        print("BEAM LOST WE REDO THE SCAN")
        self._start_time = time.time()
        super().run(user_info, _lock=False)
        self._real_scan_time += time.time() - self._start_time

    def full_turn_scan(
        self,
        delta_pos,
        nb_scans,
        collection_name="",
        dataset_name="",
        sleep=0.0,
        step_start_pos=None,
        tomo_start_pos=None,
        start_nb=1,
        direction=1,
        scan_info=None,
        run=True,
        trust_data_policy_location: bool = False,
    ):
        """
        Build and run a series of full field tomo of 360 angular range

        Arguments:
            collection_name: If set, switch to a new collection at start, else
                the actual data policy collection is used
            dataset_name: Call newcollection
                If set, a new dataset is created per layers based on this
                prefix
                If `""` (the default), a new collection is created per layers
                based on the collection prefix
            trust_data_policy_location: If true, do not initialize the
                collection/dataset at the initializtion/cleanup.
                This is used by daiquiri, and could be removed in the future.
        """
        scan_saving = current_session.scan_saving

        if dataset_name != "":
            self.pars.one_dataset_per_z = True
            self.pars.one_collection_per_z = False
        else:
            self.pars.one_dataset_per_z = False
            self.pars.one_collection_per_z = True
        if collection_name == "":
            collection_name = scan_saving.collection_name
        if dataset_name == "":
            dataset_name = scan_saving.dataset_name
        self._trust_data_policy_location = trust_data_policy_location
        if not trust_data_policy_location:
            scan_saving.newcollection(collection_name)
            scan_saving.newdataset(dataset_name)

        self.pars.step_start_pos = step_start_pos
        self.pars.start_nb = start_nb
        self.pars.delta_pos = delta_pos
        self.pars.sleep = sleep
        self.pars.nb_scans = nb_scans

        if tomo_start_pos is not None:
            self.pars.start_pos = tomo_start_pos
        self.pars.range = 360 * direction

        if scan_info is not None:
            self._scan_info = scan_info.copy()
        else:
            self._scan_info = {}

        self._setup_sequence("tomo:zseries", run=run, scan_info=scan_info)

    def half_turn_scan(
        self,
        delta_pos,
        nb_scans,
        collection_name="",
        dataset_name="",
        sleep=0.0,
        step_start_pos=None,
        tomo_start_pos=None,
        start_nb=1,
        direction=1,
        scan_info=None,
        run=True,
        trust_data_policy_location: bool = False,
    ):
        """
        Build and run a series of full field tomo of 180 angular range

        Arguments:
            collection_name: If set, switch to a new collection at start, else
                the actual data policy collection is used
            dataset_name: Call newcollection
                If set, a new dataset is created per layers based on this
                prefix
                If `""` (the default), a new collection is created per layers
                based on the collection prefix
            trust_data_policy_location: If true, do not initialize the
                collection/dataset at the initializtion/cleanup.
                This is used by daiquiri, and could be removed in the future.
        """
        scan_saving = current_session.scan_saving
        if dataset_name != "":
            self.pars.one_dataset_per_z = True
            self.pars.one_collection_per_z = False
        else:
            self.pars.one_dataset_per_z = False
            self.pars.one_collection_per_z = True
        if collection_name == "":
            collection_name = scan_saving.collection_name
        if dataset_name == "":
            dataset_name = scan_saving.dataset_name
        self._trust_data_policy_location = trust_data_policy_location
        if not trust_data_policy_location:
            scan_saving.newcollection(collection_name)
            scan_saving.newdataset(dataset_name)

        self.pars.step_start_pos = step_start_pos
        self.pars.start_nb = start_nb
        self.pars.delta_pos = delta_pos
        self.pars.sleep = sleep
        self.pars.nb_scans = nb_scans

        if tomo_start_pos is not None:
            self.pars.start_pos = tomo_start_pos
        self.pars.range = 180 * direction

        if scan_info is not None:
            self._scan_info = scan_info.copy()
        else:
            self._scan_info = {}

        self._setup_sequence("tomo:zseries", run=run, scan_info=scan_info)

    def basic_scan(
        self,
        delta_pos=None,
        nb_scans=None,
        tomo_start_pos=None,
        tomo_end_pos=None,
        tomo_n=None,
        expo_time=None,
        collection_name="",
        dataset_name="",
        sleep=0.0,
        step_start_pos=None,
        start_nb=1,
        scan_info=None,
        run=True,
        trust_data_policy_location: bool = False,
    ):
        """
        Build and run a series of full field tomo using given parameters

        Arguments:
            collection_name: If set, switch to a new collection at start, else
                the actual data policy collection is used
            dataset_name: Call newcollection
                If set, a new dataset is created per layers based on this
                prefix
                If `""` (the default), a new collection is created per layers
                based on the collection prefix
            trust_data_policy_location: If true, do not initialize the
                collection/dataset at the initializtion/cleanup.
                This is used by daiquiri, and could
                be removed in the future.
        """
        scan_saving = current_session.scan_saving

        if dataset_name != "":
            self.pars.one_dataset_per_z = True
            self.pars.one_collection_per_z = False
        else:
            self.pars.one_dataset_per_z = False
            self.pars.one_collection_per_z = True
        if collection_name == "":
            collection_name = scan_saving.collection_name
        if dataset_name == "":
            dataset_name = scan_saving.dataset_name
        self._trust_data_policy_location = trust_data_policy_location
        if not trust_data_policy_location:
            scan_saving.newcollection(collection_name)
            scan_saving.newdataset(dataset_name)

        self.pars.step_start_pos = step_start_pos

        self.pars.start_nb = start_nb
        self.pars.sleep = sleep

        if delta_pos is not None:
            self.pars.delta_pos = delta_pos
        if nb_scans is not None:
            self.pars.nb_scans = nb_scans
        if tomo_start_pos is not None:
            self.pars.start_pos = tomo_start_pos
        if tomo_end_pos is not None:
            self.pars.range = tomo_end_pos - self.pars.start_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time
        if step_start_pos is not None:
            self.pars.step_start_pos = step_start_pos

        if scan_info is not None:
            self._scan_info = scan_info.copy()
        else:
            self._scan_info = {}

        self._setup_sequence("tomo:zseries", run=run, scan_info=scan_info)

    def build_sequence(self):
        """
        Build full field tomo sequence according to scan option
        parameters (dark/flat at start/end, images on return, etc...)
        """
        do_dark_flat_at_start = (
            self._loop == self.pars.start_nb - 1
        ) or self.pars.dark_flat_for_each_scan
        if do_dark_flat_at_start and self.pars.dark_at_start:
            self.add_dark()
        if do_dark_flat_at_start and self.pars.flat_at_start:
            self.add_flat()
        if self.pars.projection_groups:
            self.add_projections_group(
                self.pars.start_pos,
                self.pars.start_pos + self.pars.range,
                self.pars.tomo_n,
                self.pars.exposure_time,
                flat_on=self.pars.flat_on,
            )
        else:
            self.add_proj(
                self.pars.start_pos,
                self.pars.start_pos + self.pars.range,
                self.pars.tomo_n,
                self.pars.exposure_time,
            )
        do_dark_flat_at_end = (
            self._loop == self.pars.nb_scans - 1
        ) or self.pars.dark_flat_for_each_scan

        if self.pars.images_on_return:
            self.add_return()
        if do_dark_flat_at_end and self.pars.flat_at_end:
            self.add_flat()
        if do_dark_flat_at_end and self.pars.dark_at_end:
            self.add_dark()

    def _cleanup(self, collection_name, dataset_name):
        """
        Restore dataset name that has been modified by z series acquisition.
        """
        scan_saving = current_session.scan_saving
        if not self._trust_data_policy_location:
            scan_saving.newcollection(collection_name)
            scan_saving.newdataset(dataset_name)
        self._estimated_end_time = None
        self._collection_name = None
        self._dataset_name = None
        self._scanfailed = False
        self._beamlost = False
        if self.pars.back_and_forth:
            self.pars.start_pos = self._start_pos
            self.pars.range = self._range
        self._nbtries = None
        self._real_scan_time = 0
        self._loop = None
        self._run = False

    def __info__(self):
        """
        Show information about the sequence parameters, and detector optic
        configuration.
        """
        info_str = f"{self.name} configuration info:\n"
        info_str += super().__info__()
        return info_str

    def _send_icat_metadata(self):
        """
        Fill metadata fields related to standard tomo
        Update metadata with fields related to zseries
        Send them to ICAT
        """
        scan_saving = current_session.scan_saving
        z_axis = self.z_axis[0]
        if setup_globals.ALIASES.get(z_axis.name) is not None:
            z_name = z_axis.original_name
        else:
            z_name = z_axis.name
        scan_saving.dataset.write_metadata_field("TOMOAcquisitionZseries_z_mot", str(z_name))
        delta_pos = self.pars.delta_pos
        scan_saving.dataset.write_metadata_field("TOMOAcquisitionZseries_z_delta", str(delta_pos))
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisitionZseries_z_n_steps", str(self.pars.nb_scans)
        )
        z_start = self.lmotpos[z_axis.name][0]
        scan_saving.dataset.write_metadata_field("TOMOAcquisitionZseries_z_start", str(z_start))
        super()._send_icat_metadata()

    def show_info(self):
        """
        Display info about acquisition
        """
        info_str = self.__info__()
        time_in_hh_mm_ss = str(
            datetime.timedelta(seconds=self._inpars.scan_time)
        ).split(":")
        hours = time_in_hh_mm_ss[0]
        minutes = time_in_hh_mm_ss[1]
        secondes = time_in_hh_mm_ss[2].split(".")[0]
        info_str += (
            "\n\nEstimated Scan Time: "
            + hours
            + " Hours "
            + minutes
            + " Minutes "
            + secondes
            + " Secondes"
            + "\n"
        )
        scan_number = self._loop + 1
        nb_scans = self.pars.nb_scans
        info_str += BOLD(f"\n\nScan number {scan_number} / {nb_scans}\n")
        if self._loop == self.pars.start_nb - 1:
            estimated_end_time = self._estimate_end_time_series()
        else:
            estimated_end_time = self._real_scan_time / self._loop
            estimated_end_time *= nb_scans - self._loop
        estimated_end_time = estimated_end_time + time.time()
        enddatetime = str(datetime.datetime.fromtimestamp(estimated_end_time))
        info_str += BOLD(f"\n\nEstimated Series End Time: {enddatetime} \n")
        print(info_str)
