import os
import time
from bliss.common.scans import ascan
from bliss import current_session
from bliss import setup_globals
from bliss.common.cleanup import cleanup, axis as cleanup_axis, capture_exceptions
from bliss.common.logtools import log_info, log_debug, log_error, log_warning
from bliss.shell.standard import umv, mv
from fscan.fscantools import FScanParamBase
from fscan.mussttools import MusstDevList
from tomo.sequencebasic import SequenceBasic, ScanType
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.group import Sequence
from tomo.fulltomo import FullFieldTomo
from tomo.scan.presets.common_header import CommonHeaderPreset


class FastZSeries(FullFieldTomo):
    """
    Class to handle z series tomo acquisition

    Z axis moves from one specified step between half or full turn scans
    """

    def __init__(self, name, config):
        super().__init__(name, config)
        self.name: str = name + "_sequence"
        """Bliss object name"""
        self.start_pos: float = None
        """z axis start position"""
        self.delta_pos: float = 0.0
        """z axis step value"""
        self.nb_scans: int = 0
        """number or half/full turn scans to perform"""
        self.sleep: float = 0.0
        """sleep time between each scan"""
        self.start_nb: int = 1
        """number of first scan, used to know from which position to start"""

    def validate(self):
        """
        Adapt z axis start position according to start_nb value
        Check attributes value to avoid to hit z axis limits
        Calculate z axis positions according to delta_pos value
        Validate standard full field tomo parameters
        """
        # FullFieldTomo validate
        super().validate()

        if self.nb_scans < 2:
            raise ValueError("The minimum number of scans is 2!")

        if self.start_nb < 1 or self.start_nb > self.nb_scans:
            raise ValueError("Invalid start scan value!")

        # Calculate start position and number of scans when not starting from the first one
        if self.start_nb > 1:
            self.start_pos = self.start_pos + (self.start_nb - 1) * self.delta_pos
            self.nb_scans = self.nb_scans - (self.start_nb - 1)

        limit = min(self._z_axis.low_limit, self._z_axis.high_limit)
        if self.start_pos + (self.nb_scans - 1) * self.delta_pos < limit:
            raise ValueError(
                f"{self._z_axis.name} start position is beyond axis limit ({limit})! \nPlease change one of those parameters: start_pos, nb_scans or delta_pos"
            )
        limit = max(self._z_axis.low_limit, self._z_axis.high_limit)
        if self.start_pos + (self.nb_scans - 1) * self.delta_pos > limit:
            raise ValueError(
                f"{self._z_axis.name} end position is beyond axis limit ({limit})! \nPlease change one of those parameters: start_pos, nb_scans or delta_pos"
            )

    def add_metadata(self, scan_info):
        """
        Fill the scan_info dictionary with the basic tomo meta data
        Add specific sequence info to tomo meta data:
        - number of scans
        - scan number of first scan of the series
        """
        super().add_metadata(scan_info)
        scan_info["technique"]["scan"]["nb_scans"] = self.nb_scans
        scan_info["technique"]["scan"]["start_nb"] = self.start_nb

    def add_proj(
        self,
        tomo_start_pos=None,
        tomo_end_pos=None,
        tomo_n=None,
        expo_time=None,
        z_start_pos=None,
        z_step_size=None,
        z_npoints=None,
        proj_scan=None,
    ):
        """
        Set the runner that sequence must use to acquire projection
        images
        If proj_scan is not specified, default projection runner
        corresponding to scan type parameter will be used
        Add projection scan to the sequence
        Add image common header preset to projection scan
        """
        detector = self._detector
        header = {}
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)

        title = f"projections 0 - {int(tomo_n)}"
        self._inpars.projection = tomo_n

        if proj_scan is not None:
            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan(
                self._rotation_axis,
                tomo_start_pos,
                tomo_end_pos,
                tomo_n,
                self._z_axis,
                z_start_pos,
                z_step_size,
                z_npoints,
                expo_time,
                run=False,
            )
            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)

        proj_scan.scan_info.update({"title": title})

    def projection_scan(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        scan_info=None,
        run=True,
    ):
        """
        Get runner and build scan corresponding to tomo scan type parameter
        Return projection scan object
        """
        runner = self.get_runner("TOMO2D")

        latency_time = self.pars.latency_time
        tomo_scan = runner(
            fast_motor,
            fast_start_pos,
            fast_end_pos,
            fast_npoints,
            slow_motor,
            slow_start_pos,
            slow_step_size,
            slow_npoints,
            expo_time,
            latency_time=latency_time,
            scan_info=scan_info,
            run=False,
        )

        self._inpars.proj_time = runner._estimate_scan_duration(self._inpars)

        if run:
            tomo_scan.run()

        return tomo_scan

    def build_sequence(self):
        """
        Build fast zseries tomo sequence according to scan option
        parameters
        """
        if self.pars.dark_at_start:
            self.add_dark()
        if self.pars.flat_at_start:
            self.add_flat()

        # get the start position
        self.original_pos = self._z_axis.position
        if self.start_pos is None:
            self.start_pos = self.original_pos

        self.add_proj(
            self.pars.start_pos,
            self.pars.start_pos + self.pars.range,
            self.pars.tomo_n,
            self.pars.exposure_time,
            self.start_pos,
            self.delta_pos,
            self.nb_scans,
        )
        if self.pars.images_on_return:
            self.add_return()
        if self.pars.flat_at_end:
            self.add_flat()
        if self.pars.dark_at_end:
            self.add_dark()

    def full_turn_scan(
        self,
        delta_pos,
        nb_scans,
        sleep=0.0,
        start_pos=None,
        start_nb=1,
        dataset_name=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run tomo of 360 angular range
        """
        self.delta_pos = delta_pos
        self.nb_scans = nb_scans
        self.sleep = sleep
        self.start_pos = start_pos
        self.start_nb = start_nb

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)

        self.pars.range = 360

        self._setup_sequence("tomo:zseries", run=run, scan_info=scan_info)

    def half_turn_scan(
        self,
        delta_pos,
        nb_scans,
        sleep=0.0,
        start_pos=None,
        start_nb=1,
        dataset_name=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run tomo of 180 angular range
        """
        self.delta_pos = delta_pos
        self.nb_scans = nb_scans
        self.sleep = sleep
        self.start_pos = start_pos
        self.start_nb = start_nb

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)

        self.pars.range = 180

        self._setup_sequence("tomo:zseries", run=run, scan_info=scan_info)

    def basic_scan(
        self,
        delta_pos,
        nb_scans,
        tomo_start_pos,
        tomo_end_pos,
        tomo_n,
        expo_time,
        sleep=0.0,
        start_pos=None,
        start_nb=1,
        dataset_name=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run tomo using given parameters
        """
        self.delta_pos = delta_pos
        self.nb_scans = nb_scans
        self.sleep = sleep
        self.start_pos = start_pos
        self.start_nb = start_nb

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if tomo_start_pos is not None:
            self.pars.start_pos = tomo_start_pos
        if tomo_end_pos is not None:
            self.pars.range = self.pars.start_pos + tomo_end_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time

        self._setup_sequence("tomo:zseries", run=run, scan_info=scan_info)

    def __info__(self):
        """
        Show information about the sequence parameters, and detector optic configuration.
        """
        info_str = f"{self.name} configuration info:\n"
        info_str += super().__info__()
        return info_str
