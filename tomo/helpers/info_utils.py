"""Helper to format the `__info__` from device in consistent way."""

from __future__ import annotations
from tomo.helpers.axis_utils import is_user_clockwise
from bliss.common.axis import Axis


def format_device(device):
    if device is None:
        return "undefined"
    return f"-> {device.name:<12s}"


def format_active(string, is_active):
    if is_active:
        return f"* {string}"
    else:
        return f"  {string}"


def format_axis_position(axis):
    if axis is None:
        return ""
    position = axis.position
    return f"{axis.axis_rounder(position)} {axis.unit}"


def format_axis_absolute_position(axis: Axis | None, position: float | None):
    if axis is None or position is None:
        return ""
    return f"{axis.axis_rounder(position)} {axis.unit}"


def format_axis_displacement(
    axis: Axis | None, position: float | None, prefix: str = ""
):
    if axis is None or position is None:
        return ""
    pos = axis.axis_rounder(position)
    if not pos.startswith("-"):
        pos = f"+{pos}"
    return f"{prefix}{pos} {axis.unit}"


def format_quantity(value, unit):
    if value is None:
        return "undefined"
    return f"{value:.3f} {unit}"


def format_magnification(optic):
    if optic is None:
        return "undefined"
    return f"x{optic.magnification:.3f}"


def format_direction(axis):
    clockwise = is_user_clockwise(axis)
    return "clockwise" if clockwise else "anticlockwise"
