from __future__ import annotations
from typing import Any, Generator
from fscan.fscantools import FScanParamBase


class ProxyParam:
    """Proxy over a list of parameters"""

    def __init__(self, *parameters: FScanParamBase | ProxyParam):
        self.__parameters: tuple[FScanParamBase | ProxyParam, ...] = parameters

    @property
    def _name(self) -> str:
        """Mimick ._name with a composite name"""
        names = [p._name for p in self._iter_params_recursive()]
        return ", ".join(names)

    def reset(self):
        """
        Reset the parameters to the default values.

        Inherited from FScanParamBase.
        """
        for p in self._iter_params_recursive():
            p.reset()

    def to_dict(self, string_format: bool = False) -> dict[str, Any]:
        """
        Returns the parameters as a dict.

        Inherited from FScanParamBase.
        """
        result: dict[str, Any] = {}
        for p in self._iter_params_recursive():
            result.update(p.to_dict(string_format=string_format))
        return result

    def from_dict(self, pars_dict: dict[str, Any], string_format: bool = False):
        """
        Update the parameters from a dict.

        Inherited from FScanParamBase.

        Raises:
            KeyError: If one of the input keys is not part of any parameters
        """
        keys = set(pars_dict.keys())
        for p in self._iter_params_recursive():
            p_dict: dict[str, Any] = {}
            for k, v in pars_dict.items():
                if hasattr(p, k):
                    p_dict[k] = v
                    keys.discard(k)
            if p_dict != {}:
                p.from_dict(p_dict, string_format=string_format)
        if len(keys) != 0:
            raise KeyError(f"None of {self._name} contains keys: {', '.join(keys)}")

    def __dir__(self):
        result = []
        for p in self._iter_params_recursive():
            result += p._par_keys + p._list_keys
        result += ["to_dict", "from_dict", "reset"]
        # This is the remaining unimplemented API from FScanParamBase
        # result += [
        #     "show",
        #     "set",
        #     "get",
        #     "to_file",
        #     "from_file",
        # ]
        return result

    def _iter_params_recursive(self) -> Generator[FScanParamBase, None, None]:
        """Iterate every referenced FScanParamBase recursivelly.

        It includes the ones from contained ProxyParam.
        """
        for p in self.__parameters:
            if isinstance(p, ProxyParam):
                for p2 in p._iter_params_recursive():
                    yield p2
            else:
                yield p

    def __get_parameter_with_attr(self, name: str) -> FScanParamBase:
        for p in self._iter_params_recursive():
            if hasattr(p, name):
                return p
        raise KeyError(f"None of {self._name} have parameter [{name}]")

    def __getattr__(self, name: str):
        try:
            p = self.__get_parameter_with_attr(name)
        except KeyError as e:
            raise AttributeError(e.args[0])
        return getattr(p, name)

    def __setattr__(self, name: str, value: Any):
        if name.startswith("_"):
            super().__setattr__(name, value)
            return
        p = self.__get_parameter_with_attr(name)
        p.__setattr__(name, value)

    def __delattr__(self, name: str):
        if name.startswith("_"):
            super().__delattr__(name)
            return
        p = self.__get_parameter_with_attr(name)
        p.__delattr__(name)

    def __info__(self):
        result = "Parameters\n\n"
        for p in self._iter_params_recursive():
            result += f"  {type(p).__name__:<15s} [redis-key={p._name}]\n\n"
            key_len = max((0,) + tuple(len(x) for x in p._cache.keys()))
            for key, val in p._cache.items():
                result += "    .{0:<{1}s} = {2}\n".format(
                    key, key_len, p._FScanParamBase__obj_2_str(key, val)
                )
            result += "\n"
        return result
