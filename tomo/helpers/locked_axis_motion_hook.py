from __future__ import annotations

import os
import typing
import contextlib
from bliss.common.hook import MotionHook
from bliss.common.axis import Axis, Motion
from bliss.config.conductor.connection import Connection
from tomo.helpers.locking_helper import lock, unlock, AlreadyLockedDevices


class LockedAxisMotionHook(MotionHook):

    def __init__(self, name: str, config: dict[str, typing.Any]):
        MotionHook.__init__(self)
        self.name = name
        self._connection: Connection | None = None
        self._enabled = True

    @property
    def enabled(self) -> bool:
        return self._enabled

    @enabled.setter
    def enabled(self, enabled: bool):
        self._enabled = enabled

    @contextlib.contextmanager
    def disabled_context(self):
        """Disable the motion hook during a context"""
        prev = self.enabled
        self.enabled = False
        try:
            yield
        finally:
            self.enabled = prev

    def _devices(self, motion_list: list[Motion]) -> list[Axis]:
        return [m.axis for m in motion_list]

    def pre_move(self, motion_list: list[Motion]):
        if not self._enabled:
            return
        if self._connection is None:
            self._connection = Connection()
            self._connection.set_client_name(f"{self.name},pid:{os.getpid()}")
        try:
            lock(*self._devices(motion_list), connection=self._connection, timeout=3)
        except AlreadyLockedDevices:
            self._connection.close()
            self._connection = None
            raise

    def post_move(self, motion_list: list[Motion]):
        if self._connection is not None:
            try:
                unlock(*self._devices(motion_list), timeout=3)
            finally:
                self._connection.close()
                self._connection = None


def expect_locked_axis_motion_hook(axis: Axis) -> LockedAxisMotionHook:
    """Make sure a LockedAxisMotionHook is set into this axis.

    If there is None, a new one is created, else the first found
    is returned.
    """
    for mh in axis.motion_hooks:
        if isinstance(mh, LockedAxisMotionHook):
            return mh
    else:
        mh = LockedAxisMotionHook(f"{axis.name}_move", {})
        axis.motion_hooks.append(mh)
        return mh
