from __future__ import annotations
from bliss.common import event


class UserSamplePixelSize:
    """
    Compute a user pixel size from unbinned user pixel size and
    camera information.
    """

    def __init__(self):
        self._camera_binning: tuple[int, int] | None = None
        self._unbinned_sample_pixel_size: float | None = None
        self._sample_pixel_size: float | None = None

    @property
    def camera_binning(self) -> tuple[int, int] | None:
        """Camera binning"""
        return self._camera_binning

    @camera_binning.setter
    def camera_binning(self, value: tuple[int, int] | None):
        if self._camera_binning == value:
            return
        self._camera_binning = value
        self._update_sample_pixel_size()

    @property
    def unbinned_sample_pixel_size(self) -> float | None:
        return self._unbinned_sample_pixel_size

    @unbinned_sample_pixel_size.setter
    def unbinned_sample_pixel_size(self, value: int | None):
        if self._unbinned_sample_pixel_size == value:
            return
        self._unbinned_sample_pixel_size = value
        self._update_sample_pixel_size()

    @property
    def sample_pixel_size(self):
        """Returns the user pixel size"""
        return self._sample_pixel_size

    @sample_pixel_size.setter
    def sample_pixel_size(self, value: float | None):
        """Set the user pixel size by changing the unbinned sample pixel size"""
        """Find the unbinned pixel size from a sample pixel size"""
        if value is None:
            self.unbinned_sample_pixel_size = None
            return
        b = self._camera_binning
        if b is None:
            b = (1, 1)
        unbinned = value / b[0]
        self.unbinned_sample_pixel_size = unbinned

    def _update_sample_pixel_size(self):
        """Update the sample pixel size from the known state"""
        spx = self._compute_sample_pixel_size()
        self._set_sample_pixel_size(spx)

    def _compute_sample_pixel_size(self) -> float | None:
        """Compute the sample pixel size from the known state"""
        ps = self._unbinned_sample_pixel_size
        if ps is None:
            return None
        b = self._camera_binning
        if b is None:
            b = (1, 1)
        return ps * b[0]

    def _set_sample_pixel_size(self, value: float | None):
        """Set a sample pixel size and send event.

        Update is cancelled if the new value is
        the same as the previous one.
        """
        if self._sample_pixel_size == value:
            return
        self._sample_pixel_size = value
        event.send(self, "sample_pixel_size", value)

    def _asdict(self):
        return {
            "camera_binning": self._camera_binning,
            "unbinned_sample_pixel_size": self.unbinned_sample_pixel_size,
            "sample_pixel_size": self._sample_pixel_size,
        }

    def __info__(self):
        return "\n".join([f"{k}: {v}" for k, v in self._asdict().items()])

    def __str__(self):
        return str(self._asdict())
