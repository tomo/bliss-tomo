class SinogramReader:
    """Connector to a master HDF5 file containing a sinogram.

    Allows to retrieve the actual available size of the sinogram.
    """

    def __init__(self, h5, sequence_path):
        assert h5.attrs["NX_class"] == "NXroot", "File is not a nexus file"
        assert h5.attrs["creator"] in [
            "Bliss",
            "blissdata",
        ], "File was not created by BLISS"

        sequence_group = h5[sequence_path]
        rotation = sequence_group["measurement/rotation"]
        translation = sequence_group["measurement/translation"]
        sinogram = sequence_group["measurement/sinogram"]
        proj_spectrum = sequence_group["measurement/proj_spectrum"]
        shape = proj_spectrum.shape

        self.rotation_shape = shape[0]
        self.translation_shape = shape[1]

        # Clamp to the max available size
        size = min(len(rotation), len(translation), len(sinogram))
        # Clamp to a fixed translation number
        size = size - size % self.translation_shape

        self.rotation = rotation[0:size]
        self.translation = translation[0:size]
        self.sinogram = sinogram[0:size]
