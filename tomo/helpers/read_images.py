"""
Helper to read data.

"""

from __future__ import annotations

import numpy
import typing
from tomo.chain_presets.image_no_saving import ImageNoSavingPreset

if typing.TYPE_CHECKING:
    from bliss.controllers.lima.lima_base import Lima
    from bliss.scanning.scan import Scan


def read_images(scan: Scan, detector: Lima) -> list[numpy.ndarray]:
    """Fetch detector image from a scan.

    If the scan was not saved, the helper can fetch the images
    from a `ImageNoSavingPreset` if the chain was setup with.
    """
    if scan.scan_info["save"]:
        lima_data = scan.streams[detector.image]
        return lima_data[:]

    chain_presets = next(scan.acq_chain._presets_master_list.values())
    for preset in chain_presets:
        if isinstance(preset, ImageNoSavingPreset):
            return preset.get_images()

    raise RuntimeError("Unexpected scan and detector. No data to read.")
