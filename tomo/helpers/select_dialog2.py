# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Module providing dialogs to interact with the user"""

import functools
import typing
import contextlib
import subprocess
import os

from prompt_toolkit.application.current import get_app

from prompt_toolkit.key_binding.key_bindings import KeyBindings
from prompt_toolkit.layout import containers
from prompt_toolkit.layout.containers import HSplit, VSplit
from prompt_toolkit.widgets import Dialog, Button, Label, Box
from prompt_toolkit.key_binding.bindings.focus import focus_next, focus_previous

from bliss.shell.cli.pt_widgets import _create_app, _ESRF_STYLE
from bliss.common.greenlet_utils import asyncio_gevent


@contextlib.contextmanager
def disabled_tmux_mouse():
    use_tmux = "TMUX" in os.environ
    try:
        if use_tmux:
            subprocess.run(["tmux", "set-option", "-g", "mouse", "off"])
    except Exception:
        pass
    try:
        yield
    finally:
        try:
            if use_tmux:
                subprocess.run(["tmux", "set-option", "-g", "mouse", "on"])
        except Exception:
            pass


def _run_dialog(dialog, style, extra_bindings=None, full_screen=True, focus=None):
    """
    Re-implementation of `bliss.shell.cli.pt_widgets._run_dialog` to provide an
    extra focus.

    FIXME: Remove it, use the one from BLISS when it is possible
    """
    with disabled_tmux_mouse():
        application = _create_app(dialog, style, extra_bindings, full_screen)
        if focus is not None:
            application.layout.focus(focus)
        return asyncio_gevent.yield_future(application.run_async())


def select_dialog2(
    title: typing.Optional[str] = None,
    text: str = "",
    ok_text: typing.Optional[str] = None,
    cancel_text: typing.Optional[str] = "Cancel",
    values=None,
    selection: str = None,
    style=None,
    full_screen=True,
):
    """
    Display a dialog with button choices (given as a list of tuples).

    Each element of the `values` list can be of the template

    - `str` -> Used as a label and the returned key
    - "" -> Append an empty row
    - `[str]` -> Used as a label and the returned key
    - `[key, label]` -> If the `value` is `None` the field is hidden

    Arguments:
        values: List of field to display
        selection: Default key or label selected

    Returns:
        The selected key or label else None if the dialog was cancelled. If a
        `ok_text` is not None, the dialog also can return `True`.
    """
    if title is None:
        title = "Select dialog"

    def button_handler(key, foo=None):
        get_app().exit(result=key)

    key_bindings = KeyBindings()
    key_bindings.add("up")(focus_previous)
    key_bindings.add("down")(focus_next)

    normalized_values = []
    for value in values:
        if isinstance(value, (tuple, list)):
            if len(value) == 1:
                normalized_values.append((value[0], value[0].capitalize()))
            if len(value) == 2:
                normalized_values.append((value[0], value[1]))
        else:
            if value == "":
                normalized_values.append(("", ""))
            else:
                label = value
                normalized_values.append((label, label.capitalize()))

    max_size = max([len(str(d[1])) for d in normalized_values])

    rows = []
    focus = None
    key_index = 1
    for key, label in normalized_values:
        on_activate = functools.partial(button_handler, key)

        if key == "":
            row = Label("")
        else:
            if label is not None:
                label_button = str(label)
                if key_index <= 9:
                    key_bindings.add(f"{key_index}")(on_activate)
                    num = f"{key_index}."
                else:
                    num = "#."
                key_index += 1

                button = Button(
                    text=label_button, handler=on_activate, width=len(label_button) + 4
                )
                if key == selection:
                    focus = button

                num_label = Label(num)
                empty = containers.Window(
                    style="class:frame.border",
                    width=max_size - len(label_button),
                    height=1,
                )
                row = VSplit(
                    [num_label, button, empty], align=containers.HorizontalAlign.LEFT
                )
            else:
                row = Label("")
                key_index = key_index + 1

        rows.append(row)

    body = HSplit(rows, key_bindings=key_bindings)

    buttons = []
    if ok_text is not None:
        on_activate = functools.partial(button_handler, True)
        ok = Button(text=ok_text, handler=on_activate)
        buttons.append(ok)

    if cancel_text is not None:
        on_activate = functools.partial(button_handler, None)
        cancel = Button(text=f"0. {cancel_text}", handler=on_activate)
        key_bindings.add(f"escape")(on_activate)
        key_bindings.add(f"0")(on_activate)
        buttons.append(cancel)

    body = Box(body)

    dialog = Dialog(
        title=title,
        body=body,
        buttons=buttons,
        with_background=True,
    )

    if style is None:
        style = _ESRF_STYLE
    return _run_dialog(
        dialog, style, full_screen=full_screen, focus=focus, extra_bindings=key_bindings
    )
