from __future__ import annotations
import numpy


def normalize_image(image: numpy.ndarray, axes: tuple[str, str]) -> numpy.ndarray:
    """
    Normalize the image as a ("-z", "y") to be displayed with a top-left origin
    """
    if axes[0][-1] == "y":
        image = numpy.swapaxes(image, 0, 1)
        axis1, axis0 = axes
    else:
        axis0, axis1 = axes
    if axis0 == "z":
        image = numpy.flip(image, axis=0)
    elif axis0 != "-z":
        raise ValueError(f"Unsupported axes '{axes}'")
    if axis1 == "-y":
        image = numpy.flip(image, axis=1)
    elif axis1 != "y":
        raise ValueError(f"Unsupported axes '{axes}'")
    return image
