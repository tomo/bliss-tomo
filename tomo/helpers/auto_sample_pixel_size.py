from __future__ import annotations
from bliss.common import event


class AutoSamplePixelSize:
    """Compute a sample pixel size from distance, detector and optic
    information.

    - camera binning
    - camera pixel size
    - sample distance from the focal point
    - detecor distance from the focal point
    - magnification
    """

    def __init__(self):
        self._camera_binning: tuple[int, int] | None = None
        self._camera_pixel_size: tuple[float, float] | None = None
        self._sample_distance: float | None = None
        self._detector_distance: float | None = None
        self._distance_coef: float | None = None
        """Ratio between sample and detector distances"""
        self._magnification: float | None = None
        self._use_magnification: bool | None = None
        self._sample_pixel_size: float | None = None

    @property
    def camera_binning(self) -> tuple[int, int] | None:
        """Camera binning"""
        return self._camera_binning

    @camera_binning.setter
    def camera_binning(self, value: tuple[int, int] | None):
        if self._camera_binning == value:
            return
        self._camera_binning = value
        self._update_sample_pixel_size()

    @property
    def camera_pixel_size(self) -> tuple[float, float] | None:
        """Camera pixel size, in micro meter"""
        return self._camera_pixel_size

    @camera_pixel_size.setter
    def camera_pixel_size(self, value: tuple[float, float] | None):
        if self._camera_pixel_size == value:
            return
        self._camera_pixel_size = value
        self._update_sample_pixel_size()

    @property
    def sample_distance(self) -> float | None:
        """Sample distance from the focal point, in milimeter"""
        return self._sample_distance

    @sample_distance.setter
    def sample_distance(self, value: float | None):
        if self._sample_distance == value:
            return
        self._sample_distance = value
        self._update_distance_coef()

    @property
    def detector_distance(self) -> float | None:
        """Detector distance from the focal point, in milimeter"""
        return self._sample_distance

    @detector_distance.setter
    def detector_distance(self, value: float | None):
        if self._detector_distance == value:
            return
        self._detector_distance = value
        self._update_distance_coef()

    @property
    def magnification(self) -> float | None:
        """Detector distance from the focal point"""
        return self._magnification

    @magnification.setter
    def magnification(self, value: float | None):
        if self._magnification == value:
            return
        self._magnification = value
        self._update_sample_pixel_size()

    @property
    def use_magnification(self) -> bool | None:
        """Detector distance from the focal point"""
        return self._use_magnification

    @use_magnification.setter
    def use_magnification(self, value: bool | None):
        if self._use_magnification == value:
            return
        self._use_magnification = value
        self._update_sample_pixel_size()

    @property
    def sample_pixel_size(self):
        """Returns the user pixel size"""
        return self._sample_pixel_size

    def _update_distance_coef(self):
        ss = self._sample_distance
        sd = self._detector_distance
        if ss is None or sd is None:
            distance_coef = None
        else:
            distance_coef = ss / sd
        if self._distance_coef == distance_coef:
            return
        self._distance_coef = distance_coef
        self._update_sample_pixel_size()

    def _update_sample_pixel_size(self):
        spx = self._compute_sample_pixel_size()
        self._set_sample_pixel_size(spx)

    def _compute_sample_pixel_size(self) -> float | None:
        px = self._camera_pixel_size
        dist_coef = self._distance_coef
        use_mag = self._use_magnification
        if px is None or dist_coef is None or use_mag is None:
            return None
        if use_mag:
            mag = self._magnification
            if mag is None:
                return None
        else:
            mag = 1.0
        b = self._camera_binning
        if b is None:
            b = 1, 1
        try:
            return px[0] * b[0] / mag * dist_coef
        except ZeroDivisionError:
            return None

    def _set_sample_pixel_size(self, value: float | None):
        if self._sample_pixel_size == value:
            return
        self._sample_pixel_size = value
        event.send(self, "sample_pixel_size", value)

    def _asdict(self):
        return {
            "camera_binning": self._camera_binning,
            "camera_pixel_size": self._camera_pixel_size,
            "sample_distance": self._sample_distance,
            "detector_distance": self._detector_distance,
            "distance_coef": self._distance_coef,
            "magnification": self._magnification,
            "use_magnification": self._use_magnification,
            "sample_pixel_size": self._sample_pixel_size,
        }

    def __info__(self):
        return "\n".join([f"{k}: {v}" for k, v in self._asdict().items()])

    def __str__(self):
        return str(self._asdict())
