# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
from bliss.shell.cli.user_dialog import (
    Validator,
    UserInput,
)


def float_or_empty(v: str) -> float | None:
    v = v.strip(" ")
    if v == "":
        return None
    return float(v)


_optional_float_validator = Validator(float_or_empty)


class UserOptionalFloatInput(UserInput):
    """Ask the user to enter/type a float value or an empty string"""

    def __init__(
        self, name=None, label="", defval=0.0, text_align=None, text_expand=False
    ):
        super().__init__(
            name=name,
            label=label,
            defval=defval,
            validator=_optional_float_validator,
            text_align=text_align,
            text_expand=text_expand,
        )

    def input_to_value(self, value: str) -> float | None:
        return float_or_empty(value)
