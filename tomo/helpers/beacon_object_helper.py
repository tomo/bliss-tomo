from __future__ import annotations
import typing
import logging
from bliss.common import event
from bliss.common.logtools import log_error


_logger = logging.getLogger(__name__)


class SyncProperties:
    """Synchronize 2 BeaconObject properties.

    This can be useful for transition between implementation.

    Or to create kind of alias, but in this case, it's really sub optimal.
    """

    def __init__(self, obj1, prop1, obj2, prop2):
        setattr(obj1, prop1, getattr(obj2, prop2))

        def prop1_updated(value):
            old = getattr(obj2, prop2)
            if old == value:
                return
            setattr(obj2, prop2, value)

        def prop2_updated(value):
            old = getattr(obj1, prop1)
            if old == value:
                return
            setattr(obj1, prop1, value)

        event.connect(obj1, prop1, prop1_updated)
        event.connect(obj2, prop2, prop2_updated)

        self.__prop1_updated = prop1_updated
        self.__prop2_updated = prop2_updated


def check_unexpected_keys(
    obj: typing.Any, config: dict[str, typing.Any], expected_keys: list[str]
):
    """Raise an exception if keys from the configuration are not expected"""
    bliss_keys = set(
        [  # BLISS related keys
            "name",
            "package",
            "module",
            "class",
            "plugin",
        ]
    )
    expected_keys2 = set(expected_keys)
    unexpected_keys = (set(config.keys()) - expected_keys2) - bliss_keys
    if len(unexpected_keys) > 0:
        for k in unexpected_keys:
            log_error(obj, "key %s is not supported", k)
        keys = "', '".join(unexpected_keys)
        raise RuntimeError(
            f"Keys '{keys}' are not supported by {obj.name} ({type(obj).__qualname__})"
        )
