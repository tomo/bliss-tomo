def memory_size(size):
    if size < 1024 * 1024:
        return size, "B"
    size /= 1024 * 1024
    unit = "MB"
    if size >= 1024:
        size /= 1024
        unit = "GB"
    return size, unit
