import weakref
from bliss.common import event
from bliss.common.hook import MotionHook


class SampleStageMotionHook(MotionHook):
    """
    Hook triggered by move action.

    When a motor from the TomoConfig is about to move, `TomoConfig` send an
    event `is_moving` with a True parameter. This event is emitted at the move
    termination with a False parameter.

    It is not triggered by motion events, as result, the BLISS process which
    have initialized the action is triggered.

    This behavior is used to avoid to execute a new CT from 2 different place at
    the same time when multiple process on the same session is used.
    """

    def __init__(self, tomoconfig, config):
        super().__init__()
        assert tomoconfig is not None
        self.__tomoconfig = weakref.ref(tomoconfig)
        self._is_moving = False
        self._setup_hooks_on_tomoconfig(tomoconfig, config)
        self.__event_sender = None

    def set_event_sender(self, sender):
        """Set the `sender` for the event.

        Should only be called at the initialization.
        """
        assert self.__event_sender is None
        self.__event_sender = weakref.ref(sender)

    def _setup_hooks_on_tomoconfig(self, tomoconfig, config):
        extra_motors = [m for m in config.get("extra_motors", [])]
        sample_stage = tomoconfig.sample_stage
        axes = [
            sample_stage.x_axis,
            sample_stage.y_axis,
            sample_stage.z_axis,
            sample_stage.rotation_axis,
            sample_stage.sample_u_axis,
            sample_stage.sample_v_axis,
            # sample_stage.sample_x_axis,  # pseudo, duplication of real motors
            # sample_stage.sample_y_axis,  # pseudo, duplication of real motors
            *extra_motors,
        ]

        for axis in axes:
            if axis:
                axis.motion_hooks.append(self)
                self._add_axis(axis)

        # It's not axis, but it also implement hooks
        detectors = tomoconfig.detectors
        if detectors:
            detectors.motion_hooks.append(self)
            for detector in detectors.detectors:
                optic = detector.optic
                if optic:
                    optic.motion_hooks.append(self)

        pusher = sample_stage.pusher
        if pusher is not None:
            pusher.motion_hooks.append(self)

        air_bearing_x = sample_stage.air_bearing_x
        if air_bearing_x is not None:
            air_bearing_x.motion_hooks.append(self)

    @property
    def _tomoconfig(self):
        # dereference the weakref
        return self.__tomoconfig()

    def _is_active(self, motion_list):
        from tomo.optic.base_optic import BaseOptic

        tc = self._tomoconfig
        for m in motion_list:
            if isinstance(m, BaseOptic):
                # Make sure the optic is part of the active detector
                active_detector = tc.detectors.active_detector
                if active_detector and active_detector.optic is m:
                    return True
            else:
                return True
        # Empty list can be skipped
        return False

    @property
    def is_moving(self):
        return self._is_moving

    @property
    def _event_sender(self):
        sender = self.__event_sender
        if sender is None:
            return self
        return sender()

    def pre_move(self, motion_list):
        if not self._is_active(motion_list):
            return
        self._is_moving = True
        sender = self._event_sender
        event.send(sender, "is_moving", True)

    def post_move(self, motion_list):
        if not self._is_moving:
            return
        self._is_moving = False
        sender = self._event_sender
        event.send(sender, "is_moving", False)
