# Compatibility with tomo <= 2.3.1

from tomo.controllers.tomo_imaging import TomoImaging  # noqa
from tomo.controllers.tomo_imaging import TomoImagingState  # noqa
from bliss.common import deprecation

deprecation.deprecated_warning(
    "Module",
    "tomo.tomo_imaging",
    replacement="tomo.controllers.tomo_imaging",
    since_version="2.3.1",
)
