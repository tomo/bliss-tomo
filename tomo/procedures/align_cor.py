from __future__ import annotations
import typing
import logging
import numpy
import contextlib
from typing_extensions import TypedDict
from .base_procedure import SessionProcedure
from tomo.globals import get_active_tomo_config
from bliss.shell.standard import umv, umvr, ct, pointscan
from tomo.helpers.read_images import read_images
from bliss.shell.standard import print_html
from bliss.shell.getval import getval_yes_no
from bliss.common.cleanup import (
    cleanup,
    capture_exceptions,
    axis as cleanup_axis,
)
from tomo.helpers.shell_utils import is_in_shell
from bliss.scanning.scan_sequence import ScanSequence
from bliss.scanning.chain import AcquisitionChannel
from silx.math.combo import min_max
from bliss.common.scans import DEFAULT_CHAIN
from tomo.chain_presets.image_no_saving import use_image_no_saving
from tomo.helpers import flatfield_correction
from tomo.helpers.beacon_object_helper import check_unexpected_keys
from tomo.helpers.tomo_default_chain import TomoDefaultChain
from tomo.helpers.locking_helper import lock_context

if typing.TYPE_CHECKING:
    from tomo.controllers.lima.lima_base import Lima
    from tomo.common.axis import Axis

_logger = logging.getLogger(__name__)

try:
    import nabu
    from nabu.estimation.tilt import CameraTilt
    from nabu.estimation import cor as nabu_cors
except ImportError:
    _logger.error("Error while importing nabu", exc_info=True)
    nabu = None


class MockedCenterOfRotation:
    def __init__(self, verbose: bool = False):
        pass

    def find_shift(self, *args, **kwargs):
        return 10.5


_COR_CALCULATORS = {"mock": MockedCenterOfRotation}


def register_nabu_cor_calculator(method, class_name):
    try:
        _COR_CALCULATORS[method] = getattr(nabu_cors, class_name)
    except Exception:
        _logger.error(
            "Method '%s' can't be registred. Nabu class nabu.estimation.cor.%s is missing",
            method,
            class_name,
        )


# All of the CoR estimators have the same API for the basic parameters
register_nabu_cor_calculator("simple", "CenterOfRotation")
register_nabu_cor_calculator("sliding-window", "CenterOfRotationSlidingWindow")
register_nabu_cor_calculator("growing-window", "CenterOfRotationGrowingWindow")
register_nabu_cor_calculator("global-search", "CenterOfRotationAdaptiveSearch")
register_nabu_cor_calculator("octave-accurate", "CenterOfRotationOctaveAccurate")


class AlignCorParameters(TypedDict, total=True):
    """Schema of what will be stored in procedure"""

    lateral_motor: Axis
    """Motor controlling the translation under the rotation"""

    pixel_size_mm: float
    """Pixel size of the acquisition"""

    nabu_method: str
    """Method used by nabu"""

    nabu_lateral_corr: float | None
    """Translation for the motor under the rotation, in mm"""

    nabu_camera_tilt: float | None
    """Rotation for the tilt of the detector, in deg"""

    nabu_exception: Exception
    """Exception occured during nabu processing"""

    lateral_corr: float | None
    """Translation for the motor under the rotation, in mm"""

    camera_tilt: float | None
    """Rotation for the tilt of the detector, in deg"""

    camera_tilt_motor: Axis | None
    """Motor controlling the detector tilt"""

    detector_channel: str
    """Identify the location of the data inside the scans"""


class AlignCor(SessionProcedure[AlignCorParameters]):
    """
    Common alignement procedure.

    It allows to improve axis rotation location and fix the detector tilt.

    It can be setup the following way.

    .. code-block:: yaml

        - name: my_align
          plugin: bliss
          class: AlignCor
          package: tomo.procedures.align_cor

          # Method to extract the CoR correction
          # For center of rotation and tilt, one of
          # (see http://www.silx.org/pub/nabu/doc/apidoc/nabu.estimation.tilt.html)
          # - `1d-correlation`: Correction for the CoR and detector tilt
          # - `fft-polar`: Correction for the CoR and detector tilt
          # For center of rotation only, one of
          # (see http://www.silx.org/pub/nabu/doc/apidoc/nabu.estimation.cor.html)
          # - `simple`
          # - `sliding-window`
          # - `growing-window`
          # - `global-search`
          # - `octave-accurate`
          method: '1d-correlation'

          # Takes this amount of flat during the procedure
          flat_n: 3
          # Takes this amount of dark during the procedure
          dark_n: 3
          # Takes this amount of proj for each angle during the procedure
          proj_n: 3

          # If true (default) move back the rotation at it's original location
          # after having taken the projections
          return: true

          # Bahaviour for the acquisition of the projection angle
          # Supports the following:
          # - `absolute`: the default, always take proj at 0 and 180 degrees
          # - `relative`: always take the actual position (p)
          #               and p-180 or p+180 depending on the rotation limits
          behaviour: absolute

          # If true, set the lateral position to 0 after the correction
          reset_position: false
    """

    def __init__(self, name: str, config: dict):
        SessionProcedure.__init__(self, name, config)
        self._method = config.get("method", "1d-correlation")
        check_unexpected_keys(
            self,
            config,
            [
                "flat_n",
                "dark_n",
                "proj_n",
                "method",
                "return",
                "behaviour",
                "save",
                "reset_position",
            ],
        )
        self._flat_n: int = config.get("flat_n", 1)
        self._dark_n: int = config.get("dark_n", 1)
        self._proj_n: int = config.get("proj_n", 1)
        self._return: bool = config.get("return", True)
        self._behaviour = config.get("behaviour", "absolute")
        self._save: bool = config.get("save", False)
        self._reset_position: bool = config.get("reset_position", False)

    def _run(self):
        tomo_config = get_active_tomo_config()
        tomo_detector = tomo_config.detectors.active_detector
        if tomo_detector is None:
            raise RuntimeError("No detector active yet")

        with lock_context(tomo_detector.detector, owner=self.name):
            with tomo_config.auto_projection.inhibit():
                self.__run(tomo_config)

    def __run(self, tomo_config):
        exposure_time = tomo_config.pars.exposure_time
        tomo_detector = tomo_config.detectors.active_detector
        if tomo_detector is None:
            raise RuntimeError("No detector active yet")

        detector = tomo_detector.detector
        optic = tomo_detector.optic
        save = self._save
        rotation_axis = tomo_config.rotation_axis
        lateral_motor = tomo_config.y_axis
        try:
            camera_tilt_motor = optic.rotc_motor
        except Exception:
            camera_tilt_motor = None
            print("No motor for the tilt correction")

        self.update_parameters(
            lateral_motor=lateral_motor,
            camera_tilt_motor=camera_tilt_motor,
            detector_channel=f"{detector.name}:{detector.image.name}",
        )

        if self._behaviour == "absolute":
            # move rotation axis to 0
            umv(rotation_axis, 0)

        # take flat image
        flat_runner = tomo_config.get_runner("flat")
        with flat_runner.disabled_locking_detector_context():
            with use_image_no_saving(DEFAULT_CHAIN, detector, enabled=not save):
                flat_scan = flat_runner(
                    exposure_time,
                    self._flat_n,
                    detector,
                    projection=1,
                    save=save,
                )
        self.update_parameters(flat_scan=flat_scan)
        flat_images = read_images(flat_scan, detector)
        flat_image = flatfield_correction.compute_flat(flat_images, inplace=True)

        # take dark image
        dark_runner = tomo_config.get_runner("dark")
        with dark_runner.disabled_locking_detector_context():
            with use_image_no_saving(DEFAULT_CHAIN, detector, enabled=not save):
                dark_scan = dark_runner(
                    exposure_time,
                    self._dark_n,
                    detector,
                    save=save,
                )

        # read back the aquired images
        self.update_parameters(dark_scan=dark_scan)
        dark_images = read_images(dark_scan, detector)
        dark_image = flatfield_correction.compute_dark(dark_images)

        # pixel size in mm
        pixel_size = tomo_detector.sample_pixel_size / 1000.0

        # do alignment scan
        al_scan = self._align_scan(
            rotation_axis,
            exposure_time,
            detector,
            save=save,
        )
        self.update_parameters(proj_scan=al_scan)

        # read back the aquired images
        images = read_images(al_scan, detector)
        proj_0 = flatfield_correction.compute_proj(
            images[0 : self._proj_n], inplace=True
        )
        proj_180 = flatfield_correction.compute_proj(
            images[self._proj_n :], inplace=True
        )

        def flatfield(proj: numpy.ndarray, dark: numpy.ndarray, flat: numpy.ndarray):
            proj = proj.astype(numpy.float32)
            dark = dark.astype(numpy.float32)
            flat = flat.astype(numpy.float32)
            with numpy.errstate(divide="ignore", invalid="ignore"):
                proj = (proj - dark) / (flat - dark)
            # Clean up dark bigger than proj
            proj[proj < 0] = 0
            # Clean up division by zero
            proj[numpy.isnan(proj)] = 0
            return proj

        proj0 = flatfield(proj_0, dark_image, flat_image)
        proj180 = flatfield(proj_180, dark_image, flat_image)
        proj0_state = min_max(proj0, min_positive=True, finite=True)
        proj180_state = min_max(proj180, min_positive=True, finite=True)

        def runner(context: SequenceContext):
            context.emit("proj0", [numpy.float32(proj0)])
            context.emit("proj0_min", [numpy.float32(proj0_state.minimum)])
            context.emit("proj0_max", [numpy.float32(proj0_state.maximum)])
            context.emit("proj0_mean", [numpy.float32(numpy.nanmean(proj0))])
            context.emit("proj0_std", [numpy.float32(numpy.nanstd(proj0))])
            context.emit(
                "proj0_min_positive", [numpy.float32(proj0_state.min_positive)]
            )
            context.emit("proj180", [numpy.float32(proj180)])
            context.emit("proj180_min", [numpy.float32(proj180_state.minimum)])
            context.emit("proj180_max", [numpy.float32(proj180_state.maximum)])
            context.emit("proj180_mean", [numpy.float32(numpy.nanmean(proj180))])
            context.emit("proj180_std", [numpy.float32(numpy.nanstd(proj180))])
            context.emit(
                "proj180_min_positive", [numpy.float32(proj180_state.min_positive)]
            )

        shape = detector.image.height, detector.image.width
        channels = [
            AcquisitionChannel("proj0", numpy.float32, shape),
            AcquisitionChannel("proj0_min", numpy.float32, tuple()),
            AcquisitionChannel("proj0_max", numpy.float32, tuple()),
            AcquisitionChannel("proj0_mean", numpy.float32, tuple()),
            AcquisitionChannel("proj0_std", numpy.float32, tuple()),
            AcquisitionChannel("proj0_min_positive", numpy.float32, tuple()),
            AcquisitionChannel("proj180", numpy.float32, shape),
            AcquisitionChannel("proj180_min", numpy.float32, tuple()),
            AcquisitionChannel("proj180_max", numpy.float32, tuple()),
            AcquisitionChannel("proj180_mean", numpy.float32, tuple()),
            AcquisitionChannel("proj180_std", numpy.float32, tuple()),
            AcquisitionChannel("proj180_min_positive", numpy.float32, tuple()),
        ]
        seq_scan = ScanSequence(runner=runner, channels=channels)
        seq_scan.run()

        source_sample_distance = tomo_config.sample_stage.source_distance
        sample_detector_distance = tomo_detector.sample_detector_distance
        self.update_parameters(data_scan=seq_scan)

        self._compute_nabu_correction(
            proj0,
            proj180,
            pixel_size,
            source_sample_distance,
            sample_detector_distance,
        )

        self.update_parameters(
            pixel_size_mm=pixel_size,
            lateral_corr=self.parameters.get("nabu_lateral_corr"),
            camera_tilt=self.parameters.get("nabu_camera_tilt"),
            in_shell=is_in_shell(),
        )

        lateral_corr = self.parameters.get("nabu_lateral_corr")
        camera_tilt = self.parameters.get("nabu_camera_tilt")
        if lateral_corr:
            print_html(f"<b>Lateral alignment position correction in mm: {lateral_corr: 5.5f}</b>")
        if camera_tilt:
            print_html(f"<b>Camera tilt in deg (motor rotc: {camera_tilt_motor.name:<10s}): {camera_tilt: 5.5f}</b>\n")
        if is_in_shell():
            self._validate_correction_in_shell()
        else:
            print("!!! Not running in BLISS shell !!!")
            self.request_and_wait_validation()

        self._apply_correction()

        ct(exposure_time, detector)

        print(f"Lateral alignment motor position = {lateral_motor.position}")
        if camera_tilt_motor is not None:
            print(
                f"Camera tilt position {camera_tilt_motor.name} = {camera_tilt_motor.position}"
            )

    def _get_proj_angles(self, rotation_motor) -> [float, float]:
        behaviour = self._behaviour
        if behaviour == "absolute":
            return 0.0, 180.0
        elif behaviour == "relative":
            p = rotation_motor.position
            # Stay closer to 0.0, it should be fine with the limits
            if p >= 180.0:
                return p, p - 180.0
            return p, p + 180.0
        raise RuntimeError(f"Behaviour configuration '{behaviour}' is not supported")

    def _align_scan(
        self, rotation_motor: Axis, expo_time: float, detector: Lima, save: bool = False
    ):
        angle_1, angle_2 = self._get_proj_angles(rotation_motor)
        default_chain = TomoDefaultChain()
        with contextlib.ExitStack() as stack:
            stack.enter_context(
                default_chain.setup_detectors_in_default_chain(
                    [detector], slow_scan=True
                )
            )
            stack.enter_context(
                use_image_no_saving(DEFAULT_CHAIN, detector, enabled=not save)
            )
            if self._return:
                stack.enter_context(
                    cleanup(rotation_motor, restore_list=(cleanup_axis.POS,))
                )

            with capture_exceptions(raise_index=0) as capture:
                with capture():
                    # scan rot
                    rot_pos_list = [angle_1] * self._proj_n + [angle_2] * self._proj_n
                    scan = pointscan(
                        rotation_motor,
                        rot_pos_list,
                        expo_time,
                        detector.image,
                        title="align scan",
                        save=save,
                    )
                    return scan

            # test if an error has occured
            if len(capture.failed) > 0:
                return None

    def _compute_nabu_correction(
        self,
        radio0,
        radio180,
        pixel_size,
        source_sample_distance,
        sample_detector_distance,
    ):
        """Compute the correction with nabu and update the nabu parameters"""
        if nabu is None:
            _logger.warning("nabu is not installed")
            return

        # flip the radioumvr(lateral_motor, correction.lat_cor180 for the calculation
        radio180_flip = numpy.fliplr(radio180)

        method = self._method
        try:
            if method in ("1d-correlation", "fft-polar"):
                # calculate the lateral correction for the rotation axis
                # and the camera tilt with line by line correlation
                cor_n_tilt_calc = CameraTilt()
                pixel_cor, camera_tilt = cor_n_tilt_calc.compute_angle(
                    radio0, radio180_flip, method=method
                )
            elif method in _COR_CALCULATORS:
                cor_class = _COR_CALCULATORS.get(method)
                if cor_class is None:
                    raise RuntimeError(f"Method '{method}' is not available")
                cor_calc = cor_class(verbose=False)
                pixel_cor = cor_calc.find_shift(
                    radio0, radio180_flip, return_relative_to_middle=True
                )
                camera_tilt = None
            else:
                raise RuntimeError(f"Method '{method}' is unknown")
        except Exception as e:
            self.update_parameters(nabu_method=method, nabu_exception=e)
        else:
            lateral_corr = -pixel_size * pixel_cor
            self.update_parameters(
                nabu_method=method,
                nabu_lateral_corr=lateral_corr,
                nabu_camera_tilt=camera_tilt,
            )

    def _validate_correction_in_shell(self):
        """User validation to accept or reject the correction"""
        apply_lateral_corr = getval_yes_no(
            "Apply the lateral position correction?", default=False
        )
        camera_tilt = self.parameters.get("camera_tilt")
        if camera_tilt is None:
            apply_camera_tilt = False
        else:
            apply_camera_tilt = getval_yes_no("Apply the camera tilt?", default=False)

        self.update_parameters(
            apply_lateral_corr=apply_lateral_corr, apply_camera_tilt=apply_camera_tilt
        )

    def _apply_correction(self):
        lateral_motor = self.get_parameter_as_device("lateral_motor")
        lateral_corr = self.parameters["lateral_corr"]
        apply_lateral_cor = self.parameters.get("apply_lateral_corr", True)
        camera_tilt_motor = self.get_parameter_as_device("camera_tilt_motor")
        camera_tilt = self.parameters["camera_tilt"]
        apply_camera_tilt = self.parameters.get("apply_camera_tilt", True)

        applied = False
        if apply_lateral_cor:
            if lateral_corr is not None:
                # apply lateral correction
                umvr(lateral_motor, lateral_corr)
                applied = True
                if self._reset_position:
                    lateral_motor.position = 0

        if apply_camera_tilt:
            if camera_tilt_motor is not None and camera_tilt is not None:
                # apply tilt correction
                umvr(camera_tilt_motor, camera_tilt)
                applied = True

        if applied:
            print("Correction successfully applied")
