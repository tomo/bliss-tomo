from __future__ import annotations
import logging
import numpy
import gevent

from bliss.shell.standard import ct, ascan, mv, flint
from bliss.common.scans import DEFAULT_CHAIN
from bliss.shell.getval import getval_yes_no
from bliss.common.cleanup import (
    cleanup,
    capture_exceptions,
    axis as cleanup_axis,
)
from bliss.common.plot import plotselect
from bliss.scanning.scan_tools import peak
from bliss.controllers.lima.roi import Roi
from bliss.common.utils import BOLD
from tomo.scintillators import utils as scintillator_utils

from tomo.helpers import flatfield_correction
from tomo.helpers.read_images import read_images
from tomo.helpers.locking_helper import lock_context
from tomo.helpers.beacon_object_helper import check_unexpected_keys
from tomo.chain_presets.image_no_saving import ImageNoSavingPreset, use_image_no_saving
from tomo.globals import get_active_tomo_config
from .base_procedure import SessionProcedure

_logger = logging.getLogger(__name__)

try:
    import nabu
    from nabu.estimation.focus import CameraFocus
except ImportError:
    _logger.error("Error while importing nabu", exc_info=True)
    nabu = None
    CameraFocus = None


class Focus(SessionProcedure):
    def __init__(self, name: str, config: dict):
        SessionProcedure.__init__(self, name, config)
        check_unexpected_keys(
            self,
            config,
            [
                "flat_n",
                "dark_n",
                "proj_n",
                "method",
                "paper",
                "use_sample",
                "save",
                "plot",
            ],
        )
        self._save = config.get("save", False)
        self._plot = config.get("plot", False)
        self._plot_images = False
        self._dark_flat_images = True
        self._flat_n: int = config.get("flat_n", 1)
        self._dark_n: int = config.get("dark_n", 1)
        self._proj_n: int = config.get("proj_n", 1)
        self._paper = config.get("paper")
        self._method = config.get("method", "fast")
        self._use_sample = config.get("use_sample", True)

    def _run(self):
        if nabu is None:
            raise RuntimeError("Nabu is not installed")
        tomo_config = get_active_tomo_config()
        tomo_detector = tomo_config.detectors.active_detector
        if tomo_detector is None:
            raise RuntimeError("No detector active yet")

        with lock_context(tomo_detector.detector, owner=self.name):
            with tomo_config.auto_projection.inhibit():
                self.__run(tomo_config)

    def __run(self, tomo_config):
        save = self._save
        plot = self._plot
        plot_images = self._plot_images
        dark_flat_images = self._dark_flat_images

        tomo_detector = tomo_config.detectors.active_detector
        detector = tomo_detector.detector
        exposure_time = tomo_config.pars.exposure_time

        flat_image = None
        dark_image = None
        if dark_flat_images:
            if self._use_sample:
                with use_image_no_saving(DEFAULT_CHAIN, detector, not save):
                    flat_runner = tomo_config.get_runner("flat")
                    with flat_runner.disabled_locking_detector_context():
                        flat_scan = flat_runner(
                            exposure_time,
                            self._flat_n,
                            detector,
                            projection=self._proj_n,
                            save=save,
                        )
                        flat_images = read_images(flat_scan, detector)
                flat_image = flatfield_correction.compute_flat(flat_images)
            else:
                flat_image = None

            with use_image_no_saving(DEFAULT_CHAIN, detector, not save):
                dark_runner = tomo_config.get_runner("dark")
                with dark_runner.disabled_locking_detector_context():
                    dark_scan = dark_runner(
                        exposure_time,
                        self._dark_n,
                        detector,
                        save=save,
                    )
                    # read back the aquired images
                    dark_images = read_images(dark_scan, detector)
            dark_image = flatfield_correction.compute_dark(dark_images)

        # prepare the focus scan parameters
        optic = tomo_detector.optic
        scan_pars = optic.focus_scan_parameters()

        scan_range = scan_pars["focus_scan_range"]
        scan_steps = scan_pars["focus_scan_steps"]
        focus_type = scan_pars["focus_type"]
        focus_motor = optic.focus_motor

        if focus_type == "Rotation" and optic.magnification < 0.7:
            scan_range /= 2
            scan_steps *= 2

        start = focus_motor.position - scan_range
        stop = focus_motor.position + scan_range

        # optics pixel size in mm
        optics_pixel_size = tomo_detector.optics_pixel_size
        print()
        print(BOLD(f"optics pixel_size = {optics_pixel_size:f} um"))
        print()

        paper = self._paper
        if paper:
            # move in paper
            print("Move in paper")
            paper.IN()

        # Do the focus scan
        focus_scan = self.focus_scan(
            focus_motor,
            start,
            stop,
            scan_steps,
            exposure_time,
            detector,
            save=save,
        )

        # read back the aquired images
        foc_images = read_images(focus_scan, detector)

        # convert to numpy array
        foc_images = numpy.array(foc_images, dtype=numpy.float32)

        # read back the aquired positions
        foc_pos = focus_scan.streams[focus_motor.name][:]

        self.focus_calc(
            foc_images,
            flat_image,
            dark_image,
            foc_pos,
            focus_motor,
            detector,
            optics_pixel_size / 1000.0,  # in mm
            plot,
            plot_images,
            dark_flat_images,
            optic,
        )

        ct(exposure_time)
        print(f"Focus motor position: {focus_motor.position} {focus_motor.unit}")

        # Move out paper
        if paper and getval_yes_no("Move out paper?", default=True):
            paper.OUT()

    def focus_calc(
        self,
        foc_images,
        flat_image,
        dark_image,
        foc_pos,
        focus_motor,
        detector,
        pixel_size_mm,
        plot=False,
        plot_images=False,
        dark_flat_images=True,
        optic=None,
    ):
        if dark_flat_images:
            if flat_image is not None:
                foc_images = (foc_images - dark_image) / (flat_image - dark_image)
            else:
                foc_images = foc_images - dark_image

        if plot_images:
            f = flint()
            if dark_flat_images:
                p = f.get_plot(
                    "image",
                    name="dark_image",
                    unique_name="dark_image",
                    selected=True,
                    closeable=True,
                )

                p.set_data(dark_image)
                p.yaxis_direction = "down"
                p.side_histogram_displayed = False

                p = f.get_plot(
                    "image",
                    name="flat_image",
                    unique_name="flat_image",
                    selected=True,
                    closeable=True,
                )

                p.set_data(flat_image)
                p.yaxis_direction = "down"
                p.side_histogram_displayed = False

            p = f.get_plot(
                "stackview",
                name="projection_images",
                unique_name="projection_images",
                selected=True,
                closeable=True,
            )

            p.set_data(foc_images)
            p.yaxis_direction = "down"
            p.side_histogram_displayed = False

        focus_calc = CameraFocus()
        # enable calculation results plotting
        if plot is True:
            focus_calc.verbose = True

        if self._method == "fast":
            focus_pos, focus_ind, tilts_vh = focus_calc.find_scintillator_tilt(
                foc_images, foc_pos
            )
        elif self._method == "accurate":
            focus_pos, focus_ind, tilts_vh = focus_calc.find_scintillator_tilt(
                foc_images, foc_pos, metric="std"
            )
            focus_pos, focus_ind = focus_calc.find_distance(
                foc_images, foc_pos, metric="psd"
            )
        else:
            raise RuntimeError(f"Method {self._method} is not supported")

        optics_flip_h, optics_flip_v = optic.image_flipping
        det_flip_v, det_flip_h = detector.image.flip
        flip = numpy.array(
            [
                # flip_h is relative to the rotation around the vertical axis
                -1.0 if (optics_flip_h != det_flip_h) else 1.0,
                # flip_v is relative to the rotation around the horizontal axis
                -1.0 if (optics_flip_v != det_flip_v) else 1.0,
            ]
        )

        tilts_corr_vh_rad = -flip * numpy.arctan2(tilts_vh, pixel_size_mm)
        tilt_corr_v, tilt_corr_h = tilts_corr_vh_rad

        print()
        print(f"Best focus position:                   {focus_pos}")
        print(
            f"Optic flipping               vert/hor: {optics_flip_v}; {optics_flip_h}"
        )
        print(
            f"Scintillator tilt correction vert/hor: {tilt_corr_v:f}; {tilt_corr_h:f} rad"
        )

        if optic is not None:
            scintillator_geometry = optic.scintillator_geometry()
            if scintillator_geometry is not None:
                try:
                    correction = scintillator_geometry.tilt_hardware_correction(
                        tilt_corr_v, tilt_corr_h
                    )
                except Exception:
                    # The modelization could be wrongly defined in the yaml file
                    # There is no check for that
                    _logger.error("Error while computing correction", exc_info=True)
                else:
                    scintillator_utils.print_tilt_results(
                        scintillator_geometry, correction
                    )
            else:
                print("No modelization for the scintillator defined")

        print()

        if plot is True:
            from matplotlib import pyplot as plt

        if plot is True:

            def pause():
                plt.pause(1)

            g = gevent.spawn(pause)

        if self.wait_validation_for_correction():
            # Always move to the best focus from the bottom
            mv(focus_motor, foc_pos[0])
            # move focus motor to maximum
            mv(focus_motor, focus_pos)

        # Kill the plot window when it was requested
        if plot is True:
            g.kill()
            # workaround for nabu plotting problem
            plt.close("all")

    def wait_validation_for_correction(self):
        """
        Wait for the user validation before correction.

        The function was created for testability.
        """
        return getval_yes_no("Move to the best focus position?", default=True)

    def focus_scan(
        self,
        focus_motor,
        start,
        stop,
        steps,
        expo_time,
        detector,
        save=False,
    ):
        # prepare roi counters for statistics
        focus_roi = Roi(0, 0, detector.image.roi[2], detector.image.roi[3])

        det_rois = detector.roi_counters
        det_rois["focus_roi"] = focus_roi

        std = detector.roi_counters.counters.focus_roi_std
        counters = (std, detector.image)

        plotselect(std)

        if not save:
            savingpreset = ImageNoSavingPreset(detector)
            DEFAULT_CHAIN.add_preset(savingpreset)
        # clean-up: move back focus motor, delete roi counters, move out paper
        restore_list = (cleanup_axis.POS,)
        with cleanup(*[focus_motor], restore_list=restore_list):
            with capture_exceptions(raise_index=0) as capture:
                with capture():
                    # execute focus scan
                    print(
                        f"Scan {focus_motor.name} from {start} to {stop}, steps {steps}"
                    )
                    scan = ascan(
                        focus_motor,
                        start,
                        stop,
                        steps,
                        expo_time,
                        counters,
                        title="focus scan",
                        save=save,
                    )

                    # get the position on the maximum of the standard deviation
                    peak()

                    # delete roi counters
                    del det_rois["focus_roi"]

                    if not save:
                        DEFAULT_CHAIN.remove_preset(savingpreset)

                    return scan

                # test if an error has occured
                if len(capture.failed) > 0:
                    # delete roi counters
                    del det_rois["focus_roi"]

                    if not save:
                        DEFAULT_CHAIN.remove_preset(savingpreset)

                    return None
