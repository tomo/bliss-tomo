from __future__ import annotations

from typing import Any
import datetime
import gevent
import math
import typing
import contextlib
from bliss import current_session
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.group import Sequence
from bliss.common.cleanup import cleanup, axis as cleanup_axis, capture_exceptions
from bliss.common.logtools import log_error, log_warning
from bliss.common.standard import mv
from bliss.common.measurementgroup import get_active as get_active_mg
from bliss.controllers.lima.lima_base import Lima
from tomo.helpers.locking_helper import lock_context
from tomo.helpers.locked_axis_motion_hook import LockedAxisMotionHook
from tomo.scan.presets.locked_lima_detectors import LockedLimaDetectorsPreset

from fscan.fscantools import FScanParamStruct
from fscan.fscantools import FScanParamBase
from fscan.fscantools import FScanModeBase

from tomo.scan.tomo_runner import TomoRunner
from tomo.scan.presets.common_header import CommonHeaderPreset
from tomo.scan.presets.wait_for_beam import WaitForBeamPreset
from tomo.scan.presets.wait_for_refill import WaitForRefillPreset
from tomo.scan.presets.round_position_preset import RoundPositionPreset
from tomo.scan.presets.beam_check import BeamCheckWatchdog, BeamLost
from tomo.metadata import TomoMetaData
from tomo.sinogram import TomoSinogram
from tomo.sequence.presets.base import SequencePreset
from tomo.helpers import string_utils
from tomo.helpers.proxy_param import ProxyParam

if typing.TYPE_CHECKING:
    from tomo.controllers.tomo_detector import TomoDetector

ScanType = FScanModeBase("ScanType", "STEP", "CONTINUOUS", "SWEEP", "INTERLACED")


class SequenceBasicPars(FScanParamBase):
    DEFAULT: dict[str, Any] = {
        "return_images_aligned_to_flats": False,
        "save_flag": True,
        "display_flag": True,
        "return_to_start_pos": True,
        "projection_groups": False,
        "flat_on": 500,
        "dark_at_start": False,
        "flat_at_start": True,
        "dark_at_end": False,
        "flat_at_end": False,
        "images_on_return": True,
        "scan_type": ScanType.CONTINUOUS,
        "half_acquisition": False,
        "full_frame_position": 0.0,
        "acquisition_position": 0.0,
        "shift_in_fov_factor": 0.0,
        "shift_in_mm": 0.0,
        "shift_in_pixels": 0,
        "shift_type": "Field-of-view factor",
        "return_to_full_frame_position": False,
        "move_to_acquisition_position": False,
        "beam_check": False,
        "wait_for_beam": False,
        "refill_check": False,
        "activate_sinogram": False,
        "round_position": False,
    }
    OBJKEYS: list[str] = []
    LISTVAL: dict[str, Any] = {"scan_type": ScanType.values}
    NOSETTINGS: list[str] = ["save_flag"]

    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            type(self).DEFAULT,
            obj_keys=type(self).OBJKEYS,
            value_list=type(self).LISTVAL,
            no_settings=type(self).NOSETTINGS,
        )

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False


def cleanup_sequence(func):
    def new_func(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except BaseException:
            for preset in self._sequence_presets.values():
                preset.cleanup()
            raise

    return new_func


class SequenceBasic(object):
    """
    Basic class containing methods shared by all tomo sequences

    Parameters
    ----------
    tomo_n : int
        Total number of projections
    flat_on : int
        Number of projections per projection group
    flat_n : int
        Number of flat images
    dark_n : int
        Number of dark images
    exposure_time : float
        Exposure time per projection
    latency_time : float
        Extra time added for continuous scan to exposure time and readout time of the detector
        to compensate rotation speed variation
    start_pos : float
        Start position of tomo scan
    range : float
        Angular range of tomo scan
    scan_type : enum
        Type of tomo scan. Can be STEP, CONTINUOUS, INTERLACED or SWEEP
    energy : float
        Energy value in keV
    source_sample_distance : float
        Distance in mm between source and sample
    sample_detector_distance : float
        Distance in mm between sample and detector
    projection_groups : boolean
        Flag to activate/deactivate projection groups in tomo acquisition
    flat_at_start : boolean
        Flag to activate/deactivate flat images acquisition at the beginning of the tomo
    flat_at_end : boolean
        Flag to activate/deactivate flat images acquisition at the end of the tomo
    dark_at_start : boolean
        Flag to activate/deactivate dark images acquisition at the beginning of the tomo
    dark_at_end : boolean
        Flag to activate/deactivate dark images acquisition at the end of the tomo
    images_on_return : boolean
        Flag to activate/deactivate images acquisition on tomo return
    return_images_aligned_to_flats : boolean
        Flag to specify if images on tomo return must be taken or not at the same position that projection group
    return_to_start_pos : boolean
        Flag to specify if rotation axis should return or not to start position after tomo acquisition
    refill_check : boolean
        Flag to activate refill detection and to pause scan during refill
    beam_check : boolean
        Flag to activate frontend lost detection and to redo scan in this case
    wait_for_beam : boolean
        Flag to activate frontend close state detection and to pause scan while frontend is closed
    round_position : boolean
        Flag to activate/deactivate rotation position rounding at the end of tomo scan
    save_flag: boolean
        Flag to activate/deactivate data saving during acquisition
    display_flag: boolean
        Flag to activate/deactivate scan display during acquisition

    **Attributes**
    name : str
        The Bliss object name
    pars : FScanParamBase
        Basic parameters shared by all sequences
    """

    def __init__(self, name, config):
        self.name = name
        self.__config = config
        # user configuration parameters

        # self._inpars = FScanParamStruct(self.pars.to_dict())
        self._inpars = None

        # The sinogram is only valid during the execution of a sequence
        self._sino: typing.Optional[TomoSinogram] = None

        self._metadata = None
        self._detector = None
        self._rotation_axis = None
        self._reference = None
        self._detectors = None
        self._y_axis = None
        self._z_axis = None
        self._fscan_config = None
        self._machinfo = None
        self._tomo_runners = None
        self._prepare_done = False

        # The tomo sequence and the scans executed by the sequence
        self._sequence = None
        self._sequence_name = "tomo:basic"
        self._scans = []
        self._scans_names = []
        self._sequence_presets = dict()
        self._use_presets = True

        # get the configuration from the Tomo configuration object
        self._tomo_config = config["tomo_config"]
        self._load_tomo_config(self._tomo_config)

        self.pars = self._create_parameters()

    @property
    def tomo_config(self):
        return self._tomo_config

    @property
    def tomo_detector(self) -> TomoDetector:
        return self._tomo_config.detectors.get_tomo_detector(self._detector)

    def _create_parameters(self):
        """
        Create pars attribute containing basic and config pars
        """
        tomo_config = self.tomo_config
        basic_pars = SequenceBasicPars(self.name)
        return ProxyParam(tomo_config.pars, basic_pars)

    def _setup_sequence(self, name=None, scan_info=None, run=True):
        """
        Setup the name of the sequence and run it if expected.
        """
        self._sequence = None
        if name:
            self._sequence_name = name
        self._prepare_done = False

        if run:
            self.run(scan_info)
        else:
            self.prepare(scan_info)

    def init_sequence(self):
        """
        Initialise the temporary sequence parameters
        """
        self._inpars = FScanParamStruct(self.pars.to_dict())
        self._inpars.nb_proj_groups = 0
        self._inpars.start_proj_groups = list()
        self._inpars.end_proj_groups = list()
        self._inpars.points_proj_groups = list()
        self._inpars.projection = 1
        self._inpars.proj_time = 0
        self._inpars.sequence = self._sequence_name

        # clear musst program
        if self._fscan_config is not None:
            musst = self._fscan_config.config["devices"]["musst"][0]
            musst.CLEAR

        # clear list of scans in sequence
        self._scans = []
        self._scans_names = []

        # get detector for sequence
        detector = self.get_controller_lima()
        self._detector = detector
        self._tomo_config.detectors.active_detector = detector

        if detector is None:
            log_warning(self, "No detector found")
            return

        pars = self.pars
        if pars.activate_sinogram:
            if not pars.dark_at_start or not pars.flat_at_start:
                raise ValueError(
                    "Dark scan or/and flat scan at start is/are missing to build sinogram."
                )
            self._sino = TomoSinogram(
                self.name,
                detector=detector,
                roi_name="sino",
            )
            active_mg = current_session.active_mg
            self._sino.setup_counters(active_mg)
        else:
            if len(detector.roi_profiles.get_rois()) > 0:
                active_mg = current_session.active_mg
                active_mg.disable(f"{detector.name}:roi_profiles*")
            self._sino = None

        if self._machinfo is not None:
            active_mg = get_active_mg()
            for counter in self._machinfo.counters:
                active_mg.add(counter)
            active_mg.enable(f"*{self._machinfo.name}*")

        self._check_saving_mode()

        # get meta data for sequence
        self._metadata = TomoMetaData(self._tomo_config, detector)

    def __info__(self):
        """
        Show information about the sequence parameters, and detector optic configuration.
        """

        def format_unit(value, unit):
            if value is None:
                return "undefined"
            return f"{value:.3f} {unit}"

        self._detector = self.get_controller_lima()
        info_str = "Basic sequence configuration info:\n"
        # sequence parameters
        info_str += self.pars.__info__()
        info_str += "\n"
        info_str += f"Source sample distance         : {format_unit(self._tomo_config.sample_stage.source_distance, 'mm')}"
        info_str += "\n"
        tomo_detector = self._tomo_config.detectors.get_tomo_detector(self._detector)
        if tomo_detector:
            tomo_detector.apply_acq_mode()
            info_str += f"Sample detector distance       : {format_unit(tomo_detector.sample_detector_distance, 'mm')}"
            info_str += "\n"
        info_str += "\n"
        #  detector - optic configuration
        info_str += self._tomo_config.detectors.__info__()
        info_str += "\n\n"

        optic = tomo_detector.optic
        if optic:
            scintillator = optic.scintillator
            info_str += f"Scintillator                   : {scintillator}"
            info_str += "\n\n"

        detector = tomo_detector.detector
        if detector:
            info_str += (
                f"Detector mode                  : {self._detector.acquisition.mode}"
            )
            info_str += "\n"
            if detector.acquisition.mode == "ACCUMULATION":
                acc_nb_frames = math.ceil(
                    self.pars.exposure_time / detector.accumulation.max_expo_time
                )
                acc_expo_time = self.pars.exposure_time / acc_nb_frames
                info_str += f"Number of subframes            : {acc_nb_frames}"
                info_str += "\n"
                info_str += f"Exposure time of one subframe  : {acc_expo_time:.3f}"
                info_str += "\n"
            info_str += (
                f"Saving format                  : {detector.saving.file_format}"
            )
            info_str += "\n"
            info_str += f"Saving mode                    : {detector.saving.mode.name}"
            info_str += "\n"
            info_str += (
                f"Saving frames per file         : {detector.saving.frames_per_file}"
            )
            info_str += "\n"
            image_size = str(detector.image.width) + "x" + str(detector.image.height)
            info_str += f"Image size                     : {image_size}"
            info_str += "\n"
            info_str += self.estimate_scan_size()
            info_str += "\n"
            info_str += self.motors_position()
        return info_str

    def estimate_scan_size(self):
        """
        Give an estimation about total size of images acquired during tomo
        """
        detector = self.get_controller_lima()
        nb_img = self.pars.tomo_n
        nb_img += self.pars.dark_n if self.pars.dark_at_start else 0
        nb_img += self.pars.dark_n if self.pars.dark_at_end else 0
        nb_img += self.pars.flat_n if self.pars.flat_at_start else 0
        nb_img += self.pars.flat_n if self.pars.flat_at_end else 0
        nb_img += (
            int(self.pars.range / 90) + 1 * self.pars.flat_n
            if self.pars.images_on_return
            and not self.pars.return_images_aligned_to_flats
            else 0
        )
        nb_proj_gp = (
            int(self.pars.tomo_n / self.pars.flat_on)
            if self.pars.tomo_n % self.pars.flat_on == 0
            else int(self.pars.tomo_n / self.pars.flat_on) + 1
        )
        nb_img += nb_proj_gp * self.pars.flat_n if self.pars.projection_groups else 0
        nb_img += (
            nb_proj_gp * self.pars.flat_n
            if self.pars.images_on_return and self.pars.return_images_aligned_to_flats
            else 0
        )
        scan_size = (
            detector.image.width * detector.image.height * detector.image.depth * nb_img
        )
        scan_size, unit = string_utils.memory_size(scan_size)
        return f"Estimated Scan Size            : {scan_size:.3f} {unit}"

    def _load_tomo_config(self, tomo_config):
        """
        Get hardware from configuration file
        """
        # Get config pars
        self.config_pars = tomo_config.pars
        # Get the rotation axis
        self._rotation_axis = tomo_config.rotation_axis
        # Get reference object to move sample out of the beam
        self._reference = tomo_config.reference
        # Get the lateral axis to do half acquisition
        self._y_axis = tomo_config.y_axis
        # Get the z axis to do z series acquisition
        self._z_axis = tomo_config.z_axis
        # Get detector optice configuration object
        self._detectors = tomo_config.detectors
        # get fscan configuration object
        self._fscan_config = tomo_config.fscan_config
        # get machine information object
        self._machinfo = tomo_config.machinfo
        self._frontend = tomo_config.frontend
        self._beam_shutter = tomo_config.beam_shutter

        # Get all defined runners from the configuration
        self._tomo_runners = tomo_config._tomo_runners

    def set_parameters(self, dict):
        """
        Update all sequence parameters present in the given dictionary
        with corresponding values
        """
        for key, value in dict.items():
            self.set_parameter(key, value)

    def get_runner(self, name):
        """
        Return the runner corresponding to the given name
        """
        return self._tomo_runners[name]

    def add_helper_scan(self, name, obj):
        """
        Add a scan to sequence with a given name
        """
        self._scans.append(obj)
        self._scans_names.append(name)

    def add_dark(self, dark_scan=None):
        """
        Set the runner that sequence must use to acquire dark images
        If dark_scan is not specified, default dark runner will be used
        """
        if self.pars.dark_n == 0:
            raise ValueError("Dark images have been requested but dark_n is set to 0")
        if dark_scan is not None:
            self.add_helper_scan("dark", dark_scan)
        else:
            # get dark runner
            dark_runner = self._tomo_runners["dark"]
            # pepare dark scan
            dark_scan = dark_runner(
                self.pars.exposure_time,
                self.pars.dark_n,
                save=self.pars.save_flag,
                run=False,
            )
            # add dark scan to tomo sequence
            self.add_helper_scan("dark", dark_scan)

    def add_flat(self, flat_scan=None):
        """
        Set the runner that sequence must use to acquire flat images
        If flat_scan is not specified, default flat runner will be used
        """
        if self.pars.flat_n == 0:
            raise ValueError("Flat images have been requested but flat_n is set to 0")
        if flat_scan is not None:
            self.add_helper_scan("flat", flat_scan)
        else:
            flat_runner = self._tomo_runners["flat"]
            flat_scan = flat_runner(
                self.pars.exposure_time,
                self.pars.flat_n,
                projection=self._inpars.projection,
                save=self.pars.save_flag,
                run=False,
            )
            self.add_helper_scan("flat", flat_scan)

    def add_return(self, return_scan=None):
        """
        Set the runner that sequence must use to acquire static images
        on return
        If return_scan is not specified, default return runner will be
        used
        """
        if return_scan is not None:
            self.add_helper_scan("return_ref", return_scan)
        else:
            return_runner = self._tomo_runners["return_ref"]
            rot_pos_list = return_runner.get_motor_positions(self._inpars)
            return_scan = return_runner(
                rot_pos_list,
                self.pars.exposure_time,
                save=self.pars.save_flag,
                run=False,
            )
            self.add_helper_scan("return_ref", return_scan)

    def get_controller_lima(self):
        """
        Get detector from active measurement group
        """
        builder = ChainBuilder([])

        for node in builder.get_nodes_by_controller_type(Lima):
            return node.controller

    def _prepare_sinogram(self):
        """
        Prepare the sinogram together with this sequence.

        This connect the sinogram to the sequence and prepare calculation objects
        for reemitting corresponding spectrum data to the sequence.
        """
        pars = self._inpars
        dark_scan = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "dark"
        ][0]
        flat_scan = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "flat"
        ][0]
        proj_scans = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name in ScanType.values
        ]
        self._sino.setup_sequence(
            self._sequence, pars.tomo_n, dark_scan, flat_scan, proj_scans
        )

    def add_metadata(self, scan_info):
        """
        Fill the scan_info dictionary with the basic tomo meta data
        """
        self._metadata.update_tomo_scan_info(scan_info, self._inpars)

        # Info about subscans
        subscans = {}
        for i, name in enumerate(self._scans_names):
            # Use names which share meanings with the UI
            type = "tomo:" + name.lower()
            subscans[f"scan{i}"] = {"type": type}
        technique = scan_info.setdefault("technique", {})
        technique["subscans"] = subscans

    def add_projections_group(
        self,
        start_pos=None,
        end_pos=None,
        tomo_n=None,
        expo_time=None,
        proj_scan=None,
        flat_on=None,
    ):
        """
        Divide tomo acquisition into several projection groups according
        to flat_on parameter
        One projection group is one projection scan followed by flat
        images
        flat_on indicates the number of projections after which flat
        images must be taken
        If number of projection groups deduced from tomo_n and flat_on
        is not an integer, an additional group with a reduced number of
        projections is created
        """
        if flat_on is None:
            flat_on = self.pars.flat_on

        if tomo_n is None:
            tomo_n = self.pars.tomo_n

        if tomo_n < flat_on:
            flat_on = tomo_n

        nb_groups = int(tomo_n / flat_on)
        groups = [start_pos]
        step = (end_pos - start_pos) / tomo_n

        for i in range(nb_groups):
            groups.append(groups[-1] + step * flat_on)

        for i in range(0, len(groups) - 1):
            self.add_proj_group(
                start_pos=groups[i],
                end_pos=groups[i + 1],
                tomo_n=flat_on,
                expo_time=expo_time,
                proj_scan=proj_scan,
            )

        if tomo_n % flat_on != 0:
            self.add_proj_group(
                start_pos=groups[-1],
                end_pos=end_pos,
                tomo_n=tomo_n % flat_on,
                expo_time=expo_time,
                proj_scan=proj_scan,
            )

    def add_proj_group(
        self, start_pos=None, end_pos=None, tomo_n=None, expo_time=None, proj_scan=None
    ):
        """
        Set the runner that sequence must use to acquire projection
        images of one projection group
        If proj_scan is not specified, default projection runner
        corresponding to scan type parameter will be used
        Add projection scan to the sequence
        Add image common header preset to projection scan
        Add flat scan to the sequence
        """
        if proj_scan is not None:
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan(
                self._rotation_axis, start_pos, end_pos, tomo_n, expo_time, run=False
            )
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        detector = self._detector
        header = {}
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)
        proj_scan.add_preset(header_preset)
        self._inpars.nb_proj_groups += 1
        self._inpars.start_proj_groups.append(start_pos)
        self._inpars.end_proj_groups.append(end_pos)
        self._inpars.points_proj_groups.append(tomo_n)
        end_proj = sum(self._inpars.points_proj_groups)
        start_proj = end_proj - self._inpars.points_proj_groups[-1]
        title = f"projections {int(start_proj)} - {int(end_proj)}"
        proj_scan.scan_info.update({"title": title})
        self._inpars.projection = end_proj
        self.add_flat()

    def add_proj(
        self, start_pos=None, end_pos=None, tomo_n=None, expo_time=None, proj_scan=None
    ):
        """
        Set the runner that sequence must use to acquire projection
        images
        If proj_scan is not specified, default projection runner
        corresponding to scan type parameter will be used
        Add projection scan to the sequence
        Add image common header preset to projection scan
        """
        detector = self._detector
        header = {}
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)

        title = f"projections 0 - {int(tomo_n)}"
        self._inpars.projection = tomo_n

        if proj_scan is not None:
            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan(
                self._rotation_axis, start_pos, end_pos, tomo_n, expo_time, run=False
            )
            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)

        proj_scan.scan_info.update({"title": title})

    def _setup_presets(self):
        """
        Call the prepare method of sequence presets
        """
        if self._use_presets:
            for preset in self._sequence_presets.values():
                preset.prepare()

    def _start_presets(self):
        """
        Call the start method of sequence presets
        """
        if self._use_presets:
            for preset in self._sequence_presets.values():
                preset.start()

    def _stop_presets(self):
        """
        Call the stop method of sequence presets
        """
        if self._use_presets:
            for preset in self._sequence_presets.values():
                preset.stop()

    def add_sequence_preset(self, preset, name=None):
        """
        Add preset given in parameter to sequence presets
        Possibility to give a name to the preset
        If no name is given, take object id
        """
        if not isinstance(preset, SequencePreset):
            raise ValueError("preset should be an instance of SequencePreset !!")
        preset_name = id(preset) if name is None else name
        self._sequence_presets[preset_name] = preset

    def remove_sequence_preset(self, name):
        """
        Remove sequence preset corresponding to the given name
        """
        if name in self._sequence_presets:
            self._sequence_presets.pop(name)

    def disable_sequence_presets(self):
        """
        Prevent sequence presets calling during acquisition
        """
        self._use_presets = False

    def enable_sequence_presets(self):
        """
        Enable sequence presets calling during acquisition
        """
        self._use_presets = True

    def motors_position(self):
        """
        Return position of several motors part of tomograph
        """
        info_str = f"\nRotation axis position: {self._rotation_axis.position:.3f}"

        lima = self._detector
        tomo_detector = self._tomo_config.detectors.get_tomo_detector(lima)
        detector_axis = tomo_detector.detector_axis
        if detector_axis is None:
            detector_axis = tomo_detector.tomo_detectors.detector_axis

        if detector_axis is not None:
            # Get the detector axis
            info_str += f"\nDectector axis position: {detector_axis.position:.3f}"
        if self._y_axis is not None:
            # Get the lateral axis to do half acquisition
            info_str += f"\nLateral axis position: {self._y_axis.position:.3f}"
        if self._z_axis is not None:
            # Get the z axis to do z series acquisition
            info_str += f"\nZ axis position: {self._z_axis.position:.3f}"
        return info_str

    def show_info(self):
        """
        Display info about acquisition
        """
        info_str = self.__info__()
        time_in_hh_mm_ss = str(
            datetime.timedelta(seconds=self._inpars.scan_time)
        ).split(":")
        hours = time_in_hh_mm_ss[0]
        minutes = time_in_hh_mm_ss[1]
        secondes = time_in_hh_mm_ss[2].split(".")[0]
        info_str += (
            "\n\nEstimated Scan Time: "
            + hours
            + " Hours "
            + minutes
            + " Minutes "
            + secondes
            + " Secondes"
            + "\n"
        )
        print(info_str)

    def build_sequence(self):
        """Build the sequence before calling `prepare` on the sequence"""
        pass

    def _check_saving_mode(self):
        detector = self._detector
        if detector is None:
            return
        if (
            detector.saving.mode == 0
            and (
                detector.saving.file_format == "HDF5"
                or detector.saving.file_format == "HDF5GZ"
                or detector.saving.file_format == "HDF5BS"
            )
        ) or (
            detector.saving.mode == 2
            and self.pars.tomo_n / detector.saving.frames_per_file > 1024
            and (
                detector.saving.file_format == "HDF5"
                or detector.saving.file_format == "HDF5GZ"
                or detector.saving.file_format == "HDF5BS"
            )
        ):
            min_frames_per_file = int(self.pars.tomo_n / 1024) + 1
            print(
                f"WARNING: it is not allowed to save {detector.saving.frames_per_file} frames per file with hdf5 format because of time for recontruction\n"
            )
            print(
                f"Set automatically number of frames per file to {min_frames_per_file}\n"
            )
            detector.saving.mode = 2
            detector.saving.frames_per_file = min_frames_per_file

    @cleanup_sequence
    def prepare(self, user_info=None):
        """
        Validate sequence parameters
        Add wait for refill and wait for beam presets to all scans of
        the sequence
        Add sequence metadata
        Create sequence bliss object
        Add sinogram if needed
        Display most important information about acquisition
        """
        try:
            self._setup_presets()
        except Exception:
            # FIXME: only setup presets have to be cleaned up
            for preset in self._sequence_presets.values():
                preset.cleanup()
            raise

        self.init_sequence()
        self.build_sequence()
        scan_info = ScanInfo.normalize(user_info)

        if self._prepare_done:
            # This was already prepared
            return
        self._reference.update_in_beam_position()
        self.validate()
        self.add_metadata(scan_info)

        if self._sino is not None:
            pars = self._inpars
            npoints = pars.tomo_n
            if pars.scan_type == "STEP":
                if pars.nb_proj_groups > 0:
                    npoints = pars.tomo_n + pars.nb_proj_groups
                else:
                    npoints = pars.tomo_n + 1

            self._sino.setup_meta(
                rotation_axis=self._rotation_axis,
                angle_start=pars.start_pos,
                angle_stop=pars.end_pos,
                nb_angles=npoints,
            )
            self._sino.add_sinogram_plot(scan_info)

        self._sequence = Sequence(title=self._sequence_name, scan_info=scan_info)
        self._add_check_refill()
        self._add_wait_for_beam()
        self._add_beam_check()
        self._add_round_position()
        if self._sino is not None:
            self._prepare_sinogram()
        self.show_info()
        self._prepare_done = True

    def validate(self):
        pass

    def projection_scan(
        self, motor, start_pos, end_pos, tomo_n, expo_time, scan_info=None, run=True
    ):
        raise NotImplementedError

    def _add_check_refill(self):
        """
        Add wait for refill preset to all scans of the sequence
        """
        if (
            self.pars.refill_check
            and self._machinfo is not None
            and self._beam_shutter is not None
        ):
            scan_time = 0
            for i in range(len(self._scans)):
                _ = self._scans[i]
                runner = self.get_runner(self._scans_names[i])
                scan_time += runner._estimate_scan_duration(self.pars)
            scan_time += 60

            refill_preset = WaitForRefillPreset(
                self._machinfo, self._beam_shutter, checktime=scan_time
            )
            self._sequence.scan.add_preset(refill_preset)

    def _add_wait_for_beam(self):
        """
        Add wait for beam preset to all scans of the sequence
        """
        if (
            self.pars.wait_for_beam
            and self._machinfo is not None
            and self._frontend is not None
        ):
            wait_for_beam_preset = WaitForBeamPreset(self._machinfo, self._frontend)
            self._sequence.scan.add_preset(wait_for_beam_preset)

    def _add_beam_check(self):
        """
        Add wait for beam preset to all scans of the sequence
        """
        if (
            self.pars.beam_check
            and self._machinfo is not None
            and self._frontend is not None
        ):
            check_beam_watchdog = BeamCheckWatchdog(self._machinfo, self._frontend)

            for i in range(len(self._scans)):
                scan = self._scans[i]
                scan.set_watchdog_callback(check_beam_watchdog)

    def _add_round_position(self):
        """
        Add wait for beam preset to all scans of the sequence
        """
        if self.pars.round_position:
            round_position_preset = RoundPositionPreset(self._rotation_axis)
            self._sequence.scan.add_preset(round_position_preset)

    @contextlib.contextmanager
    def _run_context(self, capture, lock: bool = True):
        """Common context executed at the :meth:`run`."""
        tomo_config = self._tomo_config
        lima_detector = tomo_config.detectors.active_detector.detector
        rotation_axis = tomo_config.rotation_axis
        with contextlib.ExitStack() as stack:
            if lock:
                stack.enter_context(lock_context(rotation_axis, owner=self.name))
                for mh in rotation_axis.motion_hooks:
                    # The rotation was locked by the sequence, we have to
                    # disable it for LockedAxisMotionHook because it is not reentrant
                    if isinstance(mh, LockedAxisMotionHook):
                        stack.enter_context(mh.disabled_context())

                stack.enter_context(lock_context(lima_detector, owner=self.name))

            stack.enter_context(tomo_config.auto_projection.inhibit())
            stack.enter_context(capture())
            yield

    @cleanup_sequence
    def run(self, user_info=None, _lock: bool = True):
        """
        Prepare and execute the sequence
        Start acquisition presets
        Add and run all the scans contained in the sequence
        Send ICAT metadata
        Stop acquisition presets
        Move back rotation axis to initial position if
        return_to_start_pos parameter is set to True
        If beam_check option is activate and if beam lost has been
        detected, redo acquisition
        """
        if user_info is not None:
            if self._prepare_done:
                raise RuntimeError(
                    "Scan prepare was already done. Use user_info at creation or at run, not at both."
                )
        if not self._prepare_done:
            self.prepare(user_info)

        if self.pars.return_to_start_pos:
            restore_list = (cleanup_axis.POS, cleanup_axis.VEL)
        else:
            restore_list = (cleanup_axis.VEL,)
        capture2 = None
        with capture_exceptions(raise_index=None) as capture:
            with self._run_context(capture, lock=_lock):
                with cleanup(
                    self._rotation_axis,
                    self._y_axis,
                    restore_list=restore_list,
                    verbose=True,
                ):
                    self._start_presets()
                    mv(self._rotation_axis, self.pars.start_pos)
                    with self._sequence.sequence_context() as scan_seq:
                        self._current_start = (
                            self._machinfo.counters.current.value
                            if self._machinfo is not None
                            else None
                        )
                        with capture_exceptions(raise_index=None) as capture2:
                            with capture2():
                                for scan in self._scans:
                                    scan_seq.add(scan)
                                for scan in self._scans:
                                    with contextlib.ExitStack() as stack:
                                        # Assume that the detector was already locked by the sequence
                                        # or the parent of the sequence
                                        # So what we have to disable the dedicated preset
                                        # It would be better not to generate them at the first place,
                                        # but it looks too tricky with the actual architercture
                                        for preset in scan._preset_list:
                                            if isinstance(preset, LockedLimaDetectorsPreset):
                                                stack.enter_context(preset.disabled_context())
                                        scan.run()
                        self._current_stop = (
                            self._machinfo.counters.current.value
                            if self._machinfo is not None
                            else None
                        )
                        self._send_icat_metadata()
                        self._stop_presets()

        if len(capture.failed) > 0:
            print("\n")
            log_error(
                self,
                f"A problem occured during {self.name.split(':')[-1]} sequence, sequence aborted",
            )
            log_error(self, capture.exception_infos)
            log_error(self, "\n")
            self._prepare_done = False
            self._stop_presets()
            self._sino = None
            if capture2:
                _, first_error, _ = capture2.failed[0]  # sys.exec_info()
            else:
                _, first_error, _ = capture.failed[0]  # sys.exec_info()
            if isinstance(first_error, BeamLost):
                self._beam_lost(user_info)
            else:
                raise first_error

        self._sino = None
        self._prepare_done = False

    def _beam_lost(self, user_info):
        """
        If beam_check option is activate and if beam lost has been
        detected, redo tomo acquisition
        """
        print(f"the SR current is {self._machinfo.counters.current.value:.2f} mA")
        if self._frontend.is_closed:
            print("beam was probably lost during the scan")
        elif self._machinfo.counters.current.value < 5:
            print(
                "The SR current was lower than 5mA at the end of the scan, it probably corresponds to a beam loss without closing of the front-end"
            )
        print("BEAM LOST WE REDO THE SCAN")
        scan_saving = current_session.scan_saving
        collection_name = scan_saving.collection_name
        scan_saving.newcollection(collection_name + "_fc_")
        self.run(user_info)

    def wait_state(self, state, timeout=10):
        """
        Wait for the sequence to have a specific state

        The API is the same as BLISS `Scan.wait_state`

        TODO: Part of the implementation should be moved inside BLISS Sequence
        """
        try:
            with gevent.Timeout(timeout) as t:
                while True:
                    seq = self._sequence
                    if seq is not None:
                        break
                    gevent.sleep(0.1)
                while True:
                    scan = seq.scan
                    if scan is not None:
                        break
                    gevent.sleep(0.1)
                scan.wait_state(state)
        except gevent.Timeout as catched:
            if catched is t:
                raise TimeoutError("Timout during wait_state on the tomo sequence")
            raise

    @property
    def scan_info(self):
        """
        Return scan_info dictionary from tomo scan sequence if one exists
        else raise exception
        """
        if self._sequence is not None:
            return self._sequence.scan_info
        else:
            raise Exception(
                "No tomo sequence has been created yet. Please run sequence.prepare() first"
            )

    def estimation_time(self) -> float | None:
        """
        Estimation time of the sequence (in seconds).

        By default it returns None.
        """
        return None

    def _send_icat_metadata(self):
        """
        Fill metadata fields related to standard tomo (common to all tomo
        sequences)
        """
        tomo_config = self._tomo_config
        scan_saving = current_session.scan_saving
        scan_saving.dataset.add_techniques("TOMO")
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_dark_n", str(self.pars.dark_n)
        )
        if self.pars.projection_groups:
            scan_saving.dataset.write_metadata_field(
                "TOMOAcquisition_flat_on", str(self.pars.flat_on)
            )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_scan_range", str(self.pars.range)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_source_sample_distance",
            str(tomo_config.sample_stage.source_distance),
        )
        tomo_det = self._detectors.get_tomo_detector(self._detector)
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_sample_detector_distance",
            str(tomo_det.sample_detector_distance),
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_start_angle", str(self.pars.start_pos)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_proj_n", str(self.pars.tomo_n)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_exposure_time", str(self.pars.exposure_time)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_flat_n", str(self.pars.flat_n)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_energy", str(self.pars.energy)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_camera_acq_mode", str(self._detector.acquisition.mode)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_camera_pixel_size",
            str(self._detector.proxy.camera_pixelsize),
        )
        if self._detector.acquisition.mode == "ACCUMULATION":
            scan_saving.dataset.write_metadata_field(
                "TOMOAcquisition_acc_exposure_time",
                str(self._detector.accumulation.max_expo_time),
            )
            scan_saving.dataset.write_metadata_field(
                "TOMOAcquisition_acc_frames_count",
                str(
                    int(
                        self.pars.exposure_time
                        / self._detector.accumulation.max_expo_time
                    )
                ),
            )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_scan_type", str(self.pars.scan_type)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_latency_time", str(self.pars.latency_time)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_scintillator",
            str(self._detectors.get_optic(self._detector).scintillator),
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_beam_check", str(self.pars.beam_check)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_half_acquisition", str(self.pars.half_acquisition)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_sample_pixel_size", str(tomo_det.sample_pixel_size)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_camera_flip_vert", str(self._detector.image.flip[0])
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_camera_flip_horz", str(self._detector.image.flip[1])
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_optic_type",
            str(self._detectors.get_optic(self._detector).name),
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_magnification",
            str(self._detectors.get_optic(self._detector).magnification),
        )
        try:
            flat_motion = self._reference
            y_step = flat_motion.get_relative_motion(tomo_config.y_axis)
        except ValueError:
            pass
        else:
            scan_saving.dataset.write_metadata_field(
                "TOMOAcquisition_y_step", str(y_step)
            )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_comment", str(self.pars.comment)
        )
        active_mg = get_active_mg()
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_read_srcur",
            str("current" in [counter.split(":")[-1] for counter in active_mg.enabled]),
        )
        if self._machinfo is not None:
            srcur_start = self._current_start
            srcur_stop = self._current_stop
            scan_saving.dataset.write_metadata_field(
                "TOMOAcquisition_srcur_start", str(srcur_start)
            )
            scan_saving.dataset.write_metadata_field(
                "TOMOAcquisition_srcur_stop", str(srcur_stop)
            )

        if tomo_config.detectors.detector_axis:
            scan_saving.dataset.write_metadata_field(
                "TOMOAcquisition_camera_x_mot",
                str(tomo_config.detectors.detector_axis.name),
            )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_duration", str(self.estimation_time())
        )
        optic = tomo_det.optic
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_optic_name", str(optic.description)
        )
        scan_saving.dataset.write_metadata_field(
            "TOMOAcquisition_optic_magnified_pixel_size",
            str(tomo_det.optics_pixel_size),
        )
