# Compatibility with tomo <= 2.3.1

from tomo.controllers.tomo_detector import TomoDetector  # noqa
from tomo.controllers.tomo_detector import TomoDetectorState  # noqa
from tomo.controllers.tomo_detector import TomoDetectorPixelSizeMode  # noqa
from bliss.common import deprecation

deprecation.deprecated_warning(
    "Module",
    "tomo.tomo_detector",
    replacement="tomo.controllers.tomo_detector",
    since_version="2.3.1",
)
