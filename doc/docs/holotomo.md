# Holotomo

A helper object is provied to deal with the holotomo.

It handle the setup of the distances to be used
for a specific pixel size. This is done by selecting a distance
for the sample, while the detector stays in place.

It handles 2 ways to setup the distances, based on an offset or a
ration of the first distance.

If a tracked translation-x axis is setup from the tomo config, it is used.
Else the translation-x axis is used.

## Configuration

```
- name: mrtomo_holo
  plugin: bliss
  class: Holotomo
  package: tomo.controllers.holotomo

  # reference to the main tomo configuration
  # with that we have to know everything needed
  # including sx, srot...
  tomo: $tomo_config

  # The coefficients to use to compute the distances
  # in 'ratio' mode
  ratios: [1.0, 1.0429, 1.2145, 1.5708, 1.7321, 2.0]

  # The coefficients to use to compute the distances
  # in 'offset' mode
  offsets: [0.0, 1.0, 5.0, 15.0]

  # The way to calculate the distances: One of 'offset' or 'ratio'
  calcmode: ratio

  # sleep time after a settle to a distance, in second
  settle_time: 0.06
```

## Use

The object have to be imported on the session.

After that it is also available from the tomo config object.

Else, it also can be used by it's name.

### Show the state

The holotomo object have a common info.

It displays few informations, plus actual distance.

```
DEMO_SESSION [2]: tomo_config.holotomo
         Out [2]: Holotomo:

                  pixel size          200     um
                  stabilization time    0.06  s
                  actual distance       2
                  x-axis (sx)         190.09  mm

                  distances
                    #    distance [mm]    position [mm]    fov [mm]
                  ---  ---------------  ---------------  ----------  ---
                    1           2000             104.29     25.6
                    2           2085.8           190.09     26.6982  <--
                    3           2429             533.29     31.0912
                    4           3141.6          1245.89     40.2125

```

### Setup with a pixel size

```
DEMO_SESSION [8]: tomo_config.holotomo.pixel_size = 0.025
DEMO_SESSION [9]: tomo_config.holotomo
         Out [9]: Holotomo:
                  
                  pixel size          0.025  um
                  stabilization time  0.06   s
                  actual distance
                  
                  distances
                    #    position [mm]    fov [mm]
                  ---  ---------------  ----------  --
                    1         -1895.46  0.0032
                    2         -1895.45  0.00333728
                    3         -1895.41  0.0038864
                    4         -1895.32  0.00502656
```

### Setup with the first distance

```
DEMO_SESSION [10]: tomo_config.holotomo.first_distance = 2000
DEMO_SESSION [11]: tomo_config.holotomo
         Out [11]: Holotomo:
                   
                   pixel size          200     um
                   stabilization time    0.06  s
                   actual distance
                   
                   distances
                     #    position [mm]    fov [mm]
                   ---  ---------------  ----------  --
                     1           104.29     25.6
                     2           190.09     26.6982
                     3           533.29     31.0912
                     4          1245.89     40.2125
```

### Move to a distance

`umove` and `move` are provided to move to an already setup distance.

```
DEMO_SESSION [15]: tomo_config.holotomo.move(1)
Moving sx from -895.710000 to 104.290000
```

```
DEMO_SESSION [16]: tomo_config.holotomo.umove(1)

        sx[mm]   
user   104.290000
dial   104.290000
```
