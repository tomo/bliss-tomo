
A reference position can be exposed by the `TomoConfig`.

It can be set by users, and a button from Daiquiri allow
to move the sample stage back.

This is useful to restart an alignement at a known location.

## Configuration

```yaml
- name: tomo_config
  plugin: bliss
  class: TomoConfig
  package: tomo.controllers.tomo_config

  ...

  # reference for translation x/y/z that user can setup.
  parking_position: {}  # <- empty config

  ...
```

## Use

```python
# Setup a reference manually x/y/z
tomo_config.parking_position.reference = None, 1, 2
```

```python
# Setup a reference based on the actual location of the sample stage
tomo_config.parking_position.record_reference()
```

```python
# Move back to the stored location
tomo_config.parking_position.move_to_reference()
```