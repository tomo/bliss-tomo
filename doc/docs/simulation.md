## Simulation

The project provides few simulated devices to mimic the result of a
tomo acquisition.

### Simulated Musst

This simulated Musst is needed to run the continuous scans without hardware.

A dedicated program was designed to simulate a very specific `fscan` program.

The following example define a virtual Musst, which can read the `srot` motor
from BLISS and trig a Lima simulator.

```yml
- name: musst
  plugin: bliss
  class: FakeTomoMusst
  package: tomo.controllers.fake_tomo_musst

  musst_prg_root: foo
  channels:
    - label: srot
      type: encoder
      channel: 1

  btrig_up:
    - tomo_simulator
```

### Simulated 2D detector from NXtomo file

This Lima device is based on a Lima simulator and includes a
plugin to generate a virtual image.

The main data source is a NXtomo file, which allow to display
at low cost a realistic image.

The counter part is it only allow few interaction from BLISS.
Mainly playing with the rotation axis.

The following configuration is a part of a setup of such Lima device.
It creates `NxTomoDeviceServer` and allow to define a rotation motor,
a beam shutter, and a lateral motor with 2 positions (sample in and sample out).
The output image to generate can be reach from this state, by reading the NXtomo files
described.

```yml
- class: Simulator
  tango_name: id00/simulator/tomo_simulator
  properties:
    mode: GENERATOR_PREFETCH
    nb_prefetched_frames: 1  # No needed to fetch as it is generated by a plugin

- class: LimaCCDs
  properties:
    LimaCameraType: Simulator
    BufferMaxMemory: 20
  tango_name: id00/limaccds/tomo_simulator

- class: NxTomoDeviceServer
  tango_name: id00/nxtomosimulation/tomo_simulator
  properties:
    srot: srot
    sample_y: sampy
    sample_y_out: 20
    sample_y_in: 0
    filename: data/bamboo_next_try_down4-skip10.nx
    alt_filename: data/bamboo.nx
    beam_shutter: beam_shutter
```

A NXtomo sample can be found at this place.

```
wget http://www.silx.org/pub/tomwer/datasets/bamboo_next_try_down4-skip10.nx -O bamboo.nx
```

### Simulated 2D detector from voxel file

This Lima device is based on a Lima simulator and includes a
plugin to generate a virtual image.

The main data source is a HDF5 file containing a voxel model (3D dense data).

The resulting image is generated with a fast by inaccurate algorithm.
This provides a lot of flexibility in BLISS side, and is mainly useful
to simulate sample alignment.

The following configuration is a part of a setup of such Lima device.
It creates `VoxelTomoDeviceServer` which is connected to most of the motors
of the sample stage. A filename have to be defined for the voxel data,
and others can be defined to provide a bit more realistic beam of dark.

```
- class: Simulator
  tango_name: id00/simulator/tomo3_simulator
  properties:
    mode: GENERATOR_PREFETCH
    nb_prefetched_frames: 1  # No needed to fetch as it is generated by a plugin
    frame_dim: [128, 128, 4]
    pixel_size: [1e-3, 1e-3]

- class: LimaCCDs
  properties:
    LimaCameraType: Simulator
    BufferMaxMemory: 20

- class: VoxelTomoDeviceServer
  tango_name: id00/voxeltomosimulation/tomo3_simulator
  properties:
    sx: sx
    sy: sy
    sz: sz
    srot: srot
    sampu: sampu
    sampv: sampv
    detector_y: tomo3cam_y
    detector_magnification: tomo3cam_magnification
    beam_shutter: beam_shutter
    filename: data/lucy_3m_o10_filled.h5
    beam_filename: data/beam.h5::flat
    dark_filename: data/camera.h5::dark
```
