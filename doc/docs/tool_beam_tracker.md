
For conic beam, a beam tracker can be setup.

It is split into 2.

- The `beam tracker`, which handle the modelization of the beam
- The `beam tracker calc controller` which controller the tracker axis

## Software architecture

![modelization](images/beam-tracker-architecture.png)

The `bean tracker` have to be setup with `sy_angle`, `sz_angle` in radian.

The `focal_position` for the sample stage have to be setup independently.

From this information, the beam tracker is able to update the `detector center`
automatically to follow the actual position of the sample stage.

## Configuration

```yaml
controller:
  - package: tomo.controllers.beam_tracker
    class: BeamTracker
    name: tomo_beam_tracker
    plugin: generic
```

```yaml
controller:
  - package: tomo.controllers.beam_tracker
    class: BeamTrackerCalcController
    beam_tracker: $tomo_beam_tracker
    plugin: generic

    axes:
      # sample stage
      - name: $sx
        tags: real sx
      - name: $sy
        tags: real sy
      - name: $sz
        tags: real sz

      # beam tracking
      - name: sxbeam
        tags: sxbeam
```

```yaml
- name: tomo_config
  plugin: bliss
  class: TomoConfig
  package: tomo.controllers.tomo_config

  ...

  # Contains information from the location of the beam.
  # This is used to track the sample with the beam
  beam_tracker: $tomo_beam_tracker

  # Axis with the same role as x-axis, but compensating y and z to follow
  # the beam with the sample
  tracked_translation_x: $sxbeam

```

## Use

```python
# Move with the beam tracker
umv(sxbeam, 10)
```
