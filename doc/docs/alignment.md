## Alignment ##

Tool based on Nabu preprocessing alignment module.
Three functions are available:
- align calculates the lateral correction for the rotation axis and the camera tilt with line by line correlation (http://www.silx.org/pub/nabu/doc/apidoc/nabu.estimation.tilt.html?highlight=compute_angle#nabu.estimation.tilt.CameraTilt.compute_angle)
- focus estimates best focus position and scintillator vertical and horizontal tilts (http://www.silx.org/pub/nabu/doc/apidoc/nabu.estimation.focus.html#module-nabu.estimation.focus)
- alignxc calculates shifts between beam direction and longitudinal translation axis (http://www.silx.org/pub/nabu/doc/apidoc/nabu.estimation.translation.html#module-nabu.estimation.translation)

All functions have saving and plot (from Nabu calculations) options but only align function has the possibility to display all taken images (dark, flat, image at 0 and at 180 degrees) at the end of the procedure.

```
# Configuration

- name:  mralign_tomo
  plugin: bliss
  class: Align
  package: tomo.optic.align
  sequence: $mrfull_tomo		#sequence object needed to access to detector, optic, etc

# How to use it  
mralign_tomo.align(save=False,plot=False,plot_images=False)
mralign_tomo.focus(save=False,plot=False)
mralign_tomo.alignxc(save=False,plot=False)
```



