## Flat Runner ##

Scan for images taken with sample out of beam.
By default, based on bliss loopscan.
A scan preset is automatically set to add image key value into image header for nexus compliance (see https://manual.nexusformat.org/classes/applications/NXtomo.html#nxtomo-entry-instrument-detector-image-key-field)



For shutter handling, you have the possiblity to use existing shutter preset. You can put it in place like this:
```
#In your bliss session setup file#
from tomo.scan.presets.fast_shutter import FastShutterPreset

fastshutterpreset = FastShutterPreset(your_shutter_object)

your_tomo_config.add_scan_preset(['flat'], fastshutterpreset)
```

This preset will:
	For frelon camera:
		- activate auto mode in full frame image mode 
		- activate manual mode in frame transfer image mode
	In manual mode and for all other cameras:
		- open shutter before taking images and close it after manually
		

For motor displacement handling, there is also an existing preset
```
#In your bliss session setup file#
from tomo.scan.presets.reference_motor import ReferenceMotorPreset

referencemotorpreset = ReferenceMotorPreset(your_reference_object)

your_tomo_config.add_scan_preset(['flat'], referencemotorpreset)
```

This preset will move sample out of the beam before taking images and move it in after.

Feel free to create your own presets if you want.

If you want to call flat scan independantly from tomo, you have the possibility to extract it like this:
```
#In your bliss session setup file#
flat_scan=your_tomo_config.get_runner('flat') 
``` 
