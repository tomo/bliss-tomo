## Dark Runner ##

Scan for images taken with shutter closed.
By default, based on bliss loopscan.
A scan preset is automatically set to add image key value into image header for nexus compliance (see https://manual.nexusformat.org/classes/applications/NXtomo.html#nxtomo-entry-instrument-detector-image-key-field)



For shutter handling, you have the possiblity to use existing shutter preset. You can put it in place like this:
```
#In your bliss session setup file#
from tomo.scan.presets.dark_shutter import DarkShutterPreset

darkshutterpreset = DarkShutterPreset(your_shutter_object)

your_tomo_config.add_scan_preset(['dark'], darkshutterpreset)
```

This preset will only close shutter before taking images and open it after.

Feel free to create your own shutter preset if you want.


If you want to call dark scan independantly from tomo, you have the possibility to extract it like this:
```
#In your bliss session setup file#
dark_scan=your_tomo_config.get_runner('dark') 
``` 
