## Configuration ##

### Fcan ###
Because tomo sequences are based on fscan library, you need first to configure fscan.
You can look here how to do it: https://bliss.gitlab-pages.esrf.fr/fscan/fscan_config.html
```
- name: fscan_mrtomo
  class: FScanConfig
  package: fscan.fscanconfig
  devices:
    musst: $musst
    measgroup: $mrtomocounters
    rotation_motors: $srot_eh2
```
### Optic ###
Basic optics can be configured in ebs-tomo.
For example, optic with fixed magnification:
```
#fixed magnification optic
- name: REFLECTIVE
  plugin: bliss
  class: FixedmagOptic
  package: tomo.optic.fixedmag_optic
  rotc_motor:  $rotcrefl
  focus_motor: $focusrefl
  focus_type: "rotation"     # translation or rotation
  image_flipping_hor: False
  image_flipping_vert: True
  magnification: 10
  scintillators: ["Lso23"]
```

If you want a more flexible object, there is user optic which allows you 
to specify motors, magnification, etc directly in bliss session
```
# user defined optic
- name:  uoptic
  plugin: bliss
  class: UserOptic
  package: tomo.optic.user_optic
  scintillators: ['scinti1','scinti2','scinti3']
```
There are also specific optics. You can find all the list here: #link towards optics page#

### Tomo Detector ###
Now that you have configured your optic, you can associate it one detector 
with the following object:
```
- name: dimax_optic
  plugin: bliss
  class: TomoDetector
  package: tomo.controllers.tomo_detector
  detector: $dimax
  optic: $uoptic
```
TomoDetector object allows to know detector pixel size according to camera pixel size and optic magnification.
On this object, you can also specify if you want to set sample pixel size manually (automatically calculated with sample/source distances by default).
You have to do this for all detectors you want to use in ebs-tomo.
### Tomo Detectors ###
Then to group all tomo detector objects, you have to declare one tomo detectors object:
```
- name: mrdetectors_optic
  plugin: bliss
  class: TomoDetectors
  package: tomo.controllers.tomo_detectors
  detectors:
  - $edgedzoom_optic
  - $edgezoom_optic
  - $edgetwinmic_optic
  detector_axis: $mrxc
```
Detector axis is used to deduce sample detector distance according to axis position and a specific offset value associated to each tomo detector object. 
We can then calculate pixel size automatically.

### Tomo Reference Configuration ###
This object handles axis movements to put sample out of beam when taking flat images and then put sample back in beam.
```
- name: mrref_displ
  plugin: bliss
  class: FlatMotion
  package: tomo.controllers.flat_motion
  
  motor: $yrot_eh2
  settle_time: 2.0
```
Reference object can also handle a list of motors if you need it.

### Tomo Configuration ###
After having configured previous objects you can create a tomoconfig object. 
It is used to describe the beamline:
```
- name: mrtomo_config
  plugin: bliss
  class: TomoConfig
  package: tomo.controllers.tomo_config
  
  fscan_config: $fscan_mrtomo
  reference: $mrref_displ

  detectors: $mrdetectors_optic

  sample_stage:
    # Motor under the rotation in the direction of the beam
    translation_x: null
    # Motor under the rotation orthogonal to the beam and the vertical plan
    translation_y: $yrot_eh2
    # Motor under the rotation moving the sample vertically
    translation_z: $sz_eh2

    # Motor controlling the rotation axis
    rotation: $srot_eh2       # axis object for the tomo rotation

    # Real motor moving the sample on top of the rotation axis
    sample_u: $sx_eh2
    # Real motor moving the sample on top of the rotation axis
    sample_v: $sy_eh2

  default_single_chain: $MRTOMO_int_chain                # bliss default chain to be used for dark, flat and 
                                                         # return scans when detector is in single mode
  default_accumulation_chain: $MRTOMO_int_chain_acc      # bliss default chain to be used for dark, flat and  
                                                         # return scans when detector is in accumulation mode

  machinfo: $machinfo       # bliss MachInfo object (optional)
  beam_shutter: $bsh1       # (optional)
  frontend: $frontend       # (optional)
```
These 3 last keys are used to activate some securities on tomo scans as check refill, wait for beam and check beam.
To know more about it: #link towards securities page#
