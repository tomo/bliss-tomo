## Image correction ##

Tool used to apply flat field correction on detector images
```
# Configuration 
- name: image_corr
  plugin: bliss
  class: ImageCorrection
  package: tomo.helpers.imagecorrection

# How to use it
image_corr.set_tomo(sequence_tomo_object)

image_corr.take_dark()
image_corr.take_flat()

image_corr.dark_on()		# activates correction
image_corr.flat_on()

image_corr.dark_off()		# deactivates correction
image_corr.flat_off()
```
