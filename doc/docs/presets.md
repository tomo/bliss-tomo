## Scan or Chain Presets ##

Hooks in tomo scans based on bliss preset objects (https://bliss.gitlab-pages.esrf.fr/bliss/master/scan_engine_preset.html)

bliss-tomo offers a list of presets that you can import per presets.

Everything is located inside `tomo.scan.presets`

```
from tomo.scan.presets.dark_shutter import DarkShutterPreset
```

### How to add your own presets ###
```
#In your bliss session setup file#

fastshutterpreset = MyFastShutterPreset()

flat_scan=your_tomo_sequence_object.get_runner('flat')

# If your preset is a scan preset
flat_scan.add_scan_preset(fastshutterpreset, fastshutterpresetname)

# If your preset is a chain preset
flat_scan.add_chain_preset(fastshutterpreset, fastshutterpresetname)
```
