## Tomorgaphy Acquisition Modes

The acquisition scan part refers to projection angle acquisitions taken between two reference group images.
It can be the whole scan or part of it, if intermediate references are requested.

Three scanning modes have been implemented:

* step scan
* continuous scan
* continuous scan without gap

### Definition of Scan Point Time and Motor Speed

As parameter of a tomography scan the user defines the detector **exposure time**. Inside the scanning function, the
**scan point time** is used which defines the time between 2 consecutive trigger signals. The **scan point time** takes into account:

* the image **exposure time** given by the user
* the **readout time** given by the calibration of the camera
* the **shutter time** if the shutter is controlled by the camera for each frame
* the **latency time** specified by the user to allow a safe triggering of the camera.

The **scan speed** , the speed of the rotation motor, is deduced from the **scan point time** and the projection angle.

![scan point time](images/tomo_scan_point.svg)
